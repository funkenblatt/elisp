(require 'cps-filter)

(defun re-nextmatch (re &optional group)
  (if (re-search-forward re nil t)
      (match-string (or group 0))))

(defun tpb-scrape ()
  (interactive)
  (get-url "http://thepiratebay.org/search/the%20daily%20show/0/99/0"
	   (lambda (s)
	     (dolist (i (re-findall "<a .*download this torrent.*" s t))
	       (dump i "\n")))))

(defn mklink (text)
  (make-text-button 
   text nil 'action 
   (lambda (me) (shell-command (str "wget " (shell-quote-argument text)))))
  text)

(defun tpb-lstorrents (page)
  (let: outbuf (current-buffer)
	dumper (lambda (&rest args) (with-current-buffer outbuf (mapc 'insert args))) in
    (with-temp-buffer
      (dump page)
      (goto-char (point-min))
      (while (re-search-forward "href=\"\\([^\"]*\\.torrent\\)" nil t)
	(funcall dumper (mklink (match-string 1)))
	(forward-line 3)
	(funcall dumper 
		 " Seeds: " (re-nextmatch "[0-9]+") 
		 " Leechers: " (re-nextmatch "[0-9]+")
		 "\n")))))

(defn mktlink (text)
  (make-text-button
   text nil 'action
   (lambda (me) 
     (shell-command (str "wget " (shell-quote-argument text) " -O "
			 (read-from-minibuffer "Enter filename: ")))))
  text)

(defun tokyo-links (txt)
  (let: links nil 
	link nil in
    (with-temp-buffer
      (insert txt)
      (goto-char (point-min))
      (while (re-search-forward 
	      "<a [^>]*type=\"application/x-bittorrent\"[^>]*href=\"\\([^\"]*\\)[^>]*>" 
	      nil t)
	(setq link (replace-regexp-in-string
		    "&amp;" "&"
		    (match-string 1)))
	(push (cons (buffer-substring (point) (- (search-forward "</a>") 4)) link)
	      links)))
    (dolist (i (nreverse links))
      (proper-dump (car i) "\n" (mktlink (cdr i)) "\n\n"))))

(defun tokyo-scrape ()
  (interactive)
  (get-url "http://www.tokyotosho.info/search.php?terms=one+piece"
	   (lambda (s) (tokyo-links (string-as-multibyte s)))))

(defun tpb-scrape2 ()
  (get-url "http://thepiratebay.org/search/the%20daily%20show/0/99/0"
	   'tpb-lstorrents))

(provide 'tpb)
