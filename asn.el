(defun string-strm (s)
  (let: ix 0 str s in
    (lambda ()      
      (if (= ix (length str))
	  nil
	(prog1
	    (aref str ix)
	  (setq ix (+ ix 1)))))))

(defun strmwad (strm n)
  (let: out (make-string n 0) in
    (dotimes (i n) (aset out i (funcall strm)))
    out))

(defun strmwad (strm n)
  (let: out nil in
    (dotimes (i n) (push (funcall strm) out))
    (nreverse out)))

(defun substrm (strm n)
  (lambda ()
    (if (= n 0) 
	nil
      (setq n (- n 1))
      (funcall strm))))

(defun lstrm (l)
  (lambda ()
    (pop l)))

(defun asn-tag-info (strm)
  (let: b (funcall strm)
	tag (and b (logand b #x1f))
	other-info (and b (= (logand (lsh b -5) 1) 1))
	out 0 in
    (catch 'return 
      (if (not b) (throw 'return nil))
      ;; (when (= (lsh b -6) 2)
      ;; 	(asn-length strm)
      ;; 	(throw 'return (asn-tag-info strm)))
      (if (= tag #x1f)
	  (progn
	    (setq b (funcall strm))
	    (while (= (lsh b -7) 1)
	      (setq out (* out 128)
		    out (+ out (logand b #x7f))
		    b (funcall strm)))
	    (setq out (* out 128)
		  out (+ out (logand b #x7f))))
	(setq out tag))
      (list other-info out
	    (asn-length strm)))))

(defun asn-taglen (tag)
  (caddr tag))

(defun asn-set (strm tagwad)
  (cons 'set
	(cdr (asn-seq strm tagwad))))

(defun asn-seq (strm tagwad)
  (let: len (asn-taglen tagwad)
	new-strm (substrm strm len)
	junk nil
	out nil in
    (while (setq junk (asn-parse new-strm))
      (push junk out))
    (cons 'seq (nreverse out))))

(defun asn-printable (strm tagwad)
  (strmwad strm (asn-taglen tagwad)))

(defun asn-block-tagger (tag)
  `(lambda (strm tagwad)
     (list ',tag (strmwad strm (asn-taglen tagwad)))))

(defun asn-parse (strm)
  (let: tagwad (asn-tag-info strm)
	fun (assoc (cadr tagwad) asn-parsers) in
    (cond
     ((not tagwad) nil)
     (fun (funcall (cdr fun) strm tagwad))
     (t (if (car tagwad)
	    (cons (cadr tagwad) (cdr (asn-seq strm tagwad)))
	  (list 'doh! (strmwad strm (asn-taglen tagwad))))))))

(defun asn-length (strm)
  (let: l (funcall strm)
	lbytes (if (/= (logand l #x80) 0)
		   (logand l #x7f))
	out 0 in
    (if lbytes
	(dotimes (i lbytes)
	  (setq out (* out 256)
		out (+ out (funcall strm))))
      (setq out l))
    out))

(setq asn-parsers 
      `((16 . asn-seq)
	(17 . asn-set)
	(19 . asn-printable)
	(12 . asn-printable)
	(23 . ,(asn-block-tagger 'utc-time))
	(6 . ,(asn-block-tagger 'objid))
	(4 . ,(asn-block-tagger 'ocstring))
	(3 . ,(asn-block-tagger 'bitstring))
	(2 . ,(asn-block-tagger 'int))))

(setq asn-tagnames
      '((seq . 16)
	(int . 2)
	(bitstring . 3)
	(ocstring . 4)
	(objid . 6)))

(setq asn-dispatch
      '((seq . make-asn-seq)
	(int . make-asn-block)
	(ticks . make-asn-ticks)
	(objid . make-asn-block)
	(ocstring . make-asn-block)
	(0 . make-asn-block)
	(1 . make-asn-block)
	(2 . make-asn-block)
	(3 . make-asn-block)))

(defun make-asn (expr)
  (if (assq (car expr) asn-dispatch)
      (funcall (cdr (assq (car expr) asn-dispatch))
	       expr)))

(defun make-asn-block (expr)
  (let: tagid (cdr (assq (car expr) asn-tagnames)) in
    (when (integerp (car expr))
      (setq tagid (logior (car expr) #b10000000))
      (when (listp (cadr expr)) 
	(setq tagid (logior tagid #b00100000))
	(setq expr (list nil (apply 'concat (mapcar 'make-asn (cdr expr)))))))
    (concat
     (list tagid)
     (make-asn-length (length (cadr expr)))
     (cadr expr))))

(defun make-asn-seq (expr)
  (let: tagid (logior 32 16)
	items (mapcar 'make-asn (cdr expr)) in
    (apply 'concat
	   (list tagid)
	   (make-asn-length (sum (mapcar 'length items)))
	   items)))

(defun make-asn-ticks (expr)
  (concat
   (list #b01000011)
   (make-asn-length (length (cadr expr)))
   (cadr expr)))

(defun make-asn-length (len)
  (let: out nil in
    (upon (> len 128)
	(while (/= len 0)
	  (push (logand len #xff) out)
	  (setq len (lsh len -8)))
	(push (logior (length out) #x80) out)
	:else
      (push len out))))

(defun dump-to-file (s filename)
  (let: coding-system-for-write 'binary in
    (write-region s nil filename)))

(provide 'asn)
