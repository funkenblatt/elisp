(defun make-state-func (proc)
  (lexical-let (state-func)
    (setq state-func (funcall proc (lambda (new-state-func)
				     (setq state-func new-state-func))))
    (lambda (&rest args)
      (apply state-func args))))

(defun countwad (n finish &rest argwads)
  (lexical-let ((max-i n) (i 0)
		(k finish) (finish-args argwads))
    (lambda (&rest ignored)
      (setq i (+ i 1))
      (if (= i max-i)
	  (apply k finish-args)))))

(defun recv-nbytes (n finish)
  (lexical-let ((bytesleft n) (k finish))
    (lambda (p s)
      (setq s (string-make-unibyte s))
      (if (>= (length s) bytesleft)
	  (progn
	    (funcall k)		;Presumably finish will do something interesting like changing the process filter.
	    (funcall (process-filter p) p (substring s bytesleft)))
	(setq bytesleft (- bytesleft (length s)))))))

(defun state-server (n)
  "Garbage stateful crap."
  (lexical-let ((num-packets n)
		(conn (make-network-process :server t :service 8080 :family 'ipv4
					    :name "server-of-foo" :buffer nil)))
    (set-process-filter
     conn
     (make-state-func
      (lambda (set-cont)
	(lexical-let ((sc set-cont) funkytime)
	  (setq funkytime
		(lambda (p s)
		  (lexical-let ((procwad p))
		    (funcall sc 
			     (recv-nbytes num-packets
					  (lambda ()
					    (process-send-string
					     procwad (format "Received chunk of %d bytes" num-packets))
					    (funcall sc funkytime))))
		    (funcall (process-filter p) p s))))
	  funkytime))))))



(state-server 10)
