(defun kill-self (node)
  (node.parentNode.removeChild node))

(defun prepend-node (parent node)
  (.. parent (.insertBefore node (.. parent .childNodes 0))))

(defun is-attributes (x)
  (and (= (typeof x) 'object)
       (not (instanceof x Array))))

(defun node (x parent)
  (cond
   ((= (typeof x) 'string) (document.createTextNode x))
   ((= (typeof x) 'number) (document.createTextNode (JSON.stringify x)))
   ((= (typeof x) 'undefined) (document.createTextNode ""))
   ((instanceof x Array)
    (if (= (typeof (.. x 0)) 'string)
        (let ((el (document.createElement (.. x 0)))
              (attrs (if (is-attributes (.. x 1))
                         (.. x 1))))
          (x.shift)
          (if attrs (progn (x.shift) (for-in (i attrs) (el.setAttribute i (.. attrs [i])))))
          (x.forEach (λ (x) (>>> (node x el) (if _ (el.appendChild _)))))
          el)
      (if parent
          (x.forEach (λ (x) (parent.appendChild (node x parent)))))))
   (true x)))

(defun fetch-url (url method cb data)
  (let ((req (new (XMLHttpRequest))))
    (req.open method url true)
    (setf req.onreadystatechange
          (λ (e)
             (if (= req.readyState 4)
                 (cb req.responseText req e)
               nil)))
    (req.send (or data nil))))

(defun juxt ()
  (let ((args (to-array arguments)))
    (λ (x) (args.map (λ (f) (f x))))))

(defun min-by (key-fn a)
  (a.reduce (λ (x y) (if (< (key-fn x) (key-fn y)) x y))))

(defun map (fun)
  (let ((arrays (.. arguments to-array (.slice 1)))
        (min-array (min-by (@ length) arrays)))
    (min-array.map 
     (λ (x ix) (fun.apply this (arrays.map (@ [ix])))))))

(defun to-array (array-like)
  (let ((i 0) (out []))
    (for (nil (< i array-like.length) (incf i))
      (out.push (-> array-like [i])))
    out))

(defun reductions (f a)
  (a.reduce
   (λ (accums b)
      (if (= accums.length 0)
          (accums.push b)
        (accums.push (f (.. accums [(.. accums .length (- 1))]) b)))
      accums)
   []))

(defun urlencode-obj (o)
  (let ((out ""))
    (for-in (i o)
            (if (!= out "") (incf out "&"))
            (incf out (+ (encodeURIComponent i)
                         "="
                         (encodeURIComponent (.. o [i])))))
    out))

(defun elem-offset [elem]
  (let ((x 0) (y 0))
    (for (nil elem.offsetParent (setf elem elem.offsetParent))
      (incf x elem.offsetLeft)
      (incf y elem.offsetTop))
    [x y]))
