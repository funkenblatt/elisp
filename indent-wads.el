(defun indent-let: (ip s)
  (if (check-for-token ip s 'in)
      (save-excursion
	(goto-char (cadr s))
	(+ (current-column) 2))
    (+ (current-column) 1)))

(defun check-for-token (ip s tok)
  (save-excursion
    (goto-char (cadr s))
    (forward-char)
    (catch 'break
      (while (< (point) ip)
	(if (eq (read/die) tok)
	    (throw 'break t))))))

(defun indent-upon (ip s)
  (save-excursion
    (goto-char (cadr s))
    (let: startcol (current-column) in
      (if (check-for-token ip s :else)
	  (+ startcol 2)
	(+ startcol 4)))))

(defun scheme-let: (s ip normal)
  (indent-let: ip s))

(put 'let: 'lisp-indent-function 'indent-let:)
(put 'let:: 'lisp-indent-function 'indent-let:)
(put 'upon 'lisp-indent-function 'indent-upon)
(put 'let: 'scheme-indent-function 'scheme-let:)
(put 'let! 'scheme-indent-function 'scheme-let:)
(put 'lexrec: 'lisp-indent-function 'indent-let:)
(put 'let! 'lisp-indent-function 'indent-let:)
(put 'letrec 'lisp-indent-function 'indent-let:)
(put 'w/ctx 'lisp-indent-function 2)
(put 'transforms 'lisp-indent-function 1)
(put 'lambda-opt 'scheme-indent-function 1)
(provide 'indent-wads)
