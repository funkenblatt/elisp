;; -*- lexical-binding: t; -*-
;; A simple btree implementation in elisp.
;; 
;;; Trees take the form 
;;; (list (vector (cons k1 v1) (cons k2 v2) ...) (vector ch1 ch2 ...))
;;; Internal insertion functions may return (:split node1 sep node2)

(defun updated-v (v index new)
  "Returns a copy of vector V with item at INDEX replaced with V."
  (>>> (copy-seq v) (progn (aset _ index new) _)))

(defun btree-maybe-split (keys children max)
  "Given a set of keys and child-nodes, either construct a new tree
node, or return a split."
  (if (> (length keys) max)
      (let: mid (/ max 2) mid1 (1+ mid) in
        `(:split (,(.. keys 0 mid) ,(.. children 0 mid1))
                 ,(aref keys mid)
                 (,(.. keys mid1) ,(.. children mid1))))
    (list keys children)))

(defun btree-try-keys (keys children key value)
  (let: pos (bsearch keys (comp (cur 'string< key) 'car))
        ix (max (- pos 1) 0) in
    (if (equal (car (aref keys ix)) key)
        (throw 'return 
               (list (updated-v keys ix (cons key value)) children))
      pos)))

(defun btree-insert-leaf (keys key value max-keys)
  "Inserts a key-value pair into the leaf node of a b-tree.  May return
a split."
  (let: pos (btree-try-keys keys nil key value) in
    (btree-maybe-split 
     (vsplice keys pos (cons key value)) nil max-keys)))

(defun btree-insert-internal (keys children key value max-keys)
  "Insert a key-value pair into an an internal node.  This may have
to handle a split, and may have to propagate a split upwards."
  (let: pos (btree-try-keys keys children key value)
        child (aref children pos)
        new-child (btree-insert* child key value max-keys) in
    (pcase new-child
      (`(:split ,left ,sep ,right)
       (btree-maybe-split 
        (vsplice keys pos sep)
        `[,@(.. children 0 pos) ,left ,right
          ,@(.. children (+ pos 1))]
        max-keys))
      (x (>>> (copy-seq children) 
              (progn (aset _ pos x) _)
              (list keys _))))))

(defun btree-insert* (tree key value max-keys)
  (catch 'return
    (pcase tree
      (`nil `([(,key . ,value)] nil))
      (`(,keys nil) (btree-insert-leaf keys key value max-keys))
      (`(,keys ,children) 
       (btree-insert-internal keys children key value max-keys)))))

(defun btree-insert (tree key value max-keys)
  (pcase (btree-insert* tree key value max-keys)
    (`(:split ,left ,sep ,right) `([,sep] [,left ,right]))
    (x x)))

(defun btree-delete* (tree key max-keys)
  (dbind (keys children) tree
    (let: child-ix (bsearch keys (comp (cur 'string< key) 'car))
          ix (- child-ix 1) in
      (cond
       ((and (>= ix 0) (equal (car (aref keys ix)) key))
        (if children
            (dbind (new-sep . new-child) (btree-delete-rightmost 
                                          (aref children ix) max-keys)
              (btree-delete-rebalance
               (list (updated-v keys ix new-sep) children)
               ix new-child max-keys))
          (list (vrem keys ix) nil)))
       (children 
        (iflet new-child (btree-delete* (aref children child-ix) key max-keys)
               (btree-delete-rebalance
                tree child-ix new-child max-keys)))
       (t nil)))))

(defun btree-delete (tree key max-keys)
  (when tree
    (pcase (btree-delete* tree key max-keys)
      (nil tree)
      (`([] nil) nil)
      (`([] ,children) (aref children 0))
      (x x))))

(defun btree-delete-rightmost (tree max-keys)
  (pcase tree
    (`(,keys nil) 
     (list (alast keys) (subseq keys 0 (- (length keys) 1)) nil))
    (`(,keys ,children)
     (dbind (max-k . new-child) (btree-delete-rightmost 
                                 (alast children) max-keys)
       (cons max-k
             (btree-delete-rebalance
              tree (- (length children) 1) new-child
              max-keys))))))

(defun btree-delete-rebalance (tree ix new-child max-keys)
  (let:: (keys children) tree
         (child-keys stuff) new-child
         new-children (updated-v children ix new-child) in
    (if (< (length child-keys) (/ max-keys 2))
        (let: l-sib (and (> ix 0) (- ix 1))
              r-sib (and (< ix (- (length children) 1)) 
                         (+ ix 1))
              spare (or (btree-spare l-sib children max-keys)
                        (btree-spare r-sib children max-keys))
              sib (or l-sib r-sib) in
          (if spare
              (btree-rotate (list keys new-children) ix spare)
            (btree-merge (list keys new-children) ix sib)))
      (list keys (updated-v children ix new-child)))))

(defun btree-rotate (tree dest-ix src-ix)
  (let:: (keys children) tree
         src (aref children src-ix)
         dest (aref children dest-ix)
         sep-ix (/ (+ src-ix dest-ix) 2)
         sep-key (aref keys sep-ix)
         (new-dest new-src new-sep)
         (if (< src-ix dest-ix)
             ;; rotate right
             (btree-rotate-children 
              src dest 'alast 'vdrop-right 'vcons sep-key)
           (btree-rotate-children 
            src dest (cut aref <> 0) 'subseq 'vcons-right sep-key)) in
    (>>> (copy-seq children)
         (progn (setf (aref _ dest-ix) new-dest
                      (aref _ src-ix) new-src)
                _)
         (list (updated-v keys sep-ix new-sep) _))))

(el2
 (defun btree-rotate-children (source dest v-get v-drop v-cons key)
   (let:: (src-keys src-children) source
          (dst-keys dst-children) dest in
     (let: new-dest (list (v-cons dst-keys key)
                          (and src-children 
                               (v-cons dst-children (v-get src-children))))
           new-src (list (v-drop src-keys 1)
                         (and src-children (v-drop src-children 1))) in
       (list new-dest new-src (v-get src-keys))))))

(defun btree-spare (ix child-nodes max-keys)
  (and ix
       (dbind (keys children) (aref child-nodes ix)
         (> (length keys) (/ max-keys 2)))
       ix))

(defun btree-merge (tree ix sib-ix)
  (let:: (ix sib-ix) (list (min ix sib-ix) (max ix sib-ix))
         (keys children) tree
         (a-keys a-children) (aref children ix)
         (b-keys b-children) (aref children sib-ix)
         sep-ix (/ (+ sib-ix ix) 2)
         sep-key (aref keys sep-ix)
         new-keys (vconcat a-keys `(,sep-key) b-keys)
         new-children (and a-children (vconcat a-children b-children))
         new-node (list new-keys new-children) in
    (list (vrem keys sep-ix)
          (>>> (vrem children sep-ix)
               (progn 
                 (setf (aref _ sep-ix) new-node) _)))))

(provide 'btree)
