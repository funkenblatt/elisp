;; Horrible and assworthy 2-3 b-tree implementation.
;; Done primarily to understand b-trees.
;; At some point I should probably add the ability to delete nodes.

(defstruct (bnode) (keys) (children))

(defun bleaf-p (node)
  (not (bnode-children node)))

(defun bnode-insert (node newitem cmp)
  (if (not node) (setq node (make-bnode)))
  (let: new (bnode-insert-r node newitem cmp) in
    (if (consp new)
	(make-bnode
	 :keys (vector (cadr new) nil)
	 :children (vector (car new) (caddr new) nil))
      new)))

(defun bnode-insert-r (node newitem cmp) ;Recursive subfunction
  (if (bleaf-p node)
      (bleaf-insert node newitem cmp)
    (catch 'break
      (dotimes (i 2)
	(let: key (aref (bnode-keys node) i)
	      c (and key (funcall cmp newitem key)) in
	  (cond
	   ((not c) (throw 'break (bnode-insert-internal node newitem cmp i)))
	   ((= c 0) (throw 'break node))
	   ((< c 0)
	    (throw 'break (bnode-insert-internal node newitem cmp i))))))
      (bnode-insert-internal node newitem cmp 2))))

(defun vsearch (v item cmp)
  (catch 'break
    (dotimes (i (length v))
      (if (not (aref v i)) (throw 'break nil))
      (if (= 0 (funcall cmp item (aref v i)))
	  (throw 'break i)))
    nil))

(defun bnode-key (node n)
  (aref (bnode-keys node) n))

(defun bnode-child (node n)
  (aref (bnode-children node) n))

(defun bnode-sortwad (node newitem cmp)
  (sort (append (list newitem) (bnode-keys node) nil)
	(lambda (a b) (< (funcall cmp a b) 0))))

(defun bleaf-insert (node newitem cmp)
  (cond
   ((not (bnode-keys node)) (make-bnode :keys (vector newitem nil)))
   ((vsearch (bnode-keys node) newitem cmp) node)
   ((not (bnode-key node 1))
    (make-bnode :keys (bnode-make-keywad node newitem cmp)))
   (t 
    (let: sorted (bnode-sortwad node newitem cmp) in
      (list (make-bnode :keys (vector (car sorted) nil))
	    (cadr sorted)
	    (make-bnode :keys (vector (caddr sorted) nil)))))))

(defun bnode-make-keywad (node newitem cmp)
  (if (> 0 (funcall cmp newitem (bnode-key node 0)))
      (vector newitem (bnode-key node 0))
    (vector (bnode-key node 0) newitem)))

(defun bnode-insert-internal (node newitem cmp ix)
  "Tries to recurse into child IX of NODE, does some
splitting and whatnot if it has to."
  (let: node-wad (bnode-insert-r (bnode-child node ix) newitem cmp) 
	tmpnode nil in
    (if (consp node-wad)
	(bnode-internal-split node node-wad cmp ix)
      (make-bnode :keys (bnode-keys node)
		  :children (vset (bnode-children node)
				  ix node-wad)))))

(defun bnode-internal-split (node split-obj cmp ix)
  (if (not (bnode-key node 1))
      (make-bnode
       :keys (bnode-make-keywad node (cadr split-obj) cmp)
       :children (apply 'vector
			(vsplice (bnode-children node)
				 (list (car split-obj) (caddr split-obj)) 
				 ix nil t)))
    (let: children (vsplice (bnode-children node) 
			    (list (car split-obj) (caddr split-obj))
			    ix)
	  keys (vsplice (bnode-keys node)
			(list (cadr split-obj))
			ix t) in
      (list (make-bnode :keys (vector (car keys) nil)
			:children (vector (pop children) (pop children) nil))
	    (cadr keys)
	    (make-bnode :keys (vector (caddr keys) nil)
			:children (vector (pop children) (pop children) nil))))))

(defun btree-map (proc node &optional depth) ;Inorder tree map.
  (if (not depth) (setq depth 0))
  (let: children (append (bnode-children node) nil)
	keys (append (bnode-keys node) nil) in
    (while (or keys children)
      (if (car children) 
	  (btree-map proc (car children) (+ depth 1)))
      (if (car keys)
	  (funcall proc (car keys) depth))
      (pop keys)
      (pop children))))

(defun btree-preorder (proc node &optional depth)
  (if (not depth) (setq depth 0))
  (funcall proc (bnode-keys node) depth)
  (mapc (lambda (x)
	  (if x (btree-preorder proc x (+ depth 1))))
	(bnode-children node)))

