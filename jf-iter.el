;; -*- lexical-binding: t; -*-

(defvar for-iterators (dict))

(defmacro def-iterator (name arglist &rest clauses)
  `(puthash
    ',name
    (lambda ,arglist
      `((init . ,(λ ,@(assoc-default 'init clauses)))
        (next . ,(λ ,@(assoc-default 'next clauses)))
        (current . ,(λ ,@(assoc-default 'current clauses)))
        (term . ,(λ ,@(assoc-default 'term clauses)))))
    for-iterators))

(def-iterator
  ints (&optional start stop step)
  (init () (list (or start 0)))
  (current ((ix)) ix)
  (next ((ix)) `((+ ,ix ,(or step 1))))
  (term ((ix)) (if stop `(= ,ix ,stop) nil)))

(def-iterator
  iota (&optional n start step)
  (init () (list n (or start 0)))
  (current ((n i)) i)
  (next ((n i)) `((- ,n 1) (+ ,i ,(or step 1))))
  (term ((n i)) `(= ,n 0)))

(def-iterator
  in-list (l)
  (init () `(,l))
  (current ((cell)) `(car ,cell))
  (next ((cell)) `((cdr ,cell)))
  (term ((cell)) `(not ,cell)))

(def-iterator
  in-vec (v)
  (init () `(,v 0 (length ,v)))
  (current ((v ix len)) `(aref ,v ,ix))
  (next ((v ix len)) `(,v (+ ,ix 1) ,len))
  (term ((v ix len)) `(= ,ix ,len)))

(def-iterator
  cycled (v)
  (init () `((vec ,v) 0 (length ,v)))
  (current ((vecwad ix len)) `(aref ,vecwad ,ix))
  (next ((vecwad ix len)) `(,vecwad (mod (+ ,ix 1) ,len) ,len))
  (term ((vecwad ix len)) nil))

(defun* iterator-info ((iter-name . args))
  (iflet iterator (apply (gethash iter-name for-iterators) args)
         (cl-flet ((iterfun (fun &rest args)
                            (apply (assoc-default fun iterator) args)))
           (let* ((inits (iterfun 'init))
                  (vars (mapcar (x: (gensym)) inits))
                  (updates (iterfun 'next vars))
                  (current (iterfun 'current vars))
                  (term (iterfun 'term vars)))
             (list inits vars updates (list current) (list term))))
         (error "No such generator!")))

(defun iterator-updates (vars updates)
  (>>> (mapcar* 'list vars updates)
       (filter (λ ((var update)) (not (eq var update))) _)
       (mapcar (cur 'cons 'setf) _)))

(defun iterator-basic-clause (iterator-call var body)
  (if (eq var :when)
      `(if ,iterator-call
           ,body)
    (let:: (inits vars updates (current) (term)) (iterator-info iterator-call) in
      `(let ,(mapcar* 'list vars inits)
         (while (not ,term)
           (let ((,var ,current))
             ,body)
           ,@(iterator-updates vars updates))))))

(defun iterator-and-clause (iterator-calls value-vars body)
  (let:: iter-stuff (mapcar 'iterator-info iterator-calls)
         (inits vars updates currents terms)
         (apply 'mapcar* 'append iter-stuff) in
    `(let ,(mapcar* 'list vars inits)
       (while (not (or ,@terms))
         (let ,(mapcar* 'list value-vars currents)
           ,body)
         ,@(iterator-updates vars updates)))))

(defmacro jf-iter (loop-clauses &rest body)
  (pcase loop-clauses
    (`nil `(progn ,@body))
    (`((,var-names (and . ,iters)) . ,more)
     (iterator-and-clause 
      iters var-names
      `(jf-iter ,more ,@body)))
    (`((,var-name ,iter) . ,more)
     (iterator-basic-clause 
      iter var-name `(jf-iter ,more ,@body)))))

(defmacro jf-iter-list (loop-clauses &rest body)
  (let: v (gensym) in
    `(let (,v)
       (jf-iter ,loop-clauses
                (push (progn ,@body) ,v))
       (nreverse ,v))))

(defmacro iter: (&rest stuff)
  (dbind (clauses body) (split-by (cut eq <> 'in) stuff)
    `(jf-iter ,(sublist2 clauses)
              ,@body)))

(defmacro iter/l: (&rest stuff)
  (dbind (clauses body) (split-by (cut eq <> 'in) stuff)
    `(jf-iter-list ,(sublist2 clauses)
                   ,@body)))

(provide 'jf-iter)
