;; -*- lexical-binding: t -*-
(require 'cl)
(require 'lisp-mode)

(defun distribute (product)
  (let* ((sum-p (andfn 'consp
		       (comp (orfn
			      (cur 'eq '+)
			      (cur 'eq '-))
			     'car)
		       (comp (cur '< 2) 'length)))
	 (sums (remove-if-not sum-p (cdr product)))
	 (sums (mapcar (lambda (s)
			 (pcase s
			   (`(- ,a . ,more)
			    `(+ ,a ,@(mapcar (cur 'list '-) more)))
			   (x x)))
		       sums))
	 (others (remove-if sum-p (cdr product))))
    (cons '+ (mapcar
	      (comp (cur 'append (cons '* others)))
	      (apply 'cross
		     (mapcar 'cdr sums))))))

(defun mapcat (fun l)
  (apply 'append (mapcar fun l)))

(defun interpose (l x)
  (pcase l
    (`(,i) l)
    (`(,i . ,more) (append (list i x) (interpose more x)))))

(defun to-infix (expr)
  (pcase expr
    (`(expt ,a ,b) `(,(to-infix a) ^ ,(to-infix b)))
    (`(cos ,x) `(cos (,(to-infix x))))
    (`(sin ,x) `(sin (,(to-infix x))))
    (`(,op . ,args) (interpose (mapcar 'to-infix args) op))
    (x x)))

(defun eval-replace-2 (&optional transform)
  (interactive)
  (let* ((bounds (list (save-excursion
			 (backward-sexp)
			 (point))
		       (point)))
	 (expr (read (apply 'buffer-substring bounds)))
	 (_ (apply 'delete-region bounds))
	 (result (eval (if transform
			   (funcall transform expr)
			 expr)
		       t)))
    (insert (repr result))))

(defun flatten-math (expr)
  (pcase expr
    (`(,op . ,args)
     (cons op (mapcat
	       (lambda (x)
		 (if (and (consp x) (eq (car x) op))
		     (cdr (flatten-math x))
		   (list x)))
	       args)))
    (x x)))

(defun collect-terms (expr term)
  (pcase expr
    (`(+ . ,terms)
     (let ((has-term (lambda (prod)
		       (pcase prod
			 (`(* . ,args)
			  (some (cur 'equal term) args))
			 (x (if (equal term prod) 1 nil))))))
       `(+ (* ,term (+ ,@(mapcar
			  (lambda (x)
                            (cond
                             ((consp x)
			      (>>> (remove-if (cur 'equal term) x :count 1)
				   (cond
                                    ((= (length _) 2) (cadr _))
                                    ((= (length _) 1) 1)
                                    (t _))))
                             ((equal x term) 1)
                             (t x)))
			  (remove-if-not has-term terms))))
	   ,@(remove-if has-term terms))))
    (x x)))

(defun calc->elisp (calc-expr)
  (pcase calc-expr
    (`(var ,x ,junk) x)
    (`(^ ,a ,b) `(expt ,(calc->elisp a) ,(calc->elisp b)))
    (`(,op . ,args)
     (let ((op (if (string-match-p "^calcFunc-" (symbol-name op))
		   (intern (substring (symbol-name op) 9))
		 op)))
       `(,op ,@ (mapcar 'calc->elisp args))))
    (x x)))

(defun setup-algebra-bindings ()
  (interactive)
  (local-set-key (kbd "C-c C-x") (lambda () (interactive)
				   (eval-replace-2
				    (lambda (x)
				      `(distribute ',x)))))

  (local-set-key (kbd "C-c C-f") (lambda () (interactive)
				   (eval-replace-2
				    (lambda (x)
				      `(flatten-math ',x)))))

  (local-set-key (kbd "C-c C-c") (lambda () (interactive)
				   (>>>
				    (read)
				    (eval-replace-2
				     (lambda (x)
				       `(collect-terms
					 ',x
					 ',_))))))
  (local-set-key (kbd "C-c C-n")
		 (lambda ()
		   (interactive)
		   (eval-replace-2
		    (lambda (x)
		      (list 'quote (pcase x
				     (`(- (* . ,args))
				      `(* -1 ,@args))
				     (y y)))))))

  (local-set-key
   (kbd "C-c C-s")
   (lambda ()
     (interactive)
     (eval-replace-2
      (lambda (x)
	(list 'quote
	      (pcase x
		(`(,(and (or `* `+) op) . ,args)
		 (cons op (sort* args 'string< :key 'repr)))
		(x x))))))))

;; (define-key lisp-interaction-mode-map (kbd "M-E") 'eval-replace-2)

(provide 'algebra-2)
