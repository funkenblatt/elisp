;; Gimpiest symbolic differentiator ever.
;; By Jason Feng.
(require 'comparison)

(defun map-partitions (proc l)
  "Calls PROC with two arguments, the (reversed) left partition, and the
right partition."
  (let: head nil in
    (while l
      (funcall proc head l)
      (push (pop l) head))))

(defun product-rule (expr var)
  (let: out nil in
    (map-partitions (lambda (l r)
		      (push (make-product 
			     `(,@l 
			       ,(deriv (car r) var) 
			       ,@(cdr r)))
			    out))
		    (cdr expr))
    (make-sum out)))

(defun make-product (terms)
  (catch 'break
    (let: out (lc (if (equal x 0) (throw 'break 0) x)
		  x terms (not (equal x 1))) in
      (cond
       ((not out) 1)
       ((not (cdr out)) (car out))
       (t (cons '* out))))))

(defun make-sum (terms)
  (let: out (lc x x terms (not (equal x 0))) in
    (cond
     ((not out) 0)
     ((not (cdr out)) (car out))
     (t (cons '+ out)))))

(defun make-expt (terms)
  (cond
   ((equal (cadr terms) 1) (car terms))
   ((equal (cadr terms) 0) 1)
   (t (cons '^ terms))))

(defun is-upper (sym)
  (if (symbolp sym) (setq sym (symbol-name sym)))
  (equal sym (upcase sym)))

(defun power-rule (expr var)
  (make-product
   (list (caddr expr)
	 (make-expt 
	  (list (cadr expr) (- (caddr expr) 1)))
	 (deriv (cadr expr) var))))

(defun add-rule (expr var)
  (if (eq (car expr) '-)
      (setq expr (cons (cadr expr) (lc `(* -1 ,x) x (cddr expr))))
    (pop expr))
  (make-sum (lc (deriv x var) x expr)))

(defun to-infix (expr &optional surrounding)
  (if (and expr (listp expr))
      (foo-cond
       ((and (eq (car expr) '-)
	     (not (cddr expr))) 
	=> ass
	(str "-(" (to-infix (cadr expr)) ")"))
       ((memq (car expr) '(+ - * / ^)) => junk
	(apply 'str
	       (if (and surrounding (memq surrounding (cdr junk)))
		   "("
		 "")
	       (append (join (lc (to-infix x (car expr)) x (cdr expr))
			     (str " " (car expr) " "))
		       (if (and surrounding (memq surrounding (cdr junk)))
			   '(")")
			 '("")))))
       ((memq (car expr) '(cos sin)) => ignored
	(str (car expr) "("
	     (to-infix (cadr expr))
	     ")"))
       (t => _ (str (car expr))))
    expr))

(defun deriv (expr &optional var)
  "Takes a symbolic equation represented in syntax tree form,
and returns its derivative.  If a variable to differentiate
with respect to is not supplied, it assumes an implicit
\"time\" variable for all variable names that are in all
lower case."
  (cond
   ((listp expr)
    (foolet (lhead (car expr))
      (cond 
       ((or (eq lhead '+) (eq lhead '-))
	(add-rule expr var))
       ((eq lhead '*)
	(product-rule expr var))
       ((eq lhead 'cos)
	(make-product `(-1 (sin ,(cadr expr))
			   ,(deriv (cadr expr) var))))
       ((eq lhead 'sin)
	(make-product
	`((cos ,(cadr expr)) ,(deriv (cadr expr) var))))
       ((or (eq lhead '^) (eq lhead 'expt))
	(power-rule expr var)))))
   ((symbolp expr)
    (cond
     ((is-upper expr) 0)
     (var (if (eq expr var) 1 0))
     (t (symbol-cat expr 'dot))))
   ((numberp expr) 0)))

(defun flatten (expr)
  "This doens't get used, but one day it might."
  (foolet (out expr tmp nil)
    (if (not (listp expr))
	expr
      (while (filter 
	      (lambda (x) 
		(and (listp x) 
		     (eq (car x) (car expr))))
	      (cdr out))
	(dolist (x (cdr out))
	  (if (and (listp x) (eq (car x) (car expr)))
	      (setq tmp (append (reverse (cdr x)) tmp))
	    (push x tmp)))
	(setq out (nreverse tmp))
	(push (car expr) out)
	(setq tmp nil))
      (cons (car expr)
	    (mapcar 'flatten (cdr out))))))

(defun sec-deriv-matrix (f var1 var2)
  (lc
   (lc (calc-eval
	(format ;"rewrite(simplify(expand(deriv(deriv(%s, %s), %s))), a*cos(x)^2+a*sin(x)^2 := a, 1)"
	 "simplify(expand(deriv(deriv(%s,%s),%s)))"
	 (to-infix f) i j))
       j var2)
   i var1))

(defun make-matrix (m)
  (format "[%s]"
	  (apply 'concat (lc (format "[%s]" (apply 'concat (join x ", "))) x m))))

(defun calcify-var (z)
  (if (and (symbolp z) (not (memq z '(sin cos + * - ^))))
      `(var ,z ,(symbol-cat 'var- z))
    z))

(defun deriv* (&rest args)
  (flatten (apply 'deriv args)))

(defmacro cond-unquote (pred expr)
  `(if ,pred (list ,expr)))

(defun eliminate-subs (expr)
  (cond
   ((and (consp expr) (eq (car expr) '-))
    `(+ ,(cadr expr)
        ,@(cond-unquote (cddr expr) `(* -1 (+ ,@(mapcar 'eliminate-subs (cddr expr)))))))
   ((consp expr)
    `(,(car expr)
      ,@(mapcar 'eliminate-subs (cdr expr))))
   (t expr)))

(defun expand (expr)
  (cond
   ((consp expr)
    (case (car expr)
      (+ `(+ ,@(mapcar 'expand (cdr expr))))
      (* (let: terms (mapcar 'expand (cdr expr))
               sub-sums (lc x x terms (and (consp x)
                                           (eq (car x) '+)))
               non-sums (lc x x terms (not (and (consp x)
                                                (eq (car x) '+)))) in
           (case (length sub-sums)
             (0 expr)
             (1 `(+ ,@(lc (flatten `(* ,@non-sums ,x)) x (cdar sub-sums))))
             (t (setq sub-sums (list (multiply-sums sub-sums)))
                `(+ ,@(lc (flatten `(* ,@non-sums ,x)) x (cdar sub-sums)))))))
      (/ `(/ ,@(mapcar 'expand (cdr expr))))
      (t `(,(car expr) ,(expand (cadr expr))))))
   (t expr)))

(defun multiply-sums (sums)
  (cons '+ (lc `(* ,@x) x (apply 'cross (mapcar 'cdr sums)))))

(defun sort-terms (expr &optional excluded)
  (if (and (consp expr) (not (memq (car expr) excluded)))
      (cons (car expr)
            (sort (copy-seq (mapcar (x: (sort-terms x excluded)) (cdr expr)))
                  (lambda (a b) (< (cmp a b) 0))))
    expr))

(provide 'deriv)
