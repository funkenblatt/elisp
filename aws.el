;; -*- lexical-binding: t; -*-

(require 'http-junk)

(defun aws-urlencode (s)
  (replace-regexp-in-string
   (rx (not (any (?a . ?z) (?A . ?Z) digit "-_.~")))
   (lambda (s) (upcase (format "%%%02x" (aref s 0))))
   (string-as-unibyte s) nil t))

(defun aws-canonical-query (q)
  (do-wad
   (lc (cons (str (car x)) (str (cdr x))) x q)
   (sortbag _ x (car x) 'string<)
   (lc (interp "#,(aws-urlencode (car x))=#,(aws-urlencode (cdr x))") x _)
   (strjoin "&" _)
   ))

(defun aws-signing-string (verb host request-uri canonical-q)
  (strjoin "\n" (list verb host request-uri canonical-q)))

(defun aws-signature (secret-key verb host request-uri q)
  (do-wad
   (aws-signing-string 
    verb host request-uri 
    (aws-canonical-query q))
   (hmac secret-key _ 'sha1)
   base64-encode-string))

(defun aws-req (host access-key secret-key params cont)
  (let: params `(,@params
                     (AWSAccessKeyId . ,access-key)
                     (Version . "2012-08-15")
                     (Timestamp . ,(format-time-string
                                    "%Y-%m-%dT%TZ" nil t))
                     (SignatureVersion . "2")
                     (SignatureMethod . "HmacSHA1"))
        sig (aws-signature secret-key "GET" host "/" params)
        all-params (cons `(Signature . ,sig) params) in
     (curl
      (interp "https://#,[host]/?#,(urlencode-alist all-params)")
      cont)))

(defun hmac (key msg hash)
  (setf hash (comp 'unhex hash))
  (setf key (string-as-unibyte key))
  (if (> (string-bytes key) 64)
      (setf key (funcall hash key)))
  (if (< (string-bytes key) 64)
      (setf key (str key 
                     (string-as-unibyte
                      (make-string (- 64 (string-bytes key)) 0)))))
  (let: o-key-pad (xor-bytes #x5c key)
        i-key-pad (xor-bytes #x36 key) in
    (do-wad
     (str i-key-pad (string-as-unibyte msg)) (funcall hash _)
     (str o-key-pad _) (funcall hash _))))

(defun unhex (s)
  (do-wad
   (re-findall ".." s t)
   (mapcar (comp 'read (cut str "#x" <>)) _)
   (apply 'unibyte-string _)))

(defun xor-bytes (pattern s)
  (apply 'unibyte-string (lc (logxor pattern x) x s)))

;; XML hacks
(defun filter-xml-tree (pred tree)
  "For every node, leaf or otherwise, in TREE, keep it in there if PRED returns non-nil."
  (cond
   ((not (funcall pred tree)) nil)
   ((consp tree)
    (append (list (car tree) (cadr tree))
            (mapcar (cut filter-xml-tree pred <>) (filter pred (cddr tree)))))
   (t tree)))

(defun nodes-where (pred tree)
  "Return every node in TREE matching PRED.  Does not recurse into
nodes which have matched PRED."
  (cond
   ((funcall pred tree) (list tree))
   ((consp tree) (mappend (cur 'nodes-where pred) (cddr tree)))))

(defun tagp (tag)
  (lambda (x) (and (consp x) (eq (car x) tag))))

(defun undouchify-xml (tree)
  (if (consp tree)
      (if (stringp (caddr tree))
          (cons (car tree) (caddr tree))
        (append
         (and (not (eq (car tree) 'item)) (list (car tree)))
         (mapcar #'undouchify-xml (cddr tree))))
    tree))

(defun decamelize (s)
  (let: case-fold-search nil in 
    (replace-regexp-in-string "[A-Z]" (x: (str "-" (downcase x))) (str s) t t)))

(defun undouchify-tags (instance)
  (do-wad
   (chain-ref instance 'tag-set)
   (mapcar (juxtcons (:= 'key) (:= 'value)) _)
   `((tags . ,_) ,@instance)))

(defun all-text (node)
  (apply 'str (nodes-where 'stringp node)))

(defun cleanup-xml (aws-data)
  (with-temp-buffer
    (insert aws-data)
    (do-wad
     (xml-parse-region (point-min) (point-max)) car
     (filter-xml-tree (orfn (comp 'not 'stringp) (=~ "[^ \n\r\t]")) _)
     (maptree (x: (if (symbolp x) (intern (decamelize x)) x)) _)
     )))

(defun aws-xml-alist (xml &rest tags)
  (do-wad 
   (cleanup-xml xml)
   (apply 'tag-nest _ (or tags '(item))) 
   (mapcar 'undouchify-xml _)))

(defun tag-nest (tree tag &rest more-tag)
  (do-wad
   (nodes-where (tagp tag) tree)
   (if more-tag
       (mappend (cut apply 'tag-nest <> (car more-tag) (cdr more-tag))
                _)
     _)))

(defun get-instance-stuff (access-key secret-key)
  (aws-req "us-west-1.ec2.amazonaws.com"
           access-key secret-key
           '((Action . "DescribeInstances"))
           (x: (do-wad
                (cleanup-xml x)
                (>> list 
                    iset (nodes-where (tagp 'instances-set) _)
                    i (nodes-where (tagp 'item) iset) in
                    i)
                (mapcar (comp 'undouchify-tags 'undouchify-xml) _)
                (setf instance-xml-data _))
               (pr "Done\n"))))

(defun aws-encode-param (key value)
  (cond
   ((or (stringp value) (numberp value) (symbolp value)) `((,key . ,value)))
   ((consp value)
    (apply 'append
           (lc (aws-encode-param (str key "." (car x)) (cadr x)) x
               (sublist-n value 2))))
   ((vectorp value) (aws-encode-multi-param key value))))

(defun aws-encode-multi-param (key values)
  (apply 'append
         (mapcar*
          (lambda (val n)
            (aws-encode-param (str key "." n) val))
          values (range 1 (+ (length values) 1)))))

(provide 'aws)
