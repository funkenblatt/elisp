(defun run-snd ()
  (interactive)
  (with-current-buffer (find-file "~/beastwads")
    (let: buf (run-scheme "snd") in
      (comint-send-string
       (get-buffer-process buf)
       "(load \"instruwad.scm\")
(load \"v.scm\")\n"))))

(provide 'snd)
