;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(defstruct charwad lst ix)

(defun charwad-insert (cw c)
  (if (charwad-lst cw)
      (progn (aset (car (charwad-lst cw)) (charwad-ix cw) c)
	     (charwad-ix cw (+ (charwad-ix cw) 1))
	     (when (= (charwad-ix cw) 4)
	       (charwad-ix cw 0)
	       (charwad-lst cw (cons (make-string 4 0) (charwad-lst cw)))))
    (charwad-lst cw (list (make-string 4 0)))
    (charwad-insert cw c)))

(defun charwad-yield (cw)
  (apply 'concat 
	 (reverse 
	  (cons
	   (substring (car (charwad-lst cw)) 0 (charwad-ix cw))
	   (cdr (charwad-lst cw))))))
