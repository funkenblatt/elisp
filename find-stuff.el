;; -*- lexical-binding: t; -*-

(defun find-parenthesize (stuff)
  `("(" ,@stuff ")"))

(defun make-find-args (find-expr)
  "Returns a list of arguments suitable for calling find with."
  (let: p (comp 'find-parenthesize 'make-find-args) in
    (pcase find-expr
      (`(or . ,subexprs) (>>> (mapcar p subexprs) (inter-splice _ '("-or"))))
      (`(and . ,subexprs) (>>> (mapcar p subexprs) (inter-splice _ '("-and"))))
      (`(not ,expr) `("-not" "(" ,@(make-find-args expr) ")"))
      (`dir '("-type" "d"))
      (`(,op ,arg) `(,(str "-" op) ,arg))
      ((pred symbolp) `(,(str "-" find-expr)))
      ((pred stringp) `("-name" ,find-expr)))))

(defun do-find (find-expr)
  (apply 'process-lines "find" "."
         (make-find-args find-expr)))

(provide 'find-stuff)
