;; It's all scrolly!
;; 

(defun p- (a b) (cons (- (car a) (car b)) (- (cdr a) (cdr b))))
(defun p+ (a b) (cons (+ (car a) (car b)) (+ (cdr a) (cdr b))))

(defun offset-winscroll (win dp)
  (image-forward-hscroll (car dp))
  (image-scroll-up (cdr dp)))

(defun image-mouse-drag-scroll (ev)
  (interactive "e")
  (let ((curpos (img-frame-relative-coords ev))
	lastpos
	(win (posn-window (event-start ev)))
	new-ev)
    (select-window win)
    (track-mouse
      (while (mouse-movement-p (setq new-ev (read-event)))
	(setq lastpos (img-frame-relative-coords new-ev))
	(offset-winscroll win (p- curpos lastpos))
	(setq curpos lastpos)))))

(defun img-frame-relative-coords (ev)
  (let ((pos (posn-col-row (event-start ev)))
	(win (window-inside-edges (posn-window (event-start ev)))))
    (p+ pos (cons (car win) (cadr win)))))

(provide 'image-drag)
