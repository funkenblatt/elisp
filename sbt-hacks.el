;; -*- lexical-binding: t; -*-

(defun enclosing-project (dir)
  (>>> (iterate (x: (expand-file-name (str x "/..")))
                (orfn 'sbt-project (cur 'equal "/"))
                dir)
       (and (not (equal "/" _)) _)))

(defun sbt-project (dir)
  (intersection (directory-files dir)
                '("build.sbt" "assembly.sbt")
                :test 'equal))

(define-key scala-mode-map
  (kbd "<f5>")
  (keycmd (let: default-directory default-directory in
            (cd (enclosing-project "."))
            (compile (interp "sbt compile")))))

(provide 'sbt-hacks)
