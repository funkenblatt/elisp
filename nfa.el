;; Powerset construction junk.
;; NFA syntax is such that the first state given is the starting
;; state, and consists of a list of rules, where each rule
;; is as (<state-id> (<input-label> <new-state>* )* ).
;; Does not handle epsilon transitions.

;(require 'cl)

(defun combine-states (statewad)
  "Create the name of a DFA state out of several NFA states."
  (if (memq 'accept! statewad) 
      'accept!
    (apply 'symbol-cat 
	   (join (sort (mapcar 'symbol-name statewad) 'string-lessp)
		 '-))))

(defun unify (nfa states)
  "Unify the transition functions of several NFA states into
one transition table.  State labels must compare eq (i.e. can't
be strings)."
  (foolet (newwad (mappend (lambda (x) (gethash x nfa))
			   states)
	   out (make-hash-table))
    (dolist (i newwad)
      (dolist (j (cdr i))
	(if (not (memq j (gethash (car i) out)))
	    (puthash (car i)		 
		     (cons j (gethash (car i) out))
		     out))))
    out))

(defun powerset (nfa)
  "Does a powerset wad."
  (foolet (start (car nfa)
	   dfa (make-hash-table))
    (setq nfa (alist->dict nfa))
    (make-new-state (list (car start))
		    nfa
		    dfa)
    (hash->list dfa)))

(defun make-new-state (curstates nfa dfa)
  "Make a new DFA state based on a current
set of NFA states... and then recursively generate
new DFA states based on the possible exit points of
this one."
  (catch 'return
    (when (memq 'accept! curstates)
      (puthash 'accept! nil dfa)
      (throw 'return nil))
    (foolet (exits (hash->list (unify nfa curstates)))
      (puthash (combine-states curstates)
	       (lc (list (car x) (combine-states (cdr x))) 
		   x exits)
	       dfa)
      (dolist (edge exits)
	(if (not (gethash (combine-states (cdr edge)) dfa))
	    (make-new-state (cdr edge) nfa dfa))))))

(defun dfadump (dfa)
  (foolet (maxlen (apply 'max (lc (funcall (funcomp length symbol-name car) x) x dfa)))
    (dolist (i dfa)
      (pr (car i) (str* " " (- (+ maxlen 3) (length (symbol-name (car i))))))
      (pr (cadr i) "\n")
      (dolist (j (cddr i))
	(pr (str* " " (+ maxlen 3)) j "\n"))
      (pr "\n"))))

(defun aswap (a ix1 ix2)
  (foolet (temp (aref a ix1))
    (aset a ix1 (aref a ix2))
    (aset a ix2 temp)
    a))

(defun barwad (sym)
  "Construct an NFA from a symbol or string, presumably for searching
purposes."
  (if (symbolp sym) (setq sym (symbol-name sym)))
  (foolet (name (concat sym "|")
	   ix (- (length name) 1)
	   out '()
	   startstate (intern (concat "|" sym)))
    (while (> ix 0)
      (aswap name ix (- ix 1))
      (push (list (intern name)
		  (if (caar out)
		      (list (intern (substring name ix (+ ix 1)))
			    (caar out)
			    startstate)
		    (list (intern (substring name ix (+ ix 1)))
			  'accept!)))			  
	    out)
      (setq ix (- ix 1))
      (setq name (concat name "")))
    out))

(defun sym-num (sym n)
  (symbol-cat sym (number-to-string n)))

(defun string->nfa (s)
  (lc*
   `(,(sym-num 's x)
     (,y 
      ,(if (eq (+ x 1) (length s)) 
	   'accept! 
	 (sym-num 's (+ x 1)))
      s0))
   (x y) ((range (length s)) (append s nil))))

(defmacro matchwad (s in-expr state-expr)
  "Creates a DFA that will be translated into a big-ass
case statement."
  (foolet (nfa (string->nfa s)
	   fsm (powerset nfa))
    (dfa->lisp fsm in-expr state-expr 's0)))

(defun dfa->lisp (dfa input-expr state-expr start-state)
  "Creates a lisp expression that will take an existing
state and an input expression and calculate the next state.
Intended for use in macros."
  (setq dfa (reverse dfa))
  `(case ,state-expr
     ,@(lc `(',(car state)
	     (case ,input-expr
	       ,@(lc `(,(car tran) (setq ,state-expr ',(cadr tran)))
		     tran (cdr state))
	       (t (setq ,state-expr ',start-state))))
	   state dfa 
	   (not (eq (car state) 'accept!)))))

(defmacro dfa-wad (dfa input-expr state-expr start-state)
  (dfa->lisp dfa input-expr state-expr start-state))

(provide 'nfa)
