;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(defun cross (items n)
  (cond 
   ((= n 1) (lc (list x) x items))
   (t (apply 'append
	     (lc (lc (cons i x)
		     i items)
		 x (cross items (- n 1)))))))


(defun de-semicolon (s)
  (dotimes (i (length s))
    (if (= (aref s i) 59)
	(aset s i 44)))
  s)

(defun foobandit ()
  (while (re-search-forward "\"[^\"]*\"" nil t)
    (replace-match (de-semicolon (match-string 0)))))
