(setq imap-grammar-rules
      '(("address" "\"(\"" "addr-name" "SP" "addr-adl" "SP" "addr-mailbox" "SP" "addr-host" "\")\"")

        ("addr-adl" "nstring")

        ("addr-host" "nstring")

        ("addr-mailbox" "nstring")

        ("addr-name" "nstring")

        ("append" "\"APPEND\"" "SP" "mailbox" "[" "SP" "flag-list" "]" "[" "SP" "date-time" "]" "SP" "literal")

        ("astring" "1" "*" "ASTRING-CHAR" "/" "string")

        ("ASTRING-CHAR" "ATOM-CHAR" "/" "resp-specials")

        ("atom" "1" "*" "ATOM-CHAR")

        ("ATOM-CHAR" "<any CHAR except atom-specials>")

        ("atom-specials" "\"(\"" "/" "\")\"" "/" "\"{\"" "/" "SP" "/" "CTL" "/" "list-wildcards" "/" "quoted-specials" "/" "resp-specials")

        ("authenticate" "\"AUTHENTICATE\"" "SP" "auth-type" "*" "(" "CRLF" "base64" ")")

        ("auth-type" "atom")

        ("base64" "*" "(" "4" "base64-char" ")" "[" "base64-terminal" "]")

        ("base64-char" "ALPHA" "/" "DIGIT" "/" "\"+\"" "/" "\"/\"")

        ("base64-terminal" "(" "2" "base64-char" "\"==\"" ")" "/" "(" "3" "base64-char" "\"=\"" ")")

        ("body" "\"(\"" "(" "body-type-1part" "/" "body-type-mpart" ")" "\")\"")

        ("body-extension" "nstring" "/" "number" "/" "\"(\"" "body-extension" "*" "(" "SP" "body-extension" ")" "\")\"")

        ("body-ext-1part" "body-fld-md5" "[" "SP" "body-fld-dsp" "[" "SP" "body-fld-lang" "[" "SP" "body-fld-loc" "*" "(" "SP" "body-extension" ")" "]" "]" "]")

        ("body-ext-mpart" "body-fld-param" "[" "SP" "body-fld-dsp" "[" "SP" "body-fld-lang" "[" "SP" "body-fld-loc" "*" "(" "SP" "body-extension" ")" "]" "]" "]")

        ("body-fields" "body-fld-param" "SP" "body-fld-id" "SP" "body-fld-desc" "SP" "body-fld-enc" "SP" "body-fld-octets")

        ("body-fld-desc" "nstring")

        ("body-fld-dsp" "\"(\"" "string" "SP" "body-fld-param" "\")\"" "/" "nil")

        ("body-fld-enc" "(" "DQUOTE" "(" "\"7BIT\"" "/" "\"8BIT\"" "/" "\"BINARY\"" "/" "\"BASE64\"" "/" "\"QUOTED-PRINTABLE\"" ")" "DQUOTE" ")" "/" "string")

        ("body-fld-id" "nstring")

        ("body-fld-lang" "nstring" "/" "\"(\"" "string" "*" "(" "SP" "string" ")" "\")\"")

        ("body-fld-loc" "nstring")

        ("body-fld-lines" "number")

        ("body-fld-md5" "nstring")

        ("body-fld-octets" "number")

        ("body-fld-param" "\"(\"" "string" "SP" "string" "*" "(" "SP" "string" "SP" "string" ")" "\")\"" "/" "nil")

        ("body-type-1part" "(" "body-type-basic" "/" "body-type-msg" "/" "body-type-text" ")" "[" "SP" "body-ext-1part" "]")

        ("body-type-basic" "media-basic" "SP" "body-fields")

        ("body-type-mpart" "1" "*" "body" "SP" "media-subtype" "[" "SP" "body-ext-mpart" "]")

        ("body-type-msg" "media-message" "SP" "body-fields" "SP" "envelope" "SP" "body" "SP" "body-fld-lines")

        ("body-type-text" "media-text" "SP" "body-fields" "SP" "body-fld-lines")

        ("capability" "(" "\"AUTH=\"" "auth-type" ")" "/" "atom")

        ("capability-data" "\"CAPABILITY\"" "*" "(" "SP" "capability" ")" "SP" "\"IMAP4rev1\"" "*" "(" "SP" "capability" ")")

        ("CHAR8" "%x01-ff")

        ("command" "tag" "SP" "(" "command-any" "/" "command-auth" "/" "command-nonauth" "/" "command-select" ")" "CRLF")

        ("command-any" "\"CAPABILITY\"" "/" "\"LOGOUT\"" "/" "\"NOOP\"" "/" "x-command")

        ("command-auth" "append" "/" "create" "/" "delete" "/" "examine" "/" "list" "/" "lsub" "/" "rename" "/" "select" "/" "status" "/" "subscribe" "/" "unsubscribe")

        ("command-nonauth" "login" "/" "authenticate" "/" "\"STARTTLS\"")

        ("command-select" "\"CHECK\"" "/" "\"CLOSE\"" "/" "\"EXPUNGE\"" "/" "copy" "/" "fetch" "/" "store" "/" "uid" "/" "search")

        ("continue-req" "\"+\"" "SP" "(" "resp-text" "/" "base64" ")" "CRLF")

        ("copy" "\"COPY\"" "SP" "sequence-set" "SP" "mailbox")

        ("create" "\"CREATE\"" "SP" "mailbox")

        ("date" "date-text" "/" "DQUOTE" "date-text" "DQUOTE")

        ("date-day" "1" "*" "2" "DIGIT")

        ("date-day-fixed" "(" "SP" "DIGIT" ")" "/" "2" "DIGIT")

        ("date-month" "\"Jan\"" "/" "\"Feb\"" "/" "\"Mar\"" "/" "\"Apr\"" "/" "\"May\"" "/" "\"Jun\"" "/" "\"Jul\"" "/" "\"Aug\"" "/" "\"Sep\"" "/" "\"Oct\"" "/" "\"Nov\"" "/" "\"Dec\"")

        ("date-text" "date-day" "\"-\"" "date-month" "\"-\"" "date-year")

        ("date-year" "4" "DIGIT")

        ("date-time" "DQUOTE" "date-day-fixed" "\"-\"" "date-month" "\"-\"" "date-year" "SP" "time" "SP" "zone" "DQUOTE")

        ("delete" "\"DELETE\"" "SP" "mailbox")

        ("digit-nz" "%x31-39")

        ("envelope" "\"(\"" "env-date" "SP" "env-subject" "SP" "env-from" "SP" "env-sender" "SP" "env-reply-to" "SP" "env-to" "SP" "env-cc" "SP" "env-bcc" "SP" "env-in-reply-to" "SP" "env-message-id" "\")\"")

        ("env-bcc" "\"(\"" "1" "*" "address" "\")\"" "/" "nil")

        ("env-cc" "\"(\"" "1" "*" "address" "\")\"" "/" "nil")

        ("env-date" "nstring")

        ("env-from" "\"(\"" "1" "*" "address" "\")\"" "/" "nil")

        ("env-in-reply-to" "nstring")

        ("env-message-id" "nstring")

        ("env-reply-to" "\"(\"" "1" "*" "address" "\")\"" "/" "nil")

        ("env-sender" "\"(\"" "1" "*" "address" "\")\"" "/" "nil")

        ("env-subject" "nstring")

        ("env-to" "\"(\"" "1" "*" "address" "\")\"" "/" "nil")

        ("examine" "\"EXAMINE\"" "SP" "mailbox")

        ("fetch" "\"FETCH\"" "SP" "sequence-set" "SP" "(" "\"ALL\"" "/" "\"FULL\"" "/" "\"FAST\"" "/" "fetch-att" "/" "\"(\"" "fetch-att" "*" "(" "SP" "fetch-att" ")" "\")\"" ")")

        ("fetch-att" "\"ENVELOPE\"" "/" "\"FLAGS\"" "/" "\"INTERNALDATE\"" "/" "\"RFC822\"" "[" "\".HEADER\"" "/" "\".SIZE\"" "/" "\".TEXT\"" "]" "/" "\"BODY\"" "[" "\"STRUCTURE\"" "]" "/" "\"UID\"" "/" "\"BODY\"" "section" "[" "\"<\"" "number" "\".\"" "nz-number" "\">\"" "]" "/" "\"BODY.PEEK\"" "section" "[" "\"<\"" "number" "\".\"" "nz-number" "\">\"" "]")

        ("flag" "\"\\Answered\"" "/" "\"\\Flagged\"" "/" "\"\\Deleted\"" "/" "\"\\Seen\"" "/" "\"\\Draft\"" "/" "flag-keyword" "/" "flag-extension")

        ("flag-extension" "\"\\\"" "atom")

        ("flag-fetch" "flag" "/" "\"\\Recent\"")

        ("flag-keyword" "atom")

        ("flag-list" "\"(\"" "[" "flag" "*" "(" "SP" "flag" ")" "]" "\")\"")

        ("flag-perm" "flag" "/" "\"\\*\"")

        ("greeting" "\"*\"" "SP" "(" "resp-cond-auth" "/" "resp-cond-bye" ")" "CRLF")

        ("header-fld-name" "astring")

        ("header-list" "\"(\"" "header-fld-name" "*" "(" "SP" "header-fld-name" ")" "\")\"")

        ("list" "\"LIST\"" "SP" "mailbox" "SP" "list-mailbox")

        ("list-mailbox" "1" "*" "list-char" "/" "string")

        ("list-char" "ATOM-CHAR" "/" "list-wildcards" "/" "resp-specials")

        ("list-wildcards" "\"%\"" "/" "\"*\"")

        ("literal" "\"{\"" "number" "\"}\"" "CRLF" "*" "CHAR8")

        ("login" "\"LOGIN\"" "SP" "userid" "SP" "password")

        ("lsub" "\"LSUB\"" "SP" "mailbox" "SP" "list-mailbox")

        ("mailbox" "\"INBOX\"" "/" "astring")

        ("mailbox-data" "\"FLAGS\"" "SP" "flag-list" "/" "\"LIST\"" "SP" "mailbox-list" "/" "\"LSUB\"" "SP" "mailbox-list" "/" "\"SEARCH\"" "*" "(" "SP" "nz-number" ")" "/" "\"STATUS\"" "SP" "mailbox" "SP" "\"(\"" "[" "status-att-list" "]" "\")\"" "/" "number" "SP" "\"EXISTS\"" "/" "number" "SP" "\"RECENT\"")

        ("mailbox-list" "\"(\"" "[" "mbx-list-flags" "]" "\")\"" "SP" "(" "DQUOTE" "QUOTED-CHAR" "DQUOTE" "/" "nil" ")" "SP" "mailbox")

        ("mbx-list-flags" "*" "(" "mbx-list-oflag" "SP" ")" "mbx-list-sflag" "*" "(" "SP" "mbx-list-oflag" ")" "/" "mbx-list-oflag" "*" "(" "SP" "mbx-list-oflag" ")")

        ("mbx-list-oflag" "\"\\Noinferiors\"" "/" "flag-extension")

        ("mbx-list-sflag" "\"\\Noselect\"" "/" "\"\\Marked\"" "/" "\"\\Unmarked\"")

        ("media-basic" "(" "(" "DQUOTE" "(" "\"APPLICATION\"" "/" "\"AUDIO\"" "/" "\"IMAGE\"" "/" "\"MESSAGE\"" "/" "\"VIDEO\"" ")" "DQUOTE" ")" "/" "string" ")" "SP" "media-subtype")

        ("media-message" "DQUOTE" "\"MESSAGE\"" "DQUOTE" "SP" "DQUOTE" "\"RFC822\"" "DQUOTE")

        ("media-subtype" "string")

        ("media-text" "DQUOTE" "\"TEXT\"" "DQUOTE" "SP" "media-subtype")

        ("message-data" "nz-number" "SP" "(" "\"EXPUNGE\"" "/" "(" "\"FETCH\"" "SP" "msg-att" ")" ")")

        ("msg-att" "\"(\"" "(" "msg-att-dynamic" "/" "msg-att-static" ")" "*" "(" "SP" "(" "msg-att-dynamic" "/" "msg-att-static" ")" ")" "\")\"")

        ("msg-att-dynamic" "\"FLAGS\"" "SP" "\"(\"" "[" "flag-fetch" "*" "(" "SP" "flag-fetch" ")" "]" "\")\"")

        ("msg-att-static" "\"ENVELOPE\"" "SP" "envelope" "/" "\"INTERNALDATE\"" "SP" "date-time" "/" "\"RFC822\"" "[" "\".HEADER\"" "/" "\".TEXT\"" "]" "SP" "nstring" "/" "\"RFC822.SIZE\"" "SP" "number" "/" "\"BODY\"" "[" "\"STRUCTURE\"" "]" "SP" "body" "/" "\"BODY\"" "section" "[" "\"<\"" "number" "\">\"" "]" "SP" "nstring" "/" "\"UID\"" "SP" "uniqueid")

        ("nil" "\"NIL\"")

        ("nstring" "string" "/" "nil")

        ("number" "1" "*" "DIGIT")

        ("nz-number" "digit-nz" "*" "DIGIT")

        ("password" "astring")

        ("quoted" "DQUOTE" "*" "QUOTED-CHAR" "DQUOTE")

        ("QUOTED-CHAR" "<any TEXT-CHAR except quoted-specials>" "/" "\"\\\"" "quoted-specials")

        ("quoted-specials" "DQUOTE" "/" "\"\\\"")

        ("rename" "\"RENAME\"" "SP" "mailbox" "SP" "mailbox")

        ("response" "*" "(" "continue-req" "/" "response-data" ")" "response-done")

        ("response-data" "\"*\"" "SP" "(" "resp-cond-state" "/" "resp-cond-bye" "/" "mailbox-data" "/" "message-data" "/" "capability-data" ")" "CRLF")

        ("response-done" "response-tagged" "/" "response-fatal")

        ("response-fatal" "\"*\"" "SP" "resp-cond-bye" "CRLF")

        ("response-tagged" "tag" "SP" "resp-cond-state" "CRLF")

        ("resp-cond-auth" "(" "\"OK\"" "/" "\"PREAUTH\"" ")" "SP" "resp-text")

        ("resp-cond-bye" "\"BYE\"" "SP" "resp-text")

        ("resp-cond-state" "(" "\"OK\"" "/" "\"NO\"" "/" "\"BAD\"" ")" "SP" "resp-text")

        ("resp-specials" "\"]\"")

        ("resp-text" "[" "\"[\"" "resp-text-code" "\"]\"" "SP" "]" "text")

        ("resp-text-code" "\"ALERT\"" "/" "\"BADCHARSET\"" "[" "SP" "\"(\"" "astring" "*" "(" "SP" "astring" ")" "\")\"" "]" "/" "capability-data" "/" "\"PARSE\"" "/" "\"PERMANENTFLAGS\"" "SP" "\"(\"" "[" "flag-perm" "*" "(" "SP" "flag-perm" ")" "]" "\")\"" "/" "\"READ-ONLY\"" "/" "\"READ-WRITE\"" "/" "\"TRYCREATE\"" "/" "\"UIDNEXT\"" "SP" "nz-number" "/" "\"UIDVALIDITY\"" "SP" "nz-number" "/" "\"UNSEEN\"" "SP" "nz-number" "/" "atom" "[" "SP" "1" "*" "<any TEXT-CHAR except \"]\">" "]")

        ("search" "\"SEARCH\"" "[" "SP" "\"CHARSET\"" "SP" "astring" "]" "1" "*" "(" "SP" "search-key" ")")

        ("search-key" "\"ALL\"" "/" "\"ANSWERED\"" "/" "\"BCC\"" "SP" "astring" "/" "\"BEFORE\"" "SP" "date" "/" "\"BODY\"" "SP" "astring" "/" "\"CC\"" "SP" "astring" "/" "\"DELETED\"" "/" "\"FLAGGED\"" "/" "\"FROM\"" "SP" "astring" "/" "\"KEYWORD\"" "SP" "flag-keyword" "/" "\"NEW\"" "/" "\"OLD\"" "/" "\"ON\"" "SP" "date" "/" "\"RECENT\"" "/" "\"SEEN\"" "/" "\"SINCE\"" "SP" "date" "/" "\"SUBJECT\"" "SP" "astring" "/" "\"TEXT\"" "SP" "astring" "/" "\"TO\"" "SP" "astring" "/" "\"UNANSWERED\"" "/" "\"UNDELETED\"" "/" "\"UNFLAGGED\"" "/" "\"UNKEYWORD\"" "SP" "flag-keyword" "/" "\"UNSEEN\"" "/" "\"DRAFT\"" "/" "\"HEADER\"" "SP" "header-fld-name" "SP" "astring" "/" "\"LARGER\"" "SP" "number" "/" "\"NOT\"" "SP" "search-key" "/" "\"OR\"" "SP" "search-key" "SP" "search-key" "/" "\"SENTBEFORE\"" "SP" "date" "/" "\"SENTON\"" "SP" "date" "/" "\"SENTSINCE\"" "SP" "date" "/" "\"SMALLER\"" "SP" "number" "/" "\"UID\"" "SP" "sequence-set" "/" "\"UNDRAFT\"" "/" "sequence-set" "/" "\"(\"" "search-key" "*" "(" "SP" "search-key" ")" "\")\"")

        ("section" "\"[\"" "[" "section-spec" "]" "\"]\"")

        ("section-msgtext" "\"HEADER\"" "/" "\"HEADER.FIELDS\"" "[" "\".NOT\"" "]" "SP" "header-list" "/" "\"TEXT\"")

        ("section-part" "nz-number" "*" "(" "\".\"" "nz-number" ")")

        ("section-spec" "section-msgtext" "/" "(" "section-part" "[" "\".\"" "section-text" "]" ")")

        ("section-text" "section-msgtext" "/" "\"MIME\"")

        ("select" "\"SELECT\"" "SP" "mailbox")

        ("seq-number" "nz-number" "/" "\"*\"")

        ("seq-range" "seq-number" "\":\"" "seq-number")

        ("sequence-set" "(" "seq-number" "/" "seq-range" ")" "*" "(" "\",\"" "sequence-set" ")")

        ("status" "\"STATUS\"" "SP" "mailbox" "SP" "\"(\"" "status-att" "*" "(" "SP" "status-att" ")" "\")\"")

        ("status-att" "\"MESSAGES\"" "/" "\"RECENT\"" "/" "\"UIDNEXT\"" "/" "\"UIDVALIDITY\"" "/" "\"UNSEEN\"")

        ("status-att-list" "status-att" "SP" "number" "*" "(" "SP" "status-att" "SP" "number" ")")

        ("store" "\"STORE\"" "SP" "sequence-set" "SP" "store-att-flags")

        ("store-att-flags" "(" "[" "\"+\"" "/" "\"-\"" "]" "\"FLAGS\"" "[" "\".SILENT\"" "]" ")" "SP" "(" "flag-list" "/" "(" "flag" "*" "(" "SP" "flag" ")" ")" ")")

        ("string" "quoted" "/" "literal")

        ("subscribe" "\"SUBSCRIBE\"" "SP" "mailbox")

        ("tag" "1" "*" "<any ASTRING-CHAR except \"+\">")

        ("text" "1" "*" "TEXT-CHAR")

        ("TEXT-CHAR" "<any CHAR except CR and LF>")

        ("time" "2" "DIGIT" "\":\"" "2" "DIGIT" "\":\"" "2" "DIGIT")

        ("uid" "\"UID\"" "SP" "(" "copy" "/" "fetch" "/" "search" "/" "store" ")")

        ("uniqueid" "nz-number")

        ("unsubscribe" "\"UNSUBSCRIBE\"" "SP" "mailbox")

        ("userid" "astring")

        ("x-command" "\"X\"" "atom" "<experimental command arguments>")

        ("zone" "(" "\"+\"" "/" "\"-\"" ")" "4" "DIGIT")))

(setq imap-grammar
      '(("zone"
         (:
          (or "\"+\"" "\"-\"")
          "4" "DIGIT"))
        ("x-command"
         (: "\"X\"" "atom" "<experimental command arguments>"))
        ("userid" "astring")
        ("unsubscribe"
         (: "\"UNSUBSCRIBE\"" "SP" "mailbox"))
        ("uniqueid" "nz-number")
        ("uid"
         (: "\"UID\"" "SP"
            (or "copy" "fetch" "search" "store")))
        ("time"
         (: "2" "DIGIT" "\":\"" "2" "DIGIT" "\":\"" "2" "DIGIT"))
        ("TEXT-CHAR" "<any CHAR except CR and LF>")
        ("text"
         (: "1"
            (* "TEXT-CHAR")))
        ("tag" "<any ASTRING-CHAR except \"+\">")
        ("subscribe"
         (: "\"SUBSCRIBE\"" "SP" "mailbox"))
        ("string"
         (or "quoted" "literal"))
        ("store-att-flags"
         (:
          (:
           (maybe
            (or "\"+\"" "\"-\""))
           "\"FLAGS\""
           (maybe "\".SILENT\""))
          "SP"
          (or "flag-list"
              (: "flag"
                 (*
                  (: "SP" "flag"))))))
        ("store"
         (: "\"STORE\"" "SP" "sequence-set" "SP" "store-att-flags"))
        ("status-att-list"
         (: "status-att" "SP" "number"
            (*
             (: "SP" "status-att" "SP" "number"))))
        ("status-att"
         (or "\"MESSAGES\"" "\"RECENT\"" "\"UIDNEXT\"" "\"UIDVALIDITY\"" "\"UNSEEN\""))
        ("status"
         (: "\"STATUS\"" "SP" "mailbox" "SP" "\"(\"" "status-att"
            (*
             (: "SP" "status-att"))
            "\")\""))
        ("sequence-set"
         (:
          (or "seq-range" "seq-number")
          (*
           (: "\",\"" "sequence-set"))))
        ("seq-range"
         (: "seq-number" "\":\"" "seq-number"))
        ("seq-number"
         (or "nz-number" "\"*\""))
        ("select"
         (: "\"SELECT\"" "SP" "mailbox"))
        ("section-text"
         (or "section-msgtext" "\"MIME\""))
        ("section-spec"
         (or "section-msgtext"
             (: "section-part"
                (maybe
                 (: "\".\"" "section-text")))))
        ("section-part"
         (: "nz-number"
            (*
             (: "\".\"" "nz-number"))))
        ("section-msgtext"
         (or "\"HEADER\""
             (: "\"HEADER.FIELDS\""
                (maybe "\".NOT\"")
                "SP" "header-list")
             "\"TEXT\""))
        ("section"
         (: "\"[\""
            (maybe "section-spec")
            "\"]\""))
        ("search-key"
         (or "\"ALL\"" "\"ANSWERED\""
             (: "\"BCC\"" "SP" "astring")
             (: "\"BEFORE\"" "SP" "date")
             (: "\"BODY\"" "SP" "astring")
             (: "\"CC\"" "SP" "astring")
             "\"DELETED\"" "\"FLAGGED\""
             (: "\"FROM\"" "SP" "astring")
             (: "\"KEYWORD\"" "SP" "flag-keyword")
             "\"NEW\"" "\"OLD\""
             (: "\"ON\"" "SP" "date")
             "\"RECENT\"" "\"SEEN\""
             (: "\"SINCE\"" "SP" "date")
             (: "\"SUBJECT\"" "SP" "astring")
             (: "\"TEXT\"" "SP" "astring")
             (: "\"TO\"" "SP" "astring")
             "\"UNANSWERED\"" "\"UNDELETED\"" "\"UNFLAGGED\""
             (: "\"UNKEYWORD\"" "SP" "flag-keyword")
             "\"UNSEEN\"" "\"DRAFT\""
             (: "\"HEADER\"" "SP" "header-fld-name" "SP" "astring")
             (: "\"LARGER\"" "SP" "number")
             (: "\"NOT\"" "SP" "search-key")
             (: "\"OR\"" "SP" "search-key" "SP" "search-key")
             (: "\"SENTBEFORE\"" "SP" "date")
             (: "\"SENTON\"" "SP" "date")
             (: "\"SENTSINCE\"" "SP" "date")
             (: "\"SMALLER\"" "SP" "number")
             (: "\"UID\"" "SP" "sequence-set")
             "\"UNDRAFT\"" "sequence-set"
             (: "\"(\"" "search-key"
                (*
                 (: "SP" "search-key"))
                "\")\"")))
        ("search"
         (: "\"SEARCH\""
            (maybe
             (: "SP" "\"CHARSET\"" "SP" "astring"))
            "1"
            (*
             (: "SP" "search-key"))))
        ("resp-text-code"
         (or "\"ALERT\""
             (: "\"BADCHARSET\""
                (maybe
                 (: "SP" "\"(\"" "astring"
                    (*
                     (: "SP" "astring"))
                    "\")\"")))
             "capability-data" "\"PARSE\""
             (: "\"PERMANENTFLAGS\"" "SP" "\"(\""
                (maybe
                 (: "flag-perm"
                    (*
                     (: "SP" "flag-perm"))))
                "\")\"")
             "\"READ-ONLY\"" "\"READ-WRITE\"" "\"TRYCREATE\""
             (: "\"UIDNEXT\"" "SP" "nz-number")
             (: "\"UIDVALIDITY\"" "SP" "nz-number")
             (: "\"UNSEEN\"" "SP" "nz-number")
             (: "atom"
                (maybe
                 (: "SP" "1"
                    (* "<any TEXT-CHAR except \"]\">"))))))
        ("resp-text"
         (:
          (maybe
           (: "\"[\"" "resp-text-code" "\"]\"" "SP"))
          "text"))
        ("resp-specials" "\"]\"")
        ("resp-cond-state"
         (:
          (or "\"OK\"" "\"NO\"" "\"BAD\"")
          "SP" "resp-text"))
        ("resp-cond-bye"
         (: "\"BYE\"" "SP" "resp-text"))
        ("resp-cond-auth"
         (:
          (or "\"OK\"" "\"PREAUTH\"")
          "SP" "resp-text"))
        ("response-tagged"
         (: "tag" "SP" "resp-cond-state" "CRLF"))
        ("response-fatal"
         (: "\"*\"" "SP" "resp-cond-bye" "CRLF"))
        ("response-done"
         (or "response-tagged" "response-fatal"))
        ("response-data"
         (: "\"*\"" "SP"
            (or "resp-cond-state" "resp-cond-bye" "mailbox-data" "message-data" "capability-data")
            "CRLF"))
        ("response"
         (:
          (*
           (or "continue-req" "response-data"))
          "response-done"))
        ("rename"
         (: "\"RENAME\"" "SP" "mailbox" "SP" "mailbox"))
        ("quoted-specials"
         (or "DQUOTE" "\"\\\""))
        ("QUOTED-CHAR"
         (or "<any TEXT-CHAR except quoted-specials>"
             (: "\"\\\"" "quoted-specials")))
        ("quoted"
         (: "DQUOTE"
            (* "QUOTED-CHAR")
            "DQUOTE"))
        ("password" "astring")
        ("nz-number"
         (: "digit-nz"
            (* "DIGIT")))
        ("number"
         (: "1"
            (* "DIGIT")))
        ("nstring"
         (or "string" "nil"))
        ("nil" "\"NIL\"")
        ("msg-att-static"
         (or
          (: "\"ENVELOPE\"" "SP" "envelope")
          (: "\"INTERNALDATE\"" "SP" "date-time")
          (: "\"RFC822\""
             (maybe
              (or "\".HEADER\"" "\".TEXT\""))
             "SP" "nstring")
          (: "\"RFC822.SIZE\"" "SP" "number")
          (: "\"BODY\""
             (maybe "\"STRUCTURE\"")
             "SP" "body")
          (: "\"BODY\"" "section"
             (maybe
              (: "\"<\"" "number" "\">\""))
             "SP" "nstring")
          (: "\"UID\"" "SP" "uniqueid")))
        ("msg-att-dynamic"
         (: "\"FLAGS\"" "SP" "\"(\""
            (maybe
             (: "flag-fetch"
                (*
                 (: "SP" "flag-fetch"))))
            "\")\""))
        ("msg-att"
         (: "\"(\""
            (or "msg-att-dynamic" "msg-att-static")
            (*
             (: "SP"
                (or "msg-att-dynamic" "msg-att-static")))
            "\")\""))
        ("message-data"
         (: "nz-number" "SP"
            (or "\"EXPUNGE\""
                (: "\"FETCH\"" "SP" "msg-att"))))
        ("media-text"
         (: "DQUOTE" "\"TEXT\"" "DQUOTE" "SP" "media-subtype"))
        ("media-subtype" "string")
        ("media-message"
         (: "DQUOTE" "\"MESSAGE\"" "DQUOTE" "SP" "DQUOTE" "\"RFC822\"" "DQUOTE"))
        ("media-basic"
         (:
          (or
           (: "DQUOTE"
              (or "\"APPLICATION\"" "\"AUDIO\"" "\"IMAGE\"" "\"MESSAGE\"" "\"VIDEO\"")
              "DQUOTE")
           "string")
          "SP" "media-subtype"))
        ("mbx-list-sflag"
         (or "\"\\Noselect\"" "\"\\Marked\"" "\"\\Unmarked\""))
        ("mbx-list-oflag"
         (or "\"\\Noinferiors\"" "flag-extension"))
        ("mbx-list-flags"
         (or
          (:
           (*
            (: "mbx-list-oflag" "SP"))
           "mbx-list-sflag"
           (*
            (: "SP" "mbx-list-oflag")))
          (: "mbx-list-oflag"
             (*
              (: "SP" "mbx-list-oflag")))))
        ("mailbox-list"
         (: "\"(\""
            (maybe "mbx-list-flags")
            "\")\"" "SP"
            (or
             (: "DQUOTE" "QUOTED-CHAR" "DQUOTE")
             "nil")
            "SP" "mailbox"))
        ("mailbox-data"
         (or
          (: "\"FLAGS\"" "SP" "flag-list")
          (: "\"LIST\"" "SP" "mailbox-list")
          (: "\"LSUB\"" "SP" "mailbox-list")
          (: "\"SEARCH\""
             (*
              (: "SP" "nz-number")))
          (: "\"STATUS\"" "SP" "mailbox" "SP" "\"(\""
             (maybe "status-att-list")
             "\")\"")
          (: "number" "SP" "\"EXISTS\"")
          (: "number" "SP" "\"RECENT\"")))
        ("mailbox"
         (or "\"INBOX\"" "astring"))
        ("lsub"
         (: "\"LSUB\"" "SP" "mailbox" "SP" "list-mailbox"))
        ("login"
         (: "\"LOGIN\"" "SP" "userid" "SP" "password"))
        ("literal"
         (: "\"{\"" "number" "\"}\"" "CRLF"
            (* "CHAR8")))
        ("list-wildcards"
         (or "\"%\"" "\"*\""))
        ("list-char"
         (or "ATOM-CHAR" "list-wildcards" "resp-specials"))
        ("list-mailbox"
         (or
          (: "1"
             (* "list-char"))
          "string"))
        ("list"
         (: "\"LIST\"" "SP" "mailbox" "SP" "list-mailbox"))
        ("header-list"
         (: "\"(\"" "header-fld-name"
            (*
             (: "SP" "header-fld-name"))
            "\")\""))
        ("header-fld-name" "astring")
        ("greeting"
         (: "\"*\"" "SP"
            (or "resp-cond-auth" "resp-cond-bye")
            "CRLF"))
        ("flag-perm"
         (or "flag" "\"\\*\""))
        ("flag-list"
         (: "\"(\""
            (maybe
             (: "flag"
                (*
                 (: "SP" "flag"))))
            "\")\""))
        ("flag-keyword" "atom")
        ("flag-fetch"
         (or "flag" "\"\\Recent\""))
        ("flag-extension"
         (: "\"\\\"" "atom"))
        ("flag"
         (or "\"\\Answered\"" "\"\\Flagged\"" "\"\\Deleted\"" "\"\\Seen\"" "\"\\Draft\"" "flag-keyword" "flag-extension"))
        ("fetch-att"
         (or "\"ENVELOPE\"" "\"FLAGS\"" "\"INTERNALDATE\""
             (: "\"RFC822\""
                (maybe
                 (or "\".HEADER\"" "\".SIZE\"" "\".TEXT\"")))
             (: "\"BODY\""
                (maybe "\"STRUCTURE\""))
             "\"UID\""
             (: "\"BODY\"" "section"
                (maybe
                 (: "\"<\"" "number" "\".\"" "nz-number" "\">\"")))
             (: "\"BODY.PEEK\"" "section"
                (maybe
                 (: "\"<\"" "number" "\".\"" "nz-number" "\">\"")))))
        ("fetch"
         (: "\"FETCH\"" "SP" "sequence-set" "SP"
            (or "\"ALL\"" "\"FULL\"" "\"FAST\"" "fetch-att"
                (: "\"(\"" "fetch-att"
                   (*
                    (: "SP" "fetch-att"))
                   "\")\""))))
        ("examine"
         (: "\"EXAMINE\"" "SP" "mailbox"))
        ("env-to"
         (or
          (: "\"(\"" "1"
             (* "address")
             "\")\"")
          "nil"))
        ("env-subject" "nstring")
        ("env-sender"
         (or
          (: "\"(\"" "1"
             (* "address")
             "\")\"")
          "nil"))
        ("env-reply-to"
         (or
          (: "\"(\"" "1"
             (* "address")
             "\")\"")
          "nil"))
        ("env-message-id" "nstring")
        ("env-in-reply-to" "nstring")
        ("env-from"
         (or
          (: "\"(\"" "1"
             (* "address")
             "\")\"")
          "nil"))
        ("env-date" "nstring")
        ("env-cc"
         (or
          (: "\"(\"" "1"
             (* "address")
             "\")\"")
          "nil"))
        ("env-bcc"
         (or
          (: "\"(\"" "1"
             (* "address")
             "\")\"")
          "nil"))
        ("envelope"
         (: "\"(\"" "env-date" "SP" "env-subject" "SP" "env-from" "SP" "env-sender" "SP" "env-reply-to" "SP" "env-to" "SP" "env-cc" "SP" "env-bcc" "SP" "env-in-reply-to" "SP" "env-message-id" "\")\""))
        ("digit-nz" "%x31-39")
        ("delete"
         (: "\"DELETE\"" "SP" "mailbox"))
        ("date-time"
         (: "DQUOTE" "date-day-fixed" "\"-\"" "date-month" "\"-\"" "date-year" "SP" "time" "SP" "zone" "DQUOTE"))
        ("date-year"
         (: "4" "DIGIT"))
        ("date-text"
         (: "date-day" "\"-\"" "date-month" "\"-\"" "date-year"))
        ("date-month"
         (or "\"Jan\"" "\"Feb\"" "\"Mar\"" "\"Apr\"" "\"May\"" "\"Jun\"" "\"Jul\"" "\"Aug\"" "\"Sep\"" "\"Oct\"" "\"Nov\"" "\"Dec\""))
        ("date-day-fixed"
         (or
          (: "SP" "DIGIT")
          (: "2" "DIGIT")))
        ("date-day"
         (: "1"
            (* "2")
            "DIGIT"))
        ("date"
         (or "date-text"
             (: "DQUOTE" "date-text" "DQUOTE")))
        ("create"
         (: "\"CREATE\"" "SP" "mailbox"))
        ("copy"
         (: "\"COPY\"" "SP" "sequence-set" "SP" "mailbox"))
        ("continue-req"
         (: "\"+\"" "SP"
            (or "resp-text" "base64")
            "CRLF"))
        ("command-select"
         (or "\"CHECK\"" "\"CLOSE\"" "\"EXPUNGE\"" "copy" "fetch" "store" "uid" "search"))
        ("command-nonauth"
         (or "login" "authenticate" "\"STARTTLS\""))
        ("command-auth"
         (or "append" "create" "delete" "examine" "list" "lsub" "rename" "select" "status" "subscribe" "unsubscribe"))
        ("command-any"
         (or "\"CAPABILITY\"" "\"LOGOUT\"" "\"NOOP\"" "x-command"))
        ("command"
         (: "tag" "SP"
            (or "command-any" "command-auth" "command-nonauth" "command-select")
            "CRLF"))
        ("CHAR8" "%x01-ff")
        ("capability-data"
         (: "\"CAPABILITY\""
            (*
             (: "SP" "capability"))
            "SP" "\"IMAP4rev1\""
            (*
             (: "SP" "capability"))))
        ("capability"
         (or
          (: "\"AUTH=\"" "auth-type")
          "atom"))
        ("body-type-text"
         (: "media-text" "SP" "body-fields" "SP" "body-fld-lines"))
        ("body-type-msg"
         (: "media-message" "SP" "body-fields" "SP" "envelope" "SP" "body" "SP" "body-fld-lines"))
        ("body-type-mpart"
         (: "1"
            (* "body")
            "SP" "media-subtype"
            (maybe
             (: "SP" "body-ext-mpart"))))
        ("body-type-basic"
         (: "media-basic" "SP" "body-fields"))
        ("body-type-1part"
         (:
          (or "body-type-basic" "body-type-msg" "body-type-text")
          (maybe
           (: "SP" "body-ext-1part"))))
        ("body-fld-param"
         (or
          (: "\"(\"" "string" "SP" "string"
             (*
              (: "SP" "string" "SP" "string"))
             "\")\"")
          "nil"))
        ("body-fld-octets" "number")
        ("body-fld-md5" "nstring")
        ("body-fld-lines" "number")
        ("body-fld-loc" "nstring")
        ("body-fld-lang"
         (or "nstring"
             (: "\"(\"" "string"
                (*
                 (: "SP" "string"))
                "\")\"")))
        ("body-fld-id" "nstring")
        ("body-fld-enc"
         (or
          (: "DQUOTE"
             (or "\"7BIT\"" "\"8BIT\"" "\"BINARY\"" "\"BASE64\"" "\"QUOTED-PRINTABLE\"")
             "DQUOTE")
          "string"))
        ("body-fld-dsp"
         (or
          (: "\"(\"" "string" "SP" "body-fld-param" "\")\"")
          "nil"))
        ("body-fld-desc" "nstring")
        ("body-fields"
         (: "body-fld-param" "SP" "body-fld-id" "SP" "body-fld-desc" "SP" "body-fld-enc" "SP" "body-fld-octets"))
        ("body-ext-mpart"
         (: "body-fld-param"
            (maybe
             (: "SP" "body-fld-dsp"
                (maybe
                 (: "SP" "body-fld-lang"
                    (maybe
                     (: "SP" "body-fld-loc"
                        (*
                         (: "SP" "body-extension"))))))))))
        ("body-ext-1part"
         (: "body-fld-md5"
            (maybe
             (: "SP" "body-fld-dsp"
                (maybe
                 (: "SP" "body-fld-lang"
                    (maybe
                     (: "SP" "body-fld-loc"
                        (*
                         (: "SP" "body-extension"))))))))))
        ("body-extension"
         (or "nstring" "number"
             (: "\"(\"" "body-extension"
                (*
                 (: "SP" "body-extension"))
                "\")\"")))
        ("body"
         (: "\"(\""
            (or "body-type-1part" "body-type-mpart")
            "\")\""))
        ("base64-terminal"
         (or
          (: "2" "base64-char" "\"==\"")
          (: "3" "base64-char" "\"=\"")))
        ("base64-char"
         (or "ALPHA" "DIGIT" "\"+\"" "\"/\""))
        ("base64"
         (:
          (*
           (: "4" "base64-char"))
          (maybe "base64-terminal")))
        ("auth-type" "atom")
        ("authenticate"
         (: "\"AUTHENTICATE\"" "SP" "auth-type"
            (*
             (: "CRLF" "base64"))))
        ("atom-specials"
         (or "\"(\"" "\")\"" "\"{\"" "SP" "CTL" "list-wildcards" "quoted-specials" "resp-specials"))
        ("ATOM-CHAR" "<any CHAR except atom-specials>")
        ("atom"
         (: "1"
            (* "ATOM-CHAR")))
        ("ASTRING-CHAR"
         (or "ATOM-CHAR" "resp-specials"))
        ("astring"
         (or
          (: "1"
             (* "ASTRING-CHAR"))
          "string"))
        ("append"
         (: "\"APPEND\"" "SP" "mailbox"
            (maybe
             (: "SP" "flag-list"))
            (maybe
             (: "SP" "date-time"))
            "SP" "literal"))
        ("addr-name" "nstring")
        ("addr-mailbox" "nstring")
        ("addr-host" "nstring")
        ("addr-adl" "nstring")
        ("address"
         (: "\"(\"" "addr-name" "SP" "addr-adl" "SP" "addr-mailbox" "SP" "addr-host" "\")\""))))