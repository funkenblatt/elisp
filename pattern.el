;; Runtime destructuring function.
(defun matchwad (pattern data)
  (cond
   ((vectorp pattern)
    (apply 'append
           (mapcar*
            (lambda (elem ix)
              (matchwad (aref pattern ix) elem))
            data (range (length data)))))
   ((consp pattern)
    (append (matchwad (car pattern) (car data))
	    (matchwad (cdr pattern) (cdr data))))
   ((not pattern) nil)
   ((symbolp pattern)
    (list (list pattern data)))))

;; Compile-time matching expression
;; generator functions.
(setq match-functions
      (dict
       'vector 'match-vector
       'cons 'match-cons
       'symbol 'match-symbol))

(defun match-symbol (pattern invar body else)
  (if (startswith (symbol-name pattern) "!")
      `(let ((,pattern ,invar))
         ,body)
    `(if (equal ',pattern ,invar)
         ,body)))

(defun match-it (pattern invar body else)
  (do-wad 
   (gethash (type-of pattern) match-functions)
   (if _ 
       (funcall _ pattern invar body else)
     `(if (equal ,pattern ,invar)
          ,body))))

(defun match-cons (pattern invar body else)
  (if (proper-list? pattern)
      `(if (equal (proper-list? ,invar) ,(length pattern))
           ,(match-list pattern invar body else 0))
    (match-improper pattern invar body else)))

(defun match-index-to-expr (invar index)
  (case index
    (0 `(car ,invar))
    (1 `(cadr ,invar))
    (2 `(caddr ,invar))
    (3 `(cadddr ,invar))
    (otherwise `(nth ,index ,invar ))))

(defun match-improper (pattern invar body else)
  (if (consp pattern)
      `(if (consp ,invar)
           ,(match-it (car pattern)
                      `(car ,invar)
                      (match-it (cdr pattern)
                                `(cdr ,invar) body else)
                      else))
    (match-it pattern invar body else)))

(defun match-list (pattern invar body else index)
  (if (not pattern)
      body
    (match-it (car pattern)
              (match-index-to-expr invar index)
              (match-list (cdr pattern)
                          invar
                          body else
                          (+ index 1))
              else)))

(defun match-vector (pattern invar body else)
  (match-vec-recurse pattern invar body 0))

(defun match-vec-recurse (pattern invar body index)
  (if (= index (length pattern))
      body
    (match-it (aref pattern index)
              `(aref ,invar ,index)
              (match-vec-recurse
               pattern invar body (+ index 1))
              nil)))

(defun pmatch-recurse (invar clauses)
  (if clauses
      (let: clause (head clauses 2) in
        (w/gensyms (tmp)
          `(iflet ,tmp ,(match-it (car clause)
                                  invar
                                  `(vector ,(cadr clause))
                                  nil)
                  (aref ,tmp 0)
                  ,(pmatch-recurse invar (cddr clauses)))))
    nil))

(defmacro pmatch (expr &rest clauses)
  (w/gensyms (invar)
    `(let: ,invar ,expr in
       ,(pmatch-recurse invar clauses))))

(put 'pmatch 'lisp-indent-function 1)

(provide 'pattern)
