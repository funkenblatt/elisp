(require 'clojure-hacks)

(jason-defkeys 
 cider-mode-map 
 (kbd "M-.") nil
 (kbd "M-,") nil
 (kbd "C-c C-v") nil
 (kbd "C-c C-s") 'cider-jump-to-var)

(jason-defkeys 
 cider-repl-mode-map 
 (kbd "M-.") nil
 (kbd "M-,") nil
 (kbd "C-c C-s") 'cider-jump-to-var)

(with-current-buffer (get-buffer-create "cljwad")
  (clojure-mode))

(provide 'cider-hacks)
