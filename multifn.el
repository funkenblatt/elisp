;; -*- lexical-binding: t; -*-
(defvar multifn-hierarchy-version 0)

(defun parents (sym)
  (get sym 'parents))

(defsetf parents (sym) (v) `(put ,sym 'parents (delete-duplicates ,v)))

(defun derives-from (tag parent)
  (incf multifn-hierarchy-version)
  (push parent (parents tag)))

(defun underive (tag parent)
  (setf (parents tag)
        (remove parent (parents tag))))

(defun is-a (child parent)
  (cond
   ((and (vectorp child) (vectorp parent))
    (every 'identity  (mapcar* 'is-a child parent)))
   ((and (symbolp child) (symbolp parent))
    (if (eq child parent)
        parent
      (let: p (parents child) in
        (find-if (cut is-a <> parent) p))))
   (t nil)))

(defun method-for (dispatch-value method-table)
  (do-wad
   (lc x x (hash->list method-table)
       (is-a dispatch-value (car x)))
   (and _ (max-by _ 'car 'is-a)) cdr))

(defun method-maybe-cached (multifn dval)
  (let: cache (get multifn 'method-cache)
        mtab (get multifn 'method-table)
        version (get multifn 'hierarchy-version) in
    ;; Invalidate cache if necessary
    (when (/= version multifn-hierarchy-version)
      (put multifn 'method-cache (make-hash-table :test 'equal))
      (put multifn 'hierarchy-version multifn-hierarchy-version)
      (setf cache (get multifn 'method-cache)))
    (or (gethash dval cache)
        (setf (gethash dval cache) 
              (or (method-for dval mtab)
                  (method-for 'default mtab))))))

(defmacro defmulti (name dispatch)
  `(progn
     (put ',name 'method-table (make-hash-table :test 'equal))
     (put ',name 'method-cache (make-hash-table :test 'equal))
     (put ',name 'hierarchy-version multifn-hierarchy-version)
     (defun ,name (&rest args)
       (let: dispatch-val (apply ,dispatch args)
             method (method-maybe-cached ',name dispatch-val) in
         (cond (method (apply method args))
               ((setf method (method-for 'default (get ',name 'method-table)))
                (apply method args))
               (t (error "No method matching %s" dispatch-val)))))))

(defmacro defmethod (name d-val arglist &rest body)
  `(progn 
     (puthash 
      ,d-val (lambda ,arglist ,@body)
      (get ',name 'method-table))
     (put ',name 'method-cache (make-hash-table :test 'equal))))

(defmacro def-commutative-method (name dispatch-vals arg-list &rest body)
  "Make a commutative (with respect to the dispatch values of its arguments)
method.  Dispatch-vals must be a constant vector of the same number of elements 
as arg-list, and arg-list must be a fixed-arity lambda list."
  (let: indices (vec (range (length dispatch-vals)))
        arg-vec (vec arg-list)
        out-forms nil in
    (do-perms (lambda (ixes parity)
                (push `(defmethod ,name 
                         ,(vec (lc (aref dispatch-vals x) x ixes))
                         ,(lc (aref arg-vec x) x ixes)
                         ,@body)
                      out-forms))
              indices)
    (cons 'progn out-forms)))

(defun undefmethod (name d-val)
  (remhash d-val (get name 'method-table)))

(defun type-wad (x)
  (if (functionp x) 'function (type-of x)))

(defmulti mul-2
  (lambda (a b)
    (vector (type-wad a) (type-wad b))))

(defmethod mul-2 [string integer] (a b)
  (str* a b))

(defmethod mul-2 [integer string] (n s)
  (str* s n))

(def-commutative-method mul-2 [symbol integer] (s n)
  (intern (mul-2 (symbol-name s) n)))

(def-commutative-method mul-2 [cons integer] (l n)
  (mappend (x: l) (range n)))

(def-commutative-method mul-2 [vector number] (v n)
  (let: out (make-vector (length v) nil)
        len (length v) in
    (dotimes (i len) (setf (aref out i) (mul (aref v i) n)))
    out))

(defmethod mul-2 [number number] (a b) (* a b))

;; Dot product
(defmethod mul-2 [vector vector] (a b)
  (reduce '+ (mapcar* '* a b)))

;; Cartesian product
(defmethod mul-2 [cons cons] (a b)
  (cross a b))

(def-commutative-method mul-2 [function number] (f n)
  (lambda (&rest args) (* n (apply f args))))

(defmethod mul-2 [function function] (f g)
  (lambda (&rest args) (* (apply f args) (apply g args))))

(defun mul (&rest args)
  (reduce 'mul-2 args))

(defmulti add (lambda (&rest args) (type-wad (car args))))
(defmethod add 'string (&rest args) (apply 'concat args))
(defmethod add 'vector (&rest args)
  (vec (apply 'mapcar* 'add args)))
(defmethod add 'cons (&rest args) (apply 'append args))
(defmethod add 'default (&rest args)
  (reduce 'add-2 args))

(derives-from 'integer 'number)
(derives-from 'float 'number)

(defmulti add-2
  (lambda (a b)
    (vector (type-wad a) (type-wad b))))

(defmethod add-2 [number number] (a b) (+ a b))

(defmethod add-2 [function function] (f g)
  (lambda (&rest args)
    (add (apply f args) (apply g args))))

(def-commutative-method add-2 [function number] (f n)
  (lambda (&rest args)
    (+ (apply f args) n)))
