(setq objdump "arm-elf-objdump")

(defun obj-size (obj)
  (let: data (with-temp-buffer
	       (call-process objdump nil t nil "--syms" obj)
	       (buffer-string))
	entries (split-string data "\n" t)
	sizes (lc (re-findall (str* "[0-9a-f]" 8) x t) x
		  entries)
	nonzeroes (lc (cadr x) x sizes (and x (not (equal (cadr x) "00000000")))) in
    (zip entries sizes)))


(-
 (+ #x0000001e 
    #x00000048 
    #x00000090 
    #x0000002b 
    #x0000002e 
    #x00000364 
    #x00000005)

 (+ #x0000001a 
    #x0000007c 
    #x0000005c 
    #x0000017c 
    #x00000130 
    #x00000068 
    #x00000044 
    #x00000004 ))

