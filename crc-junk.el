(defun crc16 (s)
  (let ((out #xffff))
    (dostr i s
	   (setq out (logxor out i))
	   (dotimes(j 8)
	     (if (= (logand out 1) 1)
		 (setq out (logxor (lsh out -1) #xa001))
	       (setq out (lsh out -1)))))
    out))

(defun crc16-cl (s)
  (let ((out #xffff))
    (loop for i from 0 to (- (length s) 1) do
	  (setq out (logxor out (char-code (aref s i))))
	  (loop for j from 0 to 7 do
		(setq out (if (= (logand out 1) 1)
			      (logxor (ash out -1) #xa001)
			    (ash out -1)))))
    out))
