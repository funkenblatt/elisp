(defun doc-scrollwad (n)
  `(lambda (&optional evt) 
     (interactive "e") 
     (with-selected-window (posn-window (event-start evt))
       (image-scroll-up ,n))))

(require 'image-drag)

(add-hook 
 'doc-view-mode-hook
 (lambda ()
   (local-set-key (kbd "C-c o") 'pdf-outline)
   (local-set-key [mouse-5] (doc-scrollwad 2))
   (local-set-key [down-mouse-1] 'image-mouse-drag-scroll)
   (local-set-key [mouse-4] (doc-scrollwad -2))))

(add-hook
 'image-mode-hook
 (lambda ()
   (local-set-key [mouse-5] (doc-scrollwad 2))
   (local-set-key [mouse-4] (doc-scrollwad -2))
   (local-set-key [down-mouse-1] 'image-mouse-drag-scroll)
   (local-set-key (kbd "C-c g") 
     (lambda ()
       (interactive)
       (call-process "gimp" nil 0 nil (expand-file-name buffer-file-name))))))

(defun skip-invisible ()
  (while (and (get-text-property (point) 'invisible)
	      (< (point) (point-max)))
    (forward-char)))

(defun hide-suboutlines ()
  (interactive)
  (skip-invisible)
  (let: a (thing-at-point 'line) 
	b nil start nil spaces nil in 
    (string-match "^ *" a)
    (forward-line) 
    (setq start (get-text-property (point) 'invisible))
    (setq b (match-string 0 a))
    (setq spaces (length b))
    (while (and (< (point) (point-max))
		(string-match (str "^" b " +") (thing-at-point 'line)))
      (put-text-property (point) (progn (forward-line) (point))
			 'invisible
			 (if (= (match-end 0) (+ spaces 2))
			     (not start)
			   t)))))

(provide 'doc-mods)
