;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.
(process-send-string (get-process "shell") "(send-to-emacs '(ass banditry))\n")


(setq ft-proc (get-process "shell"))
(setq outbuffer nil)
(setq ft-last-buddy nil)

(defun ft-names ()  
  (setq outbuffer (current-buffer))
  (process-send-string 
   ft-proc
   "(send-to-emacs `(ft-namewads ,@(map car (filter (nthfun 1) (ft-get-roster-list)))))\n"))

(defun ft-namewads (&rest names)
  (with-current-buffer outbuffer
    (dump
     "(ft-send-message \""
     (setq ft-last-buddy (completing-read "Buddy: " names nil nil ft-last-buddy))
     "\" "
     (format "%S" (read-from-minibuffer "Msg: "))
     ")")))

(ft-names)

(setq freetalk-handlers '((names . (lambda (obj)
				     (dolist (i (cadr obj))
				       (scratchdump i "\n"))))))
(setq freetalk-accept 'eval)

(setq freetalk-accept
      (lambda (obj)
	(if (and
	     (consp obj)
	     (assq (car obj) freetalk-handlers))
	    (funcall (cdr (assq (car obj) freetalk-handlers)) obj)
	  (pr obj))))
