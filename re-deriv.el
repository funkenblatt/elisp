(require 'pattern)
(require 'deriv)

(defun fold-right (kons knil l)
  (if l (funcall kons (car l)
                 (fold-right kons knil (cdr l)))
    knil))

(defun re-nullable (expr)
  (pmatch expr
    (or . !terms) (if (any (mapcar 're-nullable !terms)) "")
    (* . !terms) ""
    (: . !terms) (if (all (mapcar 're-nullable !terms)) "")
    "" ""
    !x nil))

(defun re-sort (expr)
  (sort-terms expr '(:)))

(defun re-or (&rest exprs)
  (let: remainders (filter 'identity exprs) in
    (cond ((not remainders) nil)
          ((not (cdr remainders)) (car remainders))
          (t (re-sort `(or ,@remainders))))))

(defun re-cat (&rest exprs)
  (let: remainders (lc x x exprs (not (equal x ""))) in
    (cond
     ((not remainders) "")
     ((all remainders) (re-flatten `(: ,@remainders)))
     (t nil))))

(defun re-* (expr)
  (cond ((not expr) nil)
        ((equal expr "") "")
        (t `(* ,expr))))

(defun re-flatten (expr)
  (if (and (consp expr)
           (memq (car expr) '(and or :)))
      (cons (car expr)
            (fold-right
             (lambda (a b)
               (if (and (consp a) (eq (car a) (car expr)))
                   (append (cdr (re-flatten a)) b)
                 (cons (re-flatten a) b)))
             nil
             (cdr expr)))
    expr))

(defun re-deriv (expr c)
  (pmatch expr
    (or . !terms) (apply 're-or (lc (re-deriv x c) x !terms))
    (* !term) (re-cat (re-deriv !term c) (re-* !term))
    (: . !terms) (re-or (apply 're-cat (re-deriv (car !terms) c)
                               (cdr !terms))
                        (if (and (re-nullable (car !terms))
                                 (cdr !terms))
                            (re-deriv `(: ,@(cdr !terms)) c)
                          nil))
    "" nil
    any ""
    !x (cond ((stringp !x) (if (eq (aref !x 0) c)
                               (substring !x 1)
                             nil))
             ((characterp !x) (if (eq !x c) "" nil)))))

(provide 're-deriv)
