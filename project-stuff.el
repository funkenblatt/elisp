;; -*- lexical-binding: t; -*-

(require 'find-stuff)

(setq project-directory default-directory)
(setq projfind-extensions '("c" "h" "cpp" "cc" "scm" "py" "clj" "cljs"))
(setf projfind-exclusions nil)

(defun find-query-extns (extensions)
  (apply 'concat 
	 (join (lc (concat "-name \"*." x "\"") 
		   x extensions)
	       " -or ")))

(defun projfind (s)
  (interactive "sSearch string: ")
  (if (equal s "") (setq s (thing-at-point 'symbol)))
  (when (and current-prefix-arg (get-buffer "*grep*"))
    (do-wad
     (generate-new-buffer "grep-results")
     (progn
       (w/buf "*grep*" (copy-to-buffer _ (point-min) (point-max)))
       (w/buf _ (grep-mode)))))
  (with-default-dir project-directory
    (>>> (get-project-files t)
         (filter (comp 'not 'file-directory-p) _)
         (mapcar (comp 'repr 'substring-no-properties) _)
         (strjoin " " _) (filedump _ "/tmp/projfind-file-list.txt"))
    (grep
     (format 
      "xargs grep -nH %s -e %s < /tmp/projfind-file-list.txt"
      (if (eq current-prefix-arg 2) "-C 3" "")
      (shell-quote-argument s)))))

(defun get-project-files-cps (cont)
  (apply 'jf-proc "find" (lambda (s) (funcall cont (split-string s "\n")))
         "." "-name"
         (ljoin (lc (concat "*." x) x projfind-extensions)
                '("-or" "-name"))))

(defun get-project-files (&optional no-dirs)
  (do-find `(and (or
                  ,@(lc `(and ,x prune) x projfind-exclusions)
                  ,@(unless no-dirs '((and dir (not (or ".*" (path "*/.*"))))))
                  ,@(lc (interp "*.#,x") x projfind-extensions)))))

(defun gen-makefile ()
  "Make a really dumb Makefile."
  (let: c-files (lc x x (get-project-files) (endswith x ".c")) in
    (dolist (i c-files)
      (dump (file-name-nondirectory (substring i 0 -2)) ".o: " i "\n")
      (dump "\t$(CC) " i " -c $(GCC_FLAGS)\n\n"))))

(defun projfiles ()
  (interactive)
  (let: buf (get-buffer-create "*Project Files*") in
    (switch-to-buffer buf)
    (setq buffer-read-only nil)
    (erase-buffer)
    (cd project-directory)
    (dolist (i (get-project-files))
      (insert-text-button
       i 'action
       (lambda (me)
	 (save-excursion
	   (goto-char me)
	   (find-file (thing-at-point 'filename))))
       'follow-link t)
      (dump "\n"))
    (setq buffer-read-only t)
    (use-local-map jason-nav-map)))

(defun find-fun-definition (s)
  (interactive "sSymbol: ")
  (if (equal s "") (setq s (thing-at-point 'symbol)))
  (projfind (format "^\\([a-zA-Z_}].*\\|#define[ \\t]*\\)[^a-zA-Z_]%s[^a-zA-Z_]\\|^%s[^a-zA-Z_]" s s)))

(defun find-struct-def (s)
  (interactive "sSymbol: ")
  (if (equal s "") (setq s (thing-at-point 'symbol)))
  (projfind (format "struct %s[^a-zA-Z)]*$" s)))

(defun find-typedef (s)
  (interactive "sType: ")
  (if (equal s "") (setq s (thing-at-point 'symbol)))
  (projfind (format "[^a-zA-Z_]%s;" s)))

(defun project-search-headers (regexp)
  (interactive "sRegexp: ")
  (with-default-dir project-directory
    (grep (format
	   "grep -nH -e %s `find . -name \"*.h\"`"
	   (shell-quote-argument regexp)))))

(defun do-with-next-key (proc key items &optional done-hook)
  "Calls PROC with elements from the list ITEMS, but waits in between
calls - for the next element of ITEMS to get processed, the user must
hit the key-combination KEY.  In between these items, you can do whatever
editing operations that you want.

When all items have been processed, DONE-HOOK will get called with no
arguments if it has been specified.  Any global binding that existed
previously for KEY will be restored upon reaching the end of the list."
  (lexlet: cb nil 
	   old-binding (lookup-key (current-global-map) key) in
    (global-set-key 
     key (lambda () (interactive) 
	   (if current-prefix-arg
	       (global-set-key key old-binding)
	     (funcall cb nil))))
    (cps-foreach
     (lambda (item cont)
       (funcall proc item)
       (setq cb cont))
     items
     (lambda (x)
       (if done-hook (funcall done-hook))
       (global-set-key key old-binding)))))

(defun projfind-do (regexp)
  (interactive (list (read-regexp "Search regexp")))
  (with-default-dir project-directory
    (let: lines (apply 'process-lines "grep" "-nH" "-e"
		       regexp (get-project-files)) in
      (do-with-next-key
       (lambda (x)
	 (let: parts (split-string x ":" t) in
	   (with-default-dir project-directory (find-file (car parts)))
	   (goto-line (read (cadr parts)))
	   (flash (line-beginning-position) (line-end-position)
		  "#ffff00")))
       (kbd "M-RET") lines (lambda () (message "Done"))))))

(defun set-projfind-extensions (extensions)
  (interactive 
   (do-wad (read-string "Extensions (space separate): "
                        (strjoin " " projfind-extensions))
           split-string list))
  (setq projfind-extensions extensions)
  (with-default-dir project-directory
    (write-region 
     (repr `(setq projfind-extensions ',projfind-extensions))
     nil "proj.el" t)))

(defun find-projfile (&optional open-both)
  (interactive)
  (with-default-dir project-directory
    (>>> (get-project-files) (cons "." _) 
         (ido-completing-read "File: " _)
         (if open-both
             (>>> (str (file-name-directory _) (file-name-base _))
                  expand-file-name
                  (progn 
                    (find-file (str _ ".m"))
                    (find-file-other-window (str _ ".h"))
                    ))
           (find-file _)))))

(setq project-find-map (make-sparse-keymap))

(jason-defkeys project-find-map
  (kbd "f") 'find-fun-definition
  (kbd "s") 'find-struct-def
  (kbd "t") 'find-typedef
  (kbd "h") 'project-search-headers
  (kbd "d") 'projfind-do
  (kbd "p") 'projfind
  (kbd "e") 'set-projfind-extensions
  (kbd "o") 'find-projfile
  (kbd "O") (keycmd (find-projfile t)))

(add-hook 'c-mode-hook
	  (lambda ()
	    (define-key c-mode-map (kbd "C-c f") project-find-map)))
  
(global-set-key [?\M-p] 'projfind)
(global-set-key (kbd "<f5>") (keycmd (with-default-dir project-directory (call-interactively 'compile))))
(global-unset-key (kbd "<f6>"))
(global-unset-key (kbd "<f7>"))
(global-set-key (kbd "C-x f") project-find-map)
