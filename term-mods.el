(add-hook 'term-mode-hook
	  (lambda ()
	    (jason-defkeys 
	     term-raw-map
	     (kbd "S-C-c") 'clipboard-kill-ring-save
	     (kbd "C-SPC") (lambda () (interactive)
			      (term-send-raw-string "\x00")))))

(provide 'term-mods)
