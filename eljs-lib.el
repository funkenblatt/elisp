(defun fetch-url (url method cb data)
  (let ((req (mk-request)))
    (setf req.onload (λ (e) (cb e.target.responseText)))
    (req.open method url true)
    (req.send (or data nil))))

(defun mk-request ()
  (.. Components.classes
      "@mozilla.org/xmlextras/xmlhttprequest;1"
      (.createInstance
       Components.interfaces.nsIXMLHttpRequest)))

(defun min-by (key-fn a)
  (a.reduce (λ (x y) (if (< (key-fn x) (key-fn y)) x y))))

(defun map (fun)
  (let ((arrays (.. arguments to-array (.slice 1)))
        (min-array (min-by (@ length) arrays)))
    (min-array.map 
     (λ (x ix) (fun.apply this (arrays.map (@ [ix])))))))

(defun to-array (array-like)
  (let ((i 0) (out []))
    (for (nil (< i array-like.length) (incf i))
      (out.push (-> array-like [i])))
    out))

(defun kill-self (node)
  (node.parentNode.removeChild node))

(defun prepend-node (parent node)
  (.. parent (.insertBefore node (.. parent .childNodes 0))))

(defun node (x)
  (cond
   ((= (typeof x) 'string) (document.createTextNode x))
   ((= (typeof x) 'number) (document.createTextNode (JSON.stringify x)))
   ((instanceof x Array)
    (let ((el (document.createElement (.. x 0))))
      (x.shift)
      (x.forEach (λ (x) (el.appendChild (node x))))
      el))
   (true x)))

(defun juxt ()
  (let ((args (to-array arguments)))
    (λ (x) (args.map (λ (f) (f x))))))

(defun get-window-mediator ()
  (.. Components.classes "@mozilla.org/appshell/window-mediator;1"
          (.getService Components.interfaces.nsIWindowMediator)))
