(require 'http-client)

(defun http-put (p cont uri body 
                   &optional content-type more-headers method)
  (get-http
   p cont uri
   nil `((Content-length . ,(length body))
         (Content-type . ,(or content-type "application/x-www-form-urlencoded"))
         ,@more-headers)
   (or method "PUT") body))

(defun* couch-req (path-components cont &key data method headers url-params)
  (pr data)
  (http-put
   (connect "localhost" 5984)
   cont
   (str "/" (strjoin "/" path-components)
        (if url-params (str "?" (urlencode-alist url-params)) ""))
   (or (and data (json-encode data)) "")
   (and data "application/json")
   headers method))

(defun couch-new-doc (db data &optional key)
  (http-put
   (connect "localhost" 5984)
   'dump
   (str "/" db)
   (json-encode
    (if key (cons (cons "_id" key) data) data))
   "application/json" nil "POST"))

(defun couch-new-design (db name data)
  (http-put
   (connect "localhost" 5984)
   'dump
   (str "/" db "/_design/" name)
   (json-encode data)
   "application/json" nil "POST"))


(provide 'couch)
