;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(defmacro time (&rest body)
  (let: start (gensym) in
    `(let ((,start (current-time)))
       (with-output-to-temp-buffer "timing"
	 (pr (funcall (byte-compile (lambda () ,@body)))
	     "\n"
	     (time-subtract (current-time) ,start))))))

(time (dotimes (i 10000000) ))


(defun word-split (word n)
  (list (substring word 0 n)
	(substring word n)))

(defun word-splits (word) (lc (word-split word n) n (range (+ (length word) 1))))

(word-splits "foobite")
(("" "foobite") ("f" "oobite") ("fo" "obite") ("foo" "bite") ("foob" "ite") ("foobi" "te") ("foobit" "e") ("foobite" ""))

(defalias :: 'substring)

(str (:: word 0 i) c (:: word i)) ;edits
(str (:: word 0 i) c (:: word (+ i 1))) ;subs
(str (:: word 0 i) (:: word (+ i 1)))   ;deletions
(str (:: word 0 i) (aref word (+ i 1)) (aref word i) (:: word (+ i 2))) ;transposes

(require 'infix)
infix

(defmacro LC (expr var lexpr &optional pred)
  (let: lexpr (if pred `(filter (lambda! (,var) ,pred) ,lexpr) lexpr) in
    `(mapcar (lambda! (,var) ,expr) ,lexpr)))


(defun empty-p (s) (= (length s) 0))

(defun edits (word)
  (let: parts (lc (list (substring word 0 i) (substring word i)) i (range (+ (length word) 1)))
	deletions (LC (str a (substring b 1)) (a b) parts (not (empty-p b)))
	transposes (LC (str* a (aref b 1) (aref b 0) (substring b 2)) (a b) parts (>= (length b) 2)) in
    (list deletions transposes)))


(LC (cons i j) (i j) (zip (range 5) (range 5 10)))



(lsplit '(lc (/ (+ x y) 8 z) (x y) blah z (range 10)) 'if)

(defmacro ec2 (expr &rest clauses)
  (let: stuff (lsplit clauses 'if)
	seqs (car stuff)
	pred (cadr stuff) in
    (pr seqs "\n")
    (if (cddr seqs)
	`(mapc (lambda! (,(car seqs))
	         (ec2 ,expr ,@(cddr seqs) ,@(cons 'if pred)))
	       ,(cadr seqs))
      `(mapc (lambda! (,(car seqs))
	       ,expr)
	     ,(if pred
		  `(filter (lambda! (,(car seqs))
			     ,(car pred))
			   ,(cadr seqs))
		(cadr seqs))))))
