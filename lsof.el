(defvar service-map 
  (do-wad (lc (split-string x) x (file-lines "/etc/services")
              (not (startswith x "#")))
          (index-by _ 'car)))

(defun do-network-shit ()
  (let: service-rx (rx ":" (group (+ (any digit (?a . ?z)))) 
                       (or "->" eos " ("))
        lines (process-lines "lsof" "-i") in
    (mapcar (comp 'split-string
                  (cut replace-regexp-in-string service-rx
                       (x: (or (cadr (gethash (match-string 1 x) service-map)) 
                               (match-string 1 x)))
                       <> t t 1))
            lines)))

(provide 'lsof)
