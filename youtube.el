;; -*- lexical-binding: t; -*-

(defun jf-proc-comint (name program cont &rest args)
  (let: buf (apply 'make-comint name program nil args) in
    (set-process-sentinel (get-buffer-process buf)
      (lambda (p s)
        (when (string-match "finished\\|exited" s)
          (if cont (funcall cont s)))))))

(defun audio-codec-ext (info)
  (let: codec (str~ (rx "Audio: " (group (+ (not (any "," space))))) info 1) in
    (assoc-default 
     codec
     '(("vorbis" . "ogg")
       ("opus" . "ogg")
       ("aac" . "aac"))
     nil codec)))

(defun get-youtube (name url output)
  (interactive (list "dicks"
                     (read-string "URL: ")
                     (read-string "Output: ")))
  (let: download-file (expand-file-name (str "/tmp/dicks/" output ".mkv")) in
    (cps-let ((_ (with-default-dir "~/code/youtube-dl/"
                   (jf-proc-comint
                    name "python" cont!
                    "-m" "youtube_dl" url
                    "-f" "bestaudio"
                    "-o" download-file)))
              (info (jf-proc "ffmpeg" cont!
                             "-i" download-file))
              (_ (jf-proc-comint
                   name "ffmpeg" cont!
                   "-i" download-file
                   "-c:a" "copy" "-vn"
                   (str "/tmp/dicks/" output "." (audio-codec-ext info)))))
      (pr "Done!\n"))))

(provide 'youtube)
