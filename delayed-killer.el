(defun is-delayed-worker (line)
  (let: columns (split-string line) in
    (str~ "delayed_job.[0-9]+" (lastcar columns))))

(defun get-delayed-workers (text &optional pids other-pred)
  (do-wad
   (split-string text "\n" t)
   (filter (or-comb 'is-delayed-worker (or other-pred 'nilfunc)) _)
   (mapcar 'split-string _)
   (if pids 
       (lc (nth 0 row) row _)
     _)))

(defn dump-delayed-workers (&optional restart-only pids cont)
  (multi-exec 
   "web" servers "ps -e -o pid,lstart,cmd | grep delayed"
   (lambda (s x)
     (if (or (not restart-only) (str~ "restart\\|stop" x))
         (do-wad
          (if restart-only (lambda (foo) (str~ "restart\\|stop" foo)))
          (get-delayed-workers x pids _)
          (if cont 
              (funcall cont s _)
            (pp (cons s _) (current-buffer))))))))

(defun get-dj-start-time (dj)
  (do-wad (strjoin " " (lc (nth x dj) x (range 2 6)))
          parse-time-string (apply 'encode-time _)))

(defun old-workers (server-info)
  (let: restart-cmd (car (lc x x (cdr server-info) (str~ "restart\\|stop" (lastcar x))))
        restart-time (get-dj-start-time restart-cmd) in
    (lc x x (cdr server-info)
        (time-less-p (get-dj-start-time x) restart-time))))

(defun old-kill-command (server-info)
  (let: olds (old-workers server-info) in
    (str "ssh web@" (car server-info) " 'kill -9 " (strjoin " " (mapcar 'car olds)) "'")))

(provide 'delayed-killer)
