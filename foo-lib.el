(defun foo-late (expr)
  (cond
   ((atom expr) `((:= ,(gensym) ,expr)))
   ((foo-special expr) (foo-special expr))
   (t (foo-call expr))))

(defun foo-special (expr)
  nil)

(defun foo-result (inseq)
  (cadr (lastcar inseq)))

(defun foo-call (expr)
  (let: fun (foo-late (car expr))
	args (mapcar 'foo-late (cdr expr))
	result (gensym)
	in
    `(,@fun
      ,@(apply 'append args)
      (:= ,result call ,(foo-result fun) ,@(mapcar 'foo-result args)))))

(defun flatten (tree)
  (mappend
   (lambda (x)
     (if (listp x)
	 (flatten x)
       (list x)))
   tree))

(defun destructify (parm &optional addr)
  (cond
   ((not parm) nil)
   ((atom parm) parm)
   ((atom (car parm)) 
    (cons (cons 'car addr) 
	  (destructify (cdr parm)
		       (cons 'cdr addr))))
   (t (append (destructify (car parm)
			   (cons 'car addr))
	      (destructify (cdr parm)
			   (cons 'cdr addr))))))

(defmacro fn (parms &rest body)
  (let: args (lc (if (atom x) x (gensym)) x parms) 
	vars (flatten parms) 
	exprs (apply 'append
		     (lc: (if (atom x) (list x)
			    (lc `(compcall ,z ,y)
				z x))
			  x y in (mapcar 'destructify parms) args))
    in
    `(lambda ,args
       (funcall
	(lambda ,vars ,@body)
	,@exprs))))

(funcall
 (fn (foo ((bar baz) wiz) balls)
     (list (+ foo baz)
	   (cons wiz balls)))
 35
 '((a 12) fuck)
 'douchebite)
