(defmacro defun-letwad (arglist &rest body)
  "Like defun, except you can use a special \"letwad\" form
that will enclose the remainder of the function body in a
foolet.  letwad only works at toplevel though."
  `(defun ,arglist
     ,@(letwad-process body)))

(defun letwad-process (l)
  (let (out)
    (catch 'break
      (while l
	(when (and (listp (car l))
		   (eq (caar l) 'letwad))
	  (push `(foolet ,(cdr (car l))
		   ,@(letwad-process (cdr l)))
		out)
	  (throw 'break nil))
	(push (car l) out)
	(pop l)))
    (nreverse out)))

(provide 'letwad)
