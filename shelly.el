;; -*- lexical-binding: t; -*-

(require 'el2)

(defvar shelly-functions
  `((s/ . (cut replace-regexp-in-string <> <> <> t))
    (ls . (lambda (x &optional all) 
            (directory-files x nil (if all nil "^[^.]"))))
    (mv . 'rename-file)
    (basename . 'file-name-base)
    (ln-s . 'make-symbolic-link)))

(defmacro shelly (&rest body)
  `(el2 (let ((default-directory default-directory)
              ,@(lc `(,var  ,fun) (var . fun) shelly-functions))
          (>>> ,@body))))

(defun path (&rest args)
  (>>> (filter 'identity args) (apply 'str _)))

(pcase-defmacro path (dir base ext)
  `(and (app file-name-directory ,dir)
        (app file-name-base ,base)
        (app file-name-extension ,ext)))

(provide 'shelly)
