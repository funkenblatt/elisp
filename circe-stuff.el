(require 'circe)
(require 'circe-highlight-all-nicks)

(defun jf-circe-login ()
  (interactive)
  (circe
   "irc.freenode.net"
   :nick "turbofail"
   :sasl-username "turbofail"
   :sasl-password (read-passwd "Password: ")
   :channels '(:after-auth "#emacs" "#clojure" "#scheme" "#lispgames" "#guile")
   :use-tls t
   :port 6697)

  (enable-circe-highlight-all-nicks))

(set-face-attribute 'circe-originator-face nil :weight 'bold)

(provide 'circe-stuff)
