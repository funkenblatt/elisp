(defun key-event-string (evt)
  (if (= evt.which))
  (+ (cond (evt.ctrlKey "C-")
           (evt.altKey "M-")
           (true ""))
     (String.fromCharCode evt.which)))

(defun keymap (km)
  (λ (evt) 
     (>>> (aref km (key-event-string evt))
          (if _ (_ evt) true))))

(defun mk-button (txt handler)
  (let ((out (document.createElement "button")))
    (out.setAttribute "label" txt)
    (setf out.onclick handler)
    out))

(defun clear-children (node)
  (.. node .childNodes to-array 
      (.forEach (λ (x) (x.parentNode.removeChild x)))))

(defun last (thing)
  (aref thing (- thing.length 1)))

(defun cycle-children (node dir hook)
  (if dir
      (let ((thing (.. node .child-nodes last)))
        (node.removeChild thing)
        (node.insertBefore thing (.. node .child-nodes 0)))
    (let ((thing (.. node .child-nodes 0)))
      (node.removeChild thing)
      (node.appendChild thing)))
  (if hook (hook (.. node.child-nodes 0)
                 (if dir (aref node.child-nodes 1) 
                   (last node.child-nodes)))))

(defun to-array (array-like)
  (let ((i 0) (out []))
    (for (nil (< i array-like.length) (incf i))
      (out.push (-> array-like [i])))
    out))

(defun tab-switch (tab-wad)
  (setf (.. document (.getElementsByTagName "tabs") 0 .selected-item) tab-wad))

(defun hide-nuts ()
  (setf dicknuts.hidden true
        nutbox.hidden true)
  (window.content.focus))

(defun show-matching-tabs (subs)
  (.. document (.get-elements-by-tag-name "tabs") 0 .child-nodes to-array
      (.filter (λ (x) (>= (-> x label (toLowerCase) (indexOf subs)) 0)))
      (.map (λ (x) (mk-button x.label (λ () (tab-switch x) (hide-nuts)))))))

(defun prepare-nuts (fun cycle-fun quit-fun)
  (setf dicknuts.hidden false
        nutbox.hidden false
        dicknuts.value ""
        dicknuts.oninput fun
        dicknuts.onkeydown 
        (dokeys "C-S" (progn (cycle-children nutbox nil cycle-fun) false)
                "C-G" (progn (if quit-fun (quit-fun)) (hide-nuts))
                "C-R" (progn (cycle-children nutbox 1 cycle-fun) false)
                "\x1b" (progn (if quit-fun (quit-fun)) (hide-nuts))
                "\r" (progn ((-> nutbox child-nodes 0 onclick)) true)))
  (dicknuts.oninput)
  (dicknuts.focus))

(defun read-tab ()
  (prepare-nuts
   (lambda ()
     (.. nutbox clear-children)
     (.. (show-matching-tabs (-> dicknuts value)) 
         (.forEach (λ (x) (nutbox.appendChild x)))))))

(defun nodes-where (fun node pred)
  (if (pred node) (fun node))
  (.. node .childNodes to-array
      (.forEach (λ (x) (nodes-where fun x pred)))))

(defun all-text (n)
  (let ((out []))
    (nodes-where
     (λ (x) (out.push x.wholeText))
     n (λ (x) (= x.nodeType 3)))
    (out.join "")))

(defun contains (s needle)
  (.. s (.toLowerCase) (.indexOf (needle.toLowerCase))
      (>= 0)))

(defun elem-offset (elem)
  (let ((x 0) (y 0))
    (for (nil elem.offsetParent (setf elem elem.offsetParent))
      (incf x (-> elem offset-left))
      (incf y (-> elem offset-top)))
    [x y]))

(defun position-compare (a b)
  (let ((ay (.. a elem-offset 1 (- content.scrollY)))
        (by (.. b elem-offset 1 (- content.scrollY))))
    (cond
     ((and (>= ay 0) (< by 0)) -1)
     ((and (< ay 0) (>= by 0)) 1)
     (true (- ay by)))))

(defun matching-links (subs)
  (.. window.content.document (.getElementsByTagName "a") to-array
      (.filter (λ (x) (contains (all-text x) subs)))
      (.sort position-compare)))

(defun link-button (link)
  (let ((out (mk-button (+ (all-text link) " -- " link.href)
                        (λ () (clear-link-borders)
                           (setf window.content.location link.href)
                           (hide-nuts)))))
    (setf out.link-node link)
    out))

(setf prev-link-matches [])

(defun clear-link-borders ()
  (prev-link-matches.forEach (λ (x) (setf x.style.border x.prev-border)))
  (setf prev-link-matches []))

(defun read-links ()
  (prepare-nuts
   (lambda ()
     (clear-children nutbox)
     (if (>= dicknuts.value.length 2)
         (let ((matches (matching-links dicknuts.value)))
           (clear-link-borders)
           (setf prev-link-matches matches)
           (matches.forEach (λ (x) (setf x.prev-border x.style.border
                                         x.style.border "3px solid #f00")))
           (setf (.. matches 0 .style .border) "3px solid #0f0")
           (.. matches (.map link-button)
               (.forEach (λ (x) (nutbox.appendChild x)))))))
   (lambda (button prev-button)
     (setf button.link-node.style.border "3px solid #0f0"
           prev-button.link-node.style.border "3px solid #f00"))
   clear-link-borders))

(setf ass-meat (λ (e) (setf last-evt e)))
;; (dokeys "C-t" (read-tab)
;;         "C-l" (read-links))

(addEventListener
 "keypress" (λ (evt) (ass-meat evt)) true)

(setf dicknuts (-> document (create-element "textbox"))
      nutbox (-> document (create-element "hbox")))

(setf (.. nutbox .style .overflow) "scroll")

(-> document (get-elements-by-tag-name "window") 0
    (appendChild dicknuts))

(-> document (get-elements-by-tag-name "window") 0
    (appendChild nutbox))

(hide-nuts)
