;; -*- lexical-binding: t; -*-
;; Fooserve part 2 - the Fooservening!
;; Reuses a lot of stuff from the http client.
(require 'http-client)
(require 'http-junk)

(defun http-response (resp-code headers body)
  (str
   (interp "HTTP/1.1 #,[resp-code]\r\n")
   (headers->str (if (assoc-default 'Content-Length headers)
                     headers
                   (cons `(Content-Length . ,(string-bytes body)) headers)))
   "\r\n"
   body))

(defun foo-filter-2 (p s)
  (letrec ((pipe (procpipe p '("connection broken by remote peer\n") s))
           (loopy 
            (lambda ()
              (cps> heads (read-http-head pipe :cont!)
                (if (or (not heads) (equal (car heads) ""))
                    nil
                  (let: headers (header->alist
                                 (lc x x (cdr heads) (str~ "^[^:]+:" x)))
                        peer (process-contact p)
                        req-parts `(,@(split-string (car heads)) ,peer)
                        cont (lambda (resp-code resp-headers body)
                               (process-send-string 
                                p (http-response resp-code resp-headers (or body "")))
                               (funcall loopy)) in
                    (cond
                     ((assoc-default "content-length" headers)
                      (let: len (read (assoc-default "content-length" headers)) in
                        (cps> data (funcall pipe len :cont!)
                          (foo-handle req-parts headers data cont))))
                     ((equal (downcase (or (assoc-default "transfer-encoding" headers) ""))
                             "chunked")
                      (cps> data (read-chunks pipe :cont!)
                        (foo-handle req-parts headers data cont)))
                     (t (foo-handle req-parts headers nil cont)))))))))
    (funcall loopy)))

(defun foo-handle (req-parts headers body cont)
  (iflet handler (assoc-default (cadr req-parts) fooserve-2-handlers 'string-match)
         (condition-case e
             (funcall handler req-parts headers body cont)
           (error (funcall cont "502 Internal Error" '((Content-Type . "text/plain"))
                           (repr e))))
         (funcall cont "404 Not Found" '((Content-Type . "text/plain"))
                  "Go fuck yourself.")))

(defun fooserve-bufs (req-parts headers body cont)
  (do-wad
   (urldecode (cadr req-parts)) (substring _ 1)
   get-buffer
   (if _
       (condition-case e 
           (progn 
             (pr "Serving " (repr _) "\n")
             (funcall cont "200 OK"
                      (cond
                       ((endswith (buffer-name _) "html") '((Content-Type . "text/html")))
                       ((endswith (buffer-name _) "css") '((Content-Type . "text/css")))
                       ((endswith (buffer-name _) "svg") '((Content-Type . "image/svg+xml")))
                       (t '((Content-Type . "text/plain"))))
                      (w/buf _ (buffer-string))))
         (error (funcall cont "502 Error"
                         '((Content-Type . "text/plain"))
                         "Shit's broke.")))
     (return-404 cont))))

(defun return-404 (cont)
  (funcall cont "404 Not Found" '((Content-Type . "text/plain")) (yow)))

(setf fooserve-2-handlers
  `(("^/goto-url/" . browse-url-listener) ("^/paste-listener/" . paste-listener) (".*" . fooserve-bufs)))

(defun paste-listener (req-parts header body cont)
  (ignore-errors
    (pr body) (kill-new body))
  (funcall cont "200 OK" '((Content-Type . "text/plain"))
           "received"))

(defun browse-url-listener (req-parts header body cont)
  (if body (browse-url body))
  (funcall cont 200 
           '(("Content-Type" . "text/html"))
           "Received"
           ))

(defun fooserve-2-start (&optional port)
  (make-server "fooserve" (or port 12346) 'foo-filter-2))

(provide 'fooserve-2)
