(defun omg-req (uri data &optional hook)
  (foolet (base-req (format "POST %s HTTP/1.1\r\nHost: omegle.com\r\nContent-Type: application/x-www-form-urlencoded\r\n" uri))
    (if data
	(progn
	  (setq data (apply 'concat (join 
			      (lc (format "%s=%s" (car x) (cdr x)) x data)
			      "&")))
	  (process-send-string
	   "foomeg"
	   (concat base-req
		   (format "Content-length: %d\r\n\r\n" (length data))
		   data)))
      (process-send-string "foomeg" (concat base-req "\r\n")))
    (if hook (setq omg-recv-hook hook))))

(defun omg-connect ()
  (make-network-process
   :host "omegle.com" :filter (lambda (p s) (if omg-recv-hook (funcall omg-recv-hook s)))
   :service 80 :family 'ipv4 :name "foomeg"))

(defun omg-start ()
  (setq omg-running t)
  (omg-connect)
  (omg-req "/start" nil 
	   (lambda (s)
	     (foolet (a (string-match "\r\n\r\n" s))
	       (when a 
		 (setq omg-id (read (substring s (+ a 4))))
		 (omg-get-events))))))

(defun omg-stop ()
  (omg-req "/disconnect" `((id . ,omg-id)))
  (setq omg-running nil)
  (delete-process "foomeg"))

(defun omg-talk (msg)
  (interactive "sTalk: \n")
  (omg-req "/send" `((id . ,omg-id)
		     (msg . ,msg))))

(defun omg-get-events ()
  (omg-req "/events" `((id . ,omg-id))
	   (lambda (s)
	     (foolet (a (string-match "\r\n\r\n" s))
	       (when a
		 (setq a (substring s (+ a 4)))
		 (unless (or (= (aref a 0) ?0)
			     (equal a "null"))
		   (scratchdump a))))))
  (if omg-running
      (run-at-time 0.5 nil 'omg-get-events)))

