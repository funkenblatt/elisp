;; -*- lexical-binding: t; -*-
(require 'http-client)
(require 'http-junk)

(defun http-post (p cont uri body &rest more-headers)
  (get-http 
   p cont uri
   nil (alist-merge
        `((Content-length . ,(length body))
          (Content-type . application/x-www-form-urlencoded))
        more-headers)
   "POST" body))

(defun multipart (alist fn)
  "Calls fn with the boundary
and a string corresponding to a multipart/form-data
representation of the key-value pairs in alist."
  (let: boundary (apply 'str (mapcar (x: (format "%02x" (random 256)))
                                     (range 10)))
    in
    (funcall
     fn boundary
     (apply 'str
            (append
             (mapcar
              (λ((k . v))
                 (interp "--#,boundary\r
Content-Disposition: form-data; name=#,(repr (str k))\r\n\r\n#,v\r\n"))
              alist)
             (list (str "--" boundary "--\r\n")))))))

(defun paste-junk (pastage &optional title channel)
  (cps> (content) (get-http (connect "paste.lisp.org") :cont! "/")
    (let: captcha (re-search
                   "What do you get when you multiply \\([0-9]*\\) by \\([0-9]*\\)"
                   content)
          captcha-id (cadr (re-search "captchaid\" value=\"\\([^\"]*\\)" content)) in
      (http-post
       (connect "paste.lisp.org") 'paste-notice "/submit"
       (urlencode-alist
        `((channel . ,(or channel "None"))
          (username . "turbofail")
          (title . ,(or title "turbofail paste"))
          (captcha . ,(str (apply '* (mapcar 'read (cdr captcha)))))
          (captchaid . ,captcha-id)
          (colorize . "Emacs Lisp")
          (expiration . "Never expires (recommended)")
          (text . ,pastage)))))))

(defun lisppaste-region (title &optional channel)
  (interactive (list (read-string "Title: ")
                     (if current-prefix-arg
                         (read-string "Channel: "))))
  (paste-junk (get-region) title channel))

(defun paste-buffer (title &optional channel)
  (interactive (list (read-string "Title: ")
                     (if current-prefix-arg
                         (read-string "Channel: "))))
  (paste-junk (buffer-string) title channel))

(defun paste-notice (x)
  (w/buf (get-buffer-create "*paste*") 
         (erase-buffer)
         (insert x))
  (do-wad 
   (str~ "Paste number \\([0-9]*\\) pasted!" x 1)
   (interp "http://paste.lisp.org/display/#,_")
   (progn (pr "Your paste: " _) (kill-new _))))

(defun sprunge-junk (txt)
  (>>> (urlencode-alist `((sprunge . ,txt)))
       (http-post (connect "sprunge.us") 
                  (x: (pr x) (kill-new (strip x)))
                  "http://sprunge.us/"
                  _)))

(defun sprunge-region ()
  (interactive)
  (sprunge-junk (get-region)))

(defun sprunge-buffer (buf)
  (interactive (list (current-buffer)))
  (w/buf buf (sprunge-junk (buffer-string))))

(defun ix-dump (stuff)
  (multipart
   `(("f:1" . ,stuff))
   (λ (b s)
      (http-post
       (connect "ix.io")
       (x: (pr "Dicks: " x)
           (kill-new x))
       "/"
       s
       `(Content-type . ,(interp "multipart/form-data; boundary=#,b"))
       '(Connection . "close")))))

(defun ix-region ()
  (interactive)
  (ix-dump (get-region)))

(defun ix-buffer (buf)
  (interactive (list (current-buffer)))
  (w/buf buf (ix-dump (buffer-string))))

(provide 'paste)
