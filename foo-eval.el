;; This code depends on macros defined in http://jason.ozbert.com/elisp/jf-lib.el

(setq undef (make-symbol "#undefined"))

;; foo-eval runs its crap in a loop for tail-call
;; goodness.  handle-tailcall takes a list of
;; junk returned from the two sub-eval functions
;; that are capable of being a "tail" site (if and
;; procedure-application) and sets the necessary
;; variables for the next loop iteration.

(defun foo-eval (form env &optional running out handle-tailcall)
  (setq running t)
  (setq handle-tailcall
	(lambda (a b c)
	  (setq running a form b env c)
	  b))
  (while running			
    (setq 				
     running nil
     out (if (consp form)
	     (guard
	      | (eq (car form) 'define) (do-define form env)
	      | (eq (car form) 'fn) (do-fn form env)
	      | (eq (car form) 'if) (apply handle-tailcall 
					   (do-if form env))
	      | (eq (car form) 'quote) (cadr form)
	      | (eq (car form) 'set) (set-var env 
					      (cadr form) 
					      (foo-eval (caddr form) env))
	      | t (apply handle-tailcall (do-application form env)))
	   (guard
	    | (vectorp form) form
	    | (not form) form
	    | (eq form t) form
	    | (symbolp form) (let: val (lookup-var env form) in
			       (if (eq val undef)
				   (signal 
				    'error (format "No such variable: %s" form)))
			       val)
	    | (numberp form) form
	    | (stringp form) form))))
  out)

(defun do-define (form env)
  (let: name (cadr form)
	val (caddr form) in
    (add-binding env name)
    (set-var env name
	     (foo-eval val env))
    nil))

(defun do-fn (form env)
  (vector 'fn (cadr form) (cddr form) env))

(defun primproc-p (obj)
  (and (vectorp obj)
       (eq (aref obj 0) 'prim)))

(defun fooproc-p (obj)
  (and (vectorp obj)
       (or (eq (aref obj 0) 'fn)
	   (eq (aref obj 0) 'prim))))

(defun do-application (form env)
  (let: proc (foo-eval (car form) env) in
    (if (not (fooproc-p proc))
	(signal 'error (format "Not a procedure: %S" (car form))))
    (if (eq (aref proc 0) 'prim)
	(list nil
	      (apply (aref proc 1) 
		     (mapcar (lambda (x) (foo-eval x env))
			     (cdr form)))
	      env)
      (let: args (mapcar (lambda (x) (foo-eval x env))
			 (cdr form))
	    junk (aref proc 2) 
	    temp-env (new-scope (aref proc 3)) in
	(dolist (i (aref proc 1))
	  (add-binding temp-env i)
	  (set-var temp-env i (pop args)))
	(while (cdr junk)
	  (foo-eval (pop junk) temp-env))
	(list t (car junk) temp-env)))))

(defun do-if (form env)
  (let: boolwad (foo-eval (cadr form) env) in
    (if boolwad
	(list t (caddr form) env)
      (list t (cadr (cddr form)) env))))

;; Environment implementation
(defun add-binding (env name)
  (puthash name nil (car env)))

(defun new-scope (env)
  (cons (make-hash-table) env))

(defun set-var (env name val)
  (catch 'break
    (while (and env
		(eq (gethash name (car env) undef) undef))
      (pop env))
    (if (not env) (signal 'error (format "Unbound variable %s" name)))
    (puthash name val (car env))))

(defun lookup-var (env name)
  (catch 'break
    (while env
      (if (not (eq (gethash name (car env) undef) undef))
	  (throw 'break (gethash name (car env))))
      (pop env))
    undef))

(defun setup-repl ()
  "Sets up a toplevel foo-environment, and binds M-RET in the current
buffer to read, eval, and print."
  (setq toplevel (list (make-hash-table)))
  (onkey (kbd "M-RET")
      (dump
       "\n"
       (repr (foo-eval
	      (preceding-sexp)
	      toplevel))
       "\n")))

;; (foo-eval '(define foo (fn () (bar))) toplevel)
;; (foo-eval '(define bar (fn () (foo))) toplevel)
;; (foo-eval '(foo) toplevel)
;;             ^^ This mutual recursion should loop forever without
;;                giving you any max-eval-depth reached crap.
