(defun process-killer (&optional ps-func kill-func)
  (interactive)
  (with-current-buffer (get-buffer-create "*ps*")
    (setf buffer-read-only nil)
    (erase-buffer)
    (do-wad 
     (if ps-func (funcall ps-func) (process-lines "ps" "-e")) 
     (mapcar 'colorize-process-info _)
     pr-align)
    (local-set-key (kbd "M-RET")
      (keycmd
       (let: sig (if current-prefix-arg (read-from-minibuffer "Signal: ") "")
             pid (car (split-string (thing-at-point 'line))) in
         (if kill-func
             (funcall kill-func pid sig)
           (shell-command (str "kill " sig " " pid))))))
    (local-set-key (kbd "g") (keycmd (funcall 'process-killer ps-func kill-func)))
    (pop-to-buffer (current-buffer))
    (setf buffer-read-only t)))

;; Example process-killer with custom process listing and killing functions:
;; (process-killer
;;  (lambda () (process-lines "ssh" "web@ec2-204-236-168-129.us-west-1.compute.amazonaws.com"
;;                            "ps" "-e" "-o" "pid,tty,time,cmd"))
;;  (lambda (sig pid)
;;    (process-lines "ssh" "web@ec2-184-72-15-91.us-west-1.compute.amazonaws.com"
;;                   "kill" sig pid)))

(defun colorize-process-info (line)
  (let: parts (split-string line) in
    `(,(propertize (car parts) 'face '(:foreground "#00ff00"))
      ,@(head (cdr parts) 2)
      ,(propertize (strjoin " " (cdddr parts))
                   'face '(:foreground "#770000")))))

(provide 'process-killer)
