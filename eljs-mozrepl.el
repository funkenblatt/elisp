;; -*- lexical-binding: t; -*-

(require 'eljs)
(require 'fooserve-2)

(setf eljs-request-id 0)
(setf eljs-requests (make-hash-table))
(push '("^/eljs-listener/" . eljs-listener) fooserve-2-handlers)
(push '("^.*eljs\\.el" . eljs-server) fooserve-2-handlers)

(defun eljs-listener (req-parts header body cont)
  (ignore-errors 
    (do-wad 
     (json-read-from-string body)
     (progn 
       (funcall (gethash (aref _ 0) eljs-requests) (aref _ 1))
       (remhash (aref _ 0) eljs-requests))))
  (funcall cont "200 OK" '((Content-Type . "text/plain"))
           "received"))

(defun eljs->mozrepl (eljs-code cont)
  (puthash (incf eljs-request-id) cont eljs-requests)
  (process-send-string 
   mozrepl 
   (str (to-js `(fetch-url 
                 "http://localhost:12345/eljs-listener/"
                 "POST" (x: nil)
                 (JSON.stringify [,eljs-request-id ,eljs-code])))
        ";\n")))

(defun eljs-load-buffer (buf)
  (nlet foo ((pt (w/buf buf (point-min))))
    (with-current-buffer buf
      (save-excursion
        (goto-char pt)
        (let: e (read/die) in
          (when e
            (process-send-string mozrepl (to-js e))
            (run-at-time 0.1 nil foo (point))))))))

(defun eljs-mozrepl-bootstrap ()
  (interactive)
  (eljs-load-buffer
   (find-file-noselect (locate-library "eljs-lib"))))

(defun eljs-compile-buffer (&optional buffer)
  (>>> (buffer-forms buffer)
       (mapcar (comp (cut str <> ";\n") 'to-js) _)
       (strjoin "\n" _)))

(defun eljs-server (req-parts header body cont)
  (or (ignore-errors
        (>>> (substring (cadr req-parts) 1) urldecode
             eljs-compile-buffer
             (funcall cont "200 OK" '((Content-Type . "text/javascript")) _))
        t)
      (funcall cont "500 Oh Shit" '((Content-Type . "text/plain"))
               "Everything is fucked.")))

(defun munge-clj-symbol (s)
  (intern (replace-regexp-in-string
           (rx (any "-!?"))
           (x: (pcase x
                 ("-" "_")
                 ("!" "_BANG_")
                 ("?" "_QMARK_")))
           (str s) nil t)))

(defun cljs->eljs (form)
  "Convert cljs-ish expression into eljs friendly form."
  (cond
   ((keywordp form)
    (pcase (str form)
      ((str ":" ns "/" name) `(cljs.core.keyword ,ns ,name))
      (x `(cljs.core.keyword ,(substring x 1)))))
   ((symbolp form)
    (pcase (str form)
      ("/" form)
      ((str ns "/" name)
       (if (equal ns "d")
           (intern (str "datascript.core." (munge-clj-symbol name)))
         (intern (str (munge-clj-symbol ns) "." (munge-clj-symbol name)))))
      (x (munge-clj-symbol x))))
   ((vectorp form)
    `(cljs.core.vector ,@(mapcar 'cljs->eljs form)))

   ((consp form) (mapcar 'cljs->eljs form))

   (t form)))

(provide 'eljs-mozrepl)
