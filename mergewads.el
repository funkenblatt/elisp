(setq disp "~/code/pqube-svn/Display/")
(setq hdisp "~/code/pqube-svn/hbranch/Display/")

(defun intersect (a b)
  (lc x x a (member x b)))

(defun set-diff (a b)
  (lc x x a (not (member x b))))

(setq fileset
      (lc (with-default-dir x
	    (process-lines "projfiles.py"))
	  x `(,disp ,hdisp)))
(apply 'set-diff fileset)

(defun diffwad (file)
  (apply 'call-process "diff" nil "*diffwad*" t 
	 "-u"
	 (mapcar 'expand-file-name 
		 (list (concat disp file) 
		       (concat hdisp file)))))

(defun all-diffs ()
  (dolist (i (car fileset)) (if (not (string-match "english\\.c" i)) (diffwad i))))

(defun diffbag (file)
  (apply '
   ediff-files
   (mapcar 'expand-file-name
	   (list (concat hdisp file)
		 (concat disp file)))))
