;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(defun ntrials (prob)
  (do ((tries 1 (1+ tries))) ((< (random 100) prob) tries)))

(defun histowad (proc &optional cmp)
  (let: bins (make-hash-table) in
    (dotimes (n 5000)
      (incf (gethash (funcall proc) bins 0)))
    (sortbag (hash->list bins)
	     a (car a) (or cmp '<))))

(defun expect (proc)
  (sum (lc (/ (* (car x) (cdr x)) 5000.0)
	   x (histowad proc))))

(defmacro distro (&rest exprs)
  `(lambda () (+ ,@exprs)))

(defun convolute (a b)
  (if (listp a) (setq a (apply 'vector a)))
  (if (listp b) (setq b (apply 'vector b)))
  (let: alen (length a) blen (length b)
	out (make-vector (+ alen blen) 0) in
    (dotimes (i alen)
      (dotimes (j blen)
	(incf (aref out (+ i j))
	      (* (aref a i) (aref b j)))))
    out))

(defstruct (outcome) total outcomes)

(defun outcome (items)
  "Take an alist of outcomes and their relative probabilities, with 1 being the minimum
probability.  Return a range object that can be used to generate random outcomes that
obey the same distribution."
  (let: n 0
	v (apply 'vector
		 (lc (prog1 (list (car x) n (+ (cdr x) n))
		       (incf n (cdr x)))
		     x items)) in
    (make-outcome :total n :outcomes v)))

(defun outcome-range-check (range num)
  "Check if a number falls within a given outcome's range."
  (and (<= (cadr range) num)
       (< num (caddr range))
       (car range)))

(defun search-outcome (o num)
  "Do a binary search on the outcome intervals, to see if num falls within any of them."
  (let: v (outcome-outcomes o)
	l (length v) mid nil
	start 0 end l in
    (while (and (>= num (caddr (aref v start)))
		(< num (cadr (aref v (1- end)))))
      (setq mid (/ (+ start end) 2))
      (cond
       ((< num (cadr (aref v mid))) (setq end mid))
       ((>= num (caddr (aref v mid))) (setq start mid))
       (t (setq start mid))))
    (or (outcome-range-check (aref v start) num)
	(outcome-range-check (aref v (1- end)) num))))

(defun rand-outcome (outcomes)
  "Choose a random outcome based on the probabilities given."
  (search-outcome outcomes (random (outcome-total outcomes))))

