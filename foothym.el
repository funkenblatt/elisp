;; -*- lexical-binding: t; -*-

(defun foothym (n m)
  "Prints out some fat beats.  Press q to make it stop.
This should probably be changed to use lexical scope
at some point."
  (let: beat 0 foothym-buf (generate-new-buffer "foothym")
        beat-list (list n m) foothym-run nil in
    (with-current-buffer foothym-buf
      (auto-save-mode -1) (erase-buffer)
      (local-set-key 
          (kbd "q") 
        (lambda () (interactive) 
          (setq beat -1) 
          (local-unset-key (kbd "q"))))
      (setq 
       foothym-run
       (lambda ()
	 (with-current-buffer foothym-buf
	   (setq buffer-read-only nil)
	   (unless (< beat 0)
	     (goto-char 0)
	     (delete-char 6)
	     (dump (if (= (% beat 2) 0) "| " "O "))
	     (ec (dump (if (= (% beat x) 0) "!!" "  "))
		 x beat-list)
	     (setq beat (% (+ beat 1) (apply '* beat-list)) buffer-read-only t)
	     (run-at-time 0.15 nil foothym-run)))))
      (dump "      ") (setq buffer-read-only t)
      (make-frame '((width . 10) (height . 5) (minibuffer . nil)))
      (funcall foothym-run)
      nil)))

;(foothym 3 4)
;(foothym 5 6)
(provide 'foothym)
