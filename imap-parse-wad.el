(defmacro scase (expr &rest clauses)
  (w/gensyms (my-expr)
    `(let ((,my-expr ,expr))
       (cond
        ,@(lc `(,(if (eq (car clause) 'otherwise)
                     t
                   `(equal ,my-expr ,(car clause)))
                ,@(cdr clause))
              clause clauses)))))

;; Shit for parsing that assworthy RFC ABNF syntax. 
(defun grammar-tokenize (rule)
  (re-findall
   (rx (or (: alpha (* (any alpha digit "-"))) ; basic "symbol" (name of a non-literal terminal or nonterminal)
           (: (+ digit))                       ; numbers
           (: "%" (+ (not space)))             ; hex literal ranges
           (: ?\" (* (not (any ?\"))) ?\")     ; quoted literal terminals
           (: "<" (* (not (any ">"))) ">")     ; character-class terminals
           (any "()*/[]=")))                   ; grouping and other operators.
   rule t))

(defun parse-grammar-expr (rule-strm)
  (let: tok (funcall rule-strm) in
    (scase tok
           (nil nil)
           ("(" (parse-grammar-rule rule-strm))
           ("[" `(maybe ,(parse-grammar-rule rule-strm)))
           ("]" nil)
           (")" nil)
           ("*" `(* ,(parse-grammar-expr rule-strm)))
           (otherwise tok))))

(defun parse-grammar-cat (rule-strm)
  (let: expr (parse-grammar-expr rule-strm) in
    (if expr
        (let: cat-exprs (list expr) in
          (while (and (funcall rule-strm 0)
                      (not (member (funcall rule-strm 0)
                                  '("/" "]" ")"))))
            (push (parse-grammar-expr rule-strm) cat-exprs))
          (if (cdr cat-exprs)
              (cons ': (reverse cat-exprs))
            expr))
      nil)))

(defun parse-grammar-rule (rule-strm)
  (let: expr (parse-grammar-cat rule-strm) in
    (if expr
        (let: or-exprs (list expr) in
          (while (equal "/" (funcall rule-strm 0))
            (funcall rule-strm)
            (push (parse-grammar-cat rule-strm) or-exprs))
          (when (cdr or-exprs) 
            (setq expr (cons 'or (reverse or-exprs))))
          (let: foo (parse-grammar-rule rule-strm) in
;            (pr "Foo: " foo "\n")
            expr))
      nil)))

(defun grammar-rule-nullable (tree &optional grammar)
  (if (consp tree)
      (case (car tree)
        (: (all (mapcar 'grammar-rule-nullable (cdr tree))))
        (or (any (mapcar 'grammar-rule-nullable (cdr tree))))
        (* t)
        (maybe t))
    (iflet x (assoc-default tree grammar)
           (grammar-rule-nullable x grammar)
           nil)))

(defun run-grammar (grammar rule tokstrm &optional classifier)
  (vardumps rule (funcall tokstrm 0))
  (cond
   ((not rule) nil)
   ((assoc-default rule grammar)
    (let: parts (run-grammar grammar 
                               (cons ': (assoc-default rule grammar)) tokstrm
                               classifier) in
      (if parts (list (cons rule parts)) nil)))
   ((consp rule)
    (case (car rule)
      (: (let: out nil in
           (catch 'break-1
             (dolist (i (cdr rule))
               (iflet result (run-grammar grammar i tokstrm classifier)
                      (push result out)
                      (if (not (grammar-rule-nullable i grammar))
                          (throw 'break-1 nil))))
             (apply 'nconc (nreverse out)))))
      ((* +) 
       (let: junk (run-grammar grammar (cadr rule) tokstrm classifier)
             accum nil in
         (while junk
           (push junk accum)
           (setq junk (run-grammar grammar (cadr rule) tokstrm classifier)))
         (apply 'append (nreverse accum))))
      (or (catch 'break
            (dolist (i (cdr rule))
              (let: forked (funcall tokstrm nil :fork t) in
                (iflet x (run-grammar grammar i forked classifier)
                       (progn (funcall tokstrm nil :reset (funcall forked nil :raw t))
                              (throw 'break x)))))))))
   (t 
    (if (equal (funcall (or classifier 'identity)
                        (funcall tokstrm 0)) 
               rule)
        (list (funcall tokstrm))
      nil))))
