;; -*- lexical-binding: t; -*-

(defun elsql-bindings ()
  (interactive)
  (local-set-key (kbd "C-c C-c")
    (keycmd (>>> (preceding-sexp) (eval _ t) render-query run-sql pp)))

  (local-set-key (kbd "C-c C-a")
    (keycmd (>>> (preceding-sexp) (eval _ t) render-query
                 (str "explain analyze " _)
                 run-sql pp)))

  (local-set-key (kbd "C-c C-e")
    (keycmd (>>> (preceding-sexp) (eval _ t) render-query (str "explain " _) run-sql pp)))

  (local-set-key (kbd "C-c C-j")
    (keycmd (>>> (preceding-sexp) (eval _ t) render-query run-sql
                 (print-tabular-sexp _) insert))))

(defun print-tabular-sexp (rows)
  (>>> (mapcar (cur 'mapcar (comp 'strip 'pp-to-string)) rows)
       (print-tabular _ " ")
       (split-string _ "\n" t)
       (mapcar (cut str " (" <> ")") _)
       (strjoin "\n" _)
       (if (not (equal _ "")) (substring _ 1) _)
       (str "(" _ ")")))

(defun do-update (table stuff)
  (>>> (render-update table stuff)
       run-sql))

(provide 'elsql-hacks)
