;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(defun pyth-triple (m n)
  (list (rpn m m * n n * -)
	(rpn m n * 2 *)
	(rpn m m * n n * +)))

(defun factor-list (start n)
  (setq out '())
  (while (< start (+ n 1))
    (if (rpn n start mod 0 =)
	(push start out))
    (setq start (+ start 1)))
  out)

(defun intersection (l1 l2)
  (setq out '())
  (dolist (i l1 out)
    (if (memq i l2) (push i out))))
    
(defun factors (n) (factor-list 2 n))

(defun scratch-dump ()
  (setq curbuf (current-buffer))
  (setq textwad (buffer-substring-no-properties (point) (mark)))
  (set-buffer (get-buffer "*scratch*"))
  (insert-before-markers textwad)
  (set-buffer curbuf))



