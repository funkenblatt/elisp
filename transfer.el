;; -*- lexical-binding: t; -*-

;;; Stuff for transferring various shit between computers

(defun serve-once (cb)
  (let: server (make-network-process
                :name "dicks"
                :service 12346
                :buffer nil
                :family 'ipv4
                :filter (lambda (p s) (pr "Dicks: " buf "\n"))
                :server t
                :host "0.0.0.0")
        client nil
    in

    (set-process-sentinel server
      (lambda (p s)
        (pcase s
          ((str "open from " addr)
           (unless client
             (setf client addr)
             (ignore-errors (funcall cb p))
             (delete-process p)
             (delete-process server))))))))

(defun serve-buffer ()
  (interactive)
  (shell-command "ifconfig")
  (let: b (current-buffer) in
    (serve-once (λ (p) (process-send-string p (w/buf b (buffer-string)))))))

(defun serve-region ()
  (interactive)
  (shell-command "ifconfig")
  (let: text (get-region) in
    (setf mark-active nil)
    (serve-once
     (λ (p)
        (process-send-string p text)))))

(setq git-daemon-process nil)

(defun stop-serving-git ()
  (interactive)
  (when git-daemon-process
    (delete-process git-daemon-process))
  (setq git-daemon-process nil))

(defun serve-git ()
  (interactive)
  (let: p (make-comint "git-daemon" "git" nil "daemon"
                       "--verbose"
                       (str "--base-path=" (expand-file-name default-directory))
                       "--export-all"
                       (expand-file-name
                        (str (vc-git-root default-directory) ".git")))
    in
    (stop-serving-git)
    (setq git-daemon-process (get-buffer-process p))))

(provide 'transfer)
