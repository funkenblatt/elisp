(defvar extra-libs
  '((minimal-el . "jfeng@ozbert.com:minimal-el.git")
    (pg.el . "git://github.com/funkenblatt/pg.el.git")
    (scala-mode2 . "git://github.com/hvesalai/scala-mode2")
    (ensime . "git://github.com/aemoncannon/ensime")
    (elsql . "git://github.com/funkenblatt/elsql.git")
    (nrepl.el . "git://github.com/kingtim/nrepl.el.git")
    (shadchen-el . "git://github.com/VincentToups/shadchen-el.git")
    (rainbow-indent . "git://github.com/funkenblatt/rainbow-indent.git")
    (epdf . "git://github.com/funkenblatt/epdf.git")
    ))

(defvar extra-lib-dir "~/.emacs.d")

(defun get-extra-stuff (&rest excludes)
  (with-temp-buffer
    (cd extra-lib-dir)
    (do-wad
     (set-difference (mapcar 'car extra-libs) excludes)
     (progn 
       (setf load-path (append (mapcar (comp 'expand-file-name 'str) _) load-path))
       _)
     (filter (comp 'not 'file-exists-p 'str) _)
     (mapcar (comp (x: (interp "git clone #,x"))
                   (cut chain-ref extra-libs <>))
             _)
     (if _
         (do-wad 
          (strjoin ";\n" _)
          (str _ " &")
          shell-command)))))

(defun get-extra-stuff-only (&rest packages)
  (apply 'get-extra-stuff (set-difference (mapcar 'car extra-libs) packages)))

(defun init-extras ()
  (>>> (mapcar (comp 'str 'car) extra-libs)
       (intersection (directory-files "~/.emacs.d") _ :test 'equal)
       (mapcar (cur 'str "~/.emacs.d/") _)
       (mapc (cut push <> load-path) _)))

(provide 'extras)
