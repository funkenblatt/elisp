(defn make-outbits (byte-out)
  "Create a bitstream output thing.  Returns a function that takes
an integer plus a number of bits, writing bytes out as necessary
using the BYTE-OUT parameter."
  (lexlet: curbit 0 curbyte 0 in
    (lambda (bits n)
      (if (/= n 0)
	  (dotimes (i n)
	    (when (= curbit 8)
	      (funcall byte-out curbyte)
	      (setq curbit 0 curbyte 0))
	    (setf curbyte
		  (logior (lsh (logand bits 1) curbit)
			  curbyte))
	    (setf bits (lsh bits -1))
	    (incf curbit))
	(funcall byte-out (logand curbyte (- 1 (expt 2 curbit))))
	(funcall byte-out -1)))))

(defn make-inbits (byte-in)
  "Makes a bitstream input port thing.  Returns a function that
will return the requested amount of bits from the source, pulling
in bytes as necessary using the BYTE-IN parameter."
  (lexlet: curbit 8 curbyte 0 in
    (lambda (nbits)
      (catch 'break
	(let: out 0 in
	  (dotimes (i nbits)
	    (when (= curbit 8)
	      (setq curbit 0 curbyte (funcall byte-in))
	      (if (= curbyte -1) (throw 'break -1)))
	    (setq out (logior out
			      (lsh (logand curbyte 1) i))
		  curbyte (lsh curbyte -1)
		  curbit (1+ curbit)))
	  out)))))

(provide 'bitstream)
