(setf myMap nil)
(setf path-stuff [])
(setf highlighted -1)

(defun info-content (lat lng elevation marker)
  (let ((button (node ['a "Add to path"]))
        (kill-button (node ['a "Kill Me!!"])))
    (setf button.onclick (λ () 
                            (path-stuff.push [lat lng elevation])
                            (show-path)))
    (setf kill-button.onclick (λ () (marker.setMap nil)))
    (node ['div button " " kill-button ['br]
                "Latitude: " lat ['br]
                "Longitude: " lng ['br]
                "Elevation: " elevation ['br]])))

(defun add-coord-marker (evt)
  (fetch-url (+ "https://nationalmap.gov/epqs/pqs.php?"
                (urlencode-obj (:: y (evt.latLng.lat)
                                   x (evt.latLng.lng)
                                   units "Meters"
                                   output "json")))
             "GET"
             (λ (d)
                (let ((elevation (.. (JSON.parse d) 
                                     "USGS_Elevation_Point_Query_Service"
                                     "Elevation_Query"
                                     "Elevation"))
                      (mark (new (google.maps.Marker 
                                  (:: map myMap
                                      title "foo"
                                      position evt.latLng))))
                      (ix path-stuff.length))
                  
                  (google.maps.event.addListener 
                   mark "click" 
                   (λ (evt) (setf highlighted ix) (show-path)))
                  (path-stuff.push [(evt.latLng.lat) (evt.latLng.lng) elevation mark
                                    (λ (new-ix) (setf ix new-ix))])
                  (show-path)))))

(defun clear-path ()
  (path-stuff.forEach (λ ([lat lng elevation mark]) (mark.setMap nil)))
  (setf path-stuff [])
  (setf highlighted -1)
  (show-path))

(defun round-to (n decimals)
  (/ (Math.round (* n (Math.pow 10 decimals)))
     (Math.pow 10 decimals)))

(defun cumulative-p ()
  (.. (document.getElementById "cumulative") .checked))

(defun show-path ()
  (let ((el (document.querySelector "#path-stuff tbody"))
        (vdeltas (>>> (path-stuff.map (@ 2)) 
                      (map (λ (a b) (- b a)) _ (_.slice 1))))
        (hdeltas (map (λ ([lat1 lng1 e1] [lat2 lng2 e2]) 
                         (earth-distance lat1 lng1 lat2 lng2))
                      path-stuff (path-stuff.slice 1)))
        (cums (reductions (λ (a b) (+ a b)) hdeltas)))
    (setf el.innerHTML "")
    (path-stuff.forEach
     (λ ([lat lng elevation mark set-ix] ix)
        (let ((n (node ['tr
                        ['td ['button "X"]]
                        ['td (round-to elevation 3)]
                        ['td (.. (if (cumulative-p) (.. cums [(- ix 1)]) (.. hdeltas [ix]))
                                 (round-to 3))] 
                        ['td (round-to (/ (.. vdeltas [ix]) (.. hdeltas [ix])) 3)]]))
              (delete-button (n.querySelector "button")))
          (set-ix ix)
          (setf delete-button.onclick (λ ()
                                         (.. path-stuff [ix] 3 (.setMap nil))
                                         (path-stuff.splice ix 1)
                                         (show-path)))
          (if (= ix highlighted) (n.setAttribute "class" "highlighted"))
          (el.appendChild n))))))

(defun initialize ()
  (let ((opts (:: center (new (google.maps.LatLng 37.85852364995698 -122.26693153381348))
                  zoom 15 mapTypeId google.maps.MapTypeId.ROADMAP)))
    (setf myMap (new (google.maps.Map (document.querySelector "#map_canvas") opts)))
    (google.maps.event.addListener 
     myMap "click"
     (λ (evt) (add-coord-marker evt)))))

(defun sin2 (x)
  (>>> (Math.sin x) (* _ _)))

(defun to-radians (deg)
  (/ (* deg Math.PI) 180))

(defun great-circle-dist (lat1 lng1 lat2 lng2)
  (let (([lat1 lng1 lat2 lng2] (.. [lat1 lng1 lat2 lng2] 
                                   (.map to-radians))))
    (.. (sin2 (/ (- lng2 lng1) 2))
        (* (Math.cos lat1) (Math.cos lat2))
        (+ (sin2 (/ (- lat2 lat1) 2)))
        Math.sqrt Math.asin (* 2))))

(defun earth-distance (lat1 lng1 lat2 lng2)
  (* (great-circle-dist lat1 lng1 lat2 lng2)
     6335.439 1000))

