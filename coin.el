;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(setq nums (apply 'vector (lc (- (random 100) 50) x (range 40))))

(let: maxsum (expt 2 28)
      curindices nil
      cursum 0 in
  (for (0 i 40)
    (setq cursum (aref nums i))
    (for (((+ i 1) j 40))
      (when (> cursum maxsum)
	(setq maxsum cursum)
	(setq curindices (list i j)))
      (setq cursum (+ cursum (aref nums j)))))
  (list maxsum curindices))

(defun extrema (a cmp &optional start)
  (if (not start) (setq start 0))
  (let: e-val (aref a start) 
	ix start in
    (for (start i (length a))
      (when (funcall cmp (aref a i) e-val)
	(setq e-val (aref a i))
	(setq ix i)))
    ix))

(defun max-subseq (seq)
  (let: tab (apply 'vector (mapcar (lambda (x) 0) seq))
	sumwad 0 in
    (dotimes (i (length seq))
      (setq sumwad (+ sumwad (aref seq i)))
      (aset tab i sumwad))
    tab))

(defun change (amt coins)
  (cond
   ((not coins) nil)
   ((< amt 0) nil)
   ((= amt 0) (list (lc 0 x coins)))
   (t (let: a (change amt (cdr coins))
	    b (change (- amt (car coins)) coins) in
	(append (lc (cons 0 x) x a)
		(lc (cons (+ (car x) 1) (cdr x))
		    x b))))))

(defun change-tab (amt coins)
  (let: tab (make-vector (+ amt 1) nil)
	cvals (apply 'vector coins)
	n (length coins)
	access (lambda (amt coins)
		 (if (or (< amt 0) (= coins n)) 
		     nil
		   (aref (aref tab amt) coins)))
	out nil in
    (aset tab 0 (apply 'vector (reverse (lc (list (lc 0 x (range (+ y 1)))) y (range n)))))
    (for (1 i (+ amt 1))
      (aset tab i (make-vector n nil))
      (aset (aref tab i) (- n 1)
	    (if (= 0 (mod i (aref cvals (- n 1))))
		(list (list (/ i (aref cvals (- n 1)))))))
      (for (1 j n)
	(let: j (- n 1 j)
	      row (aref tab i) in
	  (aset row j (append (lc (cons 0 x) x (aref row (+ j 1)))
			      (lc (cons (+ (car x) 1) (cdr x)) x (funcall access (- i (aref cvals j)) j)))))))
    (aref (aref tab amt) 0)))

(defmacro time (&rest body)
  `(let: start (current-time) 
	 ,(gensym) (progn ,@body) 
	 end (time-subtract (current-time) start) in
     (+ (cadr end) (/ (caddr end) 1e6))))

(time (change 330 '(1 5 8 7)))
(time (change-tab 330 '(1 5 8 7)))
