(defun init-specials ()
  (setq js-specials '((if . js-if) (progn . js-progn) (lambda . js-lambda) (defun . js-defun)))
  (setq js-arith-ops '(+ - * / += -= *= /= && || & | < > <= >= == =))
  (mapc (lambda (x) (push (cons x 'js-arith) js-specials)) js-arith-ops))

(defun get-special (expr)
  (if (assq (car expr) js-specials)
      (cdr (assq (car expr) js-specials))
    nil))

(defun js-funcall (expr)
  (foolet (outvar (gensym)
	   foofun (if (listp (car expr)) (js-translate (car expr)) nil)
	   tempvars (mapcar 'js-translate (cdr expr)))
    (apply 'pr "var " outvar "=" (if foofun foofun (car expr)) "("
	   (join tempvars ","))
    (pr ");")
    outvar))

(defun js-if (expr)
  (apply
   (lambda (wad pred then &rest else)
     (foolet (outvar (gensym)
	      predwad (js-translate pred))
       (pr "var " outvar "=null;")
       (pr "if (" predwad ") {") 
       (pr outvar "=" (js-translate then) ";}")
       (when else 
	 (pr " else {")
	 (pr outvar "=" (js-translate (cons 'progn else)) ";}"))
       outvar))
   expr))

(defun js-translate (expr)
  (cond
   ((atom expr) (if (not expr) 'null expr))
   ((get-special expr)
    (funcall (get-special expr) expr))
   (t (js-funcall expr))))

(defun js-progn (expr)
  (lastcar (mapcar 'js-translate (cdr expr))))

(defun js-funbag (args body &optional name)
  (if name
      (pr "function " name)
    (pr "function ")) 
  (pr (if args (join args ",") "()") "{")
  (pr "return " (js-progn (cons 'progn body)) ";}"))

(defun js-defun (expr)
  (apply (lambda (wad name arglist &rest body)
	   (js-funbag arglist body name)
	   'null) expr))

(defun js-lambda (expr)
  (apply (lambda (wad arglist &rest body)
	   (foolet (outvar (gensym))
	     (pr outvar "=" (with-output-to-string (js-funbag arglist body)) ";")
	     outvar))
	 expr))

(defun js-arith (expr)
  (let: values (lc (js-translate x) x (cdr expr)) 
	outvar (gensym) in
    (if (> (length values) 1)
	(apply 'pr "var " outvar "=" (join values (car expr)))
      (apply 'pr "var " outvar "=" (cons (car expr) values)))
    (pr ";") outvar))

;; (let: standard-output (current-buffer) in
;;   (js-translate
;;    '(lambda (foo bar)
;;       (if (gt bar 5)
;; 	  (foo 3 (baz bar))
;; 	(poot foo)
;; 	(wiz bar)
;; 	(blah wad))
;;       (douchebite))))
