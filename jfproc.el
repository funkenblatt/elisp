;; -*- lexical-binding: t; -*-

(defun jf-proc (cmd &optional cont &rest args)
  (let: out nil 
        proc (apply 'start-file-process "jf-proc" nil
                    cmd args) in
    (set-process-coding-system proc 'binary 'binary)
    (set-process-sentinel proc
      (lambda (p s)
        (when (string-match "killed\\|terminated\\|finished\\|exited" s)
          (if cont (funcall cont (apply 'concat (nreverse out)))))))
    (set-process-filter proc
                        (lambda (p s)
                          (push s out)))
    proc))

(defun jf-proc-utf8 (&rest args)
  (let: p (apply 'jf-proc args) in
    (set-process-coding-system p 'utf-8-emacs 'utf-8-emacs)))

(defun jf-proc-status (cmd &optional cont &rest args)
  (let: out nil 
        proc (apply 'start-file-process "jf-proc" nil
                    cmd args) in
    (set-process-coding-system proc 'binary 'binary)
    (set-process-sentinel proc
      (lambda (p s)
        (unless (string-match-p "run\\|suspend" s)
          (if cont (funcall cont (apply 'concat (nreverse out)) s)))))
    (set-process-filter proc
                        (lambda (p s)
                          (push s out)))
    proc))

(defun jf-shellproc (cmd &optional cont)
  (jf-proc "bash" cont "-c" cmd))

(defun wait-for-process (proc cont)
  (set-process-sentinel proc
    (lambda (p s)
      (when (string-match "killed\\|terminated\\|finished\\|exited" s)
        (funcall cont s)))))

(defun jf-proc-outbuf (name cont cmd &rest args)
  "Run process with an output buffer"
  (let ((buf (apply 'make-comint name cmd nil args)))
    (wait-for-process (get-buffer-process buf) cont)
    buf))

(provide 'jfproc)
