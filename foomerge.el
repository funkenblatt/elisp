;; -*- lexical-binding: t; -*-

(defun foomerge (local remote base outfile &optional pid)
  (let: a (expand-file-name local)
        b (expand-file-name remote) 
        c (expand-file-name base)
        out (expand-file-name outfile) in
    ;; (setq a (find-file a) b (find-file b) c (find-file c))
    (pr outfile)
    (emerge-files-with-ancestor nil a b c out
                                nil
                                (and pid
                                     (list (lambda () (signal-process pid 'SIGUSR1)))))))

(provide 'foomerge)
