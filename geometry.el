;; -*- lexical-binding: t; -*-


(defun v+ (&rest vecs) (apply 'mapcar* '+ vecs))
(defun v- (&rest vecs) (apply 'mapcar* '- vecs))
(defun v* (s v) (mapcar (cur '* s) v))

(defun bezier (p1 p2 p3)
  (lambda (s) 
    (let: ts (- 1 s) in
      (v+ (v* (* ts ts) p1)
          (v* (* 2 s ts) p2)
          (v* (* s s) p3)))))

(defun sample-curve (n curve)
  (mapcar (comp curve (cut / <> (float (- n 1)))) (range n)))

(setf svg-header "<?xml version=\"1.0\" standalone=\"no\"?>
<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" 
  \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">
")
(setf svg-ns "http://www.w3.org/2000/svg")

(defun* ray-intersection ((p1x p1y) (d1x d1y)
                          (p2x p2y) (d2x d2y))
  "Find the intersection point of two rays, where p1 and p2 are the origins
of the rays, and d1 and d2 are the direction vectors."
  (let:: delx (- p2x p1x) dely (- p2y p1y)
         det (- (* d2x d1y) (* d1x d2y))
         s (/ (- (* dely d2x) (* delx d2y)) det)
         ts (/ (- (* d1x dely) (* d1y delx)) det) in
    (list (+ p1x (* d1x s))
          (+ p1y (* d1y s)))))

(defun midpoint (a b)
  (v* 0.5 (v+ a b)))

(defun normalize (v)
  (v* (/ 1 (sqrt (vdot v v))) v))

(defun vdot (a b) (reduce '+ (mapcar* '* a b)))

(defun angle-bisector (O P Q)
  (let: p (v+ O (normalize (v- P O)))
        q (v+ O (normalize (v- Q O)))
        m (midpoint p q) in
    (v- m O)))

(defun* vsin (a b)
  (let:: (ax ay) a
         (bx by) b in
    (/ (- (* ax by) (* ay bx))
       (sqrt (vdot a a))
       (sqrt (vdot b b)))))

(defun* v-angle ((ax ay) (bx by))
  (atan (- (* ax by) (* ay bx))
        (+ (* ax bx) (* ay by))))

(defun* ortho-vec ((x y))
  (list (- y) x))

(defun svg-pathcmd (cmd &rest args)
  (str cmd (strjoin "," (mapcar 'str args))))

(defun svg-pathcmds (cmds)
  (mapconcat (cur 'apply 'svg-pathcmd) cmds " "))

(defun fillet-lines (o p q r)
  "o is the intersection, p and q are the two endpoints, r is the fillet radius."
  (let:: v (angle-bisector o p q)
         d (/ r (vsin (v- p o) v))
         c (v+ (v* (abs d) (normalize v)) o)
         (cx cy) c
         p1 (ray-intersection c (ortho-vec p) o (v- p o))
         q1 (ray-intersection c (ortho-vec q) o (v- q o))
         sweep (>>> (vsin (v- p1 c) (v- q1 c))
                    (if (< _ 0) 0 1)) in
    `(([circle cx ,cx cy ,cy r 1 fill "rgb(0,0,0)"])
      ([path fill none stroke "rgb(0,0,0)"
             d ,(svg-pathcmds `((M ,@p) (L ,@p1) (A ,r ,r 0 0 ,sweep ,@q1) (L ,@q)))])
      ([path fill none stroke "rgba(255,0,0,1.0)"
             d ,(svg-pathcmds `((M ,@o) (L ,@p1)
                                (M ,@o) (L ,@q1)))]))))

(defun svg-doc (w h &rest elements)
  (str svg-header (markup `([svg width ,w height ,h] ,@elements))))

(defun insert-svg (svg-data)
  (>>> `(image :type svg :data ,svg-data)
       (propertize " " 'display _) insert))

(provide 'geometry)
