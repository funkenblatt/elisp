(defun* c-type->str (type-spec &optional (identifier ""))
  (pcase type-spec
    (`(array ,elt-type . ,size)
     (c-type->str elt-type (str identifier "[" (or size "") "]")))
    (`(pointer ,type) (c-type->str type (str "*" identifier)))
    (`(const ,type) (c-type->str type (str "const " identifier)))
    (`(function ,return-type <- . ,arg-types)
     (>>> (mapcar (cut c-type->str <> "") arg-types) (strjoin ", " _)
          (interp "(#,identifier)(#,_)") (c-type->str return-type _)))
    ;; Function with named args
    (`(function* ,return-type <- . ,arg-types)
     (>>> (sublist-n arg-types 2) (mapcar (cur 'apply 'c-type->str) _) 
          (strjoin ", " _) (interp "(#,identifier)(#,_)")
          (c-type->str return-type _)))
    ((pred consp) (error "Invalid type spec"))
    (x (str x (if (blankp identifier) ""
                (str " " identifier))))))

(defun c-func (return-type name arg-names-and-types body)
  (str (c-type->str
        return-type
        (str (interp "(#,name)")
             "(" (strjoin ", " 
                          (mapcar (cut apply 'c-type->str <>)
                                  (sublist-n arg-names-and-types 2)))
             ")"))
       "\n{\n" body "\n}\n"))
