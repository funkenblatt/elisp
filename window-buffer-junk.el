(defun window-switch ()
  (interactive)
  (let: windows (apply 'append (lc (window-list x) x (frame-list))) 
	choice (ido-completing-read "Window: "
				(mapcar
				 (funcomp buffer-name window-buffer)
				 windows))
	result (lc x x windows (eq (get-buffer choice)
				   (window-buffer x))) in
    (when result 
      (select-frame-set-input-focus 
       (window-frame (car result)))
      (select-window (car result)))))

(defun modified-buffers ()
  (lc x x (buffer-list)
      (and (not (startswith (buffer-name x) " "))
	   (buffer-modified-p x))))

(defun buffers-with-my-mode ()
  (lc x x (buffer-list)
      (eq (buffer-mode x)
	  (buffer-mode (current-buffer)))))

(jason-defkeys 
 (current-global-map)
 (kbd "C-x w") 'window-switch)

(provide 'window-buffer-junk)
