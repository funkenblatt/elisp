;; -*- lexical-binding: t; -*-

(require 'el2)

(defun svg-propertize (svg-data)
  (>>> `(image :type svg :data ,svg-data)
       (propertize " " 'display _)))

(el2
 (defun yrange (low high thunk)
   (lambda (gp)
     (gp "set yrange [" low ":" high "]")
     (thunk gp)))

 (defun xrange (low high thunk)
   (lambda (gp)
     (gp "set xrange [" low ":" high "]")
     (thunk gp)))

 (defun trange (low high thunk)
   (lambda (gp)
     (gp "set trange [" low ":" high "]")
     (thunk gp)))

 (defun to-plot-infix (expr)
  (pcase expr
    (`(- ,x) (str "(-" (to-plot-infix x) ")"))
    (`(,(and (or '+ '* '- '/) op) . ,terms) (str "(" (strjoin (str op) (mapcar 'to-plot-infix terms)) ")"))
    (`(expt ,a ,b) (str "(" (to-plot-infix a) "**" (to-plot-infix b) ")"))
    (`(,func . ,args) (str func "(" (strjoin ", " (mapcar 'to-plot-infix args)) ")"))
    ((or (pred symbolp) (pred numberp)) (str expr))))

 (defun parametric (&rest equations)
   (lambda (gp)
     (gp "set parametric")
     (gp "plot " (strjoin ", " (mapcar 'to-plot-infix equations)))))

 (defun polar (&rest equations)
   (lambda (gp)
     (gp "set polar")
     (gp "plot " (strjoin ", " (mapcar 'to-plot-infix equations)))))

 (defun funplot (&rest equations)
   (lambda (gp)
     (gp "plot " (strjoin ", " (mapcar 'to-plot-infix equations)))))
 
 (defun plot-xy (data columns)
   "Plot data given in the form (x y1 y2 ...)"
   (lambda (gp)
     (>>>
      (mapcar (x: (interp "\"-\" with lines title #,(repr (str x))")) columns)
      (strjoin ", " _)
      (str "plot " _)
      gp)
     (let: i 1 in
       (dolist (c columns)
         (dolist (row data)
           (gp (str (car row) " " (nth i row))))
         (gp "e")
         (incf i))))))

(el2
 (defun gnuplot (thunk)
   (let: process-connection-type nil
         proc (jf-proc "gnuplot" (comp 'insert 'svg-propertize))
         gp (lambda (&rest s) (process-send-string proc (apply 'str `(,@s "\n")))) in

     (gp "set term svg size 600, 570")
     (ignore-errors (thunk gp))
     (process-send-eof proc))))

(provide 'plot)
