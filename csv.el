;; -*- lexical-binding: t; -*-

(defun read-csv-line (line)
  (let (out)
    (with-temp-buffer 
      (insert line) (goto-char (point-min))
      (ignore-errors
        (while t
          (push (read-csv-column) out)
          (forward-char)))
      (nreverse out))))

(defun read-csv-column ()
  (re-search-forward 
   (rx (or (: "\"" (* (not (any "\""))) "\"")
           (: (* (not (any ","))))))
   nil t) 
  (>>> (match-string 0)
       (if (startswith _ "\"")
           (read _)
         _)))

(provide 'csv)
