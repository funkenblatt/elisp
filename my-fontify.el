;; This is like htmlfontify, except less featureful (doesn't look for overlays)
;; and also less retarded (doesn't do weird, sticking tags within other tags shit).
;; It also seems to run a lot faster.

(setq css-properties
      '((:foreground :color t :css-name color)
        (:background :color t :css-name background)
        (:weight :css-name font-weight)))

(defun get-face-color (color)
  (if (or (symbolp color) (startswith color "#"))
      color
    (apply 'concat "#" (lc (format "%02x" (/ (* x 255) 65280)) 
                           x (color-values color)))))

(defun css-prop-get (property attribute)
  (do-wad (cdr (assq property css-properties))
          (plist-get _ attribute)))

(defun faceprop (face prop)
  (do-wad (face-attribute face prop)
          (cond ((or (eq _ 'unspecified) (not _)) nil)
                ((css-prop-get prop :color) (get-face-color _))
                (t _))))

(defun cssify-face (face)
  (let: inherited (do-wad (faceprop face :inherit) (if _ (cssify-face _) nil))
        properties (lc (list (css-prop-get x :css-name)  (faceprop face x))
                       x '(:foreground :background :weight)
                       (faceprop face x))
        css (apply 'append properties) in
    (append inherited css)))

(defun make-face-style (face)
  (apply 'make-css (str "." (symbol-name face))
         (cssify-face face)))

(defun check-inline-style (face-spec tab)
  (if (keywordp (car face-spec))
      (let: tmpface (gensym) in
        (make-face tmpface)
        (apply 'set-face-attribute tmpface nil face-spec)
        (puthash tmpface t tab)
        tmpface)
    nil))

(defun get-buffer-markup (facetab)
  "Go through a buffer, converting any \"face\" text properties into HTML span tags.
Puts any face names it encounters into FACETAB, which you should probably keep.  Returns
the HTML markup that represents the buffer contents."
  (let: inhibit-point-motion-hooks t in
    (with-output-to-string
      (pr "<pre>\n")
      (goto-char (point-min))
      (while (< (point) (point-max))
        (let: start (point) 
              facewad (or (get-text-property (point) 'face) 'default)
              end (progn (goto-char (or (next-single-property-change (point) 'face) (point-max)))
                         (point)) in
          (pr "<span class=\"" 
              (if (consp facewad) 
                  (or (check-inline-style facewad facetab)
                      (progn (ec (puthash face t facetab) face facewad)
                             (strjoin " " (mapcar 'symbol-name facewad))))
                (puthash facewad t facetab)
                facewad)
              "\">"
              (replace-regexp-in-string 
               "[<>\"&]" 
               (lambda (c) (get-assoc '(("<" . "&lt;") (">" . "&gt;")
                                        ("\"" . "&quot;") ("&" . "&amp;")) c))
               (buffer-substring start end))
              "</span>")))
      (pr "</pre>\n"))))

(defun buffer->html (&optional outfile)
  "Returns a complete HTML page that displays the contents of a particular buffer.  Dumps the markup
into OUTFILE."
  (let: faces (make-hash-table)
        output (get-buffer-markup faces) 
        title (buffer-name) in
    (with-temp-buffer
      (insert
       (markup
        `(html
          (head (title ,title)
                ((meta http-equiv Content-Type content "text/html; charset=UTF-8"))
                ([style type "text/css"]
                 ,(apply 'concat (mapcar (funcomp make-face-style car) 
                                         (hash->list faces)))))
          (body
           ,output))))
      (if outfile
          (write-region (point-min) (point-max) outfile)
        (buffer-string)))))

(defun my-fontify-handler (req-parts headers body cont)
  (let: fail (lambda ()
               (funcall cont
                        "404 Not Found"
                        '((Content-Type . "text/plain"))
                        "Derps"))
    in
    (pcase (cadr req-parts)
      ((str "/color/" x)
       (>>> (urldecode x)
            (if (get-buffer x)
                (funcall cont
                         "200 OK"
                         '((Content-Type . "text/html"))
                         (w/buf x (buffer->html)))
              (funcall fail))))
      (_ (funcall fail)))))

(defun setup-fontify-handler ()
  (interactive)
  (push '("^/color/*" . my-fontify-handler)
        fooserve-2-handlers))

(provide 'my-fontify)
