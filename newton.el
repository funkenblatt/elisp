(defun quadratic-iter (a b c guess)
  (rpn a guess guess * * c -
       2 a * guess * b +
       /))

(defun solve-quad (a b c x iters)
  (while (> iters 0)
    (setq x (quadratic-iter a b c x))
    (setq iters (- iters 1)))
  x)
