;; -*- lexical-binding: t; -*-

(defun hadoop-ls (path cont)
  (jf-shellproc
   (interp "hadoop fs -ls #,(shell-quote-argument path)")
   (x: (>>> x (split-string _ "\n" t) cdr (mapcar 'split-string _)
            (funcall cont _)))))

(defun hadoop-dirp (ls-entry)
  (startswith (car ls-entry) "d"))

(defun hadoop-show-head (filename)
  (jf-shellproc 
   (interp "hadoop fs -cat #,(shell-quote-argument filename) | head -n 100")
   (x: (with-output-to-temp-buffer (interp "*hadoop file head*")
         (with-current-buffer standard-output 
           (insert (string-as-multibyte x)))))))

(defun hadoop-show-tail (filename)
  (jf-shellproc 
   (interp "hadoop fs -tail #,(shell-quote-argument filename)")
   (x: (with-output-to-temp-buffer (interp "*hadoop file head*")
         (princ x)))))

(defun hadoop-dump-entry (txt info)
  (>>>
   (if (hadoop-dirp info)
       (keycmd (hadoop-show-dir (lastcar info)))
     (keycmd (if current-prefix-arg 
                 (hadoop-show-tail (lastcar info))
               (hadoop-show-head (lastcar info)))))
   (mklink txt _ 'default)
   (insert _ "\n")))

(defun hadoop-up-dir (path)
  (do-wad
   (split-string path "/")
   butlast (strjoin "/" _)))

(defun hadoop-show-dir (path)
  (interactive "sPath: ")
  (cps> entries (hadoop-ls path :cont!)
    (with-output-to-temp-buffer "*hadoop-fs*"
      (with-current-buffer standard-output
        (princ (str "Listing " path ":\n"))
        (insert (mklink ".." (keycmd (hadoop-show-dir (hadoop-up-dir path)))) "\n")
        (setf revert-buffer-function (lambda (&rest junk) (hadoop-show-dir path)))
        (do-wad 
         (print-tabular entries "  ")
         (split-string _ "\n" t)
         (mapcar* 'hadoop-dump-entry _ entries))))))

(provide 'hadoop-junk)
