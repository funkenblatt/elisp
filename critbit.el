(defstruct critbit left right index mask)

(defun critbit-has (tree key)
  (cond ((not tree) nil)
        ((stringp tree) (equal tree key))
        (t (if (= (logand (aref key (critbit-index tree))
                          (critbit-mask tree)) 0)
               (critbit-has (critbit-left tree) key)
             (critbit-has (critbit-right tree) key)))))

(defun first-bit (num)
  (loop for i from 7 downto 0
        while (= (logand num (ash 1 i)) 0)
        finally return (ash 1 i)))

(defun split-point (s1 s2)
  "Return the byte index and bit mask at which s1 and s2 differ."
  (let: minlen (min (length s1) (length s2))
        longer (if (> (length s1) (length s2)) s1 s2) in
    (loop for i from 0 below minlen
          while (= (aref s1 i) (aref s2 i))
          finally return
          (if (= i minlen) 
              (cons i (first-bit (aref longer minlen)))
            (cons i (first-bit (logxor (aref s1 i) (aref s2 i))))))))

(defun critbit-branch (tree key &optional branch)
  (let: which (logand (if (< (critbit-index tree) (length key))
                          (aref key (critbit-index tree))
                        0)
                      (critbit-mask tree)) in
    (if branch (if (= which 0) (critbit-left tree) (critbit-right tree)) which)))

(defun critbit-insert (tree key)
  (cond
   ((not tree) key)
   ((stringp tree) (let: ix-bit (split-point tree key)
                         out (make-critbit :index (car ix-bit) :mask (cdr ix-bit))
                         which (critbit-branch out key) in
                     (setf (critbit-left out) (if (= which 0) key tree)
                           (critbit-right out) (if (= which 0) tree key))
                     out))
   (t (let: result (critbit-insert (critbit-branch tree key t) key)
            which (critbit-branch tree key) in
        (if (or (< (critbit-index result) (critbit-index tree))
                (and (= (critbit-index result) (critbit-index tree))
                     (> (critbit-mask result) (critbit-mask tree))))
              (progn
                (setq which (critbit-branch result key))
                (make-critbit 
                 :left (if (= which 0) key tree)
                 :right (if (= which 0) tree key)
                 :index (critbit-index result) :mask (critbit-mask result)))
          (make-critbit
           :index (critbit-index tree)
           :mask (critbit-mask tree)
           :left (if (= which 0) result (critbit-left tree))
           :right (if (= which 0) (critbit-right tree) result)))))))
        