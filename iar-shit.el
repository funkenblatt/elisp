(defun projfind (s)
  "Grep for a particular string within the fileset of an IAR project"
  (interactive "sSearch string: ")
  (if (equal s "") (setq s (thing-at-point 'symbol)))
  (with-default-dir project-directory
    (if (lc x x (directory-files ".") (endswith x ".ewp"))
	(grep (format "projfind.py %s" (shell-quote-argument s))))))

(defun get-project-files ()
  (with-default-dir project-directory
    (process-lines "projfiles.py")))

(provide 'iar-shit)
