(require 'http-client)
(require 'json)

(defun init-mozrepl ()
  (setq mozrepl (connect "localhost" 4242))
  (setf (process-filter mozrepl)
        (lambda (p s)
          (unless (str~ "Component returned failure code" s)
            (pr s)))))

(init-mozrepl)

(defun browse-url-mozrepl (url &optional balls)
  (let: url (substring-no-properties url) in
    (pr url "\n")
    (process-send-string mozrepl (str "content.location.href = " (repr url) "\n"))))

(setq browse-url-browser-function 'browse-url-mozrepl)

(defun wiki (term)
  (interactive (list (if (region-active-p) (get-region) (thing-at-point 'word))))
  (let: term (if (region-active-p)
                 (get-region)
               (thing-at-point 'word)) in
    (browse-url
     (str "http://en.wikipedia.org/wiki/"
          (replace-regexp-in-string " " "_" (capitalize term))))))

(global-set-key (kbd "<S-return>") 'browse-url-at-point)

(defun mozrepl-lisppaste (stuff)
  (interactive (list (buffer-substring-no-properties
                      (region-beginning) (region-end))))
  (process-send-string
   mozrepl
   (str "content.document.getElementsByName('text')[0].value = "
        (json-encode stuff)
        "\n")))

(defun make-file-url (filename)
  (interp "file://#,(expand-file-name filename)"))

(defun browse-buffer ()
  (interactive)
  (browse-url (make-file-url buffer-file-name)))

(defun browse-buffer-htmlized ()
  (interactive)
  (require 'my-fontify)
  (let: bufname (str (make-temp-file "buf") ".html") in
    (buffer->html bufname)
    (browse-url (make-file-url bufname))))

(global-set-key (kbd "C-x j m e") (keycmd (process-send-string mozrepl "repl.enter(content);\n")))
(global-set-key (kbd "C-x j m w") (keycmd (process-send-string mozrepl "repl.whereAmI();\n")))
(global-set-key (kbd "C-x j m b") (keycmd (process-send-string mozrepl "repl.back();\n")))
(global-set-key (kbd "C-x j m E") (keycmd (process-send-string mozrepl (get-region))))
(global-set-key (kbd "C-x j m k") (keycmd (process-send-string mozrepl ";\n")))

(provide 'mozrepl)
