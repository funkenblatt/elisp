(defstruct avl data l r height)

(defmacro avl-chain (tree &rest ops)
  `(compcall ,(reverse ops) ,tree))

(defsubst avl-bal (tree &optional newval)
  (if tree (avl-height tree newval) 0))

(defun avl-transfer (dst src)
  (dotimes (i 4)
    (aset dst i (aref src i))))

(defun avl-bfactor (tree)
  (- (if (avl-r tree) (avl-bal (avl-r tree)) 0)
     (if (avl-l tree) (avl-bal (avl-l tree)) 0)))

(defsubst avl-mknod (data l r)
  (avl data l r
       (+ 1 (max (avl-bal l) (avl-bal r)))))

(defun avl-check (tree &optional tmp)
  (cond ((> (avl-bfactor tree) 1)
	 (if (= (avl-bfactor (avl-r tree)) 1)
	     (setq tmp (avl-mknod (avl-data (avl-r tree))
				  (avl-mknod (avl-data tree) (avl-l tree) (avl-l (avl-r tree)))
				  (avl-r (avl-r tree))))
	   (setq tmp (avl-mknod (avl-data (avl-l (avl-r tree)))
				(avl-mknod (avl-data tree) (avl-l tree) (compcall (avl-l avl-l avl-r) tree))
				(avl-mknod (avl-data (avl-r tree)) (compcall (avl-r avl-l avl-r) tree) (avl-r (avl-r tree)))))))
	((< (avl-bfactor tree) -1)
	 (if (= (avl-bfactor (avl-l tree)) -1)
	     (setq tmp (avl-mknod (avl-data (avl-l tree))
			    (avl-l (avl-l tree))
			    (avl-mknod (avl-data tree) (avl-r (avl-l tree)) (avl-r tree))))
	   (setq tmp (avl-mknod (avl-data (avl-r (avl-l tree)))
				(avl-mknod (avl-data (avl-l tree)) (avl-l (avl-l tree)) (compcall (avl-l avl-r avl-l) tree))
				(avl-mknod (avl-data tree) (compcall (avl-r avl-r avl-l) tree) (avl-r tree)))))))
  (if tmp (avl-transfer tree tmp))
  (not tmp))

(defun avl-insert (tree item cmp)
  (let: c (funcall cmp item (avl-data tree)) 
	action (lambda (fun nfun c)
		 (if (funcall fun tree)
		     (progn (avl-insert (funcall fun tree) item cmp)
			    (avl-bal tree (+ 1 (max (avl-bal (avl-l tree)) (avl-bal (avl-r tree)))))
			    (avl-check tree))
		   (funcall fun tree (avl item nil nil 0))
		   (if (not (and (avl-l tree) (avl-r tree)))
		       (avl-bal tree (+ (avl-bal tree) 1)))
		   (not (and (avl-l tree) (avl-r tree))))) in
    (cond ((= c 0) nil)
	  ((< c 0) (funcall action 'avl-l 'avl-r -1))
	  ((> c 0) (funcall action 'avl-r 'avl-l 1)))))

(defun avl-flatten (tree)
  `(,@(if (avl-l tree) (avl-flatten (avl-l tree)))
    ,(avl-data tree)
    ,@(if (avl-r tree) (avl-flatten (avl-r tree)))))

(defun avl-dump (tree &optional indent)
  (if (not indent) (setq indent 0))
  (when tree
    (dump (str* "  " indent) (avl-data tree) " (" (avl-bal tree) ")" "\n")
    (avl-dump (avl-l tree) (+ indent 1))
    (avl-dump (avl-r tree) (+ indent 1))))

(defun strcmp (a b)
  (cond ((string< a b) -1)
	((equal a b) 0)
	(t 1)))

(defun numcmp (a b)
  (- a b))

(setq foo (avl 10 nil nil 0))
(ec (avl-insert foo x '-) x (range 40000))

