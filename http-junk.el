;; -*- lexical-binding: t; -*-

(defun http-wad-filter (p s)
  (scratchdump s))

(defun http-wad (host uri &optional port)
  (foolet (p (make-network-process 
	      :name "http-client" :buffer nil
	      :host host :service (if port port 80) :filter 'http-wad-filter))
    (process-send-string p (format "GET %s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n" uri host))))

(defun replace-strmatch-fun (s regexp fun)
  (with-temp-buffer
    (insert-before-markers s)
    (goto-char (point-min))
    (while (re-search-forward regexp nil t)
      (replace-match (funcall fun) nil t))
    (buffer-string)))

(defun urldecode (s)
  (replace-strmatch-fun 
   s "\\+\\|%\\([0-9a-fA-F][0-9a-fA-F]\\)"
   (lambda () 
     (if (equal (match-string 0) "+") 
         " "
       (char-to-string (read (concat "#x" (match-string 1))))))))

(defun urlencode (s)
  (replace-strmatch-fun
   s "[^-a-zA-Z0-9_/.~!*'()]"
   (lambda () 
     (if (equal (match-string 0) " ")
         "+"
       (format "%%%02x" (string-to-char (match-string 0)))))))

(defun urlencode-alist (alist)
  (strjoin 
   "&" (lc (str (car x) "=" (urlencode (str (cdr x))))
           x alist)))

(defun urldecode-alist (param-string)
  (do-wad (split-string param-string "&" t)
          (lc (apply 'cons (cdr (re-search "\\([^=]+\\)=\\(.*\\)" x))) x _)
          (mapcar (consmap (urldecode a) (urldecode b)) _)))

(provide 'http-junk)
