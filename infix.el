;; Infix junk.
;; Caveats: Any variable names that contain any of these
;; operators shown below will get totally hosed.  You'll need
;; to deal with those specially somehow.

(setq infix-operators '(//= -2 += -2 -= -2 *= -2 := -2 :: -2 |= -2 &= -2
			    -> 7 => 7 
			    < 3 = 3 > 3  <= 3 >= 3 
			    + 4 - 4 | 4
			    * 5 / 5 % 5 & 5
			    ^ -6)
      infix-operator-strings
      (mapconcat
       (comp 'regexp-quote 'symbol-name 'car)
       (seq-partition infix-operators 2)
       "\\|"))

(mapc
 (lambda (x)
   (let ((sym (intern (concat (symbol-name x) "="))))
     (eval
      `(defmacro ,sym (obj val)
         `(:= ,obj (,',x ,obj ,val))))))
 '(// + - * | &))

;; Retrieve the precedence of an operator.
(defun get-prec (sym) (abs (cadr (memq sym infix-operators))))
(defun get-ifx-assoc (sym) (< (cadr (memq sym infix-operators)) 0))
(defun operator? (x) (and (symbolp x) (memq x infix-operators)))

(defun infix-preprocess (l)
  (let ((i (car l)))
    (cond2
     ((null l) nil)
     ((not (symbolp i)) (cons i (infix-preprocess (cdr l))))
     (t (let ((splits (filter (lambda (x) (not (equal x "")))
			       (re-splitwad infix-operator-strings (symbol-name i)))))
	  (if splits
	      (append (mapcar 'read splits) (infix-preprocess (cdr l)))
	    (cons i (infix-preprocess (cdr l)))))))))

(defun infix-parse (l)
  (let: l (infix-preprocess l) out nil stk nil prev nil in
    (dolist (i l)
      (cond2
       (operator? i) (progn
		       (while (and stk 
				   (if (get-ifx-assoc i)
				       (< (get-prec i) (get-prec (car stk)))
				     (<= (get-prec i) (get-prec (car stk)))))
			 (push (pop stk) out))
		       (push i stk))
       (vectorp i) (setq out (append (nreverse (infix-parse (vec->lst i))) out))
       t (push i out)))
    (reverse (append (reverse stk) out))))

(defmacro ifix (&rest expr)
  (car (rpn-parse (infix-parse expr))))

(defmacro ifix* (&rest exprs)
  (let: exprwad (lsplit exprs ':) out (list 'progn) in
    (while (cadr exprwad)
      (push `(ifix ,@(car exprwad)) out)
      (setq exprwad (lsplit (cadr exprwad) ':)))
    (if (car exprwad)
	(push `(ifix ,@(car exprwad)) out))
    (nreverse out)))

(defun hashlookup (obj key) (gethash key obj))
(defun hashset (obj key val) (puthash key val obj))
;; Generalized item lookup.  Dispatches on type.
(setq lookup-methods
      (alist->dict `((hash-table . hashlookup)
		     (vector . aref)
		     (string . aref)
		     (cons . elt))))

(defun => (obj key)
  (funcall (gethash (type-of obj) lookup-methods) obj key))

(setq itemset-methods			;No itemset for lists.
      (alist->dict 			;I'm not a big fan of mutable conses.
       `((hash-table . hashset)
	 (vector . aset)
	 (string . aset))))

(defun fancy-set (obj key value)
  (funcall (gethash (type-of obj) itemset-methods) obj key value))

(defmacro := (target value)
  (if (listp target)
      (if (eq (car target) '=>)
	  `(fancy-set ,@(cdr target) ,value)
	`(aset ,@(cdr target) ,value))
    `(setq ,target ,value)))

(defalias '^ (symbol-function 'expt))
(defalias '$ 'ifix*)
(defalias '| 'logior)
(defalias '& 'logand)
(defalias '! 'lognot)
(defalias '// (symbol-function '/))
(defalias '-> (symbol-function 'aref))
(defalias ':: (symbol-function 'append))
(defalias '~ 'progn)
(provide 'infix)
