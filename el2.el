;; -*- lexical-binding: t; -*-
;; EL2 - The EL-ening!
;; Sort of wedges Lisp-1-ness into elisp while still
;; mostly cooperating with existing elisp code.

(defun macro-p (sym)
  (and (fboundp sym)
       (pcase (symbol-function sym)
         (`(macro . ,junk) t)
         ((and (pred symbolp) s) (macro-p s))
         (x nil))))

(defun el2-bound (sym env) (member sym env))

(defun el2-add-bindings (arglist env)
  (>>> 
   arglist
   (filter (comp 'not (cut member <> '(&rest &optional &aux &key))) _)
   (append _ env)))

(defun el2-compile (expr env)
  (pcase expr
    (`(lambda ,args . ,body)
     (let ((new-bindings (el2-add-bindings args env)))
       `(lambda ,args ,@(mapcar (cut el2-compile <> new-bindings) body))))

    (`(defun ,name ,args . ,body)
     (let ((new-bindings (el2-add-bindings args env)))
       `(defun ,name ,args ,@(mapcar (cut el2-compile <> new-bindings) body))))
    
    (`(quote ,stuff) `',stuff)
    
    (`(let ,bindings . ,body)
     (let ((new-env (el2-add-bindings 
                     (mapcar (orfn (maybe 'car 'consp) 'identity) bindings)
                     env)))
       `(let ,(lc (if (consp x) 
                      `(,(car x) ,(el2-compile (cadr x) env))
                    x) x bindings)
          ,@(mapcar (cut el2-compile <> new-env) body))))

    (`(let* nil . ,body)
     (el2-compile `(progn ,@body) env))

    (`(let* ((,var ,expr) . ,more-bindings) . ,body)
     (el2-compile 
      `(let ((,var ,expr)) 
         (let* ,more-bindings ,@body))
      env))

    (`(cond (,pred . ,consequent) . ,clauses)
     (el2-compile `(if ,pred (progn ,@consequent) 
                     ,@(if clauses `((cond ,@clauses)) nil))
                  env))

    (`(,args -> . ,body)
     (el2-compile `(lambda ,args ,@body) env))

    (`(,(and (pred symbolp) sym) . ,args)
     (cond
      ((or (el2-bound sym env) (and (boundp sym) (not (fboundp sym)))) 
       `(funcall ,sym ,@(mapcar (cut el2-compile <> env) args)))
      ((macro-p sym) (el2-compile (macroexpand expr) env))
      (t `(,sym ,@(mapcar (cut el2-compile <> env) args)))))

    (`(,fun . ,args)
     `(funcall ,(el2-compile fun env) ,@(mapcar (cut el2-compile <> env) args)))

    ((and (pred vectorp) v) (el2-compile `(vector ,@(append v nil)) env))
    
    (x x)))

(defvar el2-base-env nil)

(defmacro el2 (&rest stuff)
  `(progn ,@(mapcar (cut el2-compile <> el2-base-env) stuff)))

(defun el2-eval-last-sexp ()
  (interactive)
  (let: result (eval `(el2 ,(preceding-sexp)) lexical-binding) in
    (if current-prefix-arg
        (insert (pp-to-string result))
      (pp result))))

(define-key lisp-interaction-mode-map (kbd "C-x M-e") 'el2-eval-last-sexp)

(provide 'el2)
