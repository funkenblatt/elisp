;; -*- lexical-binding: t; -*-

(defun b-reduce* (expr name val)
  (pcase expr
    (`(λ ,x ,body)
     (if (eq x name)
         expr
       `(λ ,x ,(b-reduce* body name val))))
    (`(,f ,x)
     `(,(b-reduce* f name val)
       ,(b-reduce* x name val)))
    (x (if (eq x name) val x))))

(defun b-reduce (expr)
  (pcase expr
    (`((λ ,v ,body) ,x)
     (b-reduce* body v x))
    (x x)))

(defun b-reduce-bind ()
  (interactive)
  (local-set-key (kbd "C-c C-b")
    (keycmd
     (replace-last-sexp
      (repr (b-reduce (preceding-sexp)))))))

(provide 'lambda-calculus)
