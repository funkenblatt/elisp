;; -*- lexical-binding: t; -*-

(defun clojure-change-brackets (bracket-type)
  "Change the brackets of preceding s-exp to bracket type."
  (lambda ()
    (interactive)
    (let: bounds (save-excursion 
                   (list (progn (backward-sexp) (point))
                         (progn (forward-sexp) (point))))
          text (apply 'buffer-substring bounds)
          left (substring bracket-type 0 1)
          right (substring bracket-type 1 2) in
      (when (str~ (rx bos (any "({[") (* anything) (any "})]") eos) text)
        (apply 'delete-region bounds)
        (goto-char (car bounds))
        (insert (str left (substring text 1 -1) right))))))

(defun clojure-break-lines (&optional n)
  (interactive "p")
  "Insert line breaks after all sexps at the current level"
  (paredit-backward-up)
  (paredit-forward-down)
  (let: newline (or n 1) in
    (while (ignore-errors (forward-sexp) t)
      (unless (save-excursion
                (unless (looking-at (rx (+ space)))
                  (backward-sexp))
                (str~ (rx bos "#" (* space) (not (any "{"))) (thing-at-point 'sexp)))
        (decf newline))
      (unless (or (looking-at (rx (* space) eol)) (> newline 0))
        (if (looking-at ",") (forward-char))
        (insert "\n")
        (indent-for-tab-command))
      (if (= newline 0) (setf newline (or n 1))))))

(put 'nlet 'clojure-indent-function 2)
(put 'cond-> 'clojure-indent-function 0)

(jason-defkeys clojure-mode-map (kbd "C-c C-v") (clojure-change-brackets "[]"))
(jason-defkeys clojure-mode-map (kbd "C-c C-l") (clojure-change-brackets "()"))
(jason-defkeys clojure-mode-map (kbd "C-x j RET") 'clojure-break-lines)

(add-hook 'clojure-mode-hook 'paredit-mode)

(add-to-list 'auto-mode-alist '("\\.cljc$" . clojurec-mode))

(provide 'clojure-hacks)
