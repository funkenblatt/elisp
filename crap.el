;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(defun prbuf (x)
  (princ x (current-buffer)))

(setq fun-typedef-counter 0)

(defun join (l s)
  (let ((x '()))
    (dolist (i l)
      (push i x)
      (push s x))
    (pop x)
    (apply 'concat (reverse x))))

(defun array (x) (format "%s[]" x))

(defun funtype (rtype args)
  (progn
    (prbuf
     (format "typedef %s (*foofunc%03d)(%s);" rtype fun-typedef-counter args))
    (inc fun-typedef-counter 1)
    (format "foofunc%03d" (- fun-typedef-counter 1))))

(defmacro inc (x i) `(setq ,x (+ ,x ,i)))

(funtype "int" (join (list "int" (array (funtype "int" "")) "int *") ", "))
;typedef int (*foofunc002)();typedef int (*foofunc003)(int, foofunc002[], int *);

