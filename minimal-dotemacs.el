(defun jason-window-switch (fun)
  (let ((win (funcall fun nil nil (display-graphic-p))))
    (if (not (eq (window-frame win) (selected-frame)))
	(select-frame-set-input-focus (window-frame win)))
    (select-window win)))

(defun jason-next-window ()
  (interactive)
  (jason-window-switch 'next-window))

(defun jason-prev-window ()
  (interactive)
  (jason-window-switch 'previous-window))

(defun jf-hungry-del-back ()
  "Basically the same as c-hungry-delete, only doesn't
require loading the rest of cc-cmds.el which is kind of big"
  (interactive)
  (let ((here (point)))
    (skip-chars-backward " \t\n\r\f\v")
    (if (/= (point) here) 
	(delete-region (point) here)
      (backward-delete-char-untabify 1))))

(global-set-key [?\M-.] 'jason-next-window)
(global-set-key [?\M-,] 'jason-prev-window)

(global-set-key (kbd "C-x DEL") 'backward-kill-sexp)
(global-set-key (kbd "C-x j DEL") 'jf-hungry-del-back)

(add-hook
 'clojure-mode-hook
 (lambda ()
   (local-set-key (kbd "C-x C-e") 'jf-eval-clojure)))

(defun get-last-sexp (&optional func)
  (buffer-substring
   (save-excursion 
     (funcall (or func 'backward-sexp)) 
     (point))
   (point)))

(defun jf-eval-clojure ()
  (interactive)
  (process-send-string 
   clojure-process
   (concat (get-last-sexp) "\n")))

(defun set-clojure-process ()
  (interactive)
  (setq clojure-process (get-buffer-process (current-buffer))))
