(defun get-makefile-var (var)
  (with-temp-buffer
    (insert-file-contents-literally "Makefile")
    (re-search-forward (rx-to-string `(: bol ,var (* (any ?  ?\t)) ":=")))
    (let: items nil
	  line (buffer-substring-no-properties (point) (line-end-position)) in
      (while (string-match "\\\\[ \t]*\n?$" line)
	(ec (push x items) x (split-string line) (not (equal x "\\")))
	(forward-line)
	(setq line (thing-at-point 'line)))
      (ec (push x items) x (split-string line) (not (equal x "\\")))
      items)))

(defun get-makefile-c-files () (get-makefile-var "FILES"))

(defun get-makefile-includes ()
  (lc (let: s (substring x 2) in
	(if (endswith s "/")
	    s
	  (str s "/")))
      x (get-makefile-var "INCLUDE_DIRS")))

(defun get-project-files ()
  (with-default-dir project-directory
    (apply 'append
	   (get-makefile-c-files)
	   (lc (lc (str x y) y (directory-files x nil "\\.h$"))
	       x (get-makefile-includes)))))

(defun get-dependencies ()
  (do-wad
   (lc x x (split-string (get-region))
       (endswith x ".h"))
   (ec (scratchdump x "\n") x 
       (delete-duplicates 
	(mapcar 'file-name-directory _)
	:test 'equal))))

(defun makefile-open-include ()
  (let: fn (assoc (thing-at-point 'filename)
		  (lc (cons (file-name-nondirectory x) x)
		      x (get-project-files))) in
    (if fn (find-file (cdr fn)))))

(setq open-include-func 'makefile-open-include)

	
