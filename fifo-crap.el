;; -*- lexical-binding: t; -*-
;; Two different methods of creating a pipe-like string buffering thing
;; for use with asynchronous processes.  One uses a CL defstruct, the other
;; uses CL's lexical-let to store local state.  The defstruct one will
;; probably be faster.
(require 'structmeth)

(defun string-fifo ()
  (let: q-head (cons "" nil)
        q-tail q-head
        bytes 0 
        ix 0 in
    (vector
     (lambda (s)			;Add a string to the queue.
       (setf (cdr q-tail) (cons s nil)
	     q-tail (cdr q-tail))
       (incf bytes (length s)))
     (lambda (n)			;Take n bytes from the queue.
       (if (> n bytes)
	   nil
	 (let: out nil in
	   (decf bytes n)
	   (while (> n (- (length (car q-head)) ix))
	     (push (substring (pop q-head) ix) out)
	     (setf ix 0)
	     (decf n (length (car out))))
	   (push (substring (car q-head) ix (+ ix n)) out)
	   (incf ix n)
	   (apply 'concat (nreverse out))))))))

(defsubst fifo-recv (fifo n)
  (funcall (aref fifo 1) n))

(defsubst fifo-send (fifo str)
  (funcall (aref fifo 0) str))

(defstruct strpipe q-tail q-head bytes ix)

(defstructmeth strpipe strpipe-add (s)
  (setf (cdr q-tail) (cons s nil)
	q-tail (cdr q-tail))
  (incf bytes (length s)))

(defstructmeth strpipe strpipe-recv (n)
  (if (> n bytes)
      nil
    (let: out nil in
      (decf bytes n)
      (while (> n (- (length (car q-head)) ix))
	(push (substring (pop q-head) ix) out)
	(setf ix 0)
	(decf n (length (car out))))
      (push (substring (car q-head) ix (+ ix n)) out)
      (incf ix n)
      (apply 'concat (nreverse out)))))

(defstructmeth strpipe strpipe-rdelim (delim)
  (let: h q-head
        line-block (drop-until h (x: (string-match delim x))) in
    (when line-block
      (setf q-head (cons (substring (car line-block) (match-end 0))
                         (cdr line-block))
            q-tail (if (eq line-block q-tail) q-head q-tail)
            (car line-block) (substring (car line-block) 0 (match-beginning 0)))
      (do-wad (apply 'concat (take-until h (x: (eq x (cadr line-block)))))
              (progn (decf bytes (+ (length _) 1)) _)))))

(defun strpipe ()
  (let: q (cons "" nil) in
    (make-strpipe :q-head q :q-tail q
		  :bytes 0 :ix 0)))

(defun procpipe (process &optional killstatus initial-input)
  (let: pipe (strpipe)
        ks killstatus
        cb nil
        bytes 0 in
    (if initial-input
        (strpipe-add pipe (string-as-unibyte initial-input)))
    (set-process-coding-system process 'binary 'binary)
    (set-process-filter
     process
     (lambda (p s)
       (strpipe-add pipe (string-as-unibyte s))
       (let: chunk nil 
	     temp-cb nil in
	 (while (and cb (setq chunk (strpipe-recv pipe bytes)))
	   (setq temp-cb cb 
		 cb nil
		 bytes 0)
	   (funcall temp-cb chunk)))))
    (when killstatus
      (set-process-sentinel process
	(lambda (p s)
	  (when (and cb (member s ks))
	    (funcall cb (strpipe-recv pipe (strpipe-bytes pipe)))
	    (funcall cb "")
	    (setq cb nil)))))
    (lambda (n new-cb)
      (iflet data (strpipe-recv pipe n)
             (run-at-time 0 nil new-cb data)
             (setq bytes n
                   cb new-cb)))))

(defun agetline (pipe cont)
  "Asynchronously get a line from a process-pipe."
  (nlet loop ((accum nil))
    (cps-let ((c (funcall pipe 1 cont!)))
      (cond
       ((equal c "") (funcall cont ""))
       ((= (aref c 0) 10)
        (funcall cont (apply 'concat (nreverse accum))))
       (t (funcall loop (cons c accum)))))))

(provide 'fifo-crap)
