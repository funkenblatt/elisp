;; -*- lexical-binding: t; -*-
;;; Useful shadchen patterns and shit.

(require 'shadchen)

(defpattern path (dir base ext)
  `(and (funcall 'file-name-directory ,dir)
        (funcall 'file-name-base ,base)
        (funcall 'file-name-extension ,ext)))

(defun re-groups (re)
  (lambda (s)
    (save-match-data
      (string-match re s)
      (lc (match-string x s) x (range 1 (/ (length (match-data)) 2))))))

(defpattern str (&rest args)
  (let: rx (mapcar (x: (if (stringp x) x `(group (* any)))) args)
        patterns (filter (comp 'not 'stringp) args) in
    `(funcall (re-groups (rx ,@rx)) (list ,@patterns))))

(defpattern interp (&rest args)
  (macroexpand `(interp ,@args)))

(defpattern int (arg)
  `(funcall 'string-to-number ,arg))

(provide 'defpatterns)
