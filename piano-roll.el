;; Emacs Piano Roll Editor
;; Completely undocumented.  Never use unless you are Jason Feng.
;; 
;; With that said, code is in the public domain.  You are free to fail
;; to make sense of this in any way you choose.  This depends on macros
;; and other junk defined in http://jason.ozbert.com/elisp/jf-lib.el which
;; is one of many reasons why this code is probably difficult to make use of
;; for people who aren't me.

(setq piano-roll-map
  (keymap nil 
	  [up] 'up-half-step
	  [down] 'down-half-step))

(jason-defkeys piano-roll-map 
	       [M-down] (keycmd (move-half-step 12))
	       (kbd "C-c b") (keycmd (pr (piano-numbeats)))
	       [C-down] (keycmd (move-half-step 7))
	       [S-down] (keycmd (move-half-step 4))
	       [M-up] (keycmd (move-half-step -12))
	       [C-up] (keycmd (move-half-step -7))
	       [S-up] (keycmd (move-half-step -4))
	       [M-right] (keycmd (piano-forward-char piano-notelen))
	       [M-left] (keycmd (forward-char (- piano-notelen)))
	       [right] (keycmd (piano-forward-char 1))
	       (kbd "M-l") (lambda (n) (interactive "nNote length: ") (setq piano-notelen n))
	       "n" 'down-half-step
	       "p" 'up-half-step
	       [C-return] (keycmd (piano-do-intervals 'piano-insert-note))
	       (kbd "k") 'piano-kill-interval
	       (kbd "RET") 'piano-insert-interval
	       (kbd "M-RET") (keycmd (piano-insert-interval 8)))

(defun piano-insert-interval (&optional interval)
  (interactive)
  (piano-do-interval 'piano-insert-note 
		     (or interval current-prefix-arg)))

(defun piano-kill-interval (&optional interval)
  (interactive)
  (piano-do-interval 'piano-kill-note 
		     (or interval current-prefix-arg)))

(defun piano-interval->half-steps (interval)
  (cdr (assq interval '((4 . 5) (5 . 7) (8 . 12)
			(6 . 9) (-6 . 8) (7 . 11) (-7 . 10)
			(-5 . 6) (2 . 2) (3 . 4) (-3 . 3)))))

(defun piano-do-interval (proc interval)
  (interactive)
  (setq interval (piano-interval->half-steps interval))
  (funcall proc)
  (if interval
      (save-excursion
	(move-half-step (- interval))
	(funcall proc))))

(defun piano-do-intervals (proc &optional intervals)
  (if (not intervals)
      (setq intervals (mapcar 'read (split-string (read-from-minibuffer "Intervals: ")))))
  (dolist (i intervals)
    (piano-do-interval proc i)))

(defun piano-forward-char (n)
  (let: deactivate-mark nil in 
    (dotimes (i n)
      (if (eolp) 
	  (insert " ") 
	(forward-char 1)))))

(setq piano-notes '(C C+ D D+ E F F+ G G+ A A+ B))

(defun piano-roll ()
  (interactive)
  (let: buf (generate-new-buffer "*piano roll*") in
    (switch-to-buffer buf)
    (pr-align
     (apply 'append
	    (lc (zip (reverse piano-notes) (lc i x piano-notes))
		i (range 5 -5 -1))))
    (piano-roll-mode)))

(defun piano-roll-mode ()
  (interactive)
  (setq major-mode 'piano-roll-mode
	mode-name "Piano Roll")
  (mapc 'make-local-variable
	'(piano-templates
	  piano-cell-beats))
  (use-local-map piano-roll-map))

(defun move-half-step (n)
  (interactive)
  (let: col (max (current-column) 10)
	deactivate-mark nil in
    (forward-line n)
    (if (= (point) (point-max)) (forward-line -1))
    (message (repr (piano-current-note)))
    (when (< (move-to-column col) col)
      (insert (str* " " (- col (current-column))))
      (move-to-column col))
    (flash (save-excursion (beginning-of-line) (point)) (point) "#ffff00")))

(defun down-half-step () (interactive) (move-half-step 1))
(defun up-half-step () (interactive) (move-half-step -1))

(defun piano-current-note ()
  (mapcar 'read (head (split-string (thing-at-point 'line) "|") 2)))

(setq piano-notelen 8)

(defun overwriting-insert (s)
  (save-excursion
    (mapc (lambda (i)
	    (if (not (eolp))
		(delete-char 1))
	    (insert-char i 1))
	  s)))

(setq piano-instruwad ?x)

(defun piano-insert-note ()
  (interactive)
  (let: chars (cons piano-instruwad (lc ?= x (range (- piano-notelen 1)))) in
    (overwriting-insert chars)))

(defun piano-vshift-rectangle (n)
  (let: lines (extract-rectangle (region-beginning) (region-end)) in
    (replace-rectangle (region-beginning) (region-end)
		       (str* " " (length (car lines))))
    (apply-on-rectangle
     (lambda (start end)
       (goto-char (+ (point) start))
       (save-excursion
	 (move-half-step n)
	 (overwriting-insert (pop lines))))
     (region-beginning)
     (region-end))))

(defun piano-hshift-rectangle (n)
  (if (> n 0)
      (string-insert-rectangle (region-beginning) (region-end)
			       (str* " " n))
    (apply-on-rectangle
     (lambda (start end)
       (goto-char (+ (point) start n))
       (delete-region (point) (- (point) n)))
     (region-beginning) (region-end))))

(defun piano-kill-note ()
  (interactive)
  (let: start (point) 
	end nil in
    (save-excursion
      (re-search-forward "[a-z=]=+")
      (when (= (match-beginning 0) start)
	(setq end (point))
	(delete-region start end)
	(insert (str* " " (- end start)))))))

(defun piano-whitestrip ()
  (goto-char  (point-min))
  (while (re-search-forward "[ \t]+$" nil t)
    (replace-match "")))

(defun piano-roll->notelist ()
  (let: out nil in
    (save-excursion
      (mark-whole-buffer)
      (do-region
       (lambda (x)
	 (let: parts (split-string x "|")
	       note (mapcar 'read (head parts 2))
	       times (re-findall "[^ \t\n]=*" (caddr parts)) in
	   (ec (push (list note 
			   (aref (caddr parts) (car time))
			   (car time)
			   (- (apply '- time))) 
		     out)
	       time times)))))
    (sortbag out x (nth 2 x))))		;Sort by note start time.

(setq piano-note-frequencies
      '((C . 130.8127826502993)
	(C+ . 138.59131548843604)
	(D . 146.8323839587038)
	(D+ . 155.56349186104046)
	(E . 164.81377845643496)
	(F . 174.61411571650194)
	(F+ . 184.9972113558172)
	(G . 195.99771799087463)
	(G+ . 207.65234878997256)
	(A . 220.0)
	(A+ . 233.08188075904496)
	(B . 246.94165062806206)))

(defun piano-note->freq (note)
  (let: base-freq (cdr (assq (car note) piano-note-frequencies)) in
    (if base-freq
	(* base-freq (expt 2 (cadr note))))))

(defun goto-pitch (pitch)
  (let: curpitch (cdr (assq (car (piano-current-note)) piano-pitchnums))
	newpitch (cdr (assq pitch piano-pitchnums)) in
    (move-half-step (- curpitch newpitch))))

(setq piano-pitchnums
      (lc: (cons p x) p x in 
	   piano-notes
	   (range 12)))

(dolist (i piano-notes)
  (define-key piano-roll-map
    (read-kbd-macro (str "M-" (if (endswith (symbol-name i) "+")
				  (substring (symbol-name i) 0 1)
				(downcase (symbol-name i)))))
    `(lambda ()
       (interactive)
       (goto-pitch ',i))))

(defun piano-cells->seconds (c)
  (* (/ 60.0 piano-tempo)
     4.0 c piano-cell-beats))

(defun piano-seconds->beats (s)
  (* (/ piano-tempo 60.0) s 0.25))

(defun index-of (elt lst)
  (catch 'return
    (let: n 0 in
      (while lst
	(if (equal (pop lst) elt)
	    (throw 'return n))
	(setq n (+ n 1))))))

(defun piano-render-note (note template &optional offset)
  "Renders a single piano note to some sort of template.
Optional OFFSET argument is specified as a number of whole
notes, and gets converted into seconds on the way out."
  (if (not offset) 
      (setq offset 0)
    (setq offset (* 4 (/ 60.0 piano-tempo) offset)))
  (remap-symbols
   template
   (list (cons :start (+ offset (piano-cells->seconds (caddr note))))
	 (cons :dur (piano-cells->seconds (cadddr note)))
	 (cons :freq (piano-note->freq (car note)))
	 (cons :key (+ 60 
		       (index-of (caar note) piano-notes) 
		       (* 12 (- (cadr (car note)) 1)))))))

(defun piano-render-notes (&optional offset)
  (let: notes (piano-roll->notelist) in
    (lc (piano-render-note
	 note
	 (piano-get-template (cadr note))
	 offset)
	note notes)))

(defun piano-get-template (n)
  (cdr (assq n piano-templates)))

(defun piano->snd ()
  "Pipes a piano roll straight to snd."
  (let: out (str (append '(with-sound (:play "#t"))
			 (piano-render-notes))) in
    (comint-send-string
     (get-buffer-process "*scheme*")
     (str out "\n"))))

(defun pianos->snd (buffers)
  (let: notelists (lc (with-current-buffer x (piano-render-notes))
		      x buffers)
	out (str (apply 'append '(with-sound (:play "#t")) notelists)
		 "\n") in
    (comint-send-string
     (get-buffer-process "*scheme*")
     out)))

(setq piano-cell-beats (/ 1.0 8))
(setq piano-tempo 135)

(defun piano-numbeats ()
  "Number of whole notes in a particular piano roll."
  (* (apply 'max (lc (apply '+ (cddr x)) x (piano-roll->notelist)))
     piano-cell-beats))

(defun pianos-play-visible (&optional player)
  (interactive)
  (funcall
   (or player 'pianos->snd)
   (lc (window-buffer x) x (window-list) 
       (eq (buffer-mode (window-buffer x)) 
	   'piano-roll-mode))))

(defun piano->overtone (&optional repeats)
  (interactive)
  (do-wad
   (mappend (x: (piano-render-notes (* (piano-numbeats) x)))
            (range (or repeats 1)))
   notes->overtone))

(defun pianos->overtone (buffers)
  (interactive)
  (let: notelists (lc (with-current-buffer x (piano-render-notes))
                      x buffers) in
    (do-wad
     (apply 'append notelists)
     notes->overtone)))

(defun notes->overtone (notes)
  (do-wad
   (group-by notes 'car t)
   (mapcar (x: (cons (car x) (mapcar 'cadr (cdr x)))) _)
   (maptree (x: (if (stringp x) (intern x) x)) _)
   (sortbag _ x (car x))
   (cons 'do-notes _)
   repr (str _ "\n") 
   (progn (nrepl-send-string-sync _)
          (nrepl-send-string-sync "(smegfun (now))"))
   ))

(setq bass-templates '((?x . (basswad :start :dur :freq))))
(setq sampdrum-templates '((120 sampdrum :start :dur) (121 sampdrum :start :dur "#t")))
(setq noisedrum-templates '((120 noisedrum :start :dur) (121 noisebag :start :dur)))
(setq shifty-templates '((120 noisedrum :start :dur 0) (121 noisebag :start :dur)))
(setq sawbag-templates '((120 sawbag-2 :start :dur :freq) (121 sawbag-2 :start :dur :freq :bendy "#t")))
(setq piano-templates sawbag-templates)
(setq sampdrum2-templates (lc (cons (+ ?a x) `(sampdrum-2 :start :dur ,x)) x (range 8)))
(setq sampdrum3-templates '((?x . (sampdrum-3 :start :dur)) (?y . (sampdrum-3 :start :dur "#t"))))

(provide 'piano-roll)
