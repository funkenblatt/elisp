;; Crappy functional queue.

(defstruct q hl tl)

(defun q-make (&optional hl tl)
  (vector hl tl))

(defun q-add (q item)
  (push item (q-tl q)))

(defun q-head (q)
  "Shift queue elements into head list if necessary."
  (let: hl (q-hl q)
	tl (q-tl q) in
    (when (not hl)
      (setf (q-hl q) (reverse tl)
	    (q-tl q) nil))
    (car (q-hl q))))

(defun q-take (q)
  (q-head q)
  (pop (q-hl q)))

(defun q-any (q)
  (or (q-hl q) (q-tl q)))

