;; -*- lexical-binding: t; -*-
(require 'calc)
(require 'calc-ext)
(require 'calc-prog)
(require 'el2)

(defun id-matrix (size)
  (do-wad (>> list x (range size) y (range size) in
              (if (= x y) (mathify-vec 1) (mathify-vec 0)))
          (sublist-n _ size)
          (mapcar (juxtcons (const 'vec) 'identity) _)
          (cons 'vec _)))

(defun mathify-vec (v)
  (cond
   ((vectorp v) (cons 'vec (mapcar 'mathify-vec v)))
   ((numberp v) (math-read-number (str v)))
   ((symbolp v) (math-read-expr (str v)))
   ((listp v) (cons (case (car v)
                      (expt '^)
                      (t (car v)))
                    (mapcar 'mathify-vec (cdr v))))
   (t v)))

(defun vectorize-math (m)
  (pcase m
    (`(vec . ,stuff) (vec (mapcar 'vectorize-math stuff)))
    (`(float . ,stuff) (calc-float->elisp m))
    (`(var ,v ,junk) v)
    (`(^ . ,args) `(expt ,@(mapcar 'vectorize-math args)))
    (`(,op . ,args) `(,op ,@(mapcar 'vectorize-math args)))
    (x x)))

(defmacro with-math (&rest body)
  `(el2
    (let ((+ 'calcFunc-add)
          (- 'calcFunc-sub)
          (* 'calcFunc-mul)
          (/ 'calcFunc-div)
          (expt 'calcFunc-pow)
          (inv 'calcFunc-inv)
          (transpose 'calcFunc-trn)
          (vector (lambda (&rest args) (cons 'vec (mapcar 'mathify-vec args)))))
      ,@body)))

(defun calc-float->elisp (num)
  (pcase num
    (`(float ,mantissa ,exponent) (* mantissa (expt 10 exponent)))))

(defun poly->gnuplot (coeffs)
  "Converts a set of polynomial coefficients into a gnuplot expression"
  (let: powers (nreverse (range (length coeffs))) in
    (>>> (mapcar* (λ (coeff power)
                     (interp "#,[coeff] * x**#,[power]"))
                  coeffs powers)
         (strjoin " + " _))))

(defun polyfit (xs ys degree)
  "Returns a set of coefficients for a polynomial of degree DEGREE
   that approximates the given XS and YS coordinates."
  (let: len (length xs)
        moments (mapcar (λ k (sum (mapcar (cut expt <> k) xs)))
                        (range (* 2 degree) -1 -1))
        ymoments (mapcar (λ k (vector
                               (sum (mapcar* (λ (x y) (* (expt x k) y)) xs ys))))
                         (range degree -1 -1)) in
    (>>> (range (1+ degree))
         (mapcar (λ i (subseq moments i (+ i 1 degree))) _)
         (mapcar 'vec _) vec mathify-vec
         (with-math (* (inv _) (mathify-vec (vec ymoments))))
         (mapcar (comp 'calc-float->elisp 'cadr) (cdr _)))))

(defun polyfit-plot (degree xs &optional ys)
  (poly->gnuplot
   (if ys
       (polyfit xs ys degree)
     (polyfit (range (length xs)) xs degree))))

(provide 'jf-math)
