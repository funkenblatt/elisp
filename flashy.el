;; -*- lexical-binding: t; -*-

(defun rand-highlight ()
  (cps-foreach
   (λ (x k)
     (let: pt (+ (mod (random) (point-max)) (point-min)) in
       (goto-char pt)
       (flash pt (+ pt 4) "#ffee00"))
     (wait 0.25 (funcall k nil)))
   (range 12)
   'identity))

(defun flash (start end clr &optional holdtime)
  "Causes background in region START - END to briefly show with color CLR.
Shows for 0.5 seconds, or HOLDTIME if it is given."
  (let: ov (make-overlay start end)
        rdonly buffer-read-only in
    (setq buffer-read-only nil)
    (overlay-put ov 'face `(:background ,clr))
    (setq buffer-read-only rdonly)
    (wait (or holdtime 0.5) (delete-overlay ov))))

(defmacro wait (time &rest body)
  "Execute BODY (a lexically scoped thunk-ish sort of thing)
after waiting time TIME"
  `(cps-let ((nil (run-at-time ,time nil cont!)))
     ,@body))

(add-hook 'occur-mode-find-occurrence-hook
	  (lambda ()
	    (apply 'flash (save-excursion
			    (list (progn (beginning-of-line) (point))
				  (progn (end-of-line) (point))
				  "#ffee00")))
	    (pop-to-buffer "*Occur*")))

(provide 'flashy)
