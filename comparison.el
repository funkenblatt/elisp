(defvar comparators (make-hash-table :test 'equal))
(defvar type-orders (dict 'cons 0 'vector 1 'hash-table 2 'string 3 'symbol 4 'integer 5 'float 5))

(defun cmp (a b)
  (let: typematch (- (gethash (type-of a) type-orders)
                     (gethash (type-of b) type-orders)) in
    (if (= typematch 0)
        (let: comparator (gethash (list (type-of a) (type-of b))
                                  comparators) in
          (funcall comparator a b))
      typematch)))

(defun lcompare (a b)
  (let: c (cmp (car a) (car b)) in
    (if (= c 0)
        (if a (lcompare (cdr a) (cdr b)) 0)
      c)))

(defun numcompare (a b)
  (- a b))

(puthash '(cons cons) 'lcompare comparators)

(ec (puthash x 'numcompare comparators)
    x (powset '(integer float) 2))

(puthash '(string string) (lambda (a b)
                            (cond ((string< a b) -1)
                                  ((string= a b) 0)
                                  (t 1)))
         comparators)

(puthash '(symbol symbol) (lambda (a b)
                            (cmp (symbol-name a) (symbol-name b)))
         comparators)

(provide 'comparison)
