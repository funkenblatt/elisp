;; -*- lexical-binding: t; -*-

(require 'xml-shit)
(require 'shadchen)

(setf usgs-base-url
      "http://ned.usgs.gov/epqs/pqs.php")

(defun get-elevation (lat lng cont)
  (>>>
   `((x . ,lng) (y . ,lat) (units . "Meters")
     (output . "xml"))
   urlencode-alist
   (interp "#,[usgs-base-url]?#,_")
   (get-url _  (comp cont
                     'read 'car 'xml-node-children 'car
                     (cur 'nodes-where (tagp 'Elevation))
                     'car 'xml-read))))

(defun xml-read (s)
  (with-temp-buffer (insert s) (xml-parse-region)))

(defun elevation-handler (req-parts header body cont)
  (pr body)
  (match (ignore-errors (json-read-from-string body))
    ([lat lng] (cps> elevation (get-elevation lat lng :cont!)
                 (funcall cont "200 OK" 
                          '((Content-Type . "application/json"))
                          (str elevation))))
    (x (funcall cont "404 Not Found" '((Content-Type . "text/plain"))
                "Shit's fucked son."))))

(defun add-elevation-service ()
  (interactive)
  (push '("^/elevation/" . elevation-handler) fooserve-2-handlers))

(provide 'elevation)
