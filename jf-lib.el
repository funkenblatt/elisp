;; -*- lexical-binding: t; -*-
;; Library functions, mostly for dealing with
;; lists and such.

(defun dump (&rest s)
  (dolist (i s)
    (princ i (current-buffer))))

(defmacro c*r (sym arg)
  (foolet (symwad (nreverse (append (symbol-name sym) '()))
	   funs (lc (if (eq x ?a) 'car 'cdr) x symwad (memq x '(?a ?d)))
	   out (list (car funs) arg))
    (dolist (i (cdr funs))
      (setq out (list i out)))
    out))

(defun sublists (l)
  "Return a list of 2-element overlapping sublists of l, (1 2 3 4) becomes ((1 2) (2 3) (3 4))"
  (let ((x '()) (prev '()))
    (dolist (i l)
      (if (null prev) 0
	(push `(,prev ,i) x))
      (setq prev i))
    (nreverse x)))

(defun sublist-n (l n)
  (let: out nil in
	(while l
	  (push (head l n) out)
	  (setq l (tail l n)))
	(nreverse out)))

(defun sublist2 (l)
  "Returns a list of non-overlapping 2 element sublists, (1 2 3 4) becomes ((1 2) (3 4))"
  (let ((x '()))
    (while (cdr l)
      (push (list (car l) (cadr l)) x)
      (pop l)
      (pop l))
    (nreverse x)))

(defun popn (l n)
  (foolet (out '())
    (while (and l (> n 0))
      (push (pop l) out)
      (setq n (- n 1)))
    out))

(defmacro lambda* (arglist &rest body)
  `(let (this)
     (setf this
           ,(cond
             ((or (not arglist) (consp arglist)) 
              (if (some 'consp arglist)
                  (let: tmp-args (mapcar (lambda (x) (gensym)) arglist) in
                    `(lambda ,tmp-args
                       (let:: ,@(cl-mapcan 'list arglist tmp-args)
                         in
                         ,@body)))
                `(lambda ,arglist ,@body)))
             ((symbolp arglist) `(lambda (,arglist) ,@body))
             (t (error "Invalid syntax: expected list or symbol for arglist!"))))))

(defsubst head (l n)
  (nreverse (popn l n)))

(defun heads (l n)
  (lc (head l (+ i 1)) i (range n)))

(defun tail (l n)
  (while (> n 0)
    (pop l)
    (setq n (- n 1)))
  l)

(defun tails (l n)
  (let (out)
    (dotimes (i n)
      (push l out)
      (pop l))
    (nreverse out)))

(defun startswith (s prefix)
  (let ((len (length prefix)))
    (if (< (length s) len)
	nil
      (equal (substring s 0 len) prefix))))

(defun endswith (s suffix)
  (let ((len (length suffix)))
    (if (< (length s) len)
	nil
      (equal (substring s (- len)) suffix))))

(defun get-assoc (alist key)
  (cdr (assoc key alist)))

(setq rpn-operators (make-hash-table))
(dolist (i '(mapcar gethash push lamderp setf /= += -= *= -> := aref lsh ash time-less-p time-subtract time-add sort filter cons logor logxor logand + - * / expt eql eq equal mod % and or xor = > < >= <=)) (puthash i 2 rpn-operators))
(dolist (i '(round float cdr car cadr neg log10 sin cos tan asin acos atan exp log sqrt length not)) (puthash i 1 rpn-operators))
(mapc (lambda (x) (puthash x 3 rpn-operators))
      '(aset if substring))

(defun rpn-op (op)
  (gethash op rpn-operators))

(defun neg (x) (- x))

(defmacro defrpn (name args &rest body)
  `(progn (defun ,name ,args ,@body)
	  (puthash ',name ,(length args) rpn-operators)))

(defrpn str* (s n) (apply 'concat (lc s x (range n))))

(defun rpn-parse (l)
  (let ((stk '()))
    (dolist (i l)
      (if (rpn-op i)
	  (let ((args (popn stk (rpn-op i))))
	    (setq stk (tail stk (rpn-op i)))
	    (push (cons i args) stk))
	(push i stk)))
    (nreverse stk)))

(defmacro foolet (bindings &rest l)
  "Let, now with fewer parentheses"
  (append `(let* ,(sublist2 bindings)) l))

(defun lsplit (l sep)
  (let (out)
    (while (and l (not (eq sep (car l))))
      (push (pop l) out))
    (list (nreverse out) (cdr l))))

(defun lsplit-all (l sep)
  (let: out nil
	cur nil in
    (while (cadr (setq cur (lsplit l sep)))      
      (push (car cur) out)
      (setq l (cadr cur)))
    (push (car cur) out)
    (nreverse out)))

(defmacro let: (&rest wads)
  (foolet (spl (lsplit wads 'in))
    `(foolet ,(car spl)
       ,@(cadr spl))))

(defmacro plet: (&rest wads)
  (declare (indent indent-let:))
  (pcase (lsplit wads 'in)
    (`(,l ,r) `(pcase-let* ,(sublist2 l)
                 ,@r))))

(defmacro let:: (&rest wads)
  (pcase wads
    (`(,a ,b in . ,stuff)
     `(dbind ,a ,b ,@stuff))
    (`(,a ,b . ,stuff)
     `(dbind ,a ,b (let:: ,@stuff)))))

(defmacro <l (&rest l)
  "Check to see if a series of arguments is in order or not."
  (cond 
   ((> (length l) 2)
    (cons 'and (mapcar (lambda (x) (cons '< x)) (sublists l))))
   ((eql (length l) 2) `(< ,(car l) ,(cadr l)))
   (t nil)))

(defun <=* (&rest nums)
  (loop for i in nums for j in (cdr nums)
        when (> i j) return nil
        finally return t))

(defmacro rpn (&rest l)
  (car (rpn-parse l))
  )

(defmacro rpn* (&rest l)
  `(list ,@(rpn-parse l)))

(defun prefix-parse (expr)
  (let ((stack '()) (out '()))
    (dolist (i expr)
      (if (rpn-op i)
	  (push (cons i (rpn-op i)) stack)
	(push i out)
	(setcar stack (cons (caar stack) (- (cdar stack) 1))))
      (while (and stack (= (cdar stack) 0))
	(push (caar stack) out)
	(pop stack)
	(if stack (setcar stack (cons (caar stack) (- (cdar stack) 1))))))
    (nreverse out)))

(defmacro pn (&rest body)
  (cons 'rpn (prefix-parse body)))

(defun all (l)
  (catch 'break
    (while l
      (if (not (car l)) (throw 'break nil) (setq l (cdr l))))
    (throw 'break t)))

(defun join (items sep)
  (foolet (out '())
    (dolist (i items)
      (push i out)
      (push sep out))
    (nreverse (cdr out))))

(defun prefix (p l)
  (let: out nil in
    (dolist (i l)
      (push p out)
      (push i out))
    (nreverse out)))

(defun ljoin (items seps)
  (apply 'append
	 (join (mapcar 'list items) seps)))

(defun strjoin (sep strings)
  (apply 'concat (join strings sep)))

(defun fold (proc l init)
  (foolet (out init)
    (dolist (i l)
      (setq out (funcall proc i out)))
    out))

(defun fold-right (proc l init)
  (if l (funcall proc (car l) (fold-right proc (cdr l) init))
    init))

(defun reduce-r (proc l)
  (if (cdr l) (funcall proc (car l) (reduce-r proc (cdr l)))
    (car l)))

(defun filter (pred l)
  (if (vectorp l) (setf l (append l nil)))
  (foolet (out '())
    (dolist (filter-items l)
      (if (funcall pred filter-items) (push filter-items out) 0))
    (reverse out)))

(defun filter-tree (proc l)
  (foolet (out '() tmp nil)
    (dolist (i l)
      (if (listp i)
	  (if (setq tmp (filter-tree proc i))
	      (push tmp out))
	(if (funcall proc i) (push i out))))
    (reverse out)))

(defmacro lc (expr varname lexp &rest pred)
  (if pred
      `(mapcar (lambda* (,varname) ,expr) 
	       (filter (lambda* (,varname) ,(car pred)) ,lexp))
    `(mapcar (lambda* (,varname) ,expr) ,lexp)))

(defmacro lcv (expr varname lexp &rest pred)
  `(apply 'vector (lc ,expr ,varname ,lexp ,@pred)))

(defmacro lcp (expr varname lexp pred)
  "Like the normal lc list comprehension, but now we want to do the
filtering AFTER the mapping operation."
  `(filter
    (lambda (_) ,pred)
    (mapcar (lambda (,varname) ,expr) ,lexp)))

(defmacro ec (expr varname lexp &rest pred)
  (if pred
      `(mapc (lambda (,varname) ,expr) 
	       (filter (lambda (,varname) ,(car pred)) ,lexp))
    `(mapc (lambda (,varname) ,expr) ,lexp)))

(defmacro lc* (expr varnames lexps &rest pred)
  (if pred
      `(lc (apply (lambda ,varnames ,expr) lc*-blargh-wad)
	   lc*-blargh-wad (zip ,@lexps)
	   (apply (lambda ,varnames ,@pred) lc*-blargh-wad))
    `(lc (apply (lambda ,varnames ,expr) lc*-blargh-wad)
	 lc*-blargh-wad (zip ,@lexps))))

(defmacro ec* (expr varnames lexps &rest pred)
  (if pred
      `(ec (apply (lambda ,varnames ,expr) lc*-blargh-wad)
	   lc*-blargh-wad (zip ,@lexps)
	   (apply (lambda ,varnames ,@pred) lc*-blargh-wad))
    `(ec (apply (lambda ,varnames ,expr) lc*-blargh-wad)
	 lc*-blargh-wad (zip ,@lexps))))

(defmacro lc: (expr &rest wads)
  (let:
   parts (lsplit wads 'in)
   vars (car parts) lsts-preds (lsplit (cadr parts) 'if)
   lists (car lsts-preds) pred (cadr lsts-preds) in
   `(lc* ,expr ,vars ,lists ,@pred)))

(defmacro hc (expr arglist tab &optional pred)
  `(maphash (lambda ,arglist (if ,(or pred t) ,expr))
            ,tab))

(defmacro ec: (expr &rest wads)
  (let:
   parts (lsplit wads 'in)
   vars (car parts) lsts-preds (lsplit (cadr parts) 'if)
   lists (car lsts-preds) pred (cadr lsts-preds) in
   `(ec* ,expr ,vars ,lists ,@pred)))

(defun zip (&rest lists)
  (apply 'mapcar* 'list lists))

(defun ibin (n &optional list-output pad)
  (let ((out (if (= n 0) (lc 0 x (range (or pad 8))) '())))
    (while (!= n 0)
      (dotimes (i (or pad 8))
	(push (logand n 1) out)
	(setq n (lsh n -1))))
    (if list-output
	out
      (apply 'concat (mapcar 'number-to-string out)))))

(defun array-reduce (proc array)
  (let ((i 2) 
	(out (funcall proc (aref array 0) (aref array 1))))
    (while (< i (length array))
      (setq out (funcall proc out (aref array i)))
      (setq i (1+ i)))
    out))

(defun dict (&rest items)
  (let ((out (make-hash-table :test 'equal)))
    (dolist (i (sublist2 items))
      (puthash (car i) (cadr i) out))
    out))

(defun alist->dict (alist &optional test size)
  (foolet (out (make-hash-table :test (or test 'eq)
				:size (or size 65)))
    (dolist (i alist)
      (puthash (car i) (cdr i) out))
    out))

(defun mappend (proc l)
  (apply 'append (mapcar proc l)))

(defun hash->list (table)
  (let ((out '()))
    (maphash (lambda (key value)
	       (push (cons key value) out))
	     table)
    out))

(defun range (from &optional to step)
  (unless to (setq to from) (setq from 0))
  (unless step (setq step 1))
  (let ((n from) (out '()))
    (while (if (> step 0) (< n to) (> n to))
      (push n out)
      (setq n (+ n step)))
    (nreverse out)))

(defun nestify (funcs)
  (if (not (cdr funcs)) 
      `(apply ,(if (symbolp (car funcs))
                   `(quote ,(car funcs))
                 (car funcs)) args)
    `(,(car funcs) ,(nestify (cdr funcs)))))

(defmacro funcomp (&rest funcs)
  "Creates a lambda expression that composes several functions."
  `(lambda (&rest args)
     ,(nestify funcs)))

(defmacro compcall (funcs &rest args)
  `(funcall (funcomp ,@funcs) ,@args))

(defun cross (&rest l)
  (if (not (cdr l))
      (mapcar 'list (car l))
    (foolet (z (apply 'cross (cdr l)))
      (mappend (lambda (a) (lc (cons a w) w z)) (car l)))))

(defun comb (l n)
  (cond ((not l) nil)
	((= n 1) (mapcar 'list l))
	((= (length l) n) (list l))
	(t (append (lc (cons (car l) x) x (comb (cdr l) (- n 1)))
		   (comb (cdr l) n)))))

(defun any-nulls (l) (lc x x l (not x)))

(defun any (l)
  (catch 'return
    (dolist (i l) (if i (throw 'return i)))
    nil))

(defalias 'map* 'mapcar*)

(defun proper-list? (l)
  (let: len 0 in
    (while (consp l) (pop l) (incf len))
    (and (not l) len)))

(defun mapwad (fun l)
  "Map, but works on improper lists."
  (let: out nil in
    (while (consp l) (push (funcall fun (pop l)) out))
    (if l (append (nreverse out) (funcall fun l)) (nreverse out))))

(defun maptree (proc l)
  (mapwad (lambda (x) 
	    (if (listp x)
		(maptree proc x)
	      (funcall proc x)))
	  l))

(defun maptreepred (proc tree pred)
  "Applies proc to subtrees of tree that satisfy pred."
  (lc (if (funcall pred x)
	  (funcall proc x)
	(if (listp x)
	    (maptreepred proc x pred)
	  x))
      x tree))

(defun remap-symbols (sexp smap)
  "Takes an S-expression tree, and replaces any symbol
in the alist smap with its association element."
  (maptree 
   (lambda (x) (if (get-assoc smap x) (get-assoc smap x) x))
   sexp))

(defun recur-whiles (varspecs body)
  (let ((spec (car varspecs))
	(next-spec (cadr varspecs)))
    (if (cdr varspecs) 
	`(while (< ,(cadr spec) ,(caddr spec))
	   (setq ,(cadr next-spec) ,(car next-spec))
	   ,(recur-whiles (cdr varspecs) body)
	   (setq ,(cadr spec) (+ ,(cadr spec) 1)))
      `(while (< ,(cadr spec) ,(caddr spec))
	 ,@body
	 (setq ,(cadr spec) (+ ,(cadr spec) 1))))))

(defmacro for (varspecs &rest body)
  (if (atom (car varspecs)) (setq varspecs (list varspecs)))
  (let ((varlets (lc (popn x 2) x varspecs)))
    `(let* ,varlets ,(recur-whiles varspecs body))))

(defmacro combinatory-for (varlist nelem &rest body)
  (let* ((len (length varlist))
	 (forspecs (lc: `(,(if (symbolp (car x)) `(+ ,(car x) 1) (car x))
			  ,(cadr x) 
			  ,(if (numberp nelem) (- nelem y) `(- ,nelem ,y)))
			x y in (sublists (cons 0 varlist)) (reverse (range len)))))
    `(for ,forspecs ,@body)))

(defun vmember (elt vec)
  "Searches a vector for a particular element.
Returns index if it's found nil otherwise."
  (catch 'break
    (for (0 i (length vec))
      (if (equal elt (aref vec i)) (throw 'break i)))
    nil))

(defmacro defrec (name &rest fields)
  "Takes as arguments the name of a struct, followed by
field names.  Fields can then be accessed with
\t(structname-fieldname obj)"
  `(progn
     (defun ,name ,fields (vector ,@fields))
     ,@(lc* `(defsubst ,(symbol-cat name "-" field)
	       (s &optional newval)
	       (if newval (aset s ,ix newval)
		 (aref s ,ix))) 
	    (field ix) (fields (range (length fields))))
     ,@(lc: `(defsubst ,(symbol-cat name "-" field "!")
	       (s newval)
	       (aset s ,ix newval))
	    field ix in fields (range (length fields)))))

(defmacro dec (name)
  `(setq ,name (- ,name 1)))

(defun caddr (x) (car (cdr (cdr x))))

(defun read/die ()
  (condition-case err
      (read (current-buffer))
    (error nil)))

(defun toplevel-items (start-symbols)
  (foolet (out '() form '())
    (save-excursion
      (goto-char (point-min))
      (while (setq form (read/die))
	(if (and (listp form) (memq (car form) start-symbols))
	    (push (cons (cadr form)
			(save-excursion (beginning-of-defun) (line-number-at-pos)))
		  out))))
    (nreverse out)))

(defsubst lastcar (l)
  (car (last l)))

(defun symbol-cat (&rest syms-or-strings)
  (intern 
   (apply 'concat
	  (lc (if (symbolp x) (symbol-name x) x) x syms-or-strings))))

(defmacro dostr (var str &rest body)
  (foolet (ptrvar (symbol-cat var "-string-pointer")
	   strvar (symbol-cat var "-string-var")
	   lenvar (symbol-cat var "-strlen-var"))
    `(let* ((,ptrvar 0) (,strvar ,str) (,lenvar (length ,strvar)))
       (dotimes (,ptrvar ,lenvar)
	 (let ((,var (aref ,strvar ,ptrvar)))
	   ,@body)))))

(defun strmap (proc s)
  (concat (mapcar proc s)))

(defun find-first-group (group)
  (let: matches (match-data)
        n 0 in
    (catch 'break
      (while matches
        (if (and (memq n group) (car matches))
            (throw 'break matches)
          (setq matches (tail matches 2))
          (incf n))))))

(defun re-findall (rx str &optional strings group)
  (with-temp-buffer
    (insert str)
    (goto-char (point-min))
    (let (out)
      (while (and (re-search-forward rx nil t) 
                  (/= (match-beginning 0) (match-end 0)))
        (if strings
            (push (match-string (or group 0)) out)
          (push (list (- (match-beginning 0) (point-min))
                      (- (match-end 0) (point-min)))
                out)))
      (nreverse out))))

(defun markup-extra-attrs (sym)
  (let: parts (re-findall (rx (any ".#") (+ (not (any ".#"))))
                          (symbol-name sym) t)
        classes (lc (substring x 1) x parts (startswith x "."))
        id (car (lc (substring x 1) x parts (startswith x "#"))) in
    (append (if classes `((class ,(strjoin " " classes))))
            (if id `((id ,id))))))

(defun markup-strip-extra-attrs (sym)
  (str~ "^[^.#]*" (symbol-name sym)))

(defun markup-tag-open (tagwad)
  (if (vectorp tagwad) (setq tagwad (append tagwad nil)))
  (let: raw-tag (if (listp tagwad) (car tagwad) tagwad)
        extra-attrs (markup-extra-attrs raw-tag)
        tag (markup-strip-extra-attrs raw-tag)
        all-attrs (do-wad
                   (append extra-attrs 
                           (if (listp tagwad)
                               (sublist2 (cdr tagwad))))
                   (lc (interp "#,(car x)=#,(repr (str (cadr x)))")
                       x _)) in
    (concat tag
            (if all-attrs
                (str " " (strjoin " " all-attrs))
              ""))))

(defun markup-tag-close (tagwad)
  (if (vectorp tagwad) (setq tagwad (append tagwad nil)))
  (markup-strip-extra-attrs (if (listp tagwad) (car tagwad) tagwad)))

(defun markup (&rest sexps)
  (strjoin "\n"
           (lc
            (if (and sexp (listp sexp))
                (if (not (cdr sexp))
                    (format "<%s/>" (markup-tag-open (car sexp)))
                  (format "<%s>%s</%s>" 
                          (markup-tag-open (car sexp))
                          (apply 'concat (mapcar 'markup (cdr sexp)))
                          (markup-tag-close (car sexp))))
              (format "%s" sexp))
            sexp sexps)))

(defun make-css-props (&rest properties)
  (lc (str (car x) ": " (cadr x) "; ") x (sublist2 properties)))

(defun make-css (element &rest properties)
  (apply 'str `(,element " { " ,@(apply 'make-css-props properties) "}\n")))

(defun enum-list (l)
  (range (length l)))

(defun read-file (name)
  (with-temp-buffer
    (insert-file-contents-literally name)
    (goto-char (point-min))
    (read/die)))

(defmacro cond2 (&rest clauses)
  `(cond ,@(sublist2 clauses)))

(defun re-splitwad (rx str)
  (let: matches (re-findall rx str)
	out (list (substring str 0 (caar matches))) 
	lastwad (lastcar matches) in
	(catch 'return
	  (if (not matches) (throw 'return nil))
	  (dolist (i (sublists matches))
	    (when i
	      (push (apply 'substring str (car i)) out)
	      (push (substring str (c*r cadar i) (c*r caadr i)) out)))
	  (push (apply 'substring str lastwad) out)
	  (push (substring str (cadr lastwad)) out)
	  (nreverse out))))

(defun car< (a b) (< (car a) (car b)))

(defmacro negate (fun &rest args) `(lambda ,args (not (,fun ,@args))))

(defun vec->lst (v) (append v '()))

(defun vmap (proc v)
  (let: len (length v)
	out (make-vector len nil) in
    (dotimes (i len)
      (aset out i (funcall proc (aref v i))))
    out))

(defun has (coll item)
  (funcall
   (symbol-cat (type-of coll) '-has)
   coll item))

(defun stringify (item)
  (cond ((integerp item) (char-to-string item))
	((symbolp item) (symbol-name item))
	(t item)))

(defun string-has (coll item)
  "Searches string coll for item.  If item is a list, searches
for first occurrence of any one of the items in the list.  items
may  be characters, strings, or symbols."
  (setq item (stringify item))
  (cond 
   ((stringp item) (string-match (regexp-quote item) coll))
   ((listp item) 
    (string-match 
     (strjoin "\\|" (mapcar (funcomp regexp-quote stringify) item))
     coll))))

(defun str (&rest objs)
  (apply 'concat (lc (format "%s" obj) obj objs)))

(defun strc (&rest objs)
  (apply 'concat (mapcar 'stringify objs)))

(defun get-column-widths (linelist)
  (let: out nil in
    (while (any linelist)
      (push (apply 'max (lc (length (str (car x))) x linelist)) out)
      (setq linelist (mapcar 'cdr linelist)))
    (nreverse out)))

(defun proper-dump (&rest s)
  (dolist (i s)
    (if (stringp i)
	(insert i)
      (princ i (current-buffer)))))

(defun pr-align (linelist)
  (let: widths (get-column-widths linelist) in
    (ec (progn (ec: (proper-dump y (str* " " (- (+ width 2) (length (str y)))) "|") y width in x widths)
	       (dump "\n"))
	x linelist)))

(defmacro dovec (var vexpr &rest body)
  (let: v (gensym)
	i (gensym) in
    `(let ((,v ,vexpr))
       (dotimes (,i (length ,v))
	 (let ((,var (aref ,v ,i)))
	   ,@body)))))

(defun kill-matches (rx)
  (while (re-search-forward rx nil t)
    (replace-match "" nil t)))

(defmacro sortbag (expr var cmp-expr &optional cmp)
  "Macro for doing numerical sorts on lists of some sort of data structure."
  `(sort ,expr
	 (lambda (a b)
	   (,@(if cmp (list 'funcall cmp) '(<))
	    ,(remap-symbols cmp-expr `((,var . a)))
	    ,(remap-symbols cmp-expr `((,var . b)))))))

(defun keymap (&optional parent &rest bindings)
  (let: out (make-sparse-keymap) in
    (apply 'jason-defkeys out bindings)
    (if parent (set-keymap-parent out parent))
    out))

(defmacro receive (vars expr &rest body)
  `(apply (lambda ,vars ,@body)
	  ,expr))

(defmacro upon (pred &rest clauses)
  (receive (then-clause else-clause) (lsplit clauses :else)
    `(if ,pred 
	 ,(if (cdr then-clause) 
	      (cons 'progn then-clause)
	    (car then-clause))
       ,@else-clause)))

(defmacro guard (&rest clauses)
  (let: clauses (lsplit-all (cdr clauses) '|) in
    `(cond ,@clauses)))

(defmacro cps> (var expr &rest body)
  (maptree
   (lambda (x)
     (if (eq x :cont!)
	 `(lambda ,(if (listp var) var (list var))
	    ,@body)
       x))
   expr))

(defun copy-vector (v)
  "Returns copy of vector v.  Copying is not recursive, i.e. the
identity of the elements in the return value will be identical to
those in v."
  (let: out (make-vector (length v) nil) in
    (dotimes (i (length v))
      (aset out i (aref v i)))
    out))

(defun vset (v ix val)
  "Returns a copy of v with index ix set to val."
  (let: out (copy-vector v) in
    (aset out ix val)
    out))

(defun vmap* (proc v)
  (let: n 0 in
    (mapcar
     (lambda (x)
       (prog1 
	   (funcall proc x n)
	 (setq n (+ n 1))))
     v)))

(defun vsplice (v new ix &optional add strip-nulls)
  "Splice a list of elements NEW into vector V at position IX.
If ADD is non-nil, then element at position IX is shifted down.  Otherwise, element
at position IX is removed, and elements of NEW are spliced in."
  (if (= ix (length v)) 
      (append v new)
    (apply 'append
	   (vmap*
	    (lambda (x i)
	      (let: out (if (and strip-nulls (not x)) nil (list x)) in
		(if (= i ix)
		    (if add
			(append new out)
		      new)
		  out)))
	    v))))

(defun chain-ref (alist &rest refs)
  "Descend a chain of alist or vector references"
  (while refs
    (cond
     ((not alist) (setq refs nil))
     ((consp alist)
      (setq alist (cdr (assoc (pop refs) alist))))
     ((hash-table-p alist)
      (setq alist (gethash (pop refs) alist)))
     ((vectorp alist)
      (setq alist (aref alist (pop refs))))))
  alist)

(defun refs (alist &rest refs)
  (mapcar (x: (chain-ref alist x)) refs))

(defmacro iflet (var expr then &rest else)
  (declare (indent 2))
  `(let ((,var ,expr)) (if ,var ,then ,@else)))

(defmacro when-let (var expr &rest then)
  (declare (indent 2))
  `(iflet ,var ,expr (progn ,@then)))

(defmacro do-wad (&rest exprs)
  `(let* ((_ ,(car exprs))
          ,@(lc `(_ ,(if (symbolp x) `(,x _) x)) x (cdr exprs)))
     _))

(defalias '>>> 'do-wad)

(defun maphash* (proc &rest tabs)
  "Calls PROC with (KEY . VALUES), where VALUES is a list of values associated with
that particular key for each hash table passed in.  PROC is only called for keys that
exist in every passed-in hash table."
  (maphash
   (lambda (k v)
     (let: values (lc (gethash k tab) tab tabs) in
       (if (all values)
	   (funcall proc (cons k values)))))
   (car tabs)))

(defun vrot (v amt)
  "Rotate a vector in-place such that element 0 ends up at 
index AMT (with wrappage).

abs(AMT) should be less than the length of V."
  (if (< amt 0) (setq amt (+ amt (length v))))
  (let: len (length v)
	cycle-len (/ len (gcd len amt)) 
	ncycles (/ len cycle-len)
	tmp nil 
	ix nil in
    (dotimes (i ncycles)
      (setq ix (+ i amt))
      (dotimes (j cycle-len)
	(setf tmp (aref v ix)
	      (aref v ix) (aref v i)
	      (aref v i) tmp
	      ix (mod (+ ix amt) len))))
    v))

(defmacro foo-cond (&rest clauses)
  "Kind of like `cond', except CLAUSES take the form
  
     (FOO BAR => BAZ ...)

where FOO is the condition variable, BAR is a variable name that
gets bound to the result of evaluating FOO (if FOO is non-nil) for
the duration of the execution of BAZ."
  (when clauses
    (let: clause (car clauses)
	  pred (car clause) 
	  consequent (cdddr clause) in
      `(let: ,(caddr clause) ,pred in
	 (if ,(caddr clause)
	     (progn ,@consequent)
	   (foo-cond ,@(cdr clauses)))))))

(defun hash-intersect (a &rest b)
  (let: out nil in
    (maphash (lambda (k v)
	       (if (all (lc (gethash k x) x b))
		   (push k out)))
	     a)
    out))

(defun num-to-english (n)
  (cond
   ((< n 1) nil)
   ((< n 20) (cons (aref [zero one two three four five six seven eight nine ten 
			       eleven twelve thirteen fourteen fifteen sixteen 
			       seventeen eighteen nineteen]
			 (truncate n))
		   nil))
   ((< n 100) (cons (aref [zero ten twenty thirty forty fifty sixty seventy 
				eighty ninety]
			  (/ (truncate n) 10))
		    (num-to-english (mod n 10))))
   ((< n 1000) (append (num-to-english (/ n 100))
		       '(hundred)
		       (if (>= (mod n 100) 1) '(and) nil)
		       (num-to-english (mod n 100))))
   (t (let: tenpow (truncate (log10 n))
	    powten (expt 10.0 (- tenpow (mod tenpow 3)))
	    pownames [zero thousand million billion trillion quadrillion] in
	(append
	 (num-to-english (/ n powten))
	 (list (aref pownames (/ tenpow 3)))
	 (if (and (< (mod n powten) 100)
		  (= powten 1000)
		  (/= (mod n powten) 0)) '(and) nil)
	 (num-to-english (mod n powten)))))))

(defun strip (s)
  (replace-regexp-in-string
   (rx (or (: bos (* (any " \n\r\t")))
	   (: (* (any " \n\r\t")) eos)))
   "" s))

(defun lstrip (s)
  (replace-regexp-in-string
   "^[ \n\r\t]*" "" s))

(defun rstrip (s)
  (replace-regexp-in-string
   "[ \n\r\t]*$" "" s))

(defun n-group (i n)
  "Returns the range of numbers between the two multiples of n
nearest to i.

For example, (n-group 8 5) => (5 6 7 8 9)"
  (let: start (* (/ i n) n) in
    (range start (+ start n))))

(defun transpose (columns) (if columns (apply 'mapcar* 'list columns)))

(defun max-by (l key &optional greater)
  "Finds the maximum element of L, using the value returned by the function KEY as the
item to compare by.  If GREATER is provided, it is used to do the actual comparison.  Otherwise,
'< is used by default"
  (when (not greater) (setq greater '>))
  (reduce (lambda (&rest args)
            (pcase args
              (`(,a ,b) (if (funcall greater (funcall key a) (funcall key b))
                            a b))
              (x nil)))
          l))

(defun common-length (a b)
  "Returns the length of the shared root of strings A and B, i.e.
\t(common-length \"finnegan\" \"finnagle\") => 4"
  (catch 'break
    (dotimes (i (min (length a) (length b)))
      (unless (eq (aref a i) (aref b i))
        (throw 'break i)))
    (min (length a) (length b))))

(defun make-server (name port filter)
  "Make a TCP server."
  (make-network-process
   :name name :buffer nil
   :service port :family 'ipv4
   :filter filter :server t))

(defmacro define-function-macros ()
  (let: names (mapcar (funcomp intern char-to-string) (range ?a (+ ?z 1))) in
    `(progn
       ,@(lc `(defmacro ,(symbol-cat name ':) (&rest body)
               `(lambda (,',name) ,@body))
             name names))))

(put 'receive 'lisp-indent-function 2)
(put 'upon 'lisp-indent-function 1)
(put 'cps> 'lisp-indent-function 2)

(defmacro fastrun (&rest body)
  `(funcall
    (byte-compile
     ,(do-wad
       `(lambda () ,@body)
       (if (and (boundp 'lexical-binding) lexical-binding)
           (list 'quote _)
         _)))))

(defun str~ (regexp s &optional group)
  (save-match-data
    (if (string-match regexp s)
        (if (consp group)
            (mapcar (cut match-string <> s) group)
          (match-string (or group 0) s)))))

(defun killsave (filename)
  (interactive (list (read-file-name "Save to file: ")))
  (if (file-exists-p filename)
      (write-region (str "\n" (get-region) "\n") nil filename t)
    (write-region (region-beginning) (region-end) filename))
  (delete-region (region-beginning) (region-end)))


(defmacro consmap (newcar newcdr)
  "Produces a function that takes in a cons cell, and yields a new cons cell
produced by evaluating NEWCAR and NEWCDR with the variables a and b bound to
the car and cdr of the input argument, respectively."
  (let: arg (gensym) in
    `(lambda (,arg)
       (let: a (car ,arg)
             b (cdr ,arg) in
         (cons ,newcar ,newcdr)))))

(defmacro interp (&rest templates)
  (with-temp-buffer
    (dolist (s templates) (insert s))
    (let ((last-point (point-min)) exprs)
      (goto-char (point-min))
      (while (re-search-forward "#," nil t)
	(push (buffer-substring last-point (match-beginning 0)) exprs)
	(let ((expr (read (current-buffer))))
	  (push (if (vectorp expr) (aref expr 0) expr) exprs)
	  (setf last-point (point))))
      (push (buffer-substring (point) (point-max)) exprs)
      (cons 'str (filter (comp 'not (cur 'equal "")) (nreverse exprs))))))

(defmacro w/gensyms (syms &rest body)
  `(let ,(lc `(,x ',(gensym)) x syms)
     ,@body))

(put 'w/gensyms 'lisp-indent-function 1)

(defun strip-special-arglist (arglist)
  (lc x x arglist (not (startswith (symbol-name x) "&"))))

(defmacro mksetf (name arglist newval store-expr get-expr)
  (let: tmp-arglist (lc (if (startswith (symbol-name x) "&")
                            x
                          (symbol-cat x '-arg)) x arglist) in
    `(define-setf-method ,name ,tmp-arglist
       (w/gensyms (,@(strip-special-arglist arglist) ,newval)
                  (list (list ,@(strip-special-arglist arglist))
                        (list ,@(strip-special-arglist tmp-arglist))
                        (list ,newval)
                        ,store-expr
                        ,get-expr)))))

(mksetf str~ (rx s &optional group) new
        `(progn (string-match ,rx ,s)
                (setf ,s-arg
                      (replace-match ,new t t ,s ,group)))
        `(str~ ,rx ,s ,group))

(defun single-expand (expr)
  (let: mac (cdr (symbol-function (car expr))) in
    (apply mac (cdr expr))))

(mksetf assoc (key alist) new
        `(setcdr (assoc ,key ,alist) ,newval)
        `(cdr (assoc ,key ,alist)))

(defun mklink (txt fun &optional face)
  "Make a simple buttony sort of thing, without
using the full button.el business."
  (propertize
   txt 'keymap
   (keymap nil (kbd "RET") fun
           (kbd "<mouse-2>") fun)
   'button t
   'follow-link t
   'face (or face 'jf-linky-face)
   'font-lock-face (or face 'jf-linky-face)
   'mouse-face 'highlight))

(defun split-by (func l)
  (cond
   ((not l) '())
   ((funcall func (car l)) (cons '() (split-by func (cdr l))))
   (t (let: r (split-by func (cdr l)) in
        `((,(car l) ,@(car r)) ,@(cdr r))))))

(make-face 'jf-linky-face)
(set-face-attribute
 'jf-linky-face
 nil :foreground "#0000ff")

(defmacro once-only (names &rest body)
  (let: name-temps (lc (gensym) x names) in
    ``(let ,(mapcar* 'list ',name-temps (list ,@names))
        ,(let ,(mapcar* 'list names (lc `',x x name-temps))
           ,@body))))

(put 'once-only 'lisp-indent-function 1)

(defun ldump (l) (apply 'dump (join l " ")) (dump "\n"))

(defun powset (l n) (apply 'cross (lc l x (range n))))

(defun arefs (v &rest indices)
  (lc (aref v ix) ix indices))

(defun vecwad (op v &rest indices)
  (apply op (apply 'arefs v indices)))

(defmacro vardumps (&rest vars)
  "Dump the expressions given in VARS (they don't necessarily have
to be variable) when the *debug* flag is set."
  (if (and (boundp '*debug*) *debug*)
      `(progn 
         ,@(lc `(dump ',var ": " ,var "\n") var vars)
         (dump "\n"))
    nil))

(defun areverse (v &optional start end)
  (let: start (or start 0) end (or end (- (length v) 1)) in
    (while (> end start) 
      (rotatef (aref v start) (aref v end))
      (decf end) (incf start)))
  v)

(defmacro arot (array &rest indices)
  (w/gensyms (a)
    `(let: ,a ,array in
       (rotatef ,@(lc `(aref ,a ,ix)
                      ix indices)))))

(defun next-perm (perm)
  (let: tail-ix (- (length perm) 1) in
    (while (and (> tail-ix 0)
                (vecwad '>= perm (- tail-ix 1) tail-ix))
      (decf tail-ix))
    (if (= tail-ix 0)
        nil
      (areverse perm tail-ix)
      (loop for i from tail-ix below (length perm)
            while (vecwad '<= perm i (- tail-ix 1))
            finally return 
            (progn (arot perm i (- tail-ix 1))
                   (rpn -1 
                        (length perm) tail-ix - 2 / 
                        1 + expt))))))

(defun do-perms (proc vec)
  (funcall proc vec 1)
  (let: perm-result nil parity 1 in
    (while (setq perm-result (next-perm vec))
      (setq parity (* parity perm-result))
      (funcall proc vec parity))))

(defun re-search (re s)
  (string-match re s)
  (lc (apply 'substring s x) x (sublist-n (match-data) 2)))

(defun common-suffix (a b)
  (let  ((alen (length a))
         (blen (length b)))
    (loop for i from 1 upto (min (length a) (length b))
          while (= (aref a (- alen i)) (aref b (- blen i)))
          finally return (substring a (+ (- alen i) 1)))))

(defun unquote-printable (s)
  (replace-regexp-in-string
   (rx "=" (group (or (: (any hex-digit) (any hex-digit))
                      "\n")))
   (lambda (x)
     (if (equal (match-string 1 x) "\n")
         ""
       (char-to-string
        (string-to-number (match-string 1 x) 16))))
   s nil t))

(defmacro def-reader (name default-prompt &rest body)
  "Define a function that does a completing read from
the minibuffer.  BODY should return a collection of some
sort that is appropriate for passing to completing-read."
  (w/gensyms (completions prompt)
    `(progn
       (defvar ,(symbol-cat name '-history) nil)
       (defun ,name (&optional ,prompt)
         (let: ,completions (progn ,@body) in
           (completing-read
            (or ,prompt ,default-prompt)
            ,completions
            nil nil nil ',(symbol-cat name '-history)))))))

(defun get-digits (arg)
  (case arg
    ((nil) "0123456789")
    (hex "0123456789abcdef")
    (octal "01234567")
    (alphabet "abcdefghijklmnopqrstuvwxyz")
    (base64 "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/")
    (t arg)))

(defun num2str (num &optional digits)
  (let: real-digits (get-digits digits)
        base (length real-digits) sign "" in
    (when (< num 0) (setq num (* num -1) sign "-"))
    (do ((n num (/ n base)) (out nil (cons (aref real-digits (mod n base)) out)))
        ((= n 0) (concat sign out)))))

(defun str2num (str &optional digits)
  (let: digits (get-digits digits)
        base (length digits)
        digmap (alist->dict (mapcar* 'cons digits (range base))) in
    (reduce (lambda (a b) (+ (* a base) b)) (lc (gethash x digmap) x (append str nil)))))

(defun overlay-add (ov &rest props)
  (while props
    (overlay-put ov (car props) (cadr props))
    (setq props (tail props 2))))

(defun group-by (l func &optional alist)
  (let: out (make-hash-table :test 'equal) in
    (ec (push x (gethash (funcall func x) out)) x l)
    (if alist (hash->list out) out)))

(defun count-by (l func &optional alist)
  (let: out (make-hash-table :test 'equal) in
    (if (hash-table-p l)
        (maphash 
         (lambda (k v) 
           (incf (gethash (funcall func (cons k v)) out 0)))
         l)
      (ec (incf (gethash (funcall func x) out 0)) x l))
    (if alist (hash->list out) out)))

(defun maphash! (proc tab)
  "Runs PROC over every key-value pair in tab, and changes the value
of said key to whatever PROC returns."
  (maphash
   (lambda (k v) (puthash k (funcall proc k v) tab))
   tab)
  tab)

(defmacro w/cd (dir &rest body)
  `(with-default-dir (expand-file-name ,dir) ,@body))

(defun hash-keys (tab)
  (let: out nil in (maphash (lambda (k v) (push k out)) tab) out))

(put 'w/cd 'lisp-indent-function 1)

(defun memoize (fun)
  (let: f (symbol-function fun)
           tab (make-hash-table :test 'equal) in
    (setf (symbol-function fun)
          (lambda (&rest args)
            (or (gethash args tab)
                (setf (gethash args tab) (apply f args)))))))

(defun nilfunc (&rest any-args) nil)

(defun or-comb (&rest funcs)
  (lambda (&rest args)
    (loop for i in funcs do
          (iflet x (apply i args) (return x)))))

(defun rows-and-columns (text)
  (do-wad (split-string text "\n" t) (mapcar 'split-string _)))

(defun nfold (accum init n)
  (loop for i from 0 below n do (setf init (funcall accum init i))) init)

(defun file-sample (file num)
  "Sample NUM lines out of FILE"
  (do-wad
   (process-lines "wc" file) car split-string car
   read (lc (random _) x (range num)) (sort _ '<)
   (mapcar* (lambda (a b) (list (+ a 1) (- b 1))) _ (cdr _))
   (if (> (caar _) 2) (cons (list 1 (- (caar _) 2)) _) _)
   (append _ (list (list (+ (cadr (lastcar _)) 2) '$)))
   (lc (list "-e" (str (strjoin "," (mapcar 'str x)) 'd)) x _)
   (apply 'append _) (apply 'process-lines `("sed" ,@_ ,file))))

(defun gen-password-2 ()
  (process-lines "line-sample" "/usr/share/dict/words" "4"))

(defun dump-histogram (bucket-hash &optional sort-key y-scale)
  (if (not sort-key) (setf sort-key 'identity))
  (do-wad (hash->list bucket-hash)
          (sortbag _ x (funcall sort-key (car x)))
          (lc (list (car x) (str* "=" (/ (cdr x) (or y-scale 1))) (cdr x)) x _)
          pr-align))

(defun minmax-keys (tab)
  (hash-reduce
   (λ (a (b . c))
     (if (not a) (setf a (list b b)))
     (list (min (car a) b) (max (cadr a) b)))
   nil tab))

(defmacro alist-let (keys alist &rest body)
  (if (vectorp alist)
      `(let ((,(aref alist 0) ,(aref alist 1)))
         (alist-let ,keys ,(aref alist 0) ,@body))
    `(let ,(lc (list x `(assoc-default ',x ,alist))
               x keys)
       ,@body)))

(defun fill-string (s)
  (with-temp-buffer
    (insert s) (fill-region (point-min) (point-max))
    (buffer-string)))

(defun fill-lines (s)
  (do-wad (split-string s "\n\n" t)
          (lc (strjoin "\n" (mapcar 'fill-string (split-string x "\n" t)))
              x _)
          (strjoin "\n\n" _)))

(defun humanize (str)
  (do-wad (split-string str "_" t) (mapcar 'capitalize _) (strjoin " " _)))

(defun boldify (s)
  (propertize s 'font-lock-face '(:weight bold)))

(defun colorize (s color)
  (propertize s 
              'face `(:foreground ,color)
              'font-lock-face `(:foreground ,color)))

(defun make-set (&optional elements)
  (do-wad
   (make-hash-table :test 'equal)
   (if elements
       (progn (mapc (x: (puthash x t _)) elements) _)
     _)))

(defun set-add (s e)
  (puthash e t s))

(defun set->list (s)
  (mapcar 'car (hash->list s)))

(defun !equal (a b)
  (not (equal a b)))

(defun alist-checker (key value)
  (lambda (alist)
    (do-wad
     (if (consp key) (apply 'chain-ref alist key) (chain-ref alist key))
     (if (functionp value) 
         (funcall value _)
       (equal _ value)))))

(defun apply-fns (value functions)
  (lc (funcall f value) f functions))

(defmacro cut (&rest stuff)
  (let: args (lc (gensym) x stuff (eq x '<>)) in
    `(lambda ,args
       ,(lc (if (eq x '<>) (pop args) x) x stuff))))

(defun downcase-tree (tree)
  (maptree 
   (x: (if (symbolp x) (intern (downcase (symbol-name x))) x)) tree))

(defun index-by (items func &optional mapper)
  (let: out (make-hash-table :test 'equal) in
    (ec (puthash (funcall func x) (if mapper (funcall mapper x) x) out) x items)
    out))

(defun comp (&rest funcs)
  (fold (lambda (a b) (lambda (&rest args) (funcall b (apply a args)))) (cdr funcs) (car funcs)))

(defun* curl (url cont &key data headers (method "GET") remote
                  user)
  (let: curl-params
        `(,url ,@(if data (list "--data-binary" data) nil)
               ,@(if headers 
                     (apply 'append 
                            (lc (list "-H" (str (car x) ": " (cdr x)))
                                x headers))
                   nil)
               ,@(if user (list "--user" user))
               "-X" ,method) in
    (if remote
        (apply remote "curl" cont curl-params)
      (apply 'jf-proc "curl" cont curl-params))))

(defun list-bind (monadic-func monadic-val)
  (apply 'append (mapcar monadic-func monadic-val)))

(defun list-return (val)
  (list val))

(defmacro >> (monad-name var expr &rest body)
  `(,(symbol-cat monad-name '-bind)
    (lambda (,var) ,@(if (eq (car body) 'in)
                         `((,(symbol-cat monad-name '-return) 
                            (progn ,@(cdr body))))
                       `((>> ,monad-name ,@body))))
    ,expr))

(defmacro unstructify (template expr)
  "Takes an input destructuring-bind template TEMPLATE and expression EXPR,
and returns an alist with keys being the variable-names in TEMPLATE, and the
values being their respective values."
  (let: vars nil in
    (maptree (x: (push x vars)) template)
    `(destructuring-bind ,template ,expr
       (list ,@(lc `(cons ',var ,var) var vars)))))

(defun derived-bind (&rest bindings)
  (use-local-map (apply 'keymap (current-local-map) bindings)))

(defun file-lines (f)
  (with-temp-buffer
    (insert-file-contents-literally f)
    (split-string (buffer-string) "\n")))

(defun juxt (&rest funcs)
  (lambda (&rest args) (mapcar (cut apply <> args) funcs)))

(defun first-arg (&rest args)
  (car args))

(defun maybe (fun pred)
  "Returns a function that calls FUN on its argument if the result of
PRED on its argument is non-nil, else returns nil"
  (lambda (arg)
    (if (funcall pred arg) (funcall fun arg) nil)))

(defun const (x)
  (lambda (&rest args) x))

(defalias 'dbind 'destructuring-bind)

(defun vec (l)
  (cond
   ((consp l)
    (let: len (length l)
          out (make-vector len nil) in
      (dotimes (i len)
        (setf (aref out i) (pop l)))
      out))
   ((vectorp l) l)))

(defmacro dbind-alist (template expr)
  "Evaluate EXPR, destructure the results according to TEMPLATE, and then
dump said results into an alist."
  `(destructuring-bind ,template ,expr
     (list ,@(let: out nil in
               (maptree (x: (push `(cons ',x ,x) out))
                        template)
               out))))

(defun ssh-link (server)
  (mklink server
          (keycmd
           (if current-prefix-arg
               (setf current-server-set
                     (lc x x current-server-set (not (equal x server))))
             (do-wad (cons server current-server-set) delete-duplicates
                     (setf current-server-set _)))
           (ec (pr x "\n") x current-server-set))))

(defun multi-exec-default (server cmd-out)
  (proper-dump (ssh-link server) "\n"
               ;; (colorize 
               ;;  (chain-ref (gethash server instances-by-dns) 'tags "Name")
               ;;  "#ff0000")
               
               "\n"
                cmd-out "\n"))

(defun multi-exec (username servers cmds &optional cont final-cont)
  "Calls CONT like (funcall cont hostname cmd-output) for each server in SERVERS."
  (mapcar
   (lambda (s)
     (jf-proc "ssh"
              (x: (pop servers)
                  (funcall (or cont 'multi-exec-default) s x)
                  (if (and (null servers) final-cont)
                      (funcall final-cont)))
              "-A"
              (str username "@" s)
              cmds))
   servers))

(defun juxtcons (a b)
  (lambda (&rest args) 
    (cons (apply a args) (apply b args))))

(defun =~ (re &optional group) (cut str~ re <> group))

(defun := (&rest keys)
  (lambda (tab) (apply 'chain-ref tab keys)))

(defun cur (f &rest args)
  (lambda (&rest more-args)
    (apply f (append args more-args))))

(defun orfn (&rest fns)
  (lambda (x)
    (some (cut funcall <> x) fns)))

(defun andfn (&rest fns)
  (lambda (x)
    (every (cut funcall <> x) fns)))

(defun sample (seq)
  (elt seq (random (length seq))))

(setf horizontal-split-threshold 6)

(defun n-windows (n &optional hthresh)
  (if (> n 1)
      (let: horizontal (> n (or hthresh horizontal-split-threshold))
            new-window-sections (if horizontal
                                    (or hthresh horizontal-split-threshold)
                                  (/ n 2)) in
        (do-wad 
         (if horizontal (split-window-horizontally) (split-window-vertically))
         (append (with-selected-window _ (n-windows new-window-sections hthresh))
                 (n-windows (- n new-window-sections) hthresh))))
    (list (selected-window))))

(defun show-bufs (bufs)
  (mapcar*
   (lambda (win buf) 
     (with-selected-window win
       (switch-to-buffer buf)))
   (window-list)
   bufs))

(defun remote-kill (s &optional junk)
  (interactive)
  (curl "http://localhost:12345/paste-listener/" 'pr
        :data s :method "POST"))

(defun strpad (s n c where)
  (if (< (length s) n)
      (do-wad s length (- n _) (make-string _ c)
              (if (eq where :start) (concat _ s) (concat s _)))
    s))

(defun 0pad (n &optional pad)
  (strpad (str n) (or pad 2) ?0 :start))

(defun fixup-window-tree (tree)
  (if (consp tree)
      (cons (if (car tree) 'v 'h)
            (mapcar 'fixup-window-tree (cddr tree)))
    (buffer-name (window-buffer tree))))

(defun get-windows ()
  (do-wad
   (window-tree) car
   fixup-window-tree))

(defun window-siblings (win)
  (>>> (window-right win)
       (if _ (cons _ (window-siblings _)))))

(defun window-root (win)
  (while (window-parent win)
    (setf win (window-parent win)))
  win)

(defun window-children (win)
  (>>> (window-child win)
       (and _ (cons _ (window-siblings _)))))

(defun window-split-dir (win)
  (cond ((window-left-child win) 'h)
        ((window-top-child win) 'v)
        (t nil)))

(defun jf-win-height (win)
  (dbind (left top right bottom) (window-edges win)
    (- bottom top)))

(defun jf-win-width (win)
  (dbind (left top right bottom) (window-edges win)
    (- right left)))

(defun jf-balance-window-children (win)
  "Balance all the child windows of the given window, nonrecursively"
  (let:: children (window-children win)
         w (/ (jf-win-width win) (length children))
         h (/ (jf-win-height win) (length children))
         adjust (case (window-split-dir win)
                  (h (x: (adjust-window-trailing-edge
                          x (- w (jf-win-width x)) t)))
                  (v (x: (adjust-window-trailing-edge
                          x (- h (jf-win-height x)))))) in
    (mapc adjust (butlast children))))

(defun jf-balance-windows (&optional win)
  (interactive)
  (let* ((win (or win (window-root (selected-window))))
         (children (window-children win)))
    (when children
      (jf-balance-window-children win)
      (mapc 'jf-balance-windows children))))

(defun apply-windows (tree)
  (pcase tree
    ((pred stringp) (switch-to-buffer tree))
    (`(,dir ,child) (apply-windows child))
    (`(,dir ,child . ,children)
     (do-wad
      (if (eq dir 'h) (split-window-horizontally) (split-window-vertically))
      (progn 
        (apply-windows child)
        (jf-balance-windows)
        (select-window _)
        (apply-windows `(,dir ,@children)))))))

(defun find-string-rect (s)
  (let: rows (split-string (str s) "\n")
        width (apply 'max (mapcar 'length rows)) in
    (list (length rows) width )))

(defun print-tabular (rows sep &optional vsep)
  (let: cell-rects (mapcar (cur 'mapcar 'find-string-rect) rows)
        column-widths (mapcar (comp 'cadr (cut max-by <> 'cadr)) (transpose cell-rects))
        row-heights (mapcar (comp 'car (cut max-by <> 'car)) cell-rects)
        row-width (+ (sum column-widths) (* (- (length column-widths) 1) (length sep)))
        vsep (if vsep (str "\n" (str* vsep row-width) "\n") "\n") in
    (do-wad
     (mapcar* (cut tabular-cell-row column-widths <> <> sep)
              rows row-heights)
     (strjoin vsep _)
     )))

(defun list-pad (l n item where)
  (let: len (length l) in
    (if (< len n)
        (if (eq where :start)
            `(,@(lc item x (range (- n len))) ,@l)
          `(,@l ,@(lc item x (range (- n len)))))
      l)))

(defun tabular-cell-row (widths row height separator)
  (do-wad
   (mapcar (comp (cut list-pad <> height "" :end)
                 (cut split-string <> "\n") 'str) 
           row)
   transpose
   (mapcar (comp (cur 'strjoin separator)
                 (cut mapcar* (cut strpad <> <> ?\  :end) <> widths))
    _)
   (strjoin "\n" _)))

(defun rate-retriever (counter)
  (let* ((count-fn counter)
         (last-count (funcall count-fn))
         (first-count last-count)
         (first-time (current-time))
         (last-time first-time))
    (lambda ()
      (let* ((new-count (funcall count-fn))
             (new-time (current-time))
             (dt (time-to-seconds (time-subtract new-time last-time)))
             (d (- new-count last-count)))
        (setf last-count new-count last-time new-time)
        (list (cons 'recent (/ d dt))
              (cons 'total (/ (- last-count first-count) 
                              (time-to-seconds (time-subtract last-time first-time))))
              )))))

(defun group-reduce (l key group-mapper &optional reducer)
  (do-wad
   (group-by l key t)
   (mapcar (juxtcons 
            'car 
            (comp
             (if reducer (cut reduce reducer <>) 'identity)
             (cur 'mapcar group-mapper)
             'cdr))
           _)))

(defun tab-delimited (buf)
  (do-wad
   (w/buf buf (split-string (buffer-string) "\n" t))
   (mapcar (cut split-string <> "\t") _)))

(defun iterations (val fun pred)
  (if (funcall pred val)
      nil
    (cons val (iterations (funcall fun val) fun pred))))

(defun l-iterations (val fun pred)
  (let (out) 
    (while (not (funcall pred val))
      (push val out)
      (setf val (funcall fun val))) out))

(defun line-sample (file)
  (do-wad
   (file-attributes file) (nth 7 _) random
   (interp "tail -c +#,(+ _ 1) #,file | tail -n +2 | head -n 10")
   shell-command-to-string))

(defun reductions (fun l init)
  (reduce
   (lambda (a b) (cons (funcall fun (car a) b) a))
   l :initial-value (list init)))

(defun hash-join (a ka b kb)
  (let: atab (group-by a ka)
        btab (group-by b kb)
        out nil in
    (maphash 
     (lambda (k v)
       (>>> (cross v (gethash k btab))
            (setf out (append _ out))))
     atab)
    out))

(defalias '^ 'logxor)
(defalias '& 'logand)
(defalias '| 'logior)
(defalias 'o 'comp)

(defun *: (fun) (cur 'apply fun))

(defun cur* (funs &rest other-args)
  (apply 'juxt (mapcar (cut apply 'cur <> other-args) funs)))

(defun dispatch-fn (&rest stuff)
  "Create a vaguely object-like thing.  Usually intended to be used
in situations where you want to return a set of lexical closures 
over the same environment.  When called like 

 (let ((smegma (dispatch-fn :foo fn1 :bar fn2))) ...)

the returned function will call fn1 when given :foo as its first
argument, fn2 with :bar as its first arg, etc."
  (>>> (mapcar (*: 'cons) (sublist-n stuff 2))
       (lambda (action &rest args)
         (apply (cdr (assq action _)) args))))

(defmacro fnobj (&rest stuff)
  "Convenience macro for `dispatch-fn', which see."
  `(letrec ((this (dispatch-fn 
                   ,@(mapcan (x: `(,(car x) (lambda* ,(cadr x) ,@(cddr x)))) stuff))))
           this))

(defun cons* (&rest args)
  (pcase args
    (`(,a ,b) (cons a b))
    (`(,a . ,b) (cons a (apply 'cons* b)))
    (x (error "WTF mate!"))))

(define-function-macros)

(defun dehexify (s)
  "Turns a string consisting of bytes written out as hex digits into
a unibyte string, i.e. (dehexify \"3f91a7\") => (unibyte-string #x3f #x91 #xa7)"
  (>>> (comp 'char-to-string (cut string-to-number <> 16))
       (replace-regexp-in-string (rx (repeat 2 hex-digit)) _ s t t)
       string-make-unibyte))

(defmacro defun-case (name &rest patterns)
  (let ((args (gensym)))
    `(defun ,name (&rest ,args)
       (pcase ,args
         ,@patterns))))

(defun mapcur (fun)
  "A curried version of mapcar*."
  (cur 'mapcar* fun))

(defun bsearch (v leftp)
  "Does a binary search in sorted vector V.  Predicate LESSP will be
called like (funcall leftp <array-element>), and should return true if
the search should be continued in the left half of the array.  Return
value is the index at which you would insert an item such that all items
before the insertion would have leftp false, and all items afterwards would
have lessp true."
  (let: start 0 end (- (length v) 1) in
    (while (> (- end start) 1)
      (let: mid (/ (+ start end) 2) in
        (if (funcall leftp (aref v mid))
            (setf end mid)
          (setf start mid))))
    (cond ((funcall leftp (aref v start)) start)
          ((funcall leftp (aref v end)) end)
          (t (+ end 1)))))

(defun vsplice (v pos &rest items)
  "Splice ITEMS into the vector V at position POS"
  `[,@(subseq v 0 pos) ,@items ,@(subseq v pos)])

(defun .. (s start &optional end)
  (if s (subseq s start end) nil))

(defun map-keymap-recursive (fun km &optional prefix)
  (map-keymap
   (lambda (event def) 
     (if (keymapp def) 
         (map-keymap-recursive fun def (cons event prefix))
       (funcall fun (format-kbd-macro (vec (reverse (cons event prefix)))) def)))
   km))

(defun once-fun (f)
  "Returns a new version of input function F that only ever runs once."
  (let: run nil result nil in
    (lambda (&rest args)
      (if (not run) (setf run t result (apply f args)) result))))

(defun gensymify (expr)
  (let: gensym-tab (make-hash-table) in
    (maptree
     (x: (if (and (symbolp x) 
                  (not (eq x '_))
                  (endswith (symbol-name x) "_"))
             (get-or-add x gensym-tab (gensym))
           x))
     expr)))

(defun get-or-add (key tab dflt)
  (or (gethash key tab)
      (progn (puthash key dflt tab) dflt)))

(defmacro time (&rest body)
  (gensymify
   `(let: start_ (current-time)
          result_ (progn ,@body) in
      (print (time-to-seconds (time-subtract (current-time) start_)))
      result_)))

(defun blankp (x) (str~ "^[ \t\n]*$" x))

(defun inter-splice (lists join-list)
  "(inter-splice '((a b c) (d e f) (g h i)) '(foo bar)) => 
   (a b c foo bar d e f foo bar g h i)"
  (pcase lists
    (`(,x) x)
    (`(,x . ,more) `(,@x ,@join-list . ,(inter-splice more join-list)))))

(defun path (&rest args)
  (>>> (filter 'identity args) (mapcar 'str _) (strjoin "" _)))

(defmacro nlet (name bindings &rest body)
  (declare (indent 2))
  `(letrec ((,name (lambda (,@(mapcar 'car bindings))
                     ,@body)))
           (funcall ,name ,@(mapcar 'cadr bindings))))

(defun once-thunk (thunk)
  (let: realized nil
        result nil in
    (lambda () 
      (if realized result (setf realized t result (funcall thunk))))))

(defmacro do-once (&rest body)
  (declare (indent 0))
  `(once-thunk (lambda () ,@body)))

(defun log-thingy (file item)
  (>>> (if (file-exists-p file) (read-file file) nil)
       (cons (cons (current-time) item) _)
       repr (write-region _ nil file)))

(defun buffer-forms (&optional buffer)
  "Returns all the s-expressions in a buffer in a list."
  (w/buf (or buffer (current-buffer))
    (save-excursion
      (goto-char (point-min))
      (let (forms)
        (while (>>> (read/die) (progn (and _ (push _ forms)) _)))
        (nreverse forms)))))

(defun prefix-combinator (no-prefix prefix)
  (λ () (interactive) 
     (if current-prefix-arg
         (call-interactively prefix)
       (call-interactively no-prefix))))

(defmacro xy: (&rest body) `(lambda (x y) ,@body))

(defun* linterp ((x1 y1) (x2 y2) x)
  (+ y1 (* (/ (- x x1) (- x2 x1)) (- y2 y1))))

(defun interpolator (points)
  (let: segments (mapcar* 'list points (cdr points))
        first (car points) last (lastcar points) in
    (lambda (x)
      (pcase (find-if (λ (((x1 y1) (x2 y2))) (<=* x1 x x2)) segments)
        (`(,p1 ,p2) (linterp p1 p2 x))
        (`nil (if (< x (car first)) (cadr first) (cadr last)))))))

(defun vrem (v ix)
  "(vrem [a b c d e] 2) => [a b d e]"
  `[,@(subseq v 0 ix) ,@(subseq v (+ ix 1))])

(defun alast (v)
  (aref v (- (length v) 1)))

(defmacro and-let* (bindings &rest body)
  (pcase bindings
    (`() `(progn ,@body))
    (`((,var ,expr) . ,stuff)
     `(iflet ,var ,expr (and-let* ,stuff ,@body)))))

(defun vdrop-right (v n)
  "Drop N elements from V on the right."
  (subseq v 0 (- (length v) n)))

(defun vcons (v item)
  `[,item ,@(append v nil)])

(defun vcons-right (v item)
  `[,@(append v nil) ,item])

(defun ls (x &optional all)
  (directory-files x nil (if all nil "^[^.]")))

(defmacro cps-let (specs &rest body)
  "Like `let*', except the initializer expressions in the bindings
are evaluated with a continuation stored in the `cont!' local variable.
Calling `cont!' in each binding is necessary to actually complete the
evaluation of the form."
  (declare (indent 1))
  (pcase specs
    (`((,args ,expr) . ,more)
     (>>> (if (and args (symbolp args)) (list args) args)
          `(let: cont! (lambda ,_ (cps-let ,more ,@body)) in
             ,expr)))
    (`nil `(progn ,@body))))

(defun cps-foreach (proc l cont)
  (if l (cps-let ((a (funcall proc (car l) cont!)))
	  (cps-foreach proc (cdr l) cont))
    (funcall cont l)))

(defun cps-join (k-thunks k)
  "Given a list of thunks that accept a single
continuation argument, calls `k' with the results all of those
thunks.  Results will be passed as a vector, and in-order."
  (let: results (make-vector (length k-thunks) nil)
        remaining (length results) in
    (mapcar* (λ (thunk i)
                (let: called nil in
                  (funcall thunk
                           (λ result
                              (unless called
                                (aset results i result)
                                (decf remaining)
                                (setf called t)
                                (when (= remaining 0)
                                  (funcall k results)))))))
             k-thunks
             (range (length k-thunks)))))

(defmacro time-bind (time-val &rest body)
  `(dbind (sec min hour day month year _ dst zone) (decode-time ,time-val)
     ,@body))

(defun time-alist (&optional time-val)
  (mapcar* 'cons '(sec min hour day month year dow dst zone)
           (decode-time time-val)))

(defun iterate (fun stop-pred val)
  (while (not (funcall stop-pred val))
    (setf val (funcall fun val)))
  val)

(defun without-errors (f)
  (lambda (&rest args)
    (ignore-errors (apply f args))))

(defun min-key (lessp key)
  (lambda (a b)
    (if (funcall lessp (funcall key b) (funcall key a))
        b a)))

(defun get-in (coll keys)
  (catch 'break
    (while t
      (pcase keys
        (`(,k) (throw 'break (lookup coll k)))
        (`(,k . ,more) (setf coll (lookup coll k)
                             keys more))
        (x (error "WTF YOU SHOULD BE GIVING ME A KEY MOTHERFUCKER"))))))

(defun lookup (coll key)
  (case (type-of coll)
    ((string vector) (aref coll key))
    (cons (assoc-default key coll))
    (symbol (get coll key))
    (hash-table (gethash key coll))
    (t (error "Not lookupable!  Fuck you."))))

(setf hash-hasp-sentinel (make-symbol "foo"))

(defun group-agg (&rest args)
  (pcase args
    (`(,key-fn)
     (lambda (reducer)
       (lambda (a b)
         (setf (gethash (funcall key-fn b) a)
               (if (eq (gethash (funcall key-fn b) a hash-hasp-sentinel)
                       hash-hasp-sentinel)
                   b
                 (funcall reducer (gethash (funcall key-fn b) a) b)))
         a)))
    (`(,key-fn ,dflt)
     (lambda (reducer)
       (lambda (a b)
         (setf (gethash (funcall key-fn b) a)
               (funcall reducer (gethash (funcall key-fn b) a dflt) b))
         a)))

    (`(,key-fn ,dflt ,dflt-fn)
     (lambda (reducer)
       (lambda (a b)
         (setf (gethash (funcall key-fn b) a)
               (funcall reducer (gethash (funcall key-fn b) a (funcall dflt-fn)) b))
         a)))))

(defun mapcat-agg (f &optional reduce-call)
  (lambda (reducer)
    (lambda (a b)
      (if reduce-call
          (funcall reduce-call reducer a (funcall f b))
        (reduce reducer (funcall f b) :initial-value a)))))

(defun map-agg (fun)
  (lambda (reducer)
    (lambda (a b)
      (funcall reducer a (funcall fun b)))))

(defun filter-agg (f)
  (lambda (reducer)
    (lambda (a b)
      (if (funcall f b) (funcall reducer a b) a))))

(defun conj (a b)
  (cons b a))

(defun juxt-agg (&rest reducers)
  (lambda (a b)
    (mapcar* (lambda (r val)
               (funcall r val b))
             reducers a)))

(defun hash-reduce (r init tab)
  (maphash
   (lambda (k v)
     (setf init (funcall r init (cons k v))))
   tab)
  init)

(defun lexical-cmp (lessp)
  (lambda (a b)
    (catch 'break
      (while (and a b)
        (if (not (equal (car a) (car b)))
            (throw 'break (funcall lessp (car a) (car b)))
          (pop a) (pop b)))
      (if b t))))

(defun remote-call (host code cont)
  (jf-proc "ssh" (comp cont 'read) "-q" host "emacsclient" "-e"
           (>>> code repr shell-quote-argument)))

(defun in (set)
  (cut member <> set))

(defun cps-mapcar (proc l cont)
  (if l (cps-let ((a (funcall proc (car l) cont!))
                  (r (cps-mapcar proc (cdr l) cont!)))
          (funcall cont (cons a r)))
    (funcall cont l)))

(defun csv-row (line)
  (split-string line ","))

(defun buffer-csv (&optional splitter)
  (>>> (split-string (buffer-string) "\n" t)
       (mapcar (or splitter 'csv-row) _)))

(defmacro updatef (place fun &rest args)
  (gv-get place
          (lambda (getter setter)
            (funcall setter `(funcall ,fun ,getter ,@args)))))

(pcase-defmacro str (&rest args)
  (let: re `(rx ,@(mapcar (x: (if (stringp x)
                                  x
                                `(group (minimal-match (0+ anything)))))
                          (butlast args))
                ,(if (stringp (lastcar args))
                     (lastcar args)
                   `(group (* anything))))
        matches (mapcar (x: (list '\, x)) (filter (comp 'not 'stringp) args)) in
    `(app (lambda (x)
            (str~ ,re x ',(range 1 (+ (length matches) 1))))
          ,(list '\` matches))))

(defun replace-last-sexp (replacement)
  (let: start (save-excursion (backward-sexp) (point)) in
    (delete-region start (point))
    (insert replacement)))

(defun list->pathname (paths)
  (>>> (strjoin "/" paths)
       (replace-regexp-in-string "/+" "/" _)))

(defun jf-findwad (path-spec)
  (let: my-cd (λ (x thunk)
                 (let: default-directory default-directory in
                   (cd x)
                   (funcall thunk)))
    in
    (pcase path-spec
      (`(*) (mapcar 'list (ls ".")))
      (`(* . ,more)
       (mappend
        (x: (mapcar (cur 'cons x)
                    (funcall my-cd x
                             (λ () (jf-findwad (cdr path-spec))))))
        (filter
         'file-directory-p
         (ls "."))))
      (`(,x)
       (when (file-exists-p x)
         (list (list x))))
      (`(,x . ,more)
       (when (file-exists-p x)
         (funcall my-cd x (λ () (mapcar
                                 (cur 'cons x)
                                 (jf-findwad (cdr path-spec)))))))
      (`nil nil))))

(defun alist-merge (a b)
  (append (filter (λ((k . v)) (not (assoc k b)))
                  a)
          b))
