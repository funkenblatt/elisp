;; ITEM := (NAME TYPE)
;; TYPE := u64 | u32 | u16 | u8 | vec LEN TYPE | struct ITEM ...
;; LEN := INT | NAME

(defun make-unpacker (specs)
  (let: instrm (gensym) in
    `(lambda (,instrm)
       (let* (,@(mapcar (lambda (x)
			  (make-sub-unpacker x instrm))
			specs))
	 (list ,@(lc `(cons ',x ,x) x (mapcar 'car specs)))))))

(defun make-sub-unpacker (item strm)
  (if (car item)
      `(,(car item) ,(make-sub-unpacker-expr (cdr item) strm))
    (make-sub-unpacker-expr (cdr item) strm)))

(defun unpacker-type-size (primtype)
  (cdr (assq primtype '((u8 . 1) (u16 . 2) (u32 . 4) (u64 . 8)))))

(defun numify (s)
  (let: out 0 in
    (dotimes (i (length s))
      (incf out (ash (aref s i) (* i 8))))
    out))

(defun make-sub-unpacker-expr (typewad strm)
  (case (car typewad)
    (vec (make-vec-unpacker-expr typewad strm))
    (struct `(funcall ,(make-unpacker (cdr typewad))
		      ,strm))
    (str `(funcall ,strm ,(cadr typewad)))
    (otherwise
     `(numify
       (funcall ,strm ,(unpacker-type-size (car typewad)))))))

(defun make-vec-unpacker-expr (typewad strm)
  (let: veclen (cadr typewad)
	loopcount (gensym)
	vecvar (gensym) in
    `(let ((,vecvar (make-vector ,veclen nil)))
       (dotimes (,loopcount ,veclen)
	 (setf (aref ,vecvar ,loopcount)
	       ,(make-sub-unpacker-expr
		  (cddr typewad) strm)))
       ,vecvar)))

(defn make-string-port (s)
  (lexlet: ix 0 in
    (lambda (&optional n)
      (if n 
          (substring s ix (incf ix n))
        (list ix (length s))))))

;; ((magic vec 2 u32)
;;  (fanout vec 255 u32)
;;  (size u32)
;;  (sha1 vec size vec 20 u8))

;; (setq smegma
;;       (make-unpacker
;;        '((foo u32)
;; 	 (bar vec foo str 2))))

;; (funcall smegma
;; 	 "\x00\x00\x00\x03\x01\x02\x05\x02\x01\x01")

(provide 'unpacker)
