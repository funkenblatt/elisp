(require 'cps-filter)

(make-variable-buffer-local 'rfc-toc-location)

(defun rfc-toc-goto ()
  (interactive)
  
  (let: l (thing-at-point 'line)
	chap (and (string-match "[0-9A-F][0-9.]*" l) 
		  (match-string 0 l))
	win (selected-window)
	pt (point) in
;    (set-window-buffer win (current-buffer))
    (setq rfc-toc-location (point))
    (forward-line)
    (if chap
	(re-search-forward 
	 (str "^[ \t]*" 
	      (regexp-quote chap) "\\.?[^0-9.]")))))

(setq rfc-wad-map (make-sparse-keymap))
(set-keymap-parent rfc-wad-map jason-nav-map)
(jason-defkeys 
 rfc-wad-map 
 (kbd "RET") 'rfc-toc-goto
 (kbd "M-RET") (keycmd (get-rfc ""))
 (kbd "q") (keycmd (kill-buffer))
 (kbd "t") (keycmd (let: pt (point) in (goto-char rfc-toc-location) (setq rfc-toc-location pt))))

(defun get-rfc (rfc)
  (interactive "sRFC-#: ")
  (if (equal rfc "") (setq rfc (thing-at-point 'symbol)))
  (if (stringp rfc)
      (setq rfc (progn (string-match "[0-9]+" rfc) (match-string 0 rfc))))
  (let: buf (get-buffer-create (format "rfc-%s" rfc)) in
    (switch-to-buffer buf)
    (use-local-map rfc-wad-map)
    (setq buffer-read-only nil)
    (erase-buffer)
    (get-http (connect "www.ietf.org")
	      (lambda (s) 
		(dump s) 
		(goto-char (point-min))
		(setq buffer-read-only t))
	      (format "/rfc/rfc%s" rfc))))

(provide 'rfc-wad)
