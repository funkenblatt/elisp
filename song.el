(require 'snd)

(defmacro load-parts (&rest parts)
  (cons 'progn
	(lc
	 `(with-current-buffer (find-file-noselect ,(car part))
	    ,@(lc `(setq ,(car var) ,(cadr var))
		  var (sublist2 (cdr part))))
	 part parts)))

(defun get-songparts ()
  (lc (with-current-buffer x
	(list (buffer-name x) 'piano-templates `(quote ,piano-templates)
	      'piano-cell-beats piano-cell-beats))
      x (buffer-list)
      (eq (buffer-mode x) 'piano-roll-mode)))

(defun make-loadfile (filename)
  (interactive "sOutfile: ")
  (write-region
   (with-output-to-string
     (pr "(require 'song)\n\n")
     (pp
      `(load-parts
	,@(get-songparts))))
   nil filename))

(setq foo-drumwads '((120 drumwad :start :dur 500 0.9) (121 drumwad :start :dur 3000)
		     (?z . (drumwad :start :dur 5000))))

(setq foo-squaretooth '((120 squaretooth :start :dur :freq)
			(?y . (sawbag-2 :start :dur :freq :amp 0.09))
			(?z . (saw-wad :start :dur :freq))
			(?w . (saw-face :start :dur :freq))))

(setf song-sender 'song-snd)

(defun songwad (parts)
  (let: notes (apply 'append
		     (lc (with-current-buffer (car x)
			   (if (caddr x)
			       (setq piano-templates (symbol-value (caddr x))))
			   (piano-render-notes
			    (cadr x)))
			 x parts)) in
    (funcall song-sender notes)))

(defun song-snd (notes)
  (if (not (get-buffer "*scheme*")) (run-snd))
  (comint-send-string
   (get-buffer-process "*scheme*")
   (str (append '(with-sound (:play "#t")) notes) "\n")))



(defun repetize (n start beats subparts)
  (apply 'append
	 (lc (maptree
	      (lambda (x)
		(if (integerp x)
		    (+ x start (* i beats))
		  x))
	      subparts)
	     i (range n))))

(defun song-grouplen (group)
  (apply 'max (mapcar 'song-itemlen group)))

(defun song-group (group start-time)
  (let: out nil in
    (ec (if (and (consp item) (eq (car item) '*))
	    (dotimes (i (caddr item))
	      (push (list (cadr item)
			  (+ start-time
			     (* i (with-current-buffer (cadr item)
				    (ceiling (piano-numbeats))))))
		    out))
	  (push (list item start-time) out))
	item group)
    out))

(defun song-itemlen (item)
  (let: multiplier 1 
	buffer item in
    (if (and (consp item) (eq (car item) '*))
	(setq multiplier (caddr item)
	      buffer (cadr item)))
    (with-current-buffer buffer
      (* (ceiling (piano-numbeats))
	 multiplier))))

(defmacro auto-songwad (&rest groups)
  (let: start-time 0 in
    `(songwad
      '(,@(apply 'append
		 (lc (prog1 (song-group group start-time)
		       (incf start-time (song-grouplen group)))
		     group groups))))))

(provide 'song)
