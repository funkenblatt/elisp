;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(setq grammar
      '(S -> NP VP | the NP VP ,
	NP -> A NP | N ,
	VP -> AV VP | V | V N | V the N ,
	V -> is | wipes | douches | accepts | touches | strokes | humiliates | transmogrifies
	  | prognosticates | masticates | turnipifies | personifies | uproots | returns 
	  | palms | pants | slings | destroys | defenestrates ,
	A -> fragrant | scintillating | fantastic | passionate | assworthy | 
	  ecclesiastical | odorous | pungent | moist | somber | janitorial | mechanical ,
	N -> donkey | concubine | hamster | dolphin | fissure | document | halo | 
	  champion | flange | piper | pantleg | tuareg | phantasm | officer
	  | officiator | Lamborghini | shin | tendon ,
	AV -> joyously | foolishly | excitedly | breathlessly | disturbingly
	   tenaciously | awkwardly | hurriedly | cautiously))

(defun parse-grammar (g)
  (let ((rules '()) 
	(expansions '()) 
	(stk '())
	(ntname nil))
    (dolist (i g)
      (cond
       ((eq i ',) 
	(push (cons ntname (push (nreverse stk) expansions)) rules) 
	(setq expansions '()) 
	(setq stk '()))
       ((eq i '|) 
	(push (nreverse stk) expansions) 
	(setq stk '()))
       ((eq i '->) 
	(setq ntname (pop stk)))
       (t (push i stk))))    
    (push (cons ntname (push (nreverse stk) expansions)) rules)))

(defun mappend (proc l)
  (apply 'append (mapcar proc l)))

(defun random-elt (l) (nth (random (length l)) l))

(defun get-rules (g sym)
  (cdr (assoc sym g)))

(defun get-item (g sym)
  (random-elt (get-rules g sym)))

(defun sentence (g start)
  (mappend (lambda (s) 
	     (if (assoc s g) 
		 (sentence g (get-item g s))
	       (list s))) 
	   start))

(sentence (parse-grammar grammar) '(S))

; the somber moist hamster touches the concubine
; the ecclesiastical mechanical shin masticates the piper
; The tendon is the phantasm.
; the fantastic donkey foolishly destroys officiator
