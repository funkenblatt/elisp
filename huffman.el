;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(defun huffman (sym-weights)
  (setq sym-weights (sort (mapcar 'identity sym-weights)
			  (lambda (a b)
			    (< (cdr a) (cdr b)))))
  (let: compound-weights (make-q) 
	newbag nil
	i nil in
    (while (or sym-weights (q-any compound-weights))
      (if (q-any compound-weights)
	  (progn
	    (if (or (not sym-weights)
		    (<= (cdr (q-head compound-weights))
			(cdr (car sym-weights))))
		(progn (push (q-head compound-weights)
			     newbag)
		       (q-take compound-weights))
	      (push (pop sym-weights)
		    newbag)))
	(push (pop sym-weights) newbag))
      (if i
	  (progn
	    (q-add compound-weights
		   (cons (mapcar 'car newbag)
			 (sum (mapcar 'cdr newbag))))
	    (setq newbag nil
		  i nil))
	(setq i t)))
    (caar newbag)))

(defun dump-huff (l &optional path)
  (ec: (if (not (consp x))
	   (dump (reverse (cons y path)) ": " (repr x) "\n")
	 (dump-huff x (cons y path)))
       x y in l '(0 1)))

(defun huff-codes (l &optional path)
  (apply
   'append
   (lc: (if (not (consp x))
	    (list (cons x (reverse (cons y path))))
	  (huff-codes x (cons y path)))
	x y in l '(0 1))))

(defun buffer-charfreqs ()
  (let: a (make-vector 128 0)
	s (buffer-string) in
    (dotimes (i (length s))
      ($ a->[s->i] += 1))
    a))

(setq a
      (lc: (cons x y) x y in 
	   (range 128) 
	   (append (buffer-charfreqs) nil)
	   if (/= y 0)))

(let: codes (huff-codes (huffman a))
      lengths (lc (cons (car x)
			(length (cdr x)))
		  x codes) in
  (sum (lc (* (cdr x)
	      (cdr (assoc (car x) a)))
	   x lengths)))

(defun huftree-depth (tree)
  (if (not (consp tree)) 0
    (+ 1 (max (huftree-depth (car tree))
	      (huftree-depth (cadr tree))))))

(defun print-huftree (tree)
  (if (not (consp tree))
      tree
    (cons '<
	  (lc (print-huftree x)
	      x tree))))

