;; -*- lexical-binding: t; -*-

(defun histogram->html (histogram bar-scale)
  `(table
    ,@(mapcar
       (x: `(tr (td ,(repr (car x)))
                (td ([div class bucket style ,(interp "width: #,(* bar-scale (cdr x))px")]
                     "&nbsp;"))
                (td ,(interp "(#,(cdr x))"))))
       histogram)))

(defun histogram-template (title histogram)
  `(html
    (head (title ,title)
          ([meta charset "UTF-8"])
          ([link rel stylesheet type text/css href "histogram.css"]))
    (body
     (h3 ,title)
     ,histogram)))

(defun show-histogram (title histogram bar-scale)
  (>>> histogram (histogram->html _ bar-scale)
       (histogram-template title _)
       markup (str "<!DOCTYPE html>\n" _)
       (w/buf (get-buffer-create (str title ".html"))
         (erase-buffer) (insert _))))

(provide 'histogram)
