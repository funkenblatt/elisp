(require 'infix)

(defun tree->dot (name tree buf)
  (let ((curbuf (current-buffer))
	(nodenum 1))
    (set-buffer (get-buffer buf))
    (dump (format "digraph %s {
node [shape=box]\n" name))
    (dump (format "n0 [label=\"%s\"];\n" (car tree)))
    (dump-nodes tree 0)
    (dump "}\n")
    (set-buffer curbuf)))

(defun dump-edge (root n2)
  (dump (format "n%d [label=\"%s\"];\n" nodenum (if (stringp n2) 'STR n2)))
  (dump (format "n%d -> n%d;\n"
		root
		nodenum))
  (setq nodenum (+ nodenum 1))
  (- nodenum 1))

(defun add-parent (n)
  (puthash (car n) nodenum nodes))

(defun dump-nodes (tree noderoot)
  (dolist (i (cdr tree))
    (if (atom i) (dump-edge noderoot i)
      (dump-nodes i (dump-edge noderoot (car i))))))

(defun dfa->dot (dfa)
  (let: nodetab (make-hash-table) 
	n 0 in
    (ec ($ nodetab=>(car state) := n+=1) state dfa)
    (pr "digraph balls {\nnode [shape=circle];\n")
    (dolist (state dfa)
      (prf "n%d [label=\"%s\"];\n" (=> nodetab (car state)) (car state)))
    (dolist (state dfa)
      (ec 
       (if (cdr trans)
	   (ec (prf "n%d -> n%d [label=\"%s\"];\n" 
		    (=> nodetab (car state)) (=> nodetab target) 
		    (car trans))
	       target (cdr trans)))
       trans (cdr state)))
    (pr "}\n")))
