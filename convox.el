;; -*- lexical-binding: t; -*-


(defun convox-proc (k &rest args)
  (let: process-connection-type nil in
    (apply 'jf-proc "convox" k args)))

(defun convox-get-instances (&optional k)
  (convox-proc
   (comp (or k (x: (setf convox-instances x)))
         (x: (>>> x (split-string x "\n" t) cdr
                  (mapcar (comp 'car 'split-string) _))))
   "instances"))

(defun convox-processes (k)
  (cps-let ((instances (convox-get-instances cont!)))
    (let: out-hash (dict) in
      (funcall k (list (cons 'processes (mapcar
                                         (x: (let: p (start-process "dicks" nil "convox" "instances" "ssh" x "docker" "ps") in
                                               (set-process-filter p (lambda (p s) (push s (gethash x out-hash))))
                                               p))
                                         instances))
                       (cons 'outputs out-hash))))))

(defun format-convox-processes (stuff)
  (>>> (assoc-default 'outputs stuff)
       hash->list
       (mapcar (λ ((k . v))
                  (>>> v reverse (apply 'str _) (split-string _ "\n" t)
                       (mapcar 'list _)
                       (cons k _)))
               _)
       pp))

(defun convox-app-releases (cont)
  "Return the most recently built release for every app."
  (cps-let ((apps (convox-proc cont! "apps")))
    (let: app-names (>>> apps (split-string _ "\n" t) cdr
                         (mapcar (comp 'car 'split-string) _))
      in
      (cps-mapcar
       (λ (x k) (convox-proc (y: (>>> (split-string y "\n" t)
                                      cdr car split-string
                                      (funcall k (cons x _))))
                             "releases" "-a" x))
       app-names cont))))

(provide 'convox)


;; KICK TX LISTENER
"[17:15] osbert@nermal:~$ convox scale tx-listener -a swing-subs-etc --count 0 --wait && convox scale tx-listener -a swing-subs-etc --count 1"
