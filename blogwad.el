(defun* imgbox (img &key caption link (alt-text "an image"))
  `([div class ibox style "padding: 5px"]
    ,(do-wad
      `([img style "border: 0px" src ,img alt ,alt-text])
      (if link `([a href ,link] ,_) _))
    ,@(when caption `((br) ,caption))))

(defun blog->html ()
  (let: paragraphs (split-string (buffer-string) "\n\n" t)
        title (pop paragraphs) in
    (list title 
          (markup
           `([div class entry]
             ([div class datetime] ,(format-time-string "%x %X"))
             ,@(lc (if (startswith p "(")
                       (eval (read p))
                     (markup `(p ,(eval `(interp ,p)))))
                   p paragraphs))))))

(defun blog-ref (text refnum)
  (markup `((a href ,(aref blog-refs refnum)) ,text)))

(defun blog-references (array)
  (setf blog-refs array) "")

(defun bloggify ()
  (interactive)
  (let: entry (blog->html)
	title (car entry)
        html (cadr entry) in
    (write-region 
     (str title "\n" html) nil 
     (str "/ssh:ozbert.com:/home/jfeng/public_html/content/" 
          (buffer-name) ".html"))
    (blog-enter title)))

(defvar blog-list-filename "/ssh:ozbert.com:/home/jfeng/public_html/content/blog-entries.el")

(defun blog-enter (title)
  (let: entries (if (file-exists-p blog-list-filename)
		    (filter
		     (lambda (x) (not (equal (car x) title)))
		     (read-file blog-list-filename))
		  nil)
	in
    (push (list title (current-time) (buffer-name)) entries)
    (write-region (format "%S" entries) nil blog-list-filename)
    (blog-list)
    (with-default-dir "/ssh:ozbert.com:/home/jfeng/public_html/content"
      (shell-command "rm index.html")
      (shell-command (concat "ln -s " (buffer-name) ".html index.html")))
    entries))

(defun blog-list ()
  (with-temp-buffer
    (dump "Lies\n"
          (markup `(div
                    (p "This is where old stuff goes.")
                    (ul ,@(lc `(li ([a href ,(caddr i)] ,(car i)))
                              i (read-file blog-list-filename))))))
    (write-region nil nil "/ssh:ozbert.com:/home/jfeng/public_html/content/lies.html")))

(defun blog-bullets (&rest items)
  (markup `(ul ,@(lc `(li ,x) x items))))

(provide 'blogwad)
