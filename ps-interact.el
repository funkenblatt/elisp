;; -*- lexical-binding: t; -*-
(load "fooserve-2")

(defun ps-send (ps-code &optional handler)
  (interactive (list (buffer-substring 
                      (save-excursion (backward-sexp) (point)) (point))))
  (comint-send-string 
   (inferior-lisp-proc) 
   (interp "(emacs-call (list 'funcall '#,(or handler ''ps-recv) (ps* (read))))\n"))
  (comint-send-string
   (inferior-lisp-proc)
   (str ps-code
        "\n")))

(setf parenscript-compile-jobs (make-hash-table))
(setf parenscript-compile-job-id 0)

(defun eval-ps (form)
  (interactive (list (let: minibuffer-completing-symbol t in
                       (read-from-minibuffer "Eval: "
                                             nil read-expression-map nil
                                             'read-expression-history))))
  (ps-send form))

(defun ps-recv (junk)
  (if (and (startswith junk "{")
           (endswith junk "};"))
      (setf junk (str "(" (substring junk 0 -1) ");")))
  (process-send-string mozrepl (str junk "\n")))

(global-set-key (kbd "C-x j m l") 'bootstrap-jflib)

(defun ps-bind ()
  (interactive)
  (local-set-key (kbd "M-RET") 'ps-send))

(defun include-jflib ()
  (interactive)
  (ps-send
   (with-output-to-string
     (pp
      '(top-do
        (let! head (-> (document.get-elements-by-tag-name 'head) 0)
              scripter (-> document (create-element 'script)) in
          (setf (-> scripter src) "http://localhost:12345/jflib.parenscript"
                (-> scripter type) "text/javascript")
          (-> head 
              (insert-before 
               scripter
               (-> head child-nodes 0)))))))))

(defun serve-parenscript (req-parts header body cont)
  (let: buf (get-buffer (urldecode (substring (cadr req-parts) 1))) in
    (pr "Serving " buf "\n")
    (if (not buf) 
        (return-404 cont)
      (let: jobid (incf parenscript-compile-job-id) in
        (setf (gethash jobid parenscript-compile-jobs)
              (lambda (x)
                (funcall cont "200 OK" '((Content-Type . "text/javascript")) x)))
        (ps-send (w/buf buf (interp "(progn #,(buffer-string))"))
                 (interp "(gethash #,jobid parenscript-compile-jobs)"))))))

(push '("\\.parenscript$" . serve-parenscript) fooserve-2-handlers)

(defun ps-recv-serve (ps-code)
  (funcall ps-serve-cb "200 OK" '((Content-Type . "text/javascript")) ps-code))

(defun ps-compile (filename)
  (interactive (list buffer-file-name))
  (find-file filename)
  (comint-send-string
   (inferior-lisp-proc)
   (interp "(with-open-file (f #,(repr (str filename \".js\")) :if-does-not-exist :create
                            :direction :output :if-exists :supersede)
              (princ (ps (progn #,(buffer-string))) f))\n")))

(defun bootstrap-jflib ()
  (interactive)
  (ps-send
   (repr '(defun fetch-url (url method cb &optional data)
            (let! req (new (-x-m-l-http-request)) in
              (-> req (open method url t))
              (setf (-> req onreadystatechange)
                    (lambda (evt)
                      (if (= (-> req ready-state) 4)
                          (cb (-> req response-text)
                              req evt)
                        nil)))
              (-> req (send (or data nil)))))))

  (cps> () (run-at-time 0.5 nil :cont!)
    (ps-send
     (repr '(progn 
              (var smeg-data)
              (fetch-url "http://localhost:12345/jflib.parenscript" "GET"
                         (lambda (data) (setf smeg-data data))))))

    (cps> () (run-at-time 0.5 nil :cont!)
      (ps-send (repr '(eval smeg-data))))))

(defun ps-load-buffer ()
  (interactive)
  (ps-send (repr `(fetch-url ,(interp "http://localhost:12345/#,(buffer-name)")
                             "GET"
                             (lambda (data) (setf smeg-data data)))))
  (cps> () (run-at-time 0.5 nil :cont!)
    (ps-send (repr '(eval smeg-data)))))

(defun ps-listener (req-parts header body cont)
  (ignore-errors 
    (do-wad 
     (json-read-from-string body)
     (progn 
       (funcall (gethash (aref _ 0) ps-requests) (aref _ 1))
       (remhash (aref _ 0) ps-requests))))
  (funcall cont "200 OK" '((Content-Type . "text/plain"))
           "received"))

(defun ps-browser-request (ps-code cont)
  (puthash (incf ps-request-id) cont ps-requests)
  (ps-send
   (repr `(fetch-url 
           "http://localhost:12345/ps-listener/"
           "POST"
           (lambda (x) nil)
           (-> -j-s-o-n
               (stringify
                (list ,ps-request-id ,ps-code)))))))

(setf ps-request-id 0)
(setf ps-requests (make-hash-table))
(push '("^/ps-listener/" . ps-listener) fooserve-2-handlers)

(defun retrieve-links (rx &optional dest-dir)
  "Use wget to retrieve links on the current page open in firefox."
  (let: final-dest-dir (expand-file-name dest-dir) in
    (if (not (file-exists-p final-dest-dir))
        (make-directory final-dest-dir))
    (ps-browser-request
     '(map (/x (@ x href)) (win.content.document.get-elements-by-tag-name 'a))
     (lambda (x)
       (with-default-dir final-dest-dir
         (apply 'call-process "wget" nil 0 nil 
                "-N"
                (delete-duplicates
                 (filter (cut str~ rx <>) (append x nil))
                 :test 'equal)))))))

(defun firefox-tab-switch ()
  (interactive)
  (ps-browser-request
   '(map (/x (@ x label))
         (-> document (get-elements-by-tag-name "tab")))
   (x: (wait 0.1 
         (>>> x (mapcar (comp 'string-as-multibyte 'string-as-unibyte) _) 
              uniqify-names
              (let: key (ido-completing-read "Tab: " (hash-keys _)) in
                (gethash key _))
              `(setf (-> document (get-elements-by-tag-name "tabs") 0
                         selected-index)
                     ,_) 
              pp-to-string ps-send)))))

(global-set-key (kbd "C-x j t") 'firefox-tab-switch)

(defun uniqify-names (items)
  "Take in a sequence of names.  Items with identical names will be given
unique names, and a hash table mapping the newly uniqified names to their
indexes within the original sequence will be returned."
  (>>> (mapcar* 'cons items (range (length items)))
       (group-by _ 'car t)
       (mapcar (juxtcons 'car (comp (cur 'mapcar 'cdr) 'cdr)) _)
       (mapcan (cur 'apply 'create-names) _) alist->dict))

(defun create-names (name &rest indices)
  (let: n 0 in
    (mapcar (x: (prog1 (cons (if (= n 0) name (interp "#,[name]<#,[n]>")) x)
                  (incf n))) indices)))

(put 'with-motion 'lisp-indent-function 1)

(ignore-errors (fooserve-2-start 12345))

(provide 'ps-interact)
