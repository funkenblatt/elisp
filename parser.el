'((expr (: sum EOF))
  (sum (or (: sum "+" product)
           product))
  (product (or (: product "*" factor)
               (: product term)
               factor))
  (factor (or (: term "^" factor)
              term
              (: "-" factor)))
  (term (or num-or-var
            (: "(" sum ")"))))

(defun if-take (strm tok)
  (if (if (consp tok) 
          (member (funcall strm 0) tok)
        (equal (funcall strm 0) tok))
      (funcall strm)
    nil))

(defun find-sum (strm)
  (let: sum (find-product strm)
        operator nil in
    (while (setq operator (if-take strm '(+ -)))
      (let: product (find-product strm) in
        (setq sum (list operator sum product))))
    sum))

(defun find-product (strm)
  (let: factor (find-factor strm)
        operator nil in
    (while (or (setq operator (if-take strm '(* /)))
               (numberp (funcall strm 0))
               (and (funcall strm 0) (symbolp (funcall strm 0))
                    (not (memq (funcall strm 0) '(+ - ^)))))
      (let: factor-2 (find-factor strm) in
        (setq factor (list (or operator '*) factor factor-2))))
    factor))

(defun find-factor (strm)
  (let: tok (funcall strm 0) in
    (if (equal tok '-)
        (progn (funcall strm) (list '- (find-factor strm)))
      (let: term (find-term strm) in
        (if (equal (funcall strm 0) '^)
            (progn (funcall strm)
                   (list '^ term (find-factor strm)))
          term)))))

(defun find-term (strm)
  (let: tok (funcall strm) in
    (cond
     ((equal tok "(")
      (let: sum (find-sum strm) in
        (if (not (equal (funcall strm) ")"))
            (signal 'cockbite "Invalid paren fuckwad!")
          sum)))
     ((memq tok '(+ * ^))
      (signal 'fuckwad "invalid operator placement!"))
     (t tok
        (if (and (symbolp tok) (if-take strm "("))
            (let: args (find-arglist strm) in
              (cons tok args))
          tok)))))

(defun find-arglist (strm)
  (let: tok (funcall strm 0)
        args nil in
    (while (and tok (not (equal tok ")")))
      (push (find-sum strm) args)
      (if-take strm ",")
      (setq tok (funcall strm 0)))
    (if (not tok) (signal 'ass "What the cock you stupid shit!"))
    (funcall strm)
    (nreverse args)))
