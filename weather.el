(require 'cps-filter)

(defun get-weather ()
  (interactive)
  (get-http (connect "forecast.weather.gov")
	    (lambda (data)
	      (let: coding-system-for-write 'binary in
		(write-region data nil "/tmp/weather.png")
		(find-file "/tmp/weather.png")))
	    "/wtf/meteograms/Plotter.php?lat=37.77350&lon=-122.27900&wfo=MTR&zcode=CAZ508&gset=18&gdiff=3&unit=0&tinfo=PY8&ahour=0&pcmd=1101111111111000000000000000000&lg=en&indu=1!1!1&dd=0&bw=0"
	    (lambda (headers)
	      (scratchdump headers))))

(global-set-key
 (kbd "C-x j w")
 'get-weather)

(provide 'weather)
