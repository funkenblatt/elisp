;; To use:
;; (push 'pg-complete completion-at-point-functions)
;; in whatever buffer you want to complete table names in.

(defun pg-complete-table-name (prefix)
  (do-wad
   (interp "select relname from pg_class where relname like '#,[prefix]%' and relkind = 'r'")
   (pg:exec *pg-conn* _)
   (pg:result _ :tuples)
   (mapcar 'car _)))

(defun pg-complete-column-name (stuff)
  (destructuring-bind (table prefix) (split-string stuff (rx "."))
    (do-wad
     (interp "select * from #,table limit 0")
     (pg:exec *pg-conn* _)
     (pg:result _ :attributes)
     (mapcar 'car _)
     (all-completions prefix _)
     (lc (str table "." x) x _))))

(defun pg-complete ()
  (interactive)
  (ignore-errors
    (let* ((seed (symbol-name (symbol-at-point)))
           (completions (if (string-match (rx ".") seed)
                            (pg-complete-column-name seed)
                          (pg-complete-table-name seed))))
      (and completions
           (list (- (point) (length seed)) (point) completions)))))

(defun pg-read-array-entry ()
  (let ((start (point)))
    (re-search-forward "[,\"})]" nil t)
    (if (/= (char-before) ?\")
	(buffer-substring start (- (point) 1))
      (forward-char -1)
      (prog1 (read (current-buffer))
	(re-search-forward "[,})]")))))

(defun pg-read-entries ()
  (do-wad
   (pg-read-array-entry)
   (if (= (char-before) ?,)
       (cons _ (pg-read-entries))
     (list _))))

(defun pg-read-array ()
  (re-search-forward "[{(]" nil t)
  (pg-read-entries))

(defun pg-read-array-from-string (s)
  (with-temp-buffer 
    (insert s)
    (goto-char (point-min))
    (pg-read-array)))

(defun read-pg-table ()
  (ido-completing-read
   "Table: " (pg-complete-table-name "")))

(defun pg-insert-tablename ()
  (interactive)
  (insert (read-pg-table)))

(defun preceding-sql ()
  (do-wad
   (buffer-substring
    (save-excursion 
      (search-backward ";" nil t) 
      (or (search-backward ";" nil t) (goto-char (point-min)))
      (if (= (char-after) ?\;)
          (forward-char 1))
      (point))
    (point))
   substring-no-properties))

(defun run-preceding-sql ()
  (interactive)
  (do-wad
   (preceding-sql)
   strip
   (if (endswith _ ";") _ (str _ ";"))
   (str _ "\n")
   (process-send-string target-process _)))

(defun dump-query-stdout (q)
  (>>> q render-query (interp "copy (#,_) to stdout;\n") 
       (process-send-string target-process _)))

(defun save-query (q f)
  (>>> q render-query (interp "copy (#,_) to #,(sql-expr f);\n")
       (process-send-string target-process _)))

(defun run-sql (s)
  (do-wad
   (pg:exec *pg-conn* s)
   (cons (mapcar 'car (pg:result _ :attributes))
          (pg:result _ :tuples))))

(add-hook 'sql-mode-hook (lambda () (local-set-key (kbd "C-c C-c") 'run-preceding-sql)))

(global-set-key (kbd "M-t") 'pg-insert-tablename)

(provide 'pg-stuff)
