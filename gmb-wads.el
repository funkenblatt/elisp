(defun compile-and-load ()
  "Compile current buffer's file using gambit-c, load into currently running
gsi instance."
  (interactive)
  (when buffer-file-name
    (lexlet: module-name (replace-regexp-in-string 
			  "\\.[^.]*$" "" buffer-file-name) in
      (compile (str "gsc -cc-options -O3 "
		    module-name))
      (setq my-funwad
	    (lambda (arg wads)
	      (comint-send-string 
	       (get-buffer-process scheme-buffer)
	       (format "(load \"%s\")\n"
		       module-name))
	      (remove-hook 'compilation-finish-functions
			   my-funwad)
	      (message "Oh fuck pants.")))
      (add-hook 'compilation-finish-functions
		my-funwad))))

(provide 'gmb-wads)

