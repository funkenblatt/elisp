;; -*- lexical-binding: t; -*-
(require 'http-junk)
(require 'jf-iter)

;; Utils for accessing pivotal tracker.
(defvar pivotal-token nil)
(defvar pivotal-user-info nil)
(defvar pivotal-project-id nil)

(defun* pivotal-request (cont endpoint &key data)
  (let: params (if data (str "?" (urlencode-alist data)) "")
        endpoint (strjoin "/" endpoint) in
    (curl
     (interp "https://www.pivotaltracker.com/services/v5/#,[endpoint]#,[params]")
     cont :headers `((X-TrackerToken . ,pivotal-token)))))

(defun pivotal-init (&optional cont)
  "Get initial user info."
  (if pivotal-user-info
      (when cont (funcall cont pivotal-user-info))
    (cps> data (pivotal-request :cont! '("me"))
      (>>> (json-read-from-string data)
           (setf pivotal-user-info _))
      (when cont (funcall cont pivotal-user-info)))))

(defun pivotal-pick-project (&optional cont)
  (interactive)
  (pivotal-init
   (x: (let: projects (assoc-default 'projects pivotal-user-info)
             completions (mapcar (:= 'project_name) projects) in
         (>>> (ido-completing-read
               "Project: "
               completions)
              (find-if (x: (equal (assoc-default 'project_name x) _)) projects)
              (assoc-default 'project_id _)
              (setf pivotal-project-id _)))
       (when cont (funcall cont)))))

(defun pivotal-user (field)
  (assoc-default field pivotal-user-info))

(defun pivotal-get-stories ()
  (interactive)
  (if (not pivotal-project-id)
      (pivotal-pick-project 'pivotal-get-stories)
    (pivotal-request
     (comp 'pivotal-summarize-stories 'json-read-from-string)
     (list "projects" (str pivotal-project-id) "stories")
     :data `((filter . ,(interp "owner:#,(pivotal-user 'initials)"))))))

(defun pivotal-summarize-stories (stories)
  (with-output-to-temp-buffer "*Pivotal Stories*"
    (with-current-buffer standard-output
      (>>> (append stories nil)
           (iter/l:
            (x color) (and (in-list _) (cycled ["#ccffff" "#ffffcc"])) in
            (alist-let
             (name created_at current_state estimate) x
             (list ;; (strjoin "\n" (split-string created_at "T"))
                   (mklink (fill-string name)
                           (λ () (interactive) (pivotal-show-story x))
                           `(:background ,color))
                   (if estimate (str estimate) "")
                   current_state)))
           (print-tabular _ "  ")
           insert))))

(defun pivotal-show-story (story)
  (alist-let
   (id name description created_at current_state estimate) story
   (let: buf (get-buffer-create (interp "*Pivotal Story: #,[id]*")) in
     (with-current-buffer buf
       (setf buffer-read-only nil)
       (local-set-key (kbd "q") (keycmd (kill-buffer)))
       (erase-buffer)
       (goto-char (point-min))
       (insert (propertize name 'face '(:weight bold :background "#00ff00")) "\n")
       (insert "Created: " created_at "\n")
       (insert (if estimate
                   (interp "#,[estimate] points")
                 "Unestimated") "\n\n")
       (insert (fill-string description))
       (setf buffer-read-only t))
     (switch-to-buffer buf))))

(global-set-key (kbd "C-x j l") 'pivotal-get-stories)

(provide 'pivotal)
