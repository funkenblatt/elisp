(defun gen-merges (files)
  (if (not (consp (car files))) (setf files (mapcar 'list files)))
  (if (> (length files) 1)
      (cons files
            (gen-merges (lc (apply 'append x) x (sublist2 files))))
    (list files)))

(defun split-mergesort-commands (files sort-options &optional from-stdin outfile-base)
  "Take an input data set that's been split into several files.
Sort the pieces and merge them together."
  (let: sorted-files (if from-stdin
                         (lc (interp "#,[outfile-base]#,[n].sorted") n (range (length files)))
                       (lc (str x ".sorted") x files))
        steps (gen-merges sorted-files)
        init-sort (mapcar*
                   (lambda (x sorted)
                     (if from-stdin
                         (interp "#,[x] | sort #,[sort-options] > #,[sorted]")
                       (interp "sort #,[sort-options] #,[x] > #,[x].sorted ")))
                   files sorted-files) in
    (cons
     init-sort
     (mapcar*
      (lambda (prev next)
        (let: prev-names (sublist2 (lc (strjoin "-" x) x prev))
              inputs (lc (strjoin " " x) x prev-names)
              outputs (lc (strjoin "-" x) x next) in
          (mapcar*
           (lambda (in out)
             (interp "sort -m #,[sort-options] #,[in] > #,[out] && rm #,[in]"))
           inputs outputs)))
      steps (cdr steps)))))

(defun split-mergesort (files sort-options script-filename &optional from-stdin outfile-base)
  (with-temp-buffer
    (dolist (cmdgroup (split-mergesort-commands files sort-options from-stdin outfile-base))
      (dolist (cmd cmdgroup)
        (dump cmd " &\n"))
      (dump "wait\n"))
    (write-region nil nil script-filename)))

(provide 'merges)
