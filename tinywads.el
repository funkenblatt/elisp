(setq scm-tmap '((int . ?i) (char . ?c) (double . ?f) (char* . ?s)))
(setq scm-makers '((int . mk_integer) (double . mk_real)
		   (char* . mk_string) (char . mk_character)))


(defun get-scm-funcs ()
  (let: scm-funcs nil in
    (while (re-search-forward "(\\*\\([^)]*\\))" nil t)
      (push (match-string 1) scm-funcs))
    (dolist (i scm-funcs)
      (dump "#define " i " sc->vptr->" i "\n"))))

(defun scm-arg-handler (rtype name arglist)
  (dump "pointer scm_" name "_wrap(scheme *sc, pointer args)\n{\n")
  (dump rtype " ret;\n")
  (ec: (dump type " arg" n ";\n") type n in arglist (range (length arglist)))
  (dump "int nargs=get_args(sc, args, " (scm-format-arglist arglist) 
	", (void * []) " (bracify (lc (format "&arg%d" x) x (range (length arglist))))
	");\n")
  (dump "if (nargs!=" (length arglist) ") return sc->NIL;\n")
  (dump "ret = " name "(" (strjoin ", " (lc (format "arg%d" x) x (range (length arglist)))) ");\n")
  (dump "return " (or (format "%s(sc, ret)" (cdr (assoc rtype scm-makers))) "ret") ";\n}\n"))

(defun scm-format-arglist (l)
  (concat "\"" (lc (or (cdr (assoc x scm-tmap)) ?p) x l) "\""))

(provide 'tinywads)
