;; -*- lexical-binding: t; -*-
(require 'fifo-crap)

(defun connect (host &optional port)
  (let: p (or port 80)
	conn-pool (lc x x (process-list) (equal (process-contact x)
						(list host p)))
	in
    (if conn-pool
	(car conn-pool)
      (make-network-process
       :name "foocl" :host host :service (or port 80)
       :filter (lambda (p s) )
       :coding 'binary))))

(defun headers->str (headers)
  (apply 'concat (lc (format "%s: %s\r\n" (car x) (cdr x))
		     x headers)))

(defun header->alist (headers)
  (lc (when (string-match (rx (group (* (not (any ":")))) ":" (group (* anything)))
                          header)
        (cons (downcase (match-string 1 header)) (strip (match-string 2 header))))
      header headers))

(defun http-req (process uri &optional method headers)
  (if (not method) (setq method "GET"))
  (push (cons "Connection" "Keep-Alive") headers)
  (if (not (assoc "Host" headers)) (push (cons "Host" (car (process-contact process))) headers))
  (process-send-string
   process 
   (format "%s %s HTTP/1.1\r\n%s\r\n"
	   method uri (headers->str headers))))

(defun async-dns (hostname cont)
  (if (string-match "[^0-9.]" hostname) 
      (cps-let ((info (jf-proc "host" cont! hostname)))
	(if (string-match "has address \\(.*\\)" info)
	    (funcall cont (match-string 1 info))
	  (funcall cont nil)))
    (run-at-time 0.001 nil cont hostname)))

(defun cps-connect (host cont &optional port)
  (cps-let ((host-addr (async-dns host cont!)))
    (let: p (or port 80)
	  conn-pool (lc x x (process-list) (equal (process-contact x)
						  (list host-addr p)))
      in
      (if conn-pool
	  (funcall cont (car conn-pool))
	(make-network-process
	 :name "foocl" :host host-addr :service p
	 :filter (lambda (p s) ) :coding 'binary
	 :sentinel (lambda (p s) 
                     (if (string-match "open" s)
                         (wait 0.01
                               (set-process-sentinel p (lambda (a b) ))
                               (funcall cont p))
                       (pr "Connection killed\n")
                       (delete-process p)))
	 :nowait t)))))

(defun read-http-head (proc cont)
  (nlet loop ((lines nil))
    (cps-let ((line (agetline proc cont!)))
      (if (member line '("\r" ""))
          (funcall cont (nreverse lines))
        (funcall loop (cons line lines))))))

(defun read-until-break (proc cont)
  (nlet loop ((chunks nil))
    (cps-let ((chunk (funcall proc 8192 cont!)))
      (if (equal chunk "")
          (funcall cont (apply 'concat (nreverse chunks)))
        (funcall loop (cons chunk chunks))))))

(defun read-chunks (proc cont)
  (nlet loop ((chunks nil))
    (cps-let ((chunksize (agetline proc cont!)))
      (setf chunksize (read (str "#x" chunksize)))
      (if (= chunksize 0)
          (funcall cont (apply 'concat (nreverse chunks)))
        (cps-let ((chunk (funcall proc chunksize cont!))
                  (junk (agetline proc cont!)))
          (funcall loop (cons chunk chunks)))))))

(defun chunked-p (headers)
  (catch 'return
    (dolist (header headers)
      (if (and (string-match "^transfer-encoding:" header)
	       (string-match "chunked" header))
	  (throw 'return t)))
    nil))

(defun connection-close-p (headers)
  (catch 'return
    (dolist (header headers)
      (if (and (string-match "^connection:" header)
	       (string-match "close" header))
	  (throw 'return t)))
    nil))

(defun content-length (headers)
  (catch 'return
    (dolist (header headers)
      (when (string-match "^content-length:" header)
	(throw 'return
	       (progn (string-match "[0-9]+" header)
		      (read (match-string 0 header))))))
    nil))

(defun get-http (p cont uri &optional header-hook headers method body)
  (http-req p uri (or method "GET") headers)
  (if body (process-send-string p body))
  (setq p (procpipe p '("connection broken by remote peer\n")))
  (cps-let ((heads (read-http-head p cont!)))
    ;(pr "Received headers!\n")
    (if header-hook (funcall header-hook heads))
    (let: conlen nil in
      (cond
       ((chunked-p heads) (read-chunks p cont))
       ((setq conlen (content-length heads))
	(if (= conlen 0) 
	    (funcall cont "")
	  (cps-let ((data (funcall p conlen cont!)))
	    (funcall cont data))))
       (t ;(pr "Waiting for connection break\n") 
	  (read-until-break p cont))))))

(defun get-url (url cont &optional header-hook headers)
  (let: urlstruct (url-generic-parse-url url) in
    (cps-let ((proc (cps-connect (url-host urlstruct) cont! (url-portspec urlstruct))))
      (pr "Connected!\n")
      (setq foobite proc)
      (get-http proc 
		cont (url-filename urlstruct)
		(lambda (s) 
		  (pr "Received headers!\n")
		  (if header-hook (funcall header-hook s)))
		`(("Host" . ,(url-host urlstruct))
		  ("User-Agent" .  "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.16) Gecko/20110323 Ubuntu/10.10 (maverick) Firefox/3.6.16")
		  ,@headers)))))

(provide 'http-client)
