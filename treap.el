(defrec treap data l r prio junk)

(defsubst mktreap (data l r &optional junk)
  (treap data l r (random) junk))

(defun treap-wad (parent child1 child2 sense)
  (if (< sense 0)
      (treap (treap-data parent)
	     child1 child2
	     (treap-prio parent) (treap-junk parent))
    (treap (treap-data parent)
	   child2 child1
	   (treap-prio parent) (treap-junk parent))))

(defsubst treap-leaf (tree sense)
  (if (< sense 0) 
      (treap-l tree)
    (treap-r tree)))

(defun treap-ins (tree item cmp &optional junk)
  (if tree
      (let: c (funcall cmp item (treap-data tree)) 
	    new-node (and (/= c 0) (treap-ins (treap-leaf tree c) item cmp junk)) in
	(cond
	 ((not new-node) tree)
	 ((eq new-node (treap-leaf tree c)) tree)
	 (t (if (> (treap-prio new-node)
		   (treap-prio tree))
		(treap-wad new-node tree nil (- c))
	      (treap-wad tree new-node (treap-leaf tree (- c)) c)))))
    (mktreap item nil nil junk)))

(defun l->trp (l cmp)
  (let: out nil in
    (dolist (i l)
      (setq out (treap-ins out i cmp)))
    out))

(defun treap-depth (tree)
  (if (not tree)
      0
    (+ 1 (max (treap-depth (treap-l tree))
	      (treap-depth (treap-r tree))))))

(defun treap-lookup (tree item cmp &optional default)
  (let: cmpval nil in
    (catch 'break
      (while tree
	(setq cmpval (funcall cmp item (treap-data tree)))
	(if (= cmpval 0) 
	    (throw 'break tree)
	  (setq tree (treap-leaf tree cmpval))))
      default)))

(defun range-cmp (a b)
  (- (car a) (car b)))

(defun range-check (item node)
  (cond 
   ((< item (car node)) -1)
   ((< item (sum node)) 0)
   (t 1)))

(defun treap-max (tree)
  (if (not tree)
      nil
    (while (treap-r tree)
      (setq tree (treap-r tree)))
    (cons (treap-data tree) (treap-junk tree))))

(defun treap-inorder (proc tree depth)
  (when tree
    (treap-inorder proc (treap-l tree) (+ depth 1))
    (funcall proc tree depth)
    (treap-inorder proc (treap-r tree) (+ depth 1))))
