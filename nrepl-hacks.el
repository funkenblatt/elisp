;; -*- lexical-binding: t; -*-
(require 'nrepl)
(require 'clojure-hacks)

(jason-defkeys nrepl-interaction-mode-map 
               (kbd "M-,") nil
               (kbd "M-.") nil
               (kbd "C-x M-e") 'nrepl-pretty-eval
               (kbd "<C-return>") 'nrepl-mozrepl-eval)

(jason-defkeys (if (boundp 'nrepl-repl-mode-map)
                   nrepl-repl-mode-map
                 nrepl-mode-map)
               (kbd "M-,") nil
               (kbd "M-.") nil)

(defun nrepl-pretty-eval ()
  (interactive)
  (>>> (nrepl-last-expression)
       (interp "(with-out-str (clojure.pprint/pprint #,_))")
       (nrepl-send-string 
        _ (nrepl-make-response-handler
           (current-buffer)
           (lambda (buf val) (w/buf buf (insert (read val))))
           nil nil nil)
        nrepl-buffer-ns)))

(with-current-buffer (get-buffer-create "cljwad")
  (clojure-mode))

(provide 'nrepl-hacks)

(defun nrepl-mozrepl-eval ()
  "Send some clojurescript code to mozrepl."
  (interactive)
  (>>> (nrepl-last-expression)
       (interp "(cljs.closure/-compile ['(js/identity #,_)] {})")
       (nrepl-send-string
        _ (nrepl-make-response-handler
           (current-buffer)
           (lambda (buf val)
             (>>> (read val)
                  (progn 
                    (pr _)
                    (process-send-string mozrepl _))))
           nil nil nil)
        nrepl-buffer-ns)))

(defun nrepl-alter-sexp ()
  "Alters the preceding s-expression using a supplied map expression."
  (interactive)
  (let: expr (nrepl-last-expression)
        map-expr (read-string "Mapping expression: ") in
    (>>>
     (interp 
      "(let [expr '#,expr fun (fn [x] #,map-expr)] (if (coll? expr) (map fun expr) (fun expr)))")
     (nrepl-send-string
      _ (nrepl-make-response-handler
         (current-buffer)
         (lambda (buf val)
           (w/buf buf (insert val)))
         nil nil nil)
      nrepl-buffer-ns))))

(jason-defkeys nrepl-interaction-mode-map (kbd "C-c C-l") nil)
(jason-defkeys nrepl-interaction-mode-map (kbd "C-c C-a") 'nrepl-alter-sexp)
