;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(defun slot-name (slotspec)
  (if (consp slotspec)
      (car slotspec)
    slotspec))

(defun get-slot-names (struct)
  (iflet cl (get struct 'cl--class)
    (mapcar 'cl--slot-descriptor-name (cl--class-slots cl))
    (mapcar 'slot-name (get struct 'cl-struct-slots))))

(defmacro defstructmeth (struct name args &rest body)
  "Defines a function NAME in which any free variables in BODY
that happen to correspond with struct slots in STRUCT get automagically
turned into STRUCT slot references.  This macro also adds an argument
to the provided arglist ARGS for passing in the actual struct.  This
will be a gensym, so you won't be able to access it directly, nor should
you need to."
  (let: self-arg (gensym) 
	slots (get-slot-names struct) in
    (map-varrefs
     (lambda (x ctx)
       (if (memq x slots)
	   (list (symbol-cat struct "-" x) self-arg)
	 x))
     `(defun ,name (,self-arg ,@args) ,@body))))

(defun map-let-varrefs (proc form env)
  (let: newbinds (lc (prog1 (if (consp x)
				(list (car x)
				      (map-varrefs proc (cadr x) env nil))
			      x)
		       (when (eq (car form) 'let*)
			 (push (car x) env)))
		     x (cadr form))
	newbody nil in
    (when (eq (car form) 'let)
      (setq env (append
		 (lc (if (consp x) (car x) x)
		     x (cadr form))
		 env)))
    (setq newbody
	  (lc (map-varrefs proc x env nil)
	      x (cddr form)))
    `(,(car form) ,newbinds ,@newbody)))

(defun map-lambda-varrefs (proc form env)
  (setq env (append (cadr form) env))
  `(lambda ,(cadr form)
     ,@(lc (map-varrefs proc x env nil)
	   x (cddr form))))

(defun map-cond-varrefs (proc form env)
  (let: clauses (cdr form) in
    (cons 'cond
	  (lc `(,(map-varrefs proc (car clause) env nil)
		,(map-varrefs proc (cadr clause) env nil))
	      clause clauses))))

(defun map-defun-varrefs (proc form env)
  `(defun ,(cadr form) ,(caddr form)
     ,@(cddr (map-lambda-varrefs
	      proc (cons 'lambda (cddr form)) env))))

(defun map-varrefs (proc form &optional env ctx)
  "Calls PROC for each reference to a free variable within FORM.
ENV stores bound variables.  Replaces free variable reference with
whatever PROC returns."
  (cond
   ((not form) form)
   ((eq t form) form)
   ((symbolp form)
    (if (not (memq form env))
	(funcall proc form ctx)
      form))
   ((consp form)
    (setq form (macroexpand-all form))
    (case (car form)
      ((let let*) (map-let-varrefs proc form env))
      (function (if (consp (cadr form)) 
                    (map-lambda-varrefs proc (cadr form) env)
                  form))
      (lambda (map-lambda-varrefs proc form env))
      (defun (map-defun-varrefs proc form env))
      (quote form)
      (setq `(setf ,@(lc (map-varrefs proc x env 'setf)
			 x (cdr form))))
      (cond (map-cond-varrefs proc form env))
      (t `(,(car form) ,@(lc (map-varrefs proc x env (car form))
			     x (cdr form))))))
   (t form)))

(provide 'structmeth)
