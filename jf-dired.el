;; Dired modifications

(defun file-lastmod (f &optional dir)
  (with-default-dir (or dir ".")
    (nth 5 (file-attributes f))))

(defun dired-find-file ()
  "In Dired, visit the file or directory named on this line."
  (interactive)
  ;; Bind `find-file-run-dired' so that the command works on directories
  ;; too, independent of the user's setting.
  (let ((find-file-run-dired t) filename)
    (setq filename (dired-get-file-for-visit))
    (if (file-directory-p filename)
	(find-alternate-file filename)
      (find-file filename))))

(defun dired-mouse-find-file-other-window (event)
  "In Dired, visit the file or directory name you click on."
  (interactive "e")
  (let (window pos file)
    (save-excursion
      (setq window (posn-window (event-end event))
	    pos (posn-point (event-end event)))
      (if (not (windowp window))
	  (error "No file chosen"))
      (set-buffer (window-buffer window))
      (goto-char pos)
      (setq file (dired-get-file-for-visit)))
    (if (file-directory-p file)
	(or (and (cdr dired-subdir-alist)
		 (dired-goto-subdir file))
	    (progn
	      (select-window window)
	      (find-alternate-file file)))
      ;(select-window window)
      (find-file (file-name-sans-versions file t)))))

(jason-defkeys 
 dired-mode-map
 (kbd "M-u") (keycmd (find-alternate-file ".."))
 (kbd "% a") 'dired-mark-all)

(defun dired-syncwad ()
  (let: a default-directory
	b (with-current-buffer (window-buffer (next-window)) default-directory)
	afiles (with-default-dir a (lc (intern x) x (directory-files ".") (not (file-directory-p x))))
	bfiles (with-default-dir b (lc (intern x) x (directory-files ".") (not (file-directory-p x))))
	ab-files (mapcar 'symbol-name (intersection afiles bfiles)) in
    (dolist (i ab-files)
      (let: time-a (file-lastmod i a)
	    time-b (file-lastmod i b) in
	(when (time-less-p time-b time-a)
	  (goto-char (point-min)) 
	  (search-forward i)
	  (dired-mark 1))))))

(defun dired-mark-all ()
  (interactive)
  (dired-mark-files-regexp ".*"))

(provide 'jf-dired)
