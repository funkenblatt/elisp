(defun read-grammar-shit ()
  (let: out nil in
    (while (< (point) (point-max))
      (let: l (thing-at-point 'line)
            stuff (if (startswith l "(")
                      (read/die)
                    (>>> (thing-at-point 'line) (str "(" _ ")") 
                         read
                         (remove-if (cur 'eq :=) _))) in
        (>>> (split-by (cur 'eq '|) (cdr stuff))
             (mapcar (cur 'cons ':) _)
             (cons (car stuff) _)
             (push _ out))
        (forward-line)))
    out))

(defun grammar-refs (rule)
  (pcase rule
    (`(,(or `: `or `* `maybe) . ,stuff) 
     (>> list x stuff refs (grammar-refs x) in refs))
    ((pred vectorp) (>> list x rule refs (grammar-refs x) in refs))
    ((pred symbolp) (list rule))
    (_ nil)))

(defun get-sibling-rules (grammar non-term &optional seen)
  (if (not seen) (setf seen (make-hash-table)))
  (unless (gethash non-term seen)
    (puthash non-term t seen)
    (let: rule (cons 'or (chain-ref grammar non-term))
          refs (>>> (grammar-refs rule)
                    delete-duplicates
                    (remove-if (cut gethash <> seen) _)) in
      (cons (cons non-term (chain-ref grammar non-term))
            (mappend (cut get-sibling-rules grammar <> seen) refs)))))

(defun dump-rule (grammar non-term)
  (>>> (chain-ref grammar non-term)
       (mapcar 'repr _) (strjoin "\n" _)
       (list (list (str non-term) _))
       (print-tabular _ " | ")))

(defun grammar-view (grammar)
  (setf current-grammar grammar)
  (setf referencing-rules (index-referencing-rules grammar))
  (switch-to-buffer (get-buffer-create "grammar-view"))
  (local-set-key (kbd "M-RET")
    (keycmd
     (>>> (thing-at-point 'symbol) read
          (dump-rule current-grammar _)
          (save-excursion 
            (goto-char (point-max))
            (jf-hungry-del-back)
            (insert "\n")
            (insert _ "\n")))))

  (local-set-key (kbd "<C-return>")
    (keycmd
     (>>> (thing-at-point 'symbol) read
          (chain-ref referencing-rules _) remove-duplicates
          (mapcar (cur 'dump-rule scala-grammar) _)
          (save-excursion 
            (goto-char (point-max))
            (jf-hungry-del-back)
            (insert "\n")
            (mapc (cut insert <> "\n") _))))))

(defun index-referencing-rules (grammar)
  (>>> (mappend 
        (λ ((nonterm . rules))
          (mapcar (cut cons <> nonterm) (grammar-refs (cons 'or rules)))) 
        grammar)
       (group-reduce _ 'car 'cdr)))

(provide 'grammar-stuff)
