;; Does a limited sort of CPS transform.

(setq *special-forms* '((progn . cont-prognwad)
			(if . cont-ifwad)
			(lambda . (lambda (expr) (list nil expr)))))

(defun has-cont! (subexpr)
  (tree-search subexpr (lambda (x) (eq x :cont!))))

(defun can-continue (expr)
  (eq (car expr) 'if))

(defun cont-prognwad (expr &optional temp result temp2)
  (setq temp expr)
  (catch 'return
    (while temp
      (when (listp (car temp))
	(setq result (cont-evalwad (car temp)))
	(if (car result)
	    (throw 'return
		   (list `(,@(reverse temp2) ,(car result) 
			   ,@(if (can-continue (car result)) (cdr temp) nil))
			 `(progn ,(cadr result) ,@(cdr temp))))))
      (push (pop temp) temp2))
    (list nil expr)))

(defun cont-ifwad (expr &optional temp)
  (catch 'return
    (setq temp (cont-evalwad (cadr expr)))
    (if (car temp)
	(throw 'return (list (car temp)
			     `(if ,(cadr temp) ,@(cddr expr)))))
    (setq temp (cont-evalwad (nth 2 expr)))
    (if (car temp)
	(throw 'return
	       (list `(if ,(cadr expr) ,(car temp) ,@(tail expr 3))
		     (cadr temp))))
    (setq temp (cont-evalwad (cons 'progn (cdr (cddr expr)))))
    (if (car temp)
	(list `(,@(head expr 3) ,(car temp))
	      (cadr temp))
      (list nil expr))))

(defun cont-evalwad (expr)
  (catch 'return
    (if (not (listp expr)) (throw 'return (list nil expr)))
    (if (assoc (car expr)
	       *special-forms*)
	(funcall (cdr (assoc (car expr) *special-forms*)) expr)
      (let (outlist temp cont-expr)
	(setq outlist
	      (mapcar
	       (lambda (x)
		 (if (eq x :cont!)
		     (throw 'return (list expr 'continuation-argument)))
		 (if (listp x)
		     (progn
		       (setq temp (cont-evalwad x))
		       (if (car temp) (setq cont-expr (car temp)))
		       (cadr (cont-evalwad x)))
		   x))
	       (cdr expr)))
	(push (car expr) outlist)
	(list cont-expr outlist)))))

(defun replace-cont! (expr replacement &optional found-cont!)
  (maptreepred
   (lambda (x)
     (if (not found-cont!)
	 (progn
	   (setq found-cont! t)
	   (mapcar
	    (lambda (argwad)
	      (if (eq argwad :cont!)
		  `(lambda (continuation-argument)
		     ,replacement)
		argwad))
	    x))
       x))
   expr
   (lambda (y) (and (listp y) (memq :cont! y)))))

(defun cps-convert (expr)
  (foolet (result (cont-evalwad expr) found-cont! nil)
    (if (car result)
	(cps-convert
	 (replace-cont! (car result) (cps-convert (cadr result))))
      expr)))

;; (dump (cps-convert
;;        '(progn	  
;; 	  (foo bar) 
;; 	  (if (baz wad) 
;; 	      (progn 
;; 		(fuck bar) 
;; 		(stuff wads) 
;; 		ass) 
;; 	    (if (ass bag) 
;; 		(wad stack :cont!)
;; 	      (bags :cont!)
;; 	      (foo :cont!)))
;; 	  (more stuff :cont!)
;; 	  (wads))))


;; (dump (cps-convert
;;        '(progn
;; 	  (foo :cont!)
;; 	  (setq baz (bar :cont!))
;; 	  baz)))
