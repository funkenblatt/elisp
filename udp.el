;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(setq udp-serv
      (make-network-process
       :name "bar" :buffer nil
       :service 12345 :family 'ipv4
       :filter (lambda (p s)
;		 (dump-to-file s "/tmp/junk.asn")
		 (setq recv s)
		 (pp (asn-parse (string-strm s)) (current-buffer))
		 (dump "\n"))
       :server t :coding 'binary
       :type 'datagram))


;(delete-process udp-serv)

(defun repr (x) (format "%S" x))

;; (setq udp-cl
;;       (make-network-process
;;        :name "baz" :buffer nil
;;        :service 53 :host "206.13.28.12"
;;        :family 'ipv4 :filter (lambda (p s) (dump s))
;;        :type 'datagram
;;        :coding 'binary))

(defun snmp-respond (msg)
  `(seq
    (int "\x03")
    (seq
     ,(nth 1 (nth 2 msg))
     ,(nth 2 (nth 2 msg))
     (ocstring "\x04")
     (int "\x03"))
    (ocstring ,(make-asn '(seq (ocstring "\x00\x00\x08\x01 bcdefgh")
			       (int "\x00")
			       (int "\x35\x00")
			       (ocstring "")
			       (ocstring "")
			       (ocstring ""))))
    (seq
     (ocstring "")
     (ocstring "")
     (0
      ,(nth 1 (nth 3 (nth 4 msg)))
      (int "\x00")
      (int "\x00")
      (seq 
       (seq (objid "\x2b\x06\x01\x06\x03\x0f\x01\x01\x04") 
	    (int "\x00\x00\x00\x00")))))))
