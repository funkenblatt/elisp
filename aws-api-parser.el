;; -*- lexical-binding: t; -*-
(require 'aws)

(defun get-aws-api-stuff (dir)
  (with-default-dir dir
    (do-wad 
     (directory-files "." nil "^[^.]")
     (mapcar (juxtcons (=~ "-query-\\([^.]+\\).html" 1)
                       'ec2-extract-info) _)
     (setf ec2-api-description _)
     )))

(defun describe-ec2-fn (fun)
  (interactive (list (completing-read 
                      "Action: "
                      ec2-api-description)))
  (with-output-to-temp-buffer (interp "EC2 doc")
    (dump-ec2-description (assoc fun ec2-api-description))))

(defun dump-ec2-description (fun-entry)
  (pr (car fun-entry) "\n\n")
  (dolist (i (chain-ref (cdr fun-entry) 'params))
    (pr (chain-ref i 'name) " (Required: " (chain-ref i 'required) ")\n")
    (pr (str* "=" 30) "\n")
    (pr (fill-string (chain-ref i 'description)) "\n\n"))

  (pr "Filters\n" (str* "=" 30) "\n")
  (dolist (i (chain-ref (cdr fun-entry) 'filters))
    (pr (chain-ref i 'name) "\n"
        (chain-ref i 'description) "\n\n")))

(defun collapse-whitespace (s)
  (replace-regexp-in-string "[ \n\t\r]+" " " s nil t))

(defun get-text-paragraphs (node)
  (do-wad
   (nodes-where (orfn (tagp 'p) 'stringp) node)
   (mapcar (comp 'collapse-whitespace 'strip
                 (x: (if (stringp x) x (all-text x)))) _)
   (strjoin "\n" _)))

(defun decode-aws-row (tr)
  (dbind (param-name description required . junk) (nodes-where (tagp 'td) tr)
    `((name . ,(strip (all-text param-name)))
      (description . ,(get-text-paragraphs description))
      (required . ,(strip (all-text required)))
      )))

(defun decode-filter-row (tr)
  (dbind (param-name description . junk) (nodes-where (tagp 'td) tr)
    `((name . ,(strip (all-text param-name)))
      (description . ,(get-text-paragraphs description)))))

(defun ec2-extract-info (f)
  (with-temp-buffer 
    (insert-file-contents-literally f)
    `((params . ,(extract-params-buf))
      (filters . ,(extract-filters-buf)))))

(defun extract-params-buf ()
  (goto-char (point-min))
  (let: case-fold-search nil in 
    (re-search-forward (rx "Request" (+ space) "Parameters")))
  (if (< (save-excursion (re-search-forward "<tbody>" nil t) (point))
         (save-excursion (re-search-forward (rx "Response" (+ space) "Elements"))))
      (do-wad
       (xml-parse-region
        (progn (re-search-forward "<tbody>") (match-beginning 0))
        (progn (re-search-forward "</tbody>") (match-end 0)))
       car
       (nodes-where (tagp 'tr) _)
       (mapcar 'decode-aws-row _))))

(defun extract-filters-buf ()
  (goto-char (point-min))
  (when (save-excursion (re-search-forward
                         (rx "Supported" (+ space) "Filters")
                         nil t))
    (>>>
     (let: case-fold-search nil in
       (re-search-forward (rx "<h3" (+ (not (any ">"))) ">" 
                              "Supported" (+ space) "Filters"))
       (re-search-forward (rx "<tbody"))
       (goto-char (match-beginning 0))
       (xml-parse-tag))
     cddr (mapcar 'decode-filter-row _))))

(defun make-aws-fun (cmd)
  (let: unique-params (remove-duplicates 
                       (assoc-default 'params (cdr cmd))
                       :key (comp 'convert-param-name (:= 'name)))
        required-params (filter (comp (=~ "yes") (:= 'required)) unique-params)
        keyword-params (filter (comp 'not (=~ "yes") (:= 'required)) unique-params) in
    `(defun* ,(intern (str "ec2" (decamelize (car cmd)))) 
         (ec2-requester cont
          ,@(mapcar (comp 'convert-param-name (:= 'name)) required-params)
          ,@(if keyword-params 
                (cons '&key (mapcar 
                             (comp 'convert-param-name (:= 'name))
                             keyword-params))))
       ,(with-output-to-string (dump-ec2-description cmd))
       (funcall ec2-requester
                (append
                 (list (cons "Action" ,(car cmd)))
                 ,@(lc (do-wad (chain-ref x 'name) (split-string _ "\\.n" t) car 
                               `(if ,(convert-param-name _)
                                    (aws-encode-param ,_ ,(convert-param-name _)))) 
                       x unique-params))
                cont))))

(defun convert-param-name (n &optional keyword)
  (do-wad n (split-string _ "\\.n" t) car
          (replace-regexp-in-string "\\." "" _)
          decamelize
          (if (startswith _ "-") (substring _ 1) _)
          (if keyword (str ":" _) _)
          intern))

(provide 'aws-api-parser)
