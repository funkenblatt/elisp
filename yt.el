;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(require 'cps-filter)

(defun foobite (id)
  (get-http (connect "gdata.youtube.com")
	    'barbite
	    (concat "/feeds/api/videos/" id)))

(defun barbite (x)
  (let: urlwad (with-temp-buffer
		 (dump x)
		 (goto-char (point-min))
		 (re-search-forward "format='6'")
		 (re-search-backward "url='\\([^']*\\)'")
		 (match-string 1)) in
    (dump urlwad)))

(foobite "nTbL5elVXrU")
