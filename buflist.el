(defun footest (ev)
  (interactive "e")
  (pr last-input-event)
  (switch-to-buffer
   (or
    (x-popup-menu
     ev
     `("Buffers"
       ("Fuckwad"
	,@(lc (cons (buffer-name x) x)
	      x (buflist)))))
    (current-buffer))))

(defun not-my-frame ()
  (lc x x (frame-list) (not (eq x (selected-frame)))))

(defun buflist ()
  (lc x x (buffer-list)
      (not (startswith
	    (buffer-name x) " "))))

(setq live-buffer-keymap (make-sparse-keymap))
(jason-defkeys 
 live-buffer-keymap
 (kbd "<mouse-2>") 
 (lambda (e) 
   (interactive "e")
   (foolet (end (event-end e) pnt (posn-point end) win (posn-window end)
		text (with-current-buffer (window-buffer win)
		       (goto-char pnt) (thing-at-point 'line)))
     (switch-to-buffer (substring text 0 -1))))
 (kbd "RET")
 (lambda ()
   (interactive)
   (foolet (newbuf (get-buffer (substring (thing-at-point 'line) 0 -1)))
     (with-selected-window jason-most-recent-window
       (switch-to-buffer newbuf t))))
 (kbd "d")
 (lambda ()
   (interactive)
   (kill-buffer (substring (thing-at-point 'line) 0 -1))
   (show-live-buffers))
 (kbd "n") 'next-line (kbd "p") 'previous-line)

(defun show-live-buffers (&optional foo)
  (when (and (get-buffer " Live Buffers")
	     (setq foo (lc x x (mappend 'window-list (frame-list)) (eq (window-buffer x) (get-buffer " Live Buffers")))))
    (with-selected-window (car foo)
      (foolet (pnt (window-point) strt (window-start))
	(with-current-buffer (window-buffer)
	  (setq buffer-read-only nil)
	  (erase-buffer)
	  (dolist (i (buflist))
	    (if (eq i (current-buffer))
		nil
	      (dump (buffer-name i))
	      (add-text-properties
	       (line-beginning-position) (line-end-position)
	       `(mouse-face highlight follow-link foobite keymap ,live-buffer-keymap
		 face bold))
	      (dump "\n")))
	  (setq buffer-read-only t))
	(set-window-point (car foo) pnt)
	(set-window-start (car foo) strt)))))

(defun live-buffers-autoupdate ()
  (or (eq (window-buffer (selected-window))
	  (get-buffer " Live Buffers"))
      (show-live-buffers))
  (if buflist-running
      (run-at-time 1 nil 'live-buffers-autoupdate)))

(defun start-buflist ()
  (switch-to-buffer (get-buffer-create " Live Buffers"))
  (setq buflist-running t)
  (show-live-buffers)
  (live-buffers-autoupdate))

(provide 'buflist)
