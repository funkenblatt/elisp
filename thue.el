;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(setq reverser-rules
      '("Taa" "aTa"
	"Tab" "bTa"
	"Tba" "aTb"
	"Tbb" "bTb"
	"Ta$" "L$a"
	"Tb$" "L$b"
	"aL" "La"
	"bL" "Lb"
	"DL" "DT"
	"DT$" ""))

(defun thue-iter (s gram)
  (let: matches (do-wad (lc (lc (cons a (cadr rule)) 
				a (re-findall (regexp-quote (car rule)) s))
			    rule (sublist2 gram))
			(filter 'identity _)
			(apply 'append _))
	match nil in
    (if matches
	(progn
	  (setq match (nth (random (length matches)) matches))
	  (str (substring s 0 (caar match))
	       (cdr match)
	       (substring s (cadar match))))
      s)))

(defun thue-run (s gram)
  (let: new-s (thue-iter s gram) in
    (while (not (eq new-s s))
      (dump new-s "\n")
      (setq s new-s
	    new-s (thue-iter s gram)))))


;; generates a random string, and then copies it.
(thue-run "S$" '("S" "aSa"
		"S" "bSb"
		"S" "DT"
		"Taa" "aTa"
		"Tab" "bTa"
		"Tba" "aTb"
		"Tbb" "bTb"
		"Ta$" "L$a"
		"Tb$" "L$b"
		"aL" "La"
		"bL" "Lb"
		"DL" "DT"
		"DT$" " "))

(thue-run "S$" '("S" "aSa"
		"S" "bSb"
		"S" "DT"
		"Taa" "aTa"
		"Tab" "bTa"
		"Tba" "aTb"
		"Tbb" "bTb"
		"Ta$" "L$a"
		"Tb$" "L$b"
		"aL" "La"
		"bL" "Lb"
		"DL" "DT"
		"DT$" " "))


(defun letter-counts (s)
  (let: out (make-hash-table) in
    (ec (if (gethash c out)
	    (incf (gethash c out))
	  (setf (gethash c out) 1))
	c (append s nil) (not (memq c '(?  ?\n))))
    out))

(defun hash-contains (a b)
  "Does a contain b?"
  (catch 'break
    (maphash
     (lambda (k v)
       (if (not (equal (gethash k b) v))
	   (throw 'break nil)))
     a)
    t))

(defun hash-equal (a b)
  (and (hash-contains a b)
       (= (hash-table-count a)
	  (hash-table-count b))))
