;; -*- lexical-binding: t; -*-
(require 'paredit)

(define-minor-mode paredit-slurp-mode
  "Temporarily use fast shortcuts for paredit slurp and barf operations."
  nil "[slurp]" (keymap nil (kbd "(") 'paredit-backward-slurp-sexp
                        (kbd ")") 'paredit-forward-slurp-sexp
                        (kbd "{") 'paredit-backward-barf-sexp
                        (kbd "}") 'paredit-forward-barf-sexp))

(defun paredit-to-toplevel (&optional func)
  (interactive)
  (if (numberp current-prefix-arg)
      (paredit-to-almost-toplevel current-prefix-arg func)
    (while (ignore-errors (funcall (or func 'paredit-backward-up) 1) t))))

(defun paredit-to-almost-toplevel (n &optional func)
  "Back out from current expression until you're n levels from the top level."
  (if (ignore-errors (funcall (or func 'paredit-backward-up) 1) t)
      (let ((pt (point))
            (levels (paredit-to-almost-toplevel n func)))
        (if (= levels 0) (goto-char pt))
        (- levels 1))
    n))

(defun eval-in-let (expr)
  (interactive (list (preceding-sexp)))
  "Given one expression that happens to be inside a let-expression
of some sort, eval only that expression."
  (let: let-expr (save-excursion
                   (paredit-up-until 
                    (comp (cut memq <> '(let let*)) 'car 'macroexpand))) in
    (dbind (let-thing bindings &rest body) (macroexpand let-expr)
      (print (eval `(,let-thing ,bindings ,expr) t)))))


(defun paredit-up-until (pred)
  (catch 'return
    (while (ignore-errors (paredit-backward-up 1) t)
      (save-excursion 
        (>>> (read/die)
             (if (funcall pred _) (throw 'return _)))))))

(global-set-key (kbd "C-x j P") 'paredit-slurp-mode)
(jason-defkeys
  paredit-mode-map
  (kbd "C-M-u")
  (prefix-combinator 'paredit-backward-up 'paredit-to-toplevel)
  (kbd "C-M-n")
  (prefix-combinator 
   'paredit-forward-up
   (keycmd (paredit-to-toplevel 'paredit-forward-up))))

(provide 'paredit-hacks)
