;; -*- lexical-binding: t; -*-
;; Shit to help me rsync.

(defun get-rsync-buf ()
  (>>> (buffer-list)
       (filter (andfn (comp (cur 'str~ (rx "*rsync*")) 'buffer-name)
                      (comp 'not (without-errors 'process-live-p)
                            'get-buffer-process)) _)
       car (or _ (generate-new-buffer "*rsync*"))))

(defun rsync (src-files dst &optional buf)
  (let: buf (or buf (get-rsync-buf))
        src-files (mapcar (cur 'replace-regexp-in-string
                               "/+$" "") src-files) in
    (apply 'start-process "rsync" buf 
           "rsync" "-avz"
           `(,@src-files ,dst))
    (switch-to-buffer buf)))
