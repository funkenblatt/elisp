(defun flate-dcomp (s)
  (with-temp-buffer
    (let: coding-system-for-write 'binary
	  coding-system-for-read 'binary in
      (with-temp-buffer
	(insert s)
	(call-process-region (point-min) (point-max) "dflate" t t)
	(buffer-string)))))

(defun ccitt-dcomp (s width outf)
  (filedump s "/tmp/crapwads.raw")
  (call-process "fax2tiff" nil nil nil
		"-o" outf "-4" "-X" (str width)
		"-M" "/tmp/crapwads.raw"))

(defun grabfonts ()
  (with-current-buffer "Bacon03Pure.pdf"
    (dotimes (i 100)
      (do-wad
       (pdf-dictref font-procs (symbol-cat 'a (str i)) nil t)
       (pdf-strm _)
       (let: s _
	     w (progn (string-match "/W \\([0-9]+\\)" s) (match-string 1 s))
	     ix (string-match  "ID " s) in
	 (when ix
	   (ccitt-dcomp
	    (substring s (+ ix 3) -4)
	    w (format "/tmp/bar%d.tif" i))))))))

(defun imgwrite (chars)
  (dolist (i chars)
    (if (file-exists-p (str "/tmp/bar" i ".tif"))
	(insert (propertize 
		 "a" 'display
		 `(image :file ,(str "/tmp/bar" i ".tif")
			 :type tiff))))))

(provide 'flate)
