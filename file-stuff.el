(defun make-dirtree (tree)
  (pcase tree
    (`[,filename ,contents] (write-region contents nil filename))
    ((and (pred stringp) filename)  (write-region "" nil filename))
    (`(,dir-name . ,children)
     (make-directory dir-name t)
     (with-default-dir (str default-directory "/" dir-name)
       (mapc 'make-dirtree children)))))

(provide 'file-stuff)
