(defun mk-dns-question (domain)
  (let: parts (split-string domain "\\." t)
	oparts (apply 'append (lc (list (list (length x)) x) x parts))
	qname (apply 'concat oparts) in
    (concat qname "\x00\x00\x01\x00\x01")))

(defun mk-dns-header (id)
  (unibyte-string (lsh id -8) (logand id #xff) 0 (lsh 1 7)
		  0 1 0 0 0 0 0 0))

(defun mk-dns-req (domain id)
  (concat (mk-dns-header id)
	  (mk-dns-question domain)))

(defun send-dns-req (p domain id)
  (process-send-string p (mk-dns-req domain id)))

(defun structify (template l)
  (cond
   ((consp (car template))
      (receive (a rest) (structify (car template) l)
        (receive (b more) (structify (cdr template) rest)
          (list (cons a b) more))))
   ((not template) (list nil l))
   (t (receive (stuff more) (structify (cdr template) (cdr l))
        (list (cons (car l) stuff) more)))))

(setq dns-header-structure '((id-h id-l) (flags-h flags-l) (h l) (h l) (h l) (h l)))
(setq dns-answer-structure '((type-h type-l) (class-h class-l) (ttl 1 2 3) (rlen 1)))

(defun dns-parsewad (dgram &rest s)
  (setq s (append dgram nil))
  (receive
   (header rest) (structify dns-header-structure s)
   (let: qc (intify (nth 2 header))
	 ac (intify (nth 3 header))
	 nc (intify (nth 4 header))
	 misc (intify (nth 5 header))
	 out nil in
     (pr (mapcar 'intify (cddr header)) (ibin (intify (nth 1 header))))
     (setq s rest)
     (dotimes (i qc)		;parse all queries and ignore them.
       (receive
     	(query rest) (dns-parse-query s dgram)
     	(setq s rest)))
     (dotimes (i (+ ac nc misc))
       (receive
	(ans rest) 
	(condition-case err
	    (dns-parse-answer s dgram)
	  (error (signal 'error s)))
	(push ans out)
	(setq s rest)))
     out)))

(defun intify (byte-list)
  (let: out 0 in
    (dolist (i byte-list)
      (setq out (lsh out 8)
	    out (+ out i)))
    out))

(defun dns-parse-name (s dgram)
  (let: out nil in
    (while (and (/= (lsh (car s) -6) 3)
		(/= (car s) 0))
      (push (concat (head (cdr s) (car s))) out)
      (setq s (tail (cdr s) (car s))))
    (if (and s (= (car s) 0))
	(pop s)
      (when (and s (= (lsh (car s) -6) 3))
	(setq out
	      (append (reverse (car
				(dns-parse-name
				 (append (substring dgram
						    (logand (intify (head s 2))
							    (lognot (lsh 3 14))))
					 nil)
				 dgram)))
		      out))
	(setq s (tail s 2))))
    (list (nreverse out) s)))

(defun dns-parse-query (s dgram)
  (receive
   (name rest) (dns-parse-name s dgram)
   (setq s rest)
   (list (cons name (car (structify '((a b) (a b)) s)))
	 (tail s 4))))

(defun dns-parse-answer (s dgram)
  (receive
   (name rest) (dns-parse-name s dgram)
   (setq s rest)
   (receive
    (type-class-etc rest) (structify dns-answer-structure s)
    (setq s rest)
    (setq type-class-etc (mapcar 'intify type-class-etc))
    (if (memq (car type-class-etc) '(2 5))
	(receive
	 (alias rest) (dns-parse-name s dgram)
	 `((,(car type-class-etc) ,name ,alias) ,rest))
      `((,(car type-class-etc) ,name ,(head s (nth 3 type-class-etc))) 
	,(tail s (nth 3 type-class-etc)))))))

(provide 'dns)

;; (setq udp-cl
;;       (make-network-process
;;        :name "baz" :buffer nil
;;        :service 53 :host "68.94.156.1"
;;        :family 'ipv4 :filter (lambda (p s) 
;; 			       (dump (join (mapcar 'repr (dns-parsewad s)) "\n")))
;;        :type 'datagram
;;        :coding 'binary))

;; (delete-process udp-cl)
;; (send-dns-req udp-cl "ycombinator.com" 4000)
