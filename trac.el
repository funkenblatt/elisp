(require 'cps-filter)

(setq trac-password nil)

(defun trac-passwd ()
  (or trac-password (setq trac-password (read-passwd "Password: "))))

(defun trac-auth-headers (user pass)
  (let: creds (base64-encode-string (concat user ":" pass)) in
    `(("Authorization" . ,(format "Basic %s" creds)))))

(defun trac-scrape (&optional pred)
  (let: url "192.168.10.1/PQube_Firmware/trac/report/1"
	hostname (progn (string-match "\\(http://\\)?\\([^/]+\\)\\(/.*\\)" url) (match-string 2 url))
	path (match-string 3 url)
	user "jason"
	pass (trac-passwd)
	conn (connect hostname)
	in
    (get-http conn `(lambda (data) (trac-process data ,pred))
	      path nil (trac-auth-headers user pass))))

(defun trac-process (data &optional pred out)
  (with-temp-buffer
    (pr "Processing data (" (length data) ")\n")
    (dump data)
    (goto-char (point-min))
    (while (re-search-forward 
	    "<tr class=\"color3[^>]*>\\([^\x00]*?\\)</tr>" nil t)
      (let: crap (match-string 1)
	    wads (re-findall
		  "<td[^\x00]*?</td>" crap t)
	    in
	(push
	 (lc (cons (progn (string-match "class=\"\\([^\"]*\\)\"" x)
			  (match-string 1 x))
		   (replace-regexp-in-string
		    "\n\\|\\([ \t]+\\)\\|\\(<[^>]*>\\)"
		    (lambda (s)
		      (let: matchwads (mapcar 'car (cdr (sublist2 (match-data))))
			    in
			(if (car matchwads) " " "")))
		    x))
	     x wads)
	 out))))
   (trac-display out pred))

(defun trac-display (tickets pred)
  (switch-to-buffer
   (get-buffer-create "*trac*"))
  (setq buffer-read-only nil)
  (erase-buffer)
  (local-set-key
   (kbd "n")
   (lambda () (interactive)
     (end-of-line)
     (re-search-forward "^ticket")))
  (local-set-key
   (kbd "p")
   (lambda () (interactive)
     (beginning-of-line)
     (re-search-backward "^ticket")))
  (local-set-key
   (kbd "RET")
   (lambda () (interactive)
     (let: p (thing-at-point 'paragraph)
	   ticket-num (progn (string-match "^ticket: *#\\([0-9]*\\)" p)
			     (match-string 1 p)) in
       (trac-get-ticket ticket-num))))
  (local-set-key (kbd "b")
    (lambda () (interactive)
      (let: p (thing-at-point 'paragraph)
	    ticket-num (progn (string-match "^ticket: *#\\([0-9]*\\)" p)
			     (match-string 1 p)) in
	(call-process "firefox" nil 0 nil
		      (format "http://192.168.10.1/PQube_Firmware/trac/ticket/%s" ticket-num)))))
  (dolist (i tickets)
    (when (or (not pred) (funcall pred i))
      (let: n 0 in
	(dolist (j i)
	  (dump (car j) ": " (cdr j) " ")
	  (if (= n 1) (progn (dump "\n") (setq n 0))
	    (setq n (+ n 1)))))
      (dump "\n\n")))
  (setq buffer-read-only t))

(defun trac-get-ticket (ticket)
  (get-http (connect "192.168.10.1")
	    'trac-process-ticket
	    (format "/PQube_Firmware/trac/ticket/%s" ticket)
	    nil
	    (trac-auth-headers "jason" (trac-passwd)))
  (pr ticket))

(defun trac-get-file (path)
  (lexlet: filename (read-file-name "Save as: "
				    "/tmp/"
				    "/tmp/") in
    (get-http (connect "192.168.10.1")
	      (lambda (data) 
		(let: coding-system-for-write 'binary in
		  (write-region data nil filename)
		  (find-file filename)))
	      path
	      nil
	      (trac-auth-headers "jason" (trac-passwd)))))

(defun trac-attachments (attach-data)
  (let: links (lc x x (re-findall "<a [^>]*>" attach-data t) (not (string-match "timeline" x)))
	hrefs (lc (progn (string-match "href=\"\\([^\"]*\\)\"" x) (match-string 1 x)) x
		  links)
	link-texts (and hrefs (re-findall "<a [^>]*>[^\x00]*?</a>" attach-data t)) in
    (setq link-texts (lc (progn (string-match "<a [^>]*>\\([^\x00]*?\\)</a>" x)
				(match-string 1 x)) x link-texts (not (string-match "timeline" x))))
    (zip link-texts hrefs)))

(defun trac-process-ticket (page &optional attachments comments commenters)
  (with-current-buffer (get-buffer-create "trac-poops")
    (erase-buffer)
    (dump page)
    (goto-char (point-min))
    (if (re-search-forward "<div class=\"searchable\"" nil t)
	(progn
	  (re-search-forward ">\\([^\x00]*?\\)</div>" nil t)
	  (setq page (match-string 1)))
      (setq page ""))
    (and (re-search-forward "<dl class=\"attachments\">" nil t)
	 (re-search-forward "\\([^\x00]*?\\)</dl>" nil t)
	 (setq attachments (trac-attachments (match-string 1))))
    (while (re-search-forward "Changed .* ago .*" nil t)
      (push (match-string 0) commenters)
      (re-search-forward "<div class=\"comment searchable\">\\([^\x00]*?\\)</div>" nil t)
      (push (match-string 1) comments)))
  (with-current-buffer (get-buffer-create "foo")
    (let: map (make-sparse-keymap) in
      (use-local-map map))
    (local-set-key (kbd "RET")
		   (lambda () (interactive)
		     (trac-get-file (thing-at-point 'filename))))
    (erase-buffer)
    (dump
     (replace-regexp-in-string "<[^>]*>" "" page))
    (fill-region (point-min) (point-max))
    (when comments
      (goto-char (point-max))
      (dump "\nComments\n")
      (ec: (dump (trac-tagstrip y) "\n==============================\n" 
		 (trac-fillwad (trac-tagstrip x)) "\n")
	  x y in comments commenters))
    (when attachments
      (goto-char (point-max))
      (dump "\nAttachments:\n")
      (ec (dump (cadr x) "\n") x attachments)))
  (set-window-buffer (next-window) (get-buffer "foo")))

(defun trac-tagstrip (x) (replace-regexp-in-string "<[^>]*>" "" x))

(defun trac-new (ticket-data)
  (string-match "new" (cdr (assoc "status" ticket-data))))

(defun trac-has (ticket-data field rx)
  (string-match rx (cdr (assoc field ticket-data))))

(defun trac-fancy (expr)
  (trac-scrape
   `(lambda (data)
      ,(maptreepred
	(lambda (x)
	  `(trac-has data ,@(lc (if (symbolp y) (symbol-name y) y) y x)))
	expr
	(lambda (x)
	  (vectorp x))))))

(defun trac-default ()
  (interactive)
  (trac-fancy
   '(and [owner jason]
	 (not [status closed])
	 (not [type testing]))))

(defun trac-fillwad (x)
  (with-temp-buffer
    (dump x)
    (fill-region (point-min) (point-max))
    (buffer-string)))

(global-set-key (kbd "C-x t") 'trac-default)

(provide 'trac)
