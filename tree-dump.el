;; -*- lexical-binding: t; -*-

;;; This uses the concept of a "text image", which is a list of the form
;;;
;;;     (w h fun)
;;;
;;; `fun' gets called to render one line of an image, and returns a continuation
;;; that renders the next line (or nil if there's nothing more to render)

(defun render-tree (tree leaf-p left right value)
  (if (funcall leaf-p tree)
      (let:: l (funcall left tree)
             r (funcall right tree)
             item (funcall value tree)
             (lw lh lfun) (render-tree l leaf-p left right value)
             (rw rh rfun) (render-tree r leaf-p left right value)
             subtree-widths (+ lw rw 2)
             item-width (length (str item))
             width (max subtree-widths item-width) in

        (list (max subtree-widths item-width)
              (+ (max lh rh) 3)
              (lambda ()
                (pr (str* " " (- (/ width 2) (max (/ item-width 2) 1)))
                    item
                    (str* " " (- width item-width (- (/ width 2) (max (/ item-width 2) 1)))))
                (lambda ()
                  (pr (str* " " (- (/ width 2) 1))
                      "|"
                      (str* " " (- width (/ width 2))))

                  (lambda ()
                    (let: left-pad (if (< subtree-widths width)
                                       (- (/ width 2) (max (/ subtree-widths 2) 1))
                                     0)
                      in
                      (pr (str* " " (+ left-pad (/ lw 2)))
                          (str* "-" (round (+ (/ lw 2.0) 2 (/ rw 2.0))))
                          (str* " " (- width (+ left-pad (/ lw 2)) (round (+ (/ lw 2.0) 2 (/ rw 2.0))))))
                      (lambda ()
                        (nlet lp ()
                          (pr (str* " " left-pad))
                          
                          (if lfun
                              (setf lfun (funcall lfun))
                            (pr (str* " " lw)))

                          (pr "  ")
                          
                          (if rfun
                              (setq rfun (funcall rfun))
                            (pr (str* " " rw)))

                          (pr (str* " " (- width left-pad subtree-widths)))

                          (if (or lfun rfun) lp)))))))))
    (let: item (funcall value tree) in
      (list (length (str item)) 1 (lambda () (pr item) nil)))))

(defun render-image (img)
  (pcase img
    (`(,w ,h ,f)
     (while f
       (setq f (funcall f))
       (pr "\n")))))

(defun standard-leaf-p (x)
  (pcase x
    (`(,item ,left ,right) t)
    (x nil)))

(defun standard-value (x)
  (pcase x
    (`(,item ,left ,right) item)
    (x x)))

(defun dump-tree (tree)
  (render-image (render-tree tree 'standard-leaf-p 'cadr 'caddr 'standard-value)))

;; Demonstration:
;; (render-image (render-tree '(a (b d (e h i)) (c f (g j k)))))

;; prints out

;;        a        
;;        |        
;;    ---------    
;;   b        c    
;;   |        |    
;; ----     ----   
;; d   e    f   g  
;;     |        |  
;;    ---      --- 
;;    h  i     j  k

(provide 'tree-dump)
