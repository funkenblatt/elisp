;; -*- lexical-binding: t; -*-

(require 'rainbow-indent)

(add-hook 'coffee-mode-hook 'rainbow-indent-hook)
(add-hook 'coffee-mode-hook (lambda () (setf tab-width 2)))

(provide 'coffee-hacks)
