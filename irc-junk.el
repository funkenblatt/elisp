(defun hash-intersect (a &rest b)
  (let: out nil in
    (maphash (lambda (k v)
	       (if (all (lc (gethash k x) x b))
		   (push k out)))
	     a)
    out))

(defun channel-commons (&rest channels)
  (apply 'hash-intersect
	 (lc (with-current-buffer x erc-channel-users)
	     x channels)))

(defun dump-channel-commons (n &rest channels)
  (apply 'dump
   (join
    (lc (cons x (apply 'channel-commons x))
	x (comb channels n))
    "\n")))

(defun irc-linenick ()
  (let: s (completing-read "Name: " erc-channel-users) in 
    (upon (equal s "")
	(setq s (thing-at-point 'line))
	(string-match "^<\\([^>]*\\)" s)
	(match-string 1 s)
	:else
      s)))

(add-hook 'erc-mode-hook
	  (lambda ()	    
	    (onkey (kbd "C-c f") (occur (str "^<" (irc-linenick))))
	    (onkey (kbd "C-c t") (occur (str (irc-linenick) ":")))))

(defun read-string/thingatpt (prompt &optional thing)
  (let: s (read-from-minibuffer prompt) in
    (if (equal s "")
	(thing-at-point (or thing 'symbol))
      s)))

(defun track-conversation (a b)
  (interactive (mapcar 'read-string/thingatpt '("Name A: " "Name B: ")))
  (let: nick-re (str "\\(" a "\\|" b "\\)") in
    (occur (str "^<" nick-re ">.*" nick-re ":"))))

(defun track-conv-point ()
  (interactive)
  (beginning-of-line)
  (re-search-forward "<\\([^>]*\\)> \\([^:]*\\)")
  (track-conversation
   (match-string 1) (match-string 2)))

(provide 'irc-junk)
