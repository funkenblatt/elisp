(defun scala-indent-line ()
    "Indent current line as smartly as possible.
When called repeatedly, indent each time one stop further on the right."
    (interactive)
    (let ((indentation (scala-indentation)))
      (scala-indent-line-to indentation)))

(defun scala-block-indentation ()
  (let ((block-start-bol (scala-point-after (beginning-of-line)))
        (block-start-eol (scala-point-after (end-of-line)))
        (block-after-spc (scala-point-after (scala-forward-spaces))))
    (if (> block-after-spc block-start-eol)
	(progn
	  (end-of-line)
	  (when (search-backward ")" block-start-bol t)
	    (forward-char)
	    (backward-sexp))
	  (+ (current-indentation) scala-mode-indent:step))
      (current-column))))

(defun scala-indent-from-def ()
  (let ((start (point)))
    (save-excursion
      (when (and (search-backward "def" nil t)
                 (> (scala-point-after
                     (while (progn (forward-sexp) (scala-forward-spaces)
                                   (not (looking-at "[={]")))))
                    start))
        (search-forward "(")
        (current-column)))))

(defun scala-indentation ()
  "Return the suggested indentation for the current line."
  (save-excursion
    (beginning-of-line)
    (or (and (scala-in-comment-p)
             (not (= (char-after) ?\/))
             (scala-comment-indentation))
        (scala-indentation-from-following)
        (scala-indentation-from-preceding)
        (scala-indent-from-def)
        (scala-indentation-from-block)
        0)))

(defun line-or-region ()
  (if (region-active-p)
      (get-region)
    (thing-at-point 'line)))

(define-key scala-mode-map (kbd "M-RET")
  (keycmd (process-send-string
           (get-process scala-repl-process)
           (>>> (line-or-region)
                (if (not (endswith _ "\n"))
                    (str _ "\n")
                  _)))))

(defun set-scala-repl-process ()
  (interactive)
  (setf scala-repl-process (get-buffer-process (current-buffer))))

(provide 'scala-hacks)
