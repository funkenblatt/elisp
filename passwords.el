;; -*- lexical-binding: t; -*-

(defun add-passwd ()
  (interactive)
  (let: file (read-string "Name: ")
        pass (read-passwd "Password: ")
        process-connection-type nil in
    (with-default-dir "~/passwords/"
      (>>>
       (jf-shellproc (interp "gpg --batch --no-tty --symmetric --output #,[file].gpg")
                     (x: (pr x "\n")))
       (progn
         (process-send-string _ pass)
         (process-send-eof _))))
    (clear-string pass)))

(defun get-password (name)
  (interactive 
   (>>> (mapcar 'file-name-sans-extension (ls "~/passwords"))
        (ido-completing-read "Which password?" _) list))
  (cond
   ((file-exists-p (interp "~/passwords/#,[name].gpg"))
    (jf-shellproc
     (interp "gpg -d ~/passwords/#,[name].gpg 2> /tmp/gpg.log")
     (x: (kill-new (strip x)) (pr (filestr "/tmp/gpg.log")))))

   ((file-exists-p (interp "~/passwords/#,[name].bfe"))
    (let: pass (read-passwd "Encryption Key: ") in
      (>>> (jf-shellproc
            (interp "bcrypt -o ~/passwords/#,[name].bfe 2>/dev/null")
            (x: (clear-string pass) (kill-new (strip x))))
           (progn (process-send-string _ pass)
                  (process-send-string _ "\n")))))
   
   (t (kill-new (strip (filestr (interp "~/passwords/#,name")))))))

(provide 'passwords)
