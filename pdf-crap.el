(defun read-pdfdict ()
  (let: out nil 
	stk nil
	val nil
	done nil in
    (while (setq val (read-pdfvalue))
      (cond ((eq val 'R) 
	     (push (list (car (reverse stk))
			 (cons 'R (cdr (reverse stk)))) out)
	     (setq stk nil))
	    ((and stk (symbolp val))
	     (if (symbolp (car stk))
		 (progn (push (list (car stk) val) out)
			(setq stk nil))
	       (push (reverse stk) out)
	       (setq stk (list val))))
	    (t (push val stk))))
    (if stk (push (reverse stk) out))
    (cons 'dict out)))

(defun get-trailer ()
  (goto-char (point-max))
  (search-backward "trailer")
  (let: start (match-beginning 0)
	end (match-end 0)
	trailer (progn (goto-char end) (read-pdfvalue)) 
	xref-entries nil
	prev (assq 'Prev (cdr trailer)) in
    (goto-char start)
    (upon prev
	(goto-char (+ (cadr (assq 'Prev (cdr trailer))) (point-min)))
      :else
      (search-forward "startxref")
      (goto-char (+ (read-pdfnum) (point-min))))
    (vector trailer xref-entries (point))))

(defun skip-xref ()
  (let: entries (list (read-pdfnum) (read-pdfnum)) in
    (pdf-skip-whitespace)
    (forward-char (* (cadr entries) 20))
    (buffer-substring (point) (+ (point) 100))))

(defun read-pdfname ()
  (re-search-forward "/\\([a-zA-Z0-9-]*\\)")
  (intern (match-string 1)))

(defun read-pdfnum ()
  (re-search-forward "[0-9.]+")
  (read (match-string 0)))

(defun ordered (&rest l)
  (catch 'break
    (while (cdr l)
      (or (<= (car l) (cadr l))
	  (throw 'break nil))
      (pop l))
    t))

(defmacro char (s) (aref s 0))

(defun pdf-skip-whitespace ()
  (skip-chars-forward "\t\n\r ")
  (point))

(defun read-pdfvalue ()
  (re-search-forward "[R>0-9/<tfn([]\\|]")
  (let: c (aref (match-string 0) 0) in
    (cond
     ((ordered ?0 c ?9) 
      (forward-char -1)
      (read-pdfnum))
     ((eq c ?>) (forward-char) nil)
     ((eq c ?<)
      (upon (eq (char-after) ?<)
	  (forward-char 1)
	  (read-pdfdict)
	:else
	(read-pdfhexstring)))
     ((eq c ?R) 'R)
     ((eq c ?/) 
      (forward-char -1) 
      (read-pdfname))
     ((eq c (char "(")) ; lparen
      (read-pdfstring))
     ((eq c (char "[")) ; lbracket
      (read-pdfarray))
     ((eq c ?t) (read/die) t)
     ((memq c '(?n ?f)) (read/die) 'nullity))))

(defun read-pdfarray ()
  (let: foo nil 
	out nil in
    (while (setq foo (read-pdfvalue))
      (if (eq foo 'R)
	  (push (cons 'R (reverse (list (pop out) (pop out))))
		out)
	(push foo out)))
    (apply 'vector (nreverse out))))

(defun read-pdfstring ()
  (re-search-forward "\\(.\\|\n\\)*?[^\\\\])")
  (substring (match-string 0) 0 -1))

(defun read-pdfhexstring ()
  (re-search-forward "[^>]*>")
  (substring (match-string 0) 0 -1))

(defun pdf-objref-p (x)
  (and (consp x)
       (eq (car x) 'R)))

(defun pdf-dictref (dict key &optional dflt no-deref)
  "Get a value from a PDF dictionary."
  (let: val (assq key (cdr dict)) in
    (if val
	(if (and (not no-deref)
		 (pdf-objref-p (cadr val)))
	    (pdf-get-xref pdf-xrefs (cadr val))
	  (cadr val))
      dflt)))

(defun read-pdfxref ()
  (let: sym (read/die)
	segments (list (list (read-pdfnum)
			     (read-pdfnum)
			     (pdf-skip-whitespace)))
	trailer nil in
    (catch 'break
      (while t
	(forward-char (* (cadar segments) 20))
	(if (eq (char-after (point)) ?t)
	    (throw 'break segments)
	  (push (list (read-pdfnum) (read-pdfnum)
		      (pdf-skip-whitespace))
		segments))))
    (read/die) 				;skip trailer token
    (setq trailer (read-pdfvalue))
    (when (pdf-dictref trailer 'Prev)
      (goto-char (pdf-dictref trailer 'Prev))
      (setq segments (append segments (aref (read-pdfxref) 1))))
    (vector trailer segments)))	;return trailer dict and xref segments.

(defun pdf-get-xref (xrefs objref &optional noread)
  (when (not (boundp 'pdf-indirect-objs))
    (setq pdf-indirect-objs (make-hash-table)))
  (setq objref (cadr objref))
  (if (and (gethash objref pdf-indirect-objs) (not noread))
      (gethash objref pdf-indirect-objs)
    (while (and xrefs
		(not (and (>= objref (caar xrefs))
			  (< objref (+ (caar xrefs)
				       (cadar xrefs))))))
      (pop xrefs))
    (if (not xrefs)
	nil
      (setq xrefs (car xrefs))
      (save-excursion
	(goto-char (caddr xrefs))
	(forward-char (* (- objref (car xrefs)) 20))
	(goto-char (+ (point-min) (read-pdfnum)))
	(search-forward "obj")
	(if noread
	    (point)
	  (let: val (read-pdfvalue) in
	    (puthash objref val pdf-indirect-objs)
	    val))))))

(defun pdf-getobj (objref &optional noread)
  (pdf-get-xref pdf-xrefs objref noread))

(defun pdf-catalog ()
  (unless (boundp 'pdf-doc-catalog)
    (setq pdf-doc-catalog
	  (save-excursion
	    (goto-char (point-max))
	    (search-backward "startxref" 1024)
	    (read/die)
	    (goto-char (read/die))
	    (let: junk (read-pdfxref)
		  trailer (aref junk 0)
		  xrefs (aref junk 1)
		  catalog nil in
	      (setq pdf-xrefs xrefs)
	      (when (setq catalog (pdf-dictref trailer 'Root))
		catalog)))))
  pdf-doc-catalog)

(defun pdf-outlines ()
  (let: cat (pdf-catalog)
	outlines (pdf-dictref cat 'Outlines) in
    outlines))

(defmacro do-wad (&rest actions)
  (while (cdr actions)
    (setq actions
	  (cons
	   (remap-symbols
	    (cadr actions)
	    (list (cons '_ (car actions))))
	   (cddr actions))))
  (car actions))

(defun make-nametab (name-array)
  (let: out (make-hash-table) in
    (dotimes (i (length name-array))
      (if (= (mod i 2) 1)
	  (puthash (intern (aref name-array (- i 1)))
		   (aref name-array i)
		   out)))
    out))

(defun outldump (&optional o depth maxdepth)
  (if (not o) (setq o (pdf-dictref (pdf-outlines) 'First)))
  (if (not depth) (setq depth 0 maxdepth 0))
  (let: done nil in
    (while (not done)
      (scratchdump (str* "  " depth) (pdf-dictref o 'Title) " " 
		   (pdf-dictref o 'A) "\n")
      (if (and (pdf-dictref o 'First) (< depth maxdepth))
	  (outldump (pdf-dictref o 'First) (+ depth 1) maxdepth))
      (setq o (pdf-dictref o 'Next))
      (if (not o) (setq done t)))))

(defun pdf-strm (objref)
  (let: ix (pdf-getobj objref t)
	sdict nil in
    (save-excursion 
      (goto-char (+ ix 1))
      (setq sdict (read-pdfvalue))
      (re-search-forward "stream\r?\n") 
      (buffer-substring 
       (point) (+ (point)
		  (pdf-dictref sdict 'Length))))))
