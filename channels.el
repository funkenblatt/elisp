;; -*- lexical-binding: t; -*-

(require 'el2)

;;; Queue implementation.
(defun queue ()
  (let: front nil back nil in
    (fnobj
     (:add (x) (if front
                   (setf (cdr back) (cons x nil)
                         back (cdr back))
                 (push x front)
                 (setf back front)))
     (:add-left (x) (if front (push x front)
                      (push x front)
                      (setf back front)))
     (:take () (pop front))
     (:ready () (not (null front)))
     (:get-all () front))))

(defmacro read> (var channel &rest body)
  (declare (indent 2))
  `(cps> ,var (funcall ,channel :take! :cont!)
     ,@body))

;;; Async channel
(el2
 (defun channel ()
   "Create an async channel-like thingy.  You can request data using `take!',
providing a callback function that will execute on said data, effectively
blocking until data is added to the channel using `:put!'.  If necessary,
you can also `:return!' elements from the channel, which will make said data
items be the next thing to be read by some reader."
   (let: recv-q (queue)
         dispatch-q (queue)
         nested nil
         run-loop 
         (λ () (unless nested
                 (setf nested t)
                 (unwind-protect
                     (while (and (dispatch-q :ready) (recv-q :ready))
                       ((dispatch-q :take) (recv-q :take)))
                   (setf nested nil)))) in
     (fnobj
      (:put! (x) (recv-q :add x) (run-loop))
      (:take! (f) (dispatch-q :add f) (run-loop))
      (:return! (x) (recv-q :add-left x) (run-loop)))))

 (defun get-line-channel (in-chunks cont)
   "Read a line from the string chunks that are sent to IN-CHUNKS.
Call CONT with result."
   (nlet lp ((chunks nil))
     (read> chunk in-chunks
       (if chunk
           (>>> (string-match-p "\n" chunk)
                (if _
                    (progn
                      (when (> (length chunk) (+ _ 1))
                        (in-chunks :return!  (substring chunk (+ _ 1))))
                      (>>> (substring chunk 0 _)
                           (cons _ chunks)
                           nreverse (apply 'concat _)
                           cont))
                  (lp (cons chunk chunks))))
         (cont nil)))))


 (defun read-n-channel (in-chunks n cont)
   "Read N bytes from string chunks sent to IN-CHUNKS."

   (nlet lp ((chunks nil) (remaining n))
     (read> chunk in-chunks
       (cond
        ((not chunk) (>>> chunks nreverse (apply 'concat _) cont))
        ((>= (length chunk) remaining)
         (when (> (length chunk) remaining)
           (in-chunks :return! (substring chunk remaining)))
         (>>> (substring chunk 0 remaining)
              (cons _ chunks)
              nreverse (apply 'concat _)
              cont))
        (t (lp (cons chunk chunks) (- remaining (length chunk))))))))

 ;; (defun get-line (in-chunks)
 ;;    "Returns a channel that will yield exactly 1 line, taken from the sequence of
 ;; arbitrary string chunks that are put into IN-CHUNKS."
 ;;    (let: out (channel) in
 ;;      (nlet lp ((chunks nil))
 ;;        (read> chunk in-chunks
 ;;          (if chunk
 ;;              (>>> (string-match-p "\n" chunk)
 ;;                   (if _
 ;;                       (progn
 ;;                         (out :put! (>>> (substring chunk 0 _)
 ;;                                         (cons _ chunks)
 ;;                                         nreverse (apply 'concat _)))
 ;;                         (if (> (length chunk) (+ _ 1))
 ;;                             (in-chunks :return! (substring chunk (+ _ 1)))))
 ;;                     (lp (cons chunk chunks))))
 ;;            (out :put! nil))))
 ;;      out))
 
 ;;  (defun read-n-bytes (in-chunks n)
 ;;    "Returns a channel that will yield N bytes from the stream of arbitrary-sized
 ;; chunks of shit coming in on IN-CHUNKS.  Assumes that IN-CHUNKS will always yield
 ;; unibyte strings."
 ;;    (let: out (channel) in
 ;;      (nlet lp ((chunks nil) (remaining n))
 ;;        (cps-let ((chunk (in-chunks :take! cont!)))
 ;;          (cond
 ;;           ((not chunk) (>>> chunks nreverse (apply 'concat _) (out :put! _))
 ;;            (out :put! nil))
 ;;           ((>= (length chunk) remaining) 
 ;;            (when (> (length chunk) remaining)
 ;;              (in-chunks :return! (substring chunk remaining)))
 ;;            (>>> (substring chunk 0 remaining)
 ;;                 (cons _ chunks)
 ;;                 nreverse (apply 'concat _)
 ;;                 (out :put! _)))
 ;;           (t (lp (cons chunk chunks) (- remaining (length chunk)))))))
 ;;      out))
 )


(provide 'channels)
