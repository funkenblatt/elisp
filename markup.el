(require 'my-fontify)

(setf markup-faces (dict))

(setf special-markup-shit
      (list
       (cons 'code (x: `(blockquote ,x)))
       (cons 'clojure (x: `(blockquote
                            ,(w/buf (get-buffer-create "clj-fontify")
                               (clojure-mode)
                               (erase-buffer)
                               (insert x)
                               (redisplay)
                               (get-buffer-markup markup-faces)))))
       (cons 'elisp (x: `(blockquote
                          ,(w/buf (get-buffer-create "clj-fontify")
                             (emacs-lisp-mode)
                             (erase-buffer)
                             (insert x)
                             (redisplay)
                             (get-buffer-markup markup-faces)))))))

(defun ->html ()
  (setf markup-faces (dict))
  (>>> 
   (buffer-string)
   substring-no-properties
   (split-string _ "\n\n+")
   (mapcar
    (x: (iflet stars (str~ "^*+" x)
          `(,(intern (str "h" (length stars)))
            ,(substring x (length stars)))
          (if (str~ "^," x)
              (let: body (strjoin "\n" (cdr (split-string (substring x 1) "\n")))
                    tag (intern (car (split-string (substring x 1) "\n")))
                    handler (assoc-default tag special-markup-shit) in
                (if handler
                    (funcall handler body)
                  (error "No handler for this shit")))
            `(p ,(replace-regexp-in-string 
                  (rx "`" (* (not (any "'"))) "'")
                  (x: (str "<b>" (substring x 1 -1) "</b>"))
                  x)))))
    _)
   `(html
     (head
      ((meta http-equiv Content-Type content "text/html; charset=UTF-8"))
      (title ,(cadr (car _)))
      ([style type "text/css"]
       "body { font-family: sans-serif; font-size: 0.9em }"
       "div.container {width: 60%; margin-left: auto; margin-right: auto; }\n"
       "img { border: 1px solid #000; }\n"
       ,(apply 'concat (mapcar (comp 'make-face-style 'car)
                               (hash->list markup-faces)))))
     (body
      ([div class "container"]
       ,@_)))
   markup))

(provide 'markup)
