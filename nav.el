(require 'cl)

(defvar jason-nav-map
  (keymap
   nil
   (kbd "n") 'next-line
   (kbd "p") 'previous-line
   (kbd "f") 'forward-char
   (kbd "b") 'backward-char
   [down] 'mini-scroll-up
   [up] 'mini-scroll-down
   (kbd "M-n") 'mini-scroll-up
   (kbd "M-p") 'mini-scroll-down
   (kbd "SPC") 'scroll-up
   (kbd "DEL") 'scroll-down
   (kbd "/") 'isearch-forward)
  "Navigation map.  Possibly for jason.")

(defun mini-scroll-up ()
  (interactive)
  (scroll-up 2))

(defun mini-scroll-down ()
  (interactive)
  (scroll-down 2))

(defun jason-nav-mode ()
  (interactive)
  (lexlet: oldmap (current-local-map)
           newmap (make-sparse-keymap)
           rdonly buffer-read-only in
    (setq buffer-read-only t)
    (set-keymap-parent newmap jason-nav-map)
    (use-local-map newmap)
    (onkey (kbd "q") 
	   (use-local-map oldmap)
	   (setq buffer-read-only rdonly))))

(global-set-key (kbd "C-x j n") 'jason-nav-mode)

(provide 'nav)
