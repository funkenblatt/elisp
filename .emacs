(require 'cl)
(require 'package)

(add-to-list 'package-archives 
    '("marmalade" .
      "https://marmalade-repo.org/packages/"))

(package-initialize)

(push (expand-file-name "~/elisp") load-path)
(if (eq system-type 'gnu/linux)
    (push "~/github/epdf" load-path)
  (push "~/elisp/epdf" load-path))

(load "jf-lib")
(transient-mark-mode t)

(cond 
 ((eq system-type 'gnu/linux) 
  (set-face-attribute 'default nil :family "DejaVu Sans Mono" :height 75))
 ((getenv "PULSE_LAPTOP")
  (set-face-attribute 'default nil :height 95)))

;(describe-face 'default)
(setq c-default-style "python")
(setq c-basic-offset 2)
(setq next-screen-context-lines 0)
(c-set-offset 'label 0)

;(set-face-attribute 'font-lock-keyword-face nil :weight 'bold)
;(set-face-attribute 'font-lock-builtin-face nil :weight 'bold)
;(set-face-attribute 'font-lock-function-name-face nil :weight 'bold)

(defun filedump (s f)
  (let: coding-system-for-write 'binary in
    (write-region s nil f)))

(defun repr (x) (format "%S" x))

(defun days-in-month ()
  `(31 
    ,(if (date-leap-year-p (get-date-elt 'year))
	 29
       28)
    31 30 31 30 31 31 30 31 30 31))

(defun rearrange-windows (&optional spec)
  (interactive)
  (if (not spec) 
      (setq spec 
	    (mapcar 'intern 
		    (split-string (read-from-minibuffer "Window spec: ")))))
  (foolet (bindings 
	   (lc* 
	    (cons (intern (char-to-string (+ ?a a))) 
		  (buffer-name (window-buffer b)))
	    (a b)
	    ((range (length (window-list)))
	     (window-list))))
    (wconfig (lc (or (get-assoc bindings i) i)
		 i spec))
    nil))

(defun get-date-elt (which? &optional date)
  (apply
   (lambda (second minute hour day month year dow dst zone)
     (symbol-value which?))
   (if date
       date
     (decode-time (current-time)))))

(defun jason-c-mode-hook ()
  (setq tab-width 2
	comment-start "//"
	comment-end ""
	indent-tabs-mode nil)
  (define-key c-mode-map (kbd "C-c l") 'list-cfuns)
  (define-key c-mode-map (kbd "C-c o") 'open-include))

(add-hook 'c-mode-hook 'jason-c-mode-hook)
(add-hook 'java-mode-hook 'jason-c-mode-hook)

(defalias '!= (symbol-function '/=))

(defun jf-hungry-del-back ()
  "Basically the same as c-hungry-delete, only doesn't
require loading the rest of cc-cmds.el which is kind of big"
  (interactive)
  (foolet (here (point))
    (skip-chars-backward " \t\n\r\f\v")
    (if (!= (point) here) 
	(delete-region (point) here)
      (backward-delete-char-untabify 1))))

(setq jason-most-recent-window (selected-window))
(defun jason-set-recent-window ()
  (interactive)
  (setq jason-most-recent-window (selected-window)))

(defun jason-window-switch (fun)
  (let: win (funcall fun nil nil 'visible) in
    (if (not (eq (window-frame win) (selected-frame)))
	(select-frame-set-input-focus (window-frame win)))
    (select-window win)))

(defun jason-next-window ()
  (interactive)
  (jason-window-switch 'next-window))

(defun jason-prev-window ()
  (interactive)
  (jason-window-switch 'previous-window))

(defun wconfig (configspec)
  "Take configspec, which is a tree describing the window splits and
which buffers go in each window, and make the spec happen.  Spec is given
in fixed-arity Polish notation, i.e. v h a b c produces a vertical split
whose top half contains a horizontal split with a and b in it, and whose
bottom half contains c."
  (delete-other-windows)
  (foolet (rpn-operators (alist->dict '((v . 2) (h . 2))))
    (wconfig-recurse (car (rpn-parse (prefix-parse configspec))))))

(defun wconfig-recurse (configspec)
  (if (stringp configspec)
      (switch-to-buffer configspec)
    (foolet (new-win (split-window nil nil (eq (car configspec) 'h))
	     windows (list (selected-window) new-win))
      (dolist (i (zip windows (cdr configspec)))
	(with-selected-window (car i)
	  (wconfig-recurse (cadr i)))))))

(defun sum (l) (reduce '+ l))

(defun xor (a b)
  "Logical XOR.  Not a macro, normal evaluation order is in effect."
  (rpn a b not and a not b and or))

(defun buffer-mode (buf) (with-current-buffer buf major-mode))

(setq scratchdump-buffer "*scratch*")

(defun scratchdump (&rest s)
  (with-current-buffer scratchdump-buffer
    (apply 'dump s)))

(defun rdump (&rest s)
  (dolist (i s)
    (prin1 i (current-buffer))))

(defun dump-lines (&rest s)
  (ec (dump x "\n") x s))

(defun pr (&rest s)
  (mapc 'princ s))

(defun pr1 (&rest s)
  (mapc 'prin1 s))

(defun prf (fmt &rest s)
  (pr (apply 'format fmt s)))

(defun fdump (fmt &rest args)
  (dump (apply 'format fmt args)))

(defun sfdump (fmt &rest args)
  (scratchdump (apply 'format fmt args)))

(defun path->list (p)
  (lc x x (split-string p "/") (not (equal x ""))))

(defmacro with-default-dir (dir &rest body)
  "Temporarily set the default directory to something"
  `(let ((default-directory ,dir)) ,@body))

(add-hook 'next-error-hook 
	  (lambda () 
	    (pop-to-buffer next-error-last-buffer)))

(defmacro fe (expr varname lexp)
  `(filter (lambda (,varname) ,expr) ,lexp))

(defun hexify (i)
  (cond2 (symbolp i) (string-to-number (if (startswith (symbol-name i) ",") 
					   (substring (symbol-name i) 1) 
					 (symbol-name i)) 
				       16)
	 t (string-to-number (number-to-string i) 16)))

(defmacro hex-list (&rest l)
  "See hex-vec, but produces a list."
  (cons 'list (mapcar 'hexify l)))

(defmacro hex-vec (&rest l)
  "Turns arguments in hex form into a vector of integers, without
requiring annoying-ass hash-x symbol crap."
  (cons 'vector (mapcar 'hexify l)))

(defun rot13 (word) 
  (concat 
   (lc 
    (rpn i 97 >= i 97 26 + < and	; predicate
	 i 97 - 13 + 26 mod 97 +	; if true:
	 i if)				; else clause
    i word)))

(defun rot13-2 (word)
  (concat
   (lc (pn if and (>= i 97) (pn < i + 97 26)
	   + 97 mod + 13 - i 97 26
	   i) i word)))

(put 'downcase-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)

(defun html-table (sexp id)
  (markup
   `((table id ,id)
     ,@(lc* `((tr class ,(format "%s_%d" id row))
	      ,@(lc* `((td class ,(format "%s_%d" id col)) ,item)
		     (item col) (tabrow (enum-list tabrow))))
	    (tabrow row) (sexp (enum-list sexp))))))

(defun mime-timestamp ()
  (format-time-string "%a, %d %b %Y %H:%M:%S %z (%Z)"))

(setq mime-type-list '((".gif" . "image/gif") (".jpg" . "image/jpeg")
		       (".txt" . "text/plain") (".png" . "image/png")
		       (".zip" . "application/zip")))

(defun get-mime-type (filename)
  (catch 'return
    (dolist (i mime-type-list)
      (if (endswith filename (car i)) (throw 'return (cdr i))))
    "application/octet-stream"))

(defun window-h-percent (percent)
  "Changes the current window width to some percent
of the frame width"
  (interactive "nHorizontal Percent:")
  (let* ((total-width (frame-width))
	 (desired-width (/ (* percent total-width) 100)))
    (enlarge-window (- desired-width (window-width)) t)))

(defun window-v-percent (percent)
  "Changes the current window height to some percent
of the frame width"
  (interactive "nVertical Percent:")
  (let* ((total-height (frame-height))
	 (desired-height (rpn percent total-height * 100 /)))
    (enlarge-window (- desired-height (window-height)))))

(setq inhibit-startup-screen t)

(defun line-endings (newending start end)
  (interactive "sEnding Line With: \nr")
  (if mark-active
      (replace-regexp "$" newending nil start end)
    (replace-regexp "$" newending nil (line-beginning-position) (line-end-position))))

(defun print-tree (tree &optional indents dumper)
  (if (not dumper) (setq dumper 'dump))
  (if (not indents) (setq indents 0))
  (cond ((atom tree) 
	 (dump (str* " " indents))
	 (funcall dumper tree)
	 (dump "\n"))
	((listp tree)
	 (dump (str* " " indents))
	 (funcall dumper (car tree))
	 (dump "{\n")
	 (dolist (i (cdr tree))
	   (print-tree i (+ indents 2) dumper))
	 (dump (str* " " indents) "}\n"))))

(defun listify (start end)
  (interactive "r")
  (if mark-active
      (replace-regexp "^\\(.*\\)$" "<li>\\1</li>" nil start end)))

(defun open-this (&optional evt newframe)
  (interactive "e")
  (save-excursion
    (set-buffer (get-buffer "*Project Files*"))
    (goto-char (posn-point (event-end evt)))
    (let: f (thing-at-point 'filename) in
      (when newframe
	(select-frame (make-frame)))
      (find-file f))))

(defun run-find (arg &rest cmd-args)
  (with-temp-buffer
    (if cmd-args
	(apply 'call-process-shell-command (car cmd-args) nil t nil (cdr cmd-args))
      (call-process "find" nil t nil "." "-name" arg))
    (goto-char (point-min))
    (foolet (out (split-string (buffer-substring (point-min) (point-max)) "\n" t))
      (if (cdr out)
	  out
	(car out)))))

(defvar open-include-func nil)

(defun open-include (&optional action)
  (interactive)
  (if (not action) (setq action 'find-file))
  (with-default-dir project-directory
    (if open-include-func
	(funcall open-include-func)
      (catch 'break
	(foolet (name (thing-at-point 'filename))
	  (if (lc x x (directory-files project-directory) (endswith x ".ewp")) ;Check to see if there's an IAR project.
	      (dolist (i (process-lines "includes"))	     ;If there is, check the include paths.
		(if (endswith i "/") (setq i (substring i 0 -1)))
		(when (file-exists-p (concat i "/" name))	     ;If you find the include file there...
		  (funcall action (concat i "/" name))	     ;do something with it.
		  (throw 'break nil)))			     ;Otherwise, run the UNIX find command in
	    (if (setq name (funcall (funcomp run-find lastcar path->list) name)) ;the project directory.
		(find-file-other-window name)
	      nil)))))))

(defun enum-line (nlines offset step &optional fun)
  (if (not fun) (setq fun 'dump))
  (let ((n 0))
    (while (< n nlines) 
      (funcall fun (+ (* step n) offset)) (setq n (+ n 1)) (forward-line))))

(defun get-fields ()
  (interactive)
  (let ((out '())) 
    (while (not (startswith (thing-at-point 'line) "}"))
      (re-search-forward "[a-zA-Z_].+;") 
      (push (match-string 0) out))
    (cdr out)))

(defun print-fields (varname)
  (mapc 'dump-field (get-fields))
  nil)

(defun dump-field (x)
  (let (vartype asize fieldname)
    (if (string-match "float" x)
	(setq vartype "%f")
      (setq vartype "%d"))
    (string-match "\\([a-zA-Z_]+\\)[[;]" x)
    (setq fieldname (match-string 1 x))
    (when (vmember ?\[ x)
      (string-match "\\[\\([^\\]+\\)\\]" x)
      (setq asize (match-string 1 x)))
    (if asize
	(prf "  printf(\"%s: \");
  for (i=0; i<%s; i++) printf(\"%s \", %s.%s[i]);\n  printf(\"\\n\");\n" 
		fieldname asize vartype varname fieldname)
      (prf "  printf(\"%s: %s\\n\", %s.%s);\n" 
	   fieldname vartype varname fieldname))))

(defun swap-bufs (&optional prev)
  (interactive)
  (let ((swappy (next-window))
	(curbuf (current-buffer)))
    (if prev (setq swappy (prev-window)))
    (set-window-buffer (selected-window) (window-buffer swappy))
    (set-window-buffer swappy curbuf)))

(defun propagate-buf ()
  (interactive)
  (set-window-buffer 
   (if current-prefix-arg 
       (previous-window)
     (next-window))
   (current-buffer)))

(defun bracify (l)
  "Turns an s-expression into curly-braced compound literals
suitable for use in C initializers."
  (format "{%s}" 
	  (lcat (lc (if (listp x) (bracify x) (format "%s" x)) 
		    x (join l ", ")))))

(defun lcat (l) (apply 'concat l))

(put 'upcase-region 'disabled nil)
(put 'erase-buffer 'disabled nil)

(defun get-region ()
  (buffer-substring (region-beginning) (region-end)))

(defun list-defuns-refresh ()
  (interactive)
  (set-buffer defun-list-buf)
  (list-defuns))

(defun jason-defkeys (km &rest defines)
  (dolist (i (sublist2 defines))
    (apply 'define-key km i)))

(defmacro onkey (key &rest body)
  "Locally set key to run body."
  `(local-set-key ,key
     (lambda () (interactive) ,@body)))

(put 'onkey 'lisp-indent-function 1)

(defmacro keycmd (&rest body)
  `(lambda () (interactive)
     ,@body))

(defun list-defuns ()
  "List all the defuns in the current emacs lisp buffer"
  (interactive)
  (foolet (defuns (toplevel-items '(defun defsubst defrpn))
	   outbuf (get-buffer-create "*Defun Listing*")
	   maxlen (apply 'max (lc (length (symbol-name (car x))) x defuns))
	   kmap (make-sparse-keymap))
    (jason-defkeys kmap 
		   (kbd "RET") 'list-defuns-go
		   (kbd "<mouse-2>") 'list-defuns-go
		   (kbd "r") 'list-defuns-refresh
		   (kbd "q") 'bury-buffer)
    (save-excursion
      (setq defun-list-buf (current-buffer))
      (set-buffer outbuf)
      (setq buffer-read-only nil)
      (erase-buffer)
      (dolist (i defuns)
	(dump (car i) 
	      (str* " " (- maxlen -1 (length (symbol-name (car i))))) 
	      (cdr i))
	(add-text-properties (line-beginning-position)
			     (line-end-position)
			     '(mouse-face highlight
			       follow-link foobite))
	(dump "\n"))
      (add-text-properties (point-min) (point-max)
			   `(keymap ,kmap)))
    (pop-to-buffer outbuf)))

(defun list-defuns-lineno () 
  (save-excursion 
    (set-buffer (get-buffer "*Defun Listing*"))
    (beginning-of-line)
    (re-search-forward "[0-9]+$") 
    (string-to-number (match-string 0))))

(defun list-defuns-go ()
  (interactive)
  (let ((line (list-defuns-lineno)))
    (pop-to-buffer defun-list-buf)
    (goto-line line)))

(defmacro vinc (v i &optional amt)
  (unless amt (setq amt 1))
  `(aset ,v ,i (+ (aref ,v ,i) ,amt)))

(defmacro time-ops (&rest forms)
  (cons 'progn 
	(remap-symbols forms
		       '((+ . time-add)
			 (- . time-subtract)
			 (< . time-less-p)))))

(defmacro dolines (spec &rest body) 
  "Iterates over every line of a buffer.  spec is either a variable
name, or a list of the form (var buffer-exp)." 
  (let (var buf) 
    (if (listp spec) 
	(setq var (car spec) buf (cadr spec)) 
      (setq var spec buf (quote (current-buffer)))) 
    `(save-excursion 
       (set-buffer ,buf) 
       (goto-char (point-min)) 
       (while (not (eq (line-end-position) (point-max))) 
	 (let ((,var (substring (thing-at-point 'line) 0 -1))) 
	   ,@body 
	   (forward-line))))))

(defmacro errnil (&rest forms)
  `(condition-case nil
       (progn ,@forms)
     (error nil)))

(defun kill-lines (pred)
  "Kill lines for which (pred line) returns non-nil."
  (save-excursion
    (goto-char (point-min))
    (while (not (eq (line-end-position) (point-max)))
      (let ((line (thing-at-point 'line)))
	(if (funcall pred line)
	    (delete-region (line-beginning-position) (+ (line-end-position) 1))
	  (forward-line))))))

(defun maplines (proc &rest buffers)
  "Calls proc with each line of the
specified buffers.  The lines are
taken from each buffer in the order
given"
  (if (null buffers)
      (setq buffers (list (current-buffer))))
  (dolist (buf buffers)
    (dolines (line buf)
      (funcall proc line))))

(dolist (i '((foolet . 1)
	     (local-set-key . 1)
	     (set-process-sentinel . 1)
	     (dostr . 2)
	     (for . 1)
	     (with-default-dir . 1)
	     (dolines . 1)
	     (defstruct . 1)
	     (combinatory-for . 2)))
  (put (car i) 'lisp-indent-function (cdr i)))

(defun search-words (&rest words)
  (interactive (split-string (read-from-minibuffer "Enter words (white space seperators): ")))
  (occur (apply 'strjoin "\\|" (mapcar 'regexp-quote words))))

(defun sedwad (start end sedarg)
  (interactive "r\nsSed arg: \n")
  (call-process-region start end "sed"
		       t t nil sedarg))

(defun read-replace (proc)
  (foolet (start-point (progn (backward-sexp) (point))
	   code (read/die)
	   end-point (point))
    (goto-char start-point)
    (delete-region start-point end-point)
    (dump (funcall proc code))))

(defun eval-replace (&optional repr)
  (interactive)
  (with-syntax-table lisp-interaction-mode-syntax-table
    (let: forward-sexp-function nil
          start-point (progn (backward-sexp) (point))
          code (read/die)
          end-point (point) in
      (goto-char start-point)
      (delete-region start-point end-point)
      (dump (if repr
                (format "%S" (eval code))
              (eval code))))))

(defun bufmatches (s)
  (lc (buffer-name x)
      x (buffer-list)
      (and (string-match (regexp-quote s) (buffer-name x))
	   (not (startswith (buffer-name x) " ")))))

(defun fooplete (s pred flag)
  (cond
   ((not flag) 
    (if (= (length (bufmatches s)) 1) 
	(car (bufmatches s)) 
      s))
   ((eq flag t)
    (bufmatches s))
   ((eq flag 'lambda)
    nil)))

(defun map-region (fun &optional start end)
  (let: contents (if start (buffer-substring start end) (get-region))
	out-junk (lc x x (mapcar fun (split-string contents "\n")) x) in
    (delete-region (or start (region-beginning))
		   (or end (region-end)))
    (proper-dump (strjoin "\n" out-junk))
    (dump "\n")))

;    (run-at-time 0.01 nil (lambda () (dump "\n")))))

(defun do-region (fun)
  (let: start (region-beginning)
	end (region-end)
	in
    (goto-char start)
    (while (< (point) end)
      (funcall fun (thing-at-point 'line))
      (forward-line))))

(defun all-windows ()
  (apply 
   'append
   (lc (with-selected-frame x (window-list))
       x (frame-list))))

(defmacro listwad (&rest functions)
  (multifunc 'list functions))

(defmacro andwad (&rest functions)
  (multifunc 'and functions))

(defmacro notwad (func)
  (multifunc 'not (list func)))

(defmacro with-histogram (&rest body)
  "Executes BODY with `count' fbound to increment its argument's entry in hashtable `tab'.  After
body executes, returns the hash table."
  `(let: tab (make-hash-table :test 'equal) in
     (flet ((count (x) (incf (gethash x tab 0))))
       ,@body)
     tab))

(defalias 'w/buf 'with-current-buffer "Alias for with-current-buffer")

(defun fstat (f sym)
  (apply
   (lambda (dir-p nlinks uid gid atime mtime stime size modes gid-change inode dev)
     (if (listp sym)
	 (mapcar 'symbol-value sym)
       (symbol-value sym)))
   (file-attributes f)))

(defun shell-lines (cmd)
  (process-lines "bash" "-c" cmd))

(defun varref-occur (var)
  (interactive (list (thing-at-point 'symbol)))
  (occur (str "[^a-zA-Z]" var "[^a-zA-Z]")))

;; Keymap stuff.
(global-unset-key (kbd "<mode-line> <mouse-2>")) ;Goddamn this fucking thing is annoying.
(global-unset-key (kbd "s-m"))                   ;This one too.
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "s-f") 'find-tag)
(global-set-key (kbd "M-r") (lambda () (interactive) (pr "Reverting...") (revert-buffer nil t)))
(global-set-key [?\M-.] 'jason-next-window)
(global-set-key [?\M-,] 'jason-prev-window)
(global-set-key (kbd "M-o") 'jason-next-window)
(global-set-key (kbd "M-O") 'jason-prev-window)
(global-set-key (kbd "s-SPC") 'jason-set-recent-window)
(global-set-key (kbd "<f5>") 'compile)
(global-set-key (kbd "<f2>") 'grep)
(global-unset-key [mouse-2])
(global-set-key (kbd "M-e") 'eval-replace)
(global-set-key (kbd "M-E") (lambda () (interactive) (eval-replace t)))
(global-set-key (kbd "C-x j [") 'window-h-percent)
(global-set-key (kbd "C-x j ]") 'window-v-percent)
(when (display-graphic-p)
  (global-set-key (kbd "M-[") 'window-h-percent)
  (global-set-key (kbd "M-]") 'window-v-percent))

(global-set-key (kbd "C-M-v") 'clipboard-kill-ring-save)
(global-set-key (kbd "C-x DEL") 'backward-kill-sexp)
(global-set-key (kbd "C-M-y") 'x-clipboard-yank)
(global-set-key (kbd "M-B") 'swap-bufs)
(global-set-key (kbd "M-P") 'propagate-buf)
(global-set-key (kbd "C-x j f") (lambda () (interactive) (dump (expand-file-name (read-file-name "Filename: ")))))
(global-set-key (kbd "C-x j F") (lambda () (interactive) (kill-new buffer-file-name)))
(global-set-key (kbd "C-x M-o") (lambda () (interactive) (find-file (thing-at-point 'filename))))
(global-set-key (kbd "M-L") (keycmd (insert ?λ)))
(global-set-key (kbd "M-I") (lambda () (interactive) (dump "($ )") (backward-char 1)))
(global-set-key "\C-z" 'toggle-truncate-lines)
(global-set-key (kbd "C-x j I") 'setup-bitlbee)

(global-set-key (kbd "C-x j i") 
		(keycmd 
		 (erc :server "irc.freenode.net" :nick "turbofail" :password (read-passwd "Password: "))
                 ;; (if (eq system-type 'darwin)
                 ;;     (erc :server "irc" :nick "jason"))
                 ))

(global-set-key (kbd "C-x j s") (keycmd (run-scheme "chibi-scheme")))
(global-set-key (kbd "C-x j 4") (keycmd (rearrange-windows '(h v a c v b d))))
(global-set-key (kbd "C-x j j") (keycmd (call-process "qjackctl" nil 0 nil)))
(global-set-key (kbd "C-x j a") 'alter-text)
(global-set-key (kbd "C-x j M-p") 'switch-project)

(global-set-key (kbd "<S-backspace>") 'jf-hungry-del-back)
(global-set-key 
 (kbd "C-x j e")
 (lambda ()
   (interactive)
   (require 'jf-imap)
   (email-wad)))

(defalias 'λ 'lambda*)

(define-key ctl-x-map (kbd "M-d") (lambda () (interactive) (dump default-directory)))
(define-key ctl-x-map (kbd "L") 
  (lambda (s)
    (interactive (list (if (region-active-p) 
                           (get-region)
                         (read-string "Regex:"))))
    (if (equal s "")
	(setq s (regexp-quote (thing-at-point 'symbol))))
    (occur s (when current-prefix-arg (prefix-numeric-value current-prefix-arg)))))

(global-set-key (kbd "C-x M-l") 'varref-occur)
(define-key ctl-x-map (kbd "p") 'projfiles)
(define-key ctl-x-map (kbd "P") (lambda () (interactive) (dump project-directory)))
(define-key ctl-x-map (kbd "c") 'display-commitwad)
(define-key ctl-x-map (kbd "S") 'sedwad)
(define-key ctl-x-map (kbd "R") 'rearrange-windows)
(define-key ctl-x-map (kbd "M-p") 'set-project-directory)
(global-set-key (kbd "C-x j r") 'add-reminder)
(global-set-key (kbd "C-x j DEL") 'jf-hungry-del-back)
(global-set-key (kbd "C-x C-j C-SPC") 'mark-sexp)

(define-key emacs-lisp-mode-map (kbd "<f5>") (keycmd (byte-compile-file buffer-file-name)))

(defun read-from-buffer (cb &optional name)
  (switch-to-buffer (generate-new-buffer (or name "*input*")))
  (local-set-key (kbd "C-c C-c")
    (keycmd (funcall cb (buffer-string))
            (kill-buffer))))

(defun add-reminder (&optional txt)
  (interactive)
  (if (not txt)
      (read-from-buffer 'post-reminder)
    (post-reminder txt)))

(defun post-reminder (txt)
  (find-file "/ssh:ozbert.com:/home/jfeng/public_html/remind.txt")
  (goto-char (point-min))
  (insert txt)
  (let: ending-newlines (str~ "\n*$" txt) in
    (insert (str* "\n" (- 2 (length ending-newlines)))))
  (save-buffer)
  (kill-buffer))

(defvar recent-projects nil)

(defun set-project-directory ()
  (interactive)
  (setf recent-projects (union (list default-directory) recent-projects :test 'equal))
  (load "project-stuff")
  (if (file-exists-p "proj.el")
      (load-file "proj.el")))

(defun switch-project ()
  "Switch to a previous project."
  (interactive)
  (with-default-dir (ido-completing-read "Project dir: " (copy-seq recent-projects)
                                         nil nil nil nil project-directory)
    (set-project-directory)))

(defun gen-password ()
  (car (process-lines "bash" "-c" "head -c 12 /dev/urandom | base64")))

(defun compile-all ()
  (interactive)
  (mapc (funcomp byte-compile car)
	(toplevel-items '(defun))))

(defun map-regex (proc rx)
  "Runs PROC in order to replace each match for RX in buffer.
PROC should have access to the relevant match data when it is executed."
  (while (re-search-forward rx nil t)
    (let: rep (funcall proc (match-string 0)) in
      (if rep (replace-match rep nil t)))))

(defun each-regex (proc rx)
  "Call PROC for each match against RX in the current buffer."
  (while (re-search-forward rx nil t)
    (funcall proc (match-string 0))))

(defun mark-thing-at-point (thing)
  (interactive (list (read)))
  (let: b (bounds-of-thing-at-point thing) in
    (push-mark)
    (push-mark (cdr b) nil t)
    (goto-char (car b))))

(defun kill-thing-at-point (thing)
  (interactive (list (read)))
  (let: b (bounds-of-thing-at-point thing) in
    (kill-region (car b) (cdr b))))

(defun addpath (dir)
  "Adds a directory to emacs's PATH variable."
  (interactive "DPath: ")
  (setq dir (expand-file-name dir))
  (setenv "PATH"
	  (concat dir ":" (getenv "PATH"))))

(if (eq system-type 'darwin)
    (define-key local-function-key-map (kbd "<S-tab>") (kbd "<backtab>"))
  (define-key local-function-key-map (kbd "<s-tab>") (kbd "M-TAB")))

(define-key ctl-x-map (kbd "O") (lambda () (interactive) (find-file "/ssh:ozbert.com:/home/jfeng/")))

(jason-defkeys emacs-lisp-mode-map
	       (kbd "C-c f") 'find-function
	       (kbd "C-c l") 'list-defuns
	       (kbd "C-c M-l") 'insert-lambda
	       (kbd "C-c e") 'eval-buffer
	       (kbd "C-c DEL") 'jf-hungry-del-back
	       (kbd "C-c C-c") (keycmd (byte-compile-file buffer-file-name)))

(jason-defkeys lisp-interaction-mode-map
	       (kbd "C-c f") 'find-function
	       (kbd "C-c l") 'list-defuns
	       (kbd "C-c M-l") 'insert-lambda
	       (kbd "C-c e") 'eval-buffer
	       (kbd "C-c DEL") 'jf-hungry-del-back
	       (kbd "C-c C-c") (keycmd (byte-compile-file buffer-file-name)))

(defun insert-lambda ()
  (interactive)
  (dump "(λ (x) )")
  (forward-char -1))

(defun dump-scratch-buffer ()
  "Save the current scratch buffer, do a git commit."
  (interactive)
  (when (get-buffer "*scratch*")
    (with-current-buffer "*scratch*"
      (with-default-dir "~/.emacs.d/"
        (write-region nil nil 
                      "~/.emacs.d/scratch.el")
        (call-process "git" nil 0 nil "commit" "-a" "-m"
		    (format-time-string "Scratch buffer at %Y-%m-%d %H:%M:%S"))))))

(defun filestr (f)
  (with-temp-buffer
    (insert-file-contents-literally f)
    (string-make-unibyte (buffer-string))))

(defun multi-occur-all (regexp)
  (interactive (list (read-regexp "Regexp: ")))
  (multi-occur (lc x x (buffer-list) (not (startswith (buffer-name x) " ")))
	       regexp))

(defvar alter-text-history nil)

(defun alter-text ()
  (interactive)
  (let: start nil end nil 
        default-input (if alter-text-history
                          (car alter-text-history) "x") in
    (if (region-active-p)
        (setq start (region-beginning)
              end (region-end))
      (setq start (point-min) end (point-max)))
    (map-region
     `(lambda (x) 
        (setq x (substring-no-properties x))
        ,(maptree
          (lambda (expr)
            (iflet ix (and (symbolp expr)
                           (str~ "\\$\\([0-9]+\\)" (symbol-name expr) 1))
                   `(or (nth ,(read ix) (split-string x)) "")
                   expr))
          (read
           (read-string (format "Lisp expression - default %s: " 
                                default-input)
                        nil 'alter-text-history default-input))))
     start end)))

(defun pgrep (proc &optional interactive)
  (interactive (list (read-string "Process command line matching: ") t))
  (let: lines (mapcar 'split-string (shell-lines "ps -e")) in
    (if interactive
        (with-output-to-temp-buffer "*pgrep*"
          (ec (pr x "\n") x lines
              (string-match proc (cadddr x))))
      (lc x x lines
          (string-match proc (cadddr x))))))

(defun setup-parenscript ()
  (interactive)
  (require 'ps-interact)
  (require 'mozrepl)
  (with-default-dir "~/lisp/"
    (find-file "jflib.parenscript")
    (run-lisp "sbcl"))
  (process-send-string 
   (inferior-lisp-proc)
   (str (repr `(load ,(expand-file-name "~/lisp/ps-crap.lisp"))) "\n"))
  (process-send-string
   (inferior-lisp-proc)
   (str (repr '(in-package "foo")))))

(defun to-ruby-strings (start end)
  (interactive (list (region-beginning) (region-end)))
  (map-region (lambda (x)
                (do-wad (substring-no-properties x) repr (str _ ",")))
              start end))

(defun setup-ansi-colors ()
  (interactive)
  (require 'ansi-color)
  (aset ansi-color-names-vector 3 "#777700")
  (setq ansi-color-map (ansi-color-make-color-map)))

(setup-ansi-colors)

(defun multi-read (func)
  (let: input (funcall func)
        out-list nil in
    (while (> (length input) 0)
      (push input out-list)
      (setq input (funcall func)))
    out-list))

(defun urandoms (n)
  (do-wad
   (with-temp-buffer
     (let: coding-system-for-read 'binary in
       (call-process "head" nil t nil "-c" (str (* 6 n)) "/dev/urandom")
       (buffer-string)))
   string-to-unibyte
   (re-findall (rx (repeat 6 anything)) _ t)
   (lc (reduce (lambda (a b) 
                 (+ (* a 256) b))
               x :initial-value 0)
       x _)))

(defun init-dictionary ()
  (with-temp-buffer 
    (do-wad
     (insert-file-contents-literally
      "/usr/share/dict/words")
     (split-string (buffer-string) "\n" t)
     (apply 'vector _)
     (setf dictionary-words _))))

(defun final-gen-password ()
  (lc (aref dictionary-words (mod x (length dictionary-words)))
      x (urandoms 4)))

(defvar current-server-set nil)

(defun next-link ()
  (interactive)
  (while (progn
           (goto-char (next-single-property-change 
                       (point) 'keymap))
           (and (not (get-text-property (point) 'keymap))
                (< (point) (point-max))))
    t))

(defun prev-link ()
  (interactive)
  (while (progn (goto-char (previous-single-property-change
                            (point) 'keymap))
                (and (not (get-text-property (point) 'keymap))
                     (> (point) (point-min))))
    t))

(def-reader remote-server "Server: " nil)

(def-reader read-username "Username: " nil)

(def-reader read-remote-path "Remote path: " nil)

(defun multi-save (username servers local-path remote-path)
  (interactive
   (list (read-username) (multi-read 'remote-server)
         buffer-file-name (read-remote-path)))
  (dolist (s (or servers current-server-set))
    (jf-proc "scp" 'pr local-path (str username "@" s ":" remote-path))))

(defun dump-irc-buffers (&optional no-save)
  (interactive)
  (let: irc-buffers (lc x x (buffer-list) 
                        (and (eq (buffer-mode x) 'erc-mode)
                             (startswith (buffer-name x) "#")))
        timestamp (format-time-string "%s") in
    (dolist (buf irc-buffers)
      (w/buf buf
             (unless no-save 
               (write-region 
                nil nil 
                (interp 
                 "~/.emacs.d/#,(substring (buffer-name buf) 1)-#,[timestamp].log")))
             (erc-truncate-buffer-to-size 100000)))))

(defun get-last-sexp (&optional func)
  (do-wad
   (save-excursion 
     (funcall (or func 'backward-sexp)) 
     (point))
   (buffer-substring _ (point))))

(defun set-clojure-process ()
  (interactive)
  (setq clojure-process (get-buffer-process (current-buffer))))

(defun get-lein ()
  (interactive)
  (make-directory "~/bin" t)
  (shell-command "curl https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein > ~/bin/lein; chmod u+x ~/bin/lein &")
  (setenv "PATH" (concat (expand-file-name "~/bin") ":" (getenv "PATH"))))

(defun synchronize-auth-sock ()
  (interactive)
  (do-wad
   (current-buffer) get-buffer-process
   (process-send-string 
    _ (str "export SSH_AUTH_SOCK=" (getenv "SSH_AUTH_SOCK") "\n"))))

(defun max-column ()
  (interactive)
  (>>> (buffer-string) (split-string _ "\n" t) (max-by _ 'length)
       (interp "Max length: #,(length _)\n#,_") pr))

(defun edit-sshconf ()
  (interactive)
  (find-file "~/.ssh/config"))

(defun add-path (p)
  (interactive (if current-prefix-arg
                   (list (read-file-name "Dir: "))
                 (list (expand-file-name default-directory))))
  (updatef (getenv "PATH") (cut concat p ":" <>)))

(add-hook 'kill-emacs-hook 'dump-scratch-buffer)
(add-hook 'kill-emacs-hook 'dump-irc-buffers)

(dolist (i '(list-cfun-get-lineno list-cfuns-go list-cfuns-propertize list-cfuns))
  (let (inter)
    (if (memq i '(list-cfuns list-cfuns-go)) (setq inter t))
    (autoload i "c-junk" nil inter)))

(autoload 'clojure-mode "clojure-mode" "A clojure mode." t)
;; (add-to-list 'auto-mode-alist '("\\.cljs?$" . clojure-mode))
(autoload 'arc-mode "arc" "Arc editing mode." t)
(autoload 'svn-browse "jf-svn" "A shitty svn browser." t)
(autoload 'jf-shellproc "jfproc" "Utility shit for async processes.")
(add-to-list 'auto-mode-alist '("\\.arc$" . arc-mode))
(add-to-list 'auto-mode-alist '("\\.pr$" . piano-roll-mode))
(autoload 'piano-roll-mode "piano-roll" "Mode for editing and playing piano-roll-like things." t)
(autoload 'piano-roll "piano-roll" "Opens a blank piano roll." t)
(autoload 'jfgi "googly" nil t)
(autoload 'hnsearch "googly" nil t)
(autoload 'setup-bitlbee "bitlbee" nil t)

(autoload 'haml-mode "haml-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.haml$" . haml-mode))
(add-hook 'haml-mode-hook
	  (lambda ()
	    (let: new-stx-tab (make-syntax-table (syntax-table)) in
	      (modify-syntax-entry ?' "." new-stx-tab))))

(add-hook 'dired-mode-hook
	  (lambda ()
	    (require 'jf-dired)))
(add-hook 'scheme-mode-hook
	  (lambda ()
	    (define-key scheme-mode-map (kbd "C-c DEL") 'jf-hungry-del-back)))

(add-hook 'ruby-mode-hook
	  (lambda ()
	    (make-local-variable 'forward-sexp-function)
	    (setq forward-sexp-function 'ruby-forward-sexp)
            (local-set-key (kbd "C-c DEL") 'jf-hungry-del-back)))

(add-hook 'js-mode-hook
	  (lambda ()
	    "Kill that stupid fucking keybinding for M-."
	    (local-unset-key (kbd "M-."))))

(add-hook 'ibuffer-mode-hook
          (lambda ()
            (load "ibuf-ext")
            (load "ibuffer-mods")
            (local-set-key (kbd "/ v") 'ibuffer-filter-by-not-visible)))

(add-hook 'occur-mode-hook
          (lambda ()
            (jason-defkeys occur-mode-map 
                           (kbd "n") 'next-line
                           (kbd "p") 'previous-line)))

(add-hook 'paredit-mode-hook
          (lambda ()
            (jason-defkeys
             paredit-mode-map
             (kbd "C-j") nil
             (kbd "{") 'paredit-open-curly
             (kbd "}") 'paredit-close-curly
             (kbd "C-x j (") 'paredit-backward-slurp-sexp
             (kbd "C-x j )") 'paredit-forward-slurp-sexp
             (kbd "C-x j {") 'paredit-backward-barf-sexp
             (kbd "C-x j }") 'paredit-forward-barf-sexp)))

(add-hook 'lisp-interaction-mode-hook
          (lambda ()
            (paredit-mode 1)))

(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (paredit-mode 1)))

(add-hook 
 'clojure-mode-hook
 (once-fun
  (lambda ()
    (define-key clojure-mode-map
      (kbd "M-RET")
      (lambda ()
        (interactive)
        (process-send-string 
         clojure-process
         (str (get-last-sexp) "\n")))))))

(add-hook
 'css-mode-hook
 (lambda ()
   (setq css-indent-offset 2)))

(defalias '% (symbol-function 'mod))

(setq dired-dwim-target t)
(menu-bar-mode -1)
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode 0))

(mapc 'require '(indent-wads shell-mods
		 doc-mods git-wads 
		 window-buffer-junk nav flashy))

(mapc 'load '("project-stuff" "term-mods"))
(setq inferior-lisp-program "clj" scheme-program-name "ypsilon")

(setq grep-run nil)
(setq grep-use-null-device nil)

(require 'server)
;; (if (not (server-running-p)) (server-start))
(set-project-directory)

(iswitchb-mode 1)

(if (and (eq system-type 'gnu/linux) 
         (condition-case foo
             (equal (list (x-display-pixel-width) (x-display-pixel-height))
                    '(1440 900))
           (error nil)))
    (set-face-attribute 'default nil :height 75))

(put 'narrow-to-region 'disabled nil)

(setq-default truncate-lines t)

(setq irbsh-startup-hook
      (list (lambda ()
	      (local-set-key (kbd "RET") 'comint-send-input))))

(if (eq system-type 'darwin) (set-frame-parameter nil 'cursor-color "#000000"))

(setq ring-bell-function (lambda () ))

(setq erc-autojoin-channels-alist
      `(,@(if (eq system-type 'darwin) 
              '(("irc" "#eng"))) 
        ("freenode.net" "#emacs" "#scheme" "#lispgames" "#guile")))


(setq-default indent-tabs-mode nil)

(setq js-indent-level 2)

(add-to-list 'auto-mode-alist '("\\.sass$" . sass-mode))
(autoload 'sass-mode "sass-mode" nil t)

;; Reload latest scratch buffer.
(w/buf "*scratch*"
       (when (file-exists-p "~/.emacs.d/scratch.el")
         (let: coding-system-for-read 'utf-8-emacs in
           (insert-file-contents "~/.emacs.d/scratch.el" nil nil nil t)))
       (wait 0.5 
             (w/buf "*scratch*"
               (local-set-key (kbd "C-x C-s") 'dump-scratch-buffer))))

(add-to-list 'auto-mode-alist '("\\.parenscript$" . lisp-mode))
(add-to-list 'auto-mode-alist '("\\.sld$" . scheme-mode))
(push '("\\.stub$" . scheme-mode) auto-mode-alist)
(push '("chibi-ffi$" . scheme-mode) auto-mode-alist)
(add-to-list 'auto-mode-alist '("\\.ly$" . LilyPond-mode))

(add-to-list 
 'term-mode-hook
 (lambda () 
   (jason-defkeys 
    term-raw-map 
    (kbd "M-.") nil 
    (kbd "M-,") nil
    (kbd "M-[") nil
    (kbd "M-]") nil)))

(setf use-dialog-box nil
      use-file-dialog nil
      erc-server-auto-reconnect nil)
(put 'scroll-left 'disabled nil)

(run-at-time 1 nil (lambda () (setq-default lexical-binding t)))

(global-set-key
 (kbd "C-x C-c")
 (keycmd (setf saved-windows (get-windows))
         (save-buffers-kill-terminal)))

(global-set-key 
 (kbd "C-x j r")
 (lambda () (interactive)
   (apply-windows saved-windows)))

(global-set-key
 (kbd "C-x j p") 'paredit-mode)

(define-key emacs-lisp-mode-map
  (kbd "C-c C-l") 
  (keycmd 
   (save-excursion 
     (goto-char (point-min))
     (insert ";; -*- lexical-binding: t; -*-\n"))))

(global-set-key (kbd "C-x j M-w") (keycmd (remote-kill (car kill-ring))))
(global-unset-key (kbd "s-q"))
(global-unset-key (kbd "s-w"))

(require 'paredit)
(require 'passwords)

(setf backup-directory-alist '((".*" . "~/.emacs.d/")))

(require 'uniquify)
(setf uniquify-buffer-name-style 'forward)
(setf async-shell-command-buffer 'new-buffer)

(require 'extras)
(init-extras)

(set-face-attribute 'mode-line nil :background "#ddffdd")
(set-face-attribute 'mode-line-inactive nil :background "#aaaaaa")

(defun setup-fooserve ()
  (interactive)
  (require 'fooserve-2)
  (fooserve-2-start 12345))

(defun setup-webext-shit ()
  (interactive)
  (require 'webext-shit)
  (setup-fooserve)

  (setf webext-conn (connect "127.0.0.1" 12347))

  (firefox-execute
   '(browser.contentScripts.register
     (:: matches ["*://www.reddit.com/*"]
         runAt "document_start"
         js [(:: code "window.location=window.location.protocol+\"//old.reddit.com/\"+window.location.pathname")])))

  (local-set-key (kbd "M-RET")
     (keycmd 
      (>>> (preceding-sexp) to-js (interp "#,_;\n")
           json-encode-string
           repr
           (process-send-string webext-conn _)))))

(defun setup-play ()
  (interactive)
  (require 'play)
  (with-default-dir "~/mp3s/"
    (shell-command "ls" (play))))

(setenv "EDITOR" "emacsclient")
