;; Helper function for vector macros
(defun do-for-vec (op &rest vecnames)
  (if (not (listp op)) (setq op (list op)))
  (lc (append op (lc `(aref ,x ,i) x vecnames))
      i (range 3)))

(defmacro vecmap (op &rest vecnames) 
  (cons 'vector (apply 'do-for-vec op vecnames)))

(defmacro vecaccum (accum op &rest vecnames)
  (cons accum (apply 'do-for-vec op vecnames)))

(defrpn vec+ (v1 v2) (vecmap + v1 v2))
(defrpn vec- (v1 v2) (vecmap - v1 v2))
(defrpn vec* (v1 v2) (vecaccum + * v1 v2))
(defrpn vec-scale (v n) (vecmap (* n) v))
(defrpn vec-2 (v) (vec* v v))
(defsubst vec-mag (v) (sqrt (vec-2 v)))

(defsubst origin (obj) (aref obj 0))
(defsubst dir (ray) (aref ray 1))
(defsubst radius (sph) (aref sph 1))

(defun ray-isect (ray sphere)
  (foolet (x (vec- (origin sphere) (origin ray))
	   vx (vec* (dir ray) x)
	   r2 (* (radius sphere) (radius sphere))
	   v2 (vec* (dir ray) (dir ray))
	   x2 (vec* x x)
	   discr (rpn vx vx * v2 x2 r2 - * -))
	  (if (<= discr 0) nil
	    (min (rpn vx discr sqrt + v2 /)
		 (rpn vx discr sqrt - v2 /)))))

(defun ray-grid (width height)
  "Iterate through all the rays in a width x height
pixel grid.  The viewing plane is 1.0 units wide,
at a distance 1.0 units in the z-direction from the
origin.  The eye is at the origin."
  (foolet (ysize (rpn height float width /)
	   dx (/ 1.0 width)
	   dy (/ ysize height)
	   xinit (rpn -1.0 dx + 2 /) yinit (rpn ysize dy - 2 /)
	   out (make-string (* width height) 0)
	   ix 0)
    (for ((0 j height) (0 i width))	 
	 (aset out ix (ray-scene (vector (rpn i dx * xinit +)
					 (rpn yinit j dy * -)
					 1.0)))
	 (setq ix (+ ix 1)))
    out))

(defun find-hit (ray) ; This function is retarded.
  (let (minhit hit hitsphere)
    (dolist (i ray-scene-spheres)
      (setq hit (ray-isect ray i))
      (if (and hit (< hit 0)) (setq hit nil))
      (when hit
	(when (or (not minhit) (<= hit minhit)) 
	  (setq minhit hit)
	  (setq hitsphere i))))
    (if (not hitsphere)
	nil
      (vector (vec+ (origin ray)
		    (vec-scale (dir ray) minhit))
	      hitsphere))))

;(defstruct hit point sphere)
(defsubst hit-point (hit) (aref hit 0))
(defsubst hit-sphere (hit) (aref hit 1))

(defsubst lambert (n l)
  (rpn (vec* n l) (vec-mag n) (vec-mag l) * / 59.0 * 4 + round))

(defun ray-scene (ray-dir)
  (catch 'return
    (let (ray hit light normal)
      (setq ray (vector [0 0 0] ray-dir))
      (setq hit (find-hit ray))
      (if (not hit) (throw 'return 21))
      (setq normal (vec- (origin (hit-sphere hit)) (hit-point hit)))
      (setq light (vec- (hit-point hit) ray-scene-light))
      (if (<= (vec* normal light) 0)
	  (throw 'return 4))
      (setq ray (vector (hit-point hit) 
			(vec-scale light -1)))
      (if (find-hit ray)
	  (rpn 4 (lambert normal light) 3 / +)
	(lambert normal light)))))

(defun compile-ray-funcs ()
  (mapc 'byte-compile '(vec+ vec- vec* vec-scale vec-2 vec-mag ray-isect ray-grid find-hit ray-scene lambert)))

(compile-ray-funcs)
