;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

;; SUM := SUM + TERM | SUM - TERM | TERM
;; TERM := ID | -TERM

(setq simple-statewad
(make-statewad
'((start
   id (push have-id)
   - (push have-neg)
   term have-term
   sum  have-sum)
  (have-id
   (+ - $) (reduce 1 term))
  (have-term
   (+ - $) (reduce 1 sum))
  (have-sum
   (+ -) (push have-sum+))
  (have-sum+
   id (push have-id)
   - (push have-neg)
   term have-sum+term)
  (have-sum+term
   otherwise (reduce 3 sum))
  (have-neg
   id (push have-id)
   - (push have-neg)
   term have-neg-term)
  (have-neg-term
   otherwise (reduce 2 term)))))

(defun make-state (state)
  (cons 
   (car state)
   (alist->dict
    (apply 'append
	   (lc (if (consp (car x))
		   (lc (cons y (cadr x))
		       y (car x))
		 (list (cons (car x) (cadr x))))
	       x (sublist2 (cdr state)))))))

(defun make-statewad (states)
  (alist->dict (mapcar 'make-state states)))

(defun run-statewad (states stack input)
  (let: curstate (gethash (car stack) states)
	action (or (gethash input curstate)
		   (gethash 'otherwise curstate)) in
    (case (car action)
      (reduce 
       (pr "Reducing to " (caddr action) "! " stack "\n")
       (dotimes (i (cadr action))
	 (pop stack))
       (push (gethash
	      (caddr action)
	      (gethash (car stack)
		       states))
	     stack)
       (setq stack (run-statewad states stack input)))
      (push (push (cadr action) stack)))
    stack))

;; SUM := SUM + PRODUCT | PRODUCT
;; PRODUCT := PRODUCT * FACTOR | PRODUCT / FACTOR | PRODUCT FACTOR | FACTOR
;; FACTOR := TERM ^ FACTOR | - FACTOR | TERM
;; TERM := IDENTIFIER | IDENTIFIER ( SUM ) | ( SUM ) | NUM

'((start
   id (push have-id)
   lparen (push have-lparen)
   - (push have-neg)
   ;; goto states
   term (goto have-term))
  (have-id
   lparen (push have-id-lparen)
   (+ - / * ^ id) (reduce 1 term))
  (have-term
   (+ - / * id) (reduce 1 factor)
   ^ (push have-term^))
  (have-term^
   id (push have-id)
   lparen (push have-lparen)
   - (push have-neg)
   ;; goto states
   factor (goto have-term^factor))
  (have-term^factor
   otherwise (reduce 3 factor))
  (have-neg
   id (push have-id)
   lparen (push have-lparen)
   - (push have-neg)
   ;; goto states
   factor (smegma)))



  