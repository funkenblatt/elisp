;; -*- lexical-binding: t; -*-
(require 'eljs)
(require 'http-client)

(defun firefox-execute (code)
  (>>> code
       to-js
       json-encode-string substring-no-properties
       repr
       (process-send-string webext-conn _)))

(defun firefox-tab-execute (code)
  (firefox-execute
   `(.. (browser.tabs.executeScript
         (:: code ,(to-js code))))))

(defun eval-slack (cmd channel)
  (when channel
    (firefox-tab-execute
     `(..
       (document.querySelectorAll ".p-channel_sidebar__name")
       (.forEach (λ (x) (if (== x.innerText ,channel)
                            (x.parentNode.click)))))))

  (run-at-time 0.1 nil
               (lambda ()
                 (firefox-tab-execute
                  `(progn
                     (.. (document.querySelector ".ql-editor p")
                         .innerHTML
                         (setf ,(str "/" cmd " "
                                     (if current-prefix-arg
                                         (>>> (cider-current-ns)
                                              (interp "(binding [*ns* (find-ns '#,_)] (eval '#,(cider-last-sexp)))"))
                                       (cider-last-sexp)))))

                     (.. (document.querySelector "#msg_input .ql-editor")
                         (.focus))))


                 (run-at-time 0.1 nil
                              (lambda ()
                                (firefox-tab-execute
                                 '(.. (document.querySelector ".ql-editor")
                                      (.dispatchEvent
                                       (new (KeyboardEvent "keydown"
                                                           (:: which 13
                                                               keyCode 13)))))))))))

(defun eval-current ()
  (interactive)
  (eval-slack "norby-prod" nil))

(defun eval-norby ()
  (interactive)
  (eval-slack "norby-prod" "Slackbot"))

(defun eval-dev ()
  (interactive)
  (eval-slack "jason-dev" "dev-testing"))

(defun bytes->len (b)
  (do ((i (- (length b) 1) (- i 1))
       (out 0 (+ (* 256 out) (aref b i))))
      ((< i 0) out)))

(defun receive-process-messages (process callback)
  (let: pipe (procpipe process) in
    (nlet lp ()
      (cps-let ((len (funcall pipe 4 cont!))
                (msg (funcall pipe (bytes->len len) cont!)))
        (funcall callback msg)
        (funcall lp)))))

'(kill-new
  (json-encode
   '((manifest_version . 2)
     (name . "Extwad")
     (version . "1.0")
     (description . "Do shit")
     (permissions . ["tabs" "nativeMessaging"
                     "*://*/*"])
     (background (scripts . ["extwad_background.js"]))
     (content_security_policy . "script-src 'self' 'unsafe-eval'; object-src 'self'")
     (applications
      (gecko
       (id . "extwad@jfeng.ozbert.com")
       (strict_min_version . "50.0"))))))


'(kill-new (json-encode
            '((name . "herpderp")
              (description . "Stupid native shit.")
              (path . "/home/jason/bin/herpderp")
              (type . "stdio")
              (allowed_extensions . ["extwad@jfeng.ozbert.com"]))))

'(progn
   (.. (browser.windows.getCurrent)
       (.then (λ (w) (console.log w))))

   (browser.tabs.update (:: url "https://www.google.com/"))

   (.. (browser.tabs.query (:: active true))
       (.then (λ (tabs)
                 (console.log (JSON.stringify (tabs.map (juxt (@ url) (@ id))))))))

   (.. (browser.tabs.query (:: currentWindow true active true))
       (.then (λ (tabs)
                 (console.log (JSON.stringify (tabs.map (juxt (@ url) (@ id))))))))

   (setf to-array (λ (array-like)
                     (let ((i 0) (out []))
                       (for (nil (< i array-like.length) (incf i))
                         (out.push (-> array-like [i])))
                       out)))

   (setf juxt (λ ()
                 (let ((args (to-array arguments)))
                   (λ (x) (args.map (λ (f) (f x)))))))

   (local-set-key (kbd "M-RET")
     (keycmd 
      (>>> (preceding-sexp) to-js (interp "#,_;\n")
           json-encode-string
           repr
           (process-send-string webext-conn _))))

   (browser.contentScripts.register
    (:: matches ["*://www.reddit.com/*"]
        runAt "document_start"
        js [(:: code "window.location=window.location.protocol+\"//old.reddit.com/\"+window.location.pathname")]))

   (setf browse-url-browser-function
         (lambda (url &rest junk)
           (firefox-execute `(browser.tabs.update (:: url ,url)))))

;;; ELJS for background script.
   (progn
     (defvar port (browser.runtime.connectNative "herpderp"))
     (port.onMessage.addListener
      (λ (response)
         (console.log (eval response)))))

   (setf p (connect "127.0.0.1" 12347))
   (receive-process-messages p (lambda (m) (let: result (json-read-from-string m) in
                                             (pp result)
                                             (setf webext-last-result result))))

   (setf webext-callbacks (dict))
   (setf webext-cb-id 1)

   (receive-process-messages
    p (x: (let ((msg (json-read-from-string (string-as-multibyte x))))
            (iflet cb (and (consp msg) (assoc-default 'callback msg))
              (iflet f (gethash cb webext-callbacks)
                (funcall f msg))
              (pp msg)))))

   (defun firefox-tab-eval (code callback)
     "Run code in the current tab of firefox and call callback with the result of it."
     (let: cb-id webext-cb-id in
       (incf webext-cb-id)
       (puthash cb-id
                (lambda (message)
                  (funcall callback (assoc-default 'result message))
                  (remhash cb-id webext-callbacks))
                webext-callbacks)

       (firefox-execute
        `(.. (browser.tabs.executeScript
              (:: code ,(to-js code)))
             (.then (λ (result) (port.postMessage (:: result (aref result 0)
                                                      callback ,cb-id))))))
       webext-callbacks))

   (defun firefox-eval (code callback)
     (let: cb-id webext-cb-id in
       (incf webext-cb-id)
       (puthash cb-id
                (lambda (msg)
                  (funcall callback (assoc-default 'result msg))
                  (remhash cb-id webext-callbacks))
                webext-callbacks)
       (firefox-execute
        `(let ((ret (lambda (x)
                      (port.postMessage (:: result x
                                            callback ,cb-id)))))
           ,code))))

   (delete-process p))

(provide 'webext-shit)
