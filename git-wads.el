;; -*- lexical-binding: t; -*-
(require 'jfproc)
(require 'vc-git)

(setq vc-log-view-type 'long)

(defun git-process (cmd cont &rest args)
  (let: buf (get-buffer-create (format "*git-%s*" cmd)) 
        dir (vc-git-root default-directory)
        process-connection-type nil in
    (when dir
      (with-current-buffer buf 
        (setq default-directory dir)
        (setq buffer-read-only nil)
        (erase-buffer))
      (apply
       'jf-proc-utf8
       "git"
       (lambda (s)
         (with-current-buffer buf
           (insert s)
           (funcall cont buf)))
       "--no-pager" cmd 
       (lc (if (stringp x) x (funcall x)) x args)))))

(defun git-switch-func (mode cmd &rest args)
  (lambda ()
    (interactive)
    (cps> (buf) (apply 'git-process cmd :cont! args)
      (unless (memq buf (mapcar 'window-buffer (window-list)))
        (switch-to-buffer buf))
      (goto-char (point-min))
      (funcall mode))))

(defun git-display-func (mode cmd &rest args)
  `(lambda ()
     (interactive)
     (cps> buf (git-process ,cmd :cont! ,@args)
       (display-buffer buf)
       (with-current-buffer buf
	 (goto-char (point-min))
	 (,mode)))))

(add-hook 
 'vc-git-log-view-mode-hook
 (lambda ()
   (use-local-map (copy-keymap (current-local-map)))
   (local-set-key (kbd "g") 'revert-buffer)
   (local-set-key (kbd "C-c g") (keycmd (git-grep (read-string "Pattern: ") (log-view-current-tag))))
   (local-set-key (kbd "RET")
     (keycmd (git-show (log-view-current-tag))))
   (local-set-key (kbd "M-RET")
     (git-switch-func 'fundamental-mode "cherry-pick" 'log-view-current-tag))))

(global-set-key
 (kbd "C-x g d")
 (lambda ()
   (interactive)
   (with-default-dir (vc-git-root default-directory)
     (if current-prefix-arg
         (funcall (git-switch-func 'diff-mode "diff" "--ignore-space-at-eol" (read-branch t)))
       (funcall (git-switch-func 'diff-mode "diff" "--ignore-space-at-eol" ))))))

(defun git-log (head &optional filename)
  (interactive (if current-prefix-arg
                   (list (read-branch t))
                 '("HEAD")))
  (let: mode major-mode in
    (funcall (apply
              'git-switch-func 
              (lambda ()
                (make-local-variable 'revert-buffer-function)
                (setq revert-buffer-function
                      (lambda (junk ass) (git-log head)))
                (vc-git-log-view-mode)
                (when filename
                  (derived-bind
                   (kbd "s") 
                   (keycmd (git-file-in-branch 
                            (log-view-current-tag)
                            (x: (funcall mode))
                            (git-relpath filename))))))
              "log" "-100" head
              (if filename (list "--" filename))))))

(global-set-key (kbd "C-x g l") 'git-log)

(global-set-key
 (kbd "C-x g L")
 (lambda ()
   (interactive)   
   (git-log "HEAD" buffer-file-name)))

(global-set-key
 (kbd "C-x g c")
 (lambda ()
   "Commit that shit."
   (interactive)
   (git-commit t)))

(global-set-key
 (kbd "C-x g M-a")
 (keycmd (jf-proc "git" 'pr "add" (file-name-nondirectory buffer-file-name))))

(defun git-commit (&optional all)
  (let: buf (get-buffer-create "*git-commit-msg*") in
    (switch-to-buffer buf)
    (local-set-key (kbd "C-c C-c")
      (lambda ()
	(interactive)
	(apply 'jf-proc "git" 'pr "--no-pager" "commit"
	       (append (if all '("-a"))
		       (list "-m" (buffer-string))))
	(kill-buffer)))))

(defun git-show (branch-path) 
  (interactive (list (read-branch t)))
  (funcall (git-switch-func (or (assoc-default branch-path auto-mode-alist 'string-match)
				'diff-mode)
			    "show" branch-path)))

(defun git-handle-conflict (success resolve abort &rest cmd-args)
  (cps> (output status) (apply 'jf-proc-status "git" :cont! cmd-args)
    (if (and (str~ "exited abnormally" status)
             (str~ "CONFLICT" output))
        (when (y-or-n-p "Conflicts detected!! Resolve conflicts automatically? ")
          (cps> junk (jf-proc "git" :cont! 
                             "mergetool" "-t" "foomerge" "--no-prompt")
            (let: buf (git-status) in
              (with-current-buffer buf 
                (local-set-key (kbd "C-c C-c") (keycmd (funcall resolve)))
                (local-set-key (kbd "C-c C-a") (keycmd (funcall abort)))))))
      (funcall success output))))

(defun git-run-and-revert (&rest cmd-args)
  (apply 'jf-proc "git" (x: (pr x) (revert-vc-bufs)) cmd-args))

(defun git-merge (target)
  (interactive (list (read-branch t)))
  (git-handle-conflict 
   (λ (x) (pr x) (revert-vc-bufs))
   (λ () (git-run-and-revert "commit"))
   (λ () (git-run-and-revert "reset" "HEAD" "--hard"))
   "merge" target))

(defun git-rebase (target)
  (interactive (list (read-branch t)))
  (git-handle-conflict
   'pr (λ () (git-rebase "--continue"))
   (λ () (git-run-and-revert "git rebase --abort &"))
   "rebase" target))

(global-set-key
 (kbd "C-x g s") 'git-show)

(global-set-key
 (kbd "C-x g M")
 (lambda ()
   (interactive)
   (shell-command "git mergetool -t foomerge &")))

(global-set-key
 (kbd "C-x g g")
 (lambda () (interactive) 
   (apply 'call-process "gitk" nil 0 nil (and current-prefix-arg (list (read-branch t))))))


(defun git-file-in-branch (branch &optional cont filename)
  (interactive (list (read-branch t)))
  (if (not filename) (setq filename (buffer-file-name)))
  (let: mode major-mode
	   path (git-relpath filename) in
     (cps> buf (git-process "show" :cont! (concat branch ":" path))
       (switch-to-buffer buf)
       (funcall mode)
       (kill-carriage-returns)
       (if cont (funcall cont buf)))))

(global-set-key (kbd "C-x g S") 'git-file-in-branch)

(global-set-key
 (kbd "C-x g D")
 (lambda (branch)
   (interactive (list (read-branch t)))
   (let: this-buf (current-buffer) in
     (cps> buf (git-file-in-branch branch :cont!)
       (ediff-buffers this-buf buf)))))

(defun read-many-filenames (prompt)
  (let: out nil name in
    (while (not (equal (setq name (read-file-name prompt)) ""))
      (push name out))
    out))

(defun this-line-stripped ()
  (let: out (thing-at-point 'line) in
    (if (endswith out "\n")
	(substring out 0 -1)
      out)))

(defun git-status ()
  (interactive)
  (let: buf (get-buffer-create "*git-add*") 
	dir default-directory 
	color "#00ff00" in
    (unless (memq buf (mapcar 'window-buffer (window-list)))
      (switch-to-buffer buf))
    (with-current-buffer buf
      (setq buffer-read-only nil)
      (cd dir)
      (erase-buffer)
      (shell-command "git --no-pager status | grep -v \\~\\$" t)
      (map-region
       (lambda (line)
	 (if (string-match "Changed but not updated\\|Changes not staged for commit\\|Unmerged paths" line)
	       (setq color "#ff0000"))
	 (when-let line (str~ (rx (? "#") "\t" (group (* nonl))) line 1)
           (when (string-match (rx (group (or "modified" "new file" "deleted")) ":" 
                                   (* (any " \t")))
                               line)
             (>>> (substring line (match-end 0))
                  (propertize _ 'face `(:foreground ,color)
                              'deleted (equal (match-string 1 line) "deleted"))
                  (setf line _)))
           line))
       (point-min) (point-max))
      (goto-char (point-min))
      (use-local-map (copy-keymap jason-nav-map))
      (onkey (kbd "RET")
	(pr "Adding " (this-line-stripped) "...")
        (cps> (x) (jf-proc 
                  "git" :cont! 
                  (if (get-text-property (point) 'deleted) "rm" "add")
                  (this-line-stripped))
          (pr x)
          (let: linenum (line-number-at-pos (point)) in
            (git-status)
            (goto-line linenum))))
      (onkey (kbd "c")
	(save-excursion
	  (funcall (git-display-func 'diff-mode "diff" (this-line-stripped))))
	(pr "Adding " (this-line-stripped) "...")
	(cps> x (jf-proc "git" :cont! "add" (this-line-stripped))
	  (git-commit)))
      (onkey (kbd "C")
	(save-excursion
	  (funcall (git-display-func 'diff-mode "diff" "--cached" "HEAD")))
	(git-commit))
      (onkey (kbd "C-k")
        (let: file (this-line-stripped) in
          (jf-proc "rm" (lambda (x) (pr "Deleted " file "\n") 
                          (let: linenum (line-number-at-pos (point)) in
                            (git-status)
                            (goto-line linenum)))
                   file)))

      (onkey (kbd "R")
        (let: file (this-line-stripped) in
          (if (file-exists-p file)
              (jf-proc "git" (x: (pr x "\n"))
                       "reset" file)
            (jf-shellproc (interp "git show HEAD:#,(git-relpath file) > #,file")
                          (x: (pr x "\n"))))))
      (local-set-key (kbd "g") 'git-status)
      (onkey (kbd "M-RET")
	(find-file (thing-at-point 'filename)))
      (onkey (kbd "d")
	(funcall (git-switch-func 'diff-mode "diff" (this-line-stripped))))
      (onkey (kbd "D")
	(funcall (git-switch-func 'diff-mode "diff" "--cached" "HEAD")))
      (onkey (kbd "q") (kill-buffer))
      (setq buffer-read-only t))
    buf))

(global-set-key (kbd "C-x g a") 'git-status)

(defun git-relpath (filename)
  (let: this (expand-file-name filename)
	root (expand-file-name (vc-git-root this)) in
    (substring this (length root))))

(global-set-key
 (kbd "C-x g M-d")
 (lambda (branch)
   (interactive (list (read-branch t)))
   (let: path (git-relpath (buffer-file-name)) in
     (funcall (git-switch-func 'diff-mode "diff"
			       (concat branch ":" path)
			       (concat "HEAD:" path)))
     (kill-carriage-returns))))

(global-set-key
 (kbd "C-x g b")
 (lambda (branch)
   (interactive (list (read-branch t)))
   (cps> (stuff)
       (if (string-match "^remotes/" branch)
           (jf-proc "git" :cont! "checkout" "-b"
                    (read-string "Enter a local name for this branch: ") branch)
         (jf-proc "git" :cont! "checkout" branch))
     (pr stuff)
     (revert-vc-bufs))))

(global-set-key
 (kbd "C-x g B")
 (git-switch-func 'fundamental-mode "blame" 
                  (lambda () (expand-file-name buffer-file-name))))

(global-set-key
 (kbd "C-x g k")
 (lambda (branch)
   (interactive (list (read-branch t)))
   (call-process "git" nil nil nil "branch" "-D" branch)))

(defun git-branch-map-path ()
  (str (vc-git-root ".") ".git/branch-map.el"))

(defun git-show-branch-map ()
  (interactive)
  (pr (filestr (git-branch-map-path))))

(defun git-branch-stuff (branch)
  (let: branch-map (git-branch-map-path) in
    (if (file-exists-p branch-map)
        (chain-ref (read-file branch-map) branch))))

(defun git-set-push-target (remote branch-name)
  (interactive (list (read-string "Target remote: ")
                     (read-string "Target branch: ")))
  (cps> branch (git-curbranch :cont!)
    (filedump
     (repr (cons
            (list branch remote branch-name)
            (filter
             (comp 'not (cur 'equal branch) 'car)
             (ignore-errors (read-file (git-branch-map-path))))))
     (git-branch-map-path))))

(defun git-push (remote remote-head)
  (interactive (list (if current-prefix-arg (read-remote) nil)
                     (if current-prefix-arg
                         (read-string "Remote branch name: ")
                       nil)))
  (message "Pushing...")
  (cps> (branch) (git-curbranch :cont!)
    (let: branch-info (if (not remote)
                          (git-branch-stuff branch))
          remote (or remote (car branch-info))
          remote-spec (iflet head (or remote-head
                                      (cadr branch-info))
                          (str branch ":" head)
                        branch)
      in
      (jf-shellproc (interp "git push #,[remote] #,[remote-spec]") 'pr))))

(global-set-key (kbd "C-x g !") 'git-push)

(global-set-key (kbd "C-x g p")
  (keycmd (jf-shellproc "git pull" (lambda (s)
                                     (pr s)
                                     (revert-vc-bufs)))))

(global-set-key (kbd "C-x g m") 'git-merge)

(global-set-key
 (kbd "C-x g C-r")
 (lambda ()
   (interactive)
   (call-process-shell-command 
    (str "git show " (str "HEAD:" (git-relpath (buffer-file-name))) " > " (buffer-file-name)))
   (revert-buffer nil t)))

(global-set-key (kbd "C-x g r") 'git-rebase)

(global-set-key
 (kbd "C-x g M-r")
 (lambda ()
   (interactive)
   (shell-command (format "git rebase -i %s &" (read-branch t "Rebase from:")))))

(global-set-key (kbd "C-x g M-c")
  (keycmd (find-file (str (vc-git-root default-directory) ".git/config"))))

(add-hook
 'diff-mode-hook
 (lambda ()
   (setq diff-auto-refine-mode nil)
   (define-key diff-mode-shared-map (kbd "q") nil)
   (local-set-key (kbd "q") (lambda () (interactive) (kill-buffer nil)))

   (local-unset-key (kbd "M-o"))
   
   (set-face-attribute 'diff-added nil
		       :foreground "green")
   (set-face-attribute 'diff-removed nil
		       :foreground "red")

   (goto-char (point-min))
   (search-forward "---")))

(defun git-branchlist (&optional all)
  (with-temp-buffer
    (shell-command
     (if all "git branch -a" "git branch") t)
    (lc (substring x 2) x
	(split-string (buffer-string) "\n" t))))

(defvar read-branch-history nil)

(defun read-branch (&optional all prompt)
  (completing-read
   (or prompt "Branch: ")
   (git-branchlist all) nil nil nil 'read-branch-history))

(def-reader read-remote "Remote: " (process-lines "git" "remote"))

(defun kill-carriage-returns ()
  (interactive)
  (let: buffer-read-only nil in
    (goto-char (point-min))
    (while (search-forward "\r" nil t)
      (replace-match ""))))

(defun vc-bufs (&optional vc-root)
  (if (not vc-root) (setq vc-root (vc-git-root (expand-file-name (or buffer-file-name default-directory)))))
  (lc x x (buffer-list) 
      (with-current-buffer x
        (and buffer-file-name (file-exists-p buffer-file-name)
             (equal (vc-git-root (expand-file-name buffer-file-name))
                    vc-root)))))

(defun show-vc-bufs ()
  (interactive)
  (let: bufs (vc-bufs) in
    (with-output-to-temp-buffer "*vc-bufs*"
      (w/buf standard-output
        (mapc (lambda (x) 
                (insert (mklink (buffer-name x) (keycmd (switch-to-buffer x))))
                (insert "\n"))
              bufs)))))

(defun revert-vc-bufs ()
  (interactive)
  (setf git-pending-reverts (union (vc-bufs) git-pending-reverts))
  (revert-visible-pending-bufs))

(defvar git-pending-reverts nil)

(defun revert-visible-pending-bufs ()
  (let: reverts (intersection (mapcar 'window-buffer (window-list))
                              git-pending-reverts)
        window-configuration-change-hook nil in
    (setf git-pending-reverts 
          (set-difference git-pending-reverts reverts))
    (mapc (x: (with-current-buffer x (revert-buffer nil t)))
          reverts)))

(add-hook 
 'window-configuration-change-hook
 'revert-visible-pending-bufs)

(setq diff-default-read-only t)

(defun git-ls-tree (branch)
  (interactive (list (read-branch t)))
  (cps> output (git-process "ls-tree" :cont! branch)
    (switch-to-buffer output)
    (local-set-key (kbd "RET")
      (keycmd (git-show (str branch ":" (git-relpath (thing-at-point 'filename))))))
    (local-set-key (kbd "M-RET")
      (keycmd
       (jf-shellproc (str "git show "
			  branch ":" (git-relpath (thing-at-point 'filename))
			  " > " (file-name-nondirectory (thing-at-point 'filename)))
		     (lambda (s) (message "Retreived")))))))

(defun git-publish-branch (branch)
  "Push a branch to a remote, and then set that branch's upstream."
  (interactive (list (read-branch t)))
  (shell-command
   (str "git push origin " branch))
  (shell-command
   (str "git branch --set-upstream " branch
	" origin/" branch)))

(defun git-grep (pattern &optional branch)
  (interactive (list (read-string "Pattern: ")
                     (and current-prefix-arg (read-branch t))))
  (jf-proc "git" 'pr "--no-pager" "grep" pattern (or branch "HEAD")))

(defun git-curbranch (cont)
  "Calls CONT with the current branch in this repository."
  (interactive (list 'pr))
  (cps> (branches) (jf-proc "git" :cont! "--no-pager" "branch")
    (let: lines (split-string branches "\n" t) in
      (do-wad (lc x x lines (startswith x "*"))
              (substring (car _) 2)
              (funcall cont _)))))

(defun git-fetch (&optional remote)
  (interactive (list (read-remote)))
  (shell-command (interp "git fetch #,(or remote \"\") &")))

(global-set-key (kbd "C-x g f") 'git-fetch)

(setq git-graph-commit-re
      (rx bol (* any) " commit "
          (group (repeat 40 (any (?a . ?f) (?0 . ?9))))))

(defun git-graph-log-next ()
  (interactive)
  (end-of-line)
  (re-search-forward git-graph-commit-re)
  (beginning-of-line))

(defun git-graph-log-prev ()
  (interactive)
  (beginning-of-line)
  (re-search-backward git-graph-commit-re))

(defun git-current-commit ()
  (save-excursion
    (end-of-line)
    (re-search-backward git-graph-commit-re)
    (match-string 1)))

(setq git-graph-log-keymap 
      (keymap
       nil
       (kbd "n") 'git-graph-log-next
       (kbd "M-l") 'git-graph-log-relog
       (kbd "g") 'revert-buffer
       (kbd "p") 'git-graph-log-prev
       (kbd "q") (keycmd (kill-buffer))
       (kbd "d") (keycmd
                  (funcall (git-switch-func 'diff-mode "diff" "--ignore-space-at-eol"
                                            (git-current-commit) "HEAD")))
       (kbd "c") (keycmd (cps> (out) (jf-proc "git" :cont! "checkout" (git-current-commit))
                           (pr out)
                           (revert-vc-bufs)))
       (kbd "RET") (keycmd (git-show (git-current-commit)))
       (kbd "M-RET") (keycmd (jf-proc "git" 'pr "cherry-pick" (git-current-commit)))))

(defun highlight-heads (head-hashes head-re)
  (when head-re
    (goto-char (point-min))
    (map-regex
     (x: (iflet o (overlays-at (match-beginning 0))
                (overlay-add (car o)
                             'face '(:background "#00ff00" :weight bold :foreground "black")
                             'display (str x " | " (gethash x head-hashes))))
         x)
     head-re)))

(defun git-graph-log (&rest heads)
  (interactive (cond ((equal current-prefix-arg 2)
                      (multi-read (lambda () (read-branch t))))
                     (current-prefix-arg (list (read-branch t)))
                     (t (list "HEAD"))))
  (pr heads "\n")
  (funcall
   (apply
    'git-switch-func
    (lambda ()
      (fundamental-mode)
      (mapc 'delete-overlay (overlays-in (point-min) (point-max)))
      (ansi-color-apply-on-region (point-min) (point-max))
      (use-local-map git-graph-log-keymap)
      (make-local-variable 'revert-buffer-function)
      (setq revert-buffer-function (lambda (&rest junk) (apply 'git-graph-log heads)))
      (let: head-hashes (get-commit-names)
               head-re (ignore-errors (rx-to-string (cons 'or (hash-keys head-hashes)))) in
        (save-excursion (highlight-heads head-hashes head-re))
        (local-set-key (kbd "N") 
          (keycmd (end-of-line) (re-search-forward head-re)))
        (local-set-key (kbd "P") 
          (keycmd (beginning-of-line) (re-search-backward head-re))))
      (setq buffer-read-only t))
    "log" "--graph" "--color" "-50" heads)))

(defun git-graph-log-relog ()
  (interactive)
  (let: old-back-fn (lookup-key (current-local-map) (kbd "DEL"))
           old-revert revert-buffer-function in
    (git-graph-log (git-current-commit))
    (local-set-key (kbd "DEL")
      (keycmd (local-set-key (kbd "DEL") old-back-fn)
              (funcall old-revert)))))

(defun get-commit-names ()
  (w/cd (str (vc-git-root default-directory) ".git/refs/")
    (do-wad
     (re-findall "commit \\([a-f0-9]+\\)$" (buffer-string) t 1)
     (ignore-errors 
       (apply 'process-lines "grep" (strjoin "\\|" _)
              "." "-r" 
              (if (file-exists-p "../packed-refs") 
                  '("../packed-refs"))))
     (lc (if (string-match "^../packed-refs" x)
             (reverse (split-string (substring x (length "../packed-refs:"))))
           (split-string x ":"))
         x _ (not (str~ (rx "^") x)))
     (lc (list (replace-regexp-in-string
                (rx (or "refs" ".") (or "/heads/" "/")) "" (car x))
               (cadr x))
         x _)
     (remove-duplicates _ :test (lambda (a b) (equal (car a) (car b)))
                        :from-end t)
     (group-by _ 'cadr)
     (maphash! (lambda (k v) (mapcar 'car v)) _))))

(defun git-file-diff-heads (head1 head2)
  (interactive (list (read-branch t) (read-branch t)))
  (let: path (git-relpath buffer-file-name) in
    (funcall (git-switch-func 'diff-mode "diff"
                              (str head1 ":" path)
                              (str head2 ":" path)))))

(defun git-new-repo (host repo-name &optional path)
  (interactive (list 
                (if current-prefix-arg
                    (read-from-minibuffer "Host spec: ")
                  "ozbert.com")
                (str (file-name-nondirectory 
                      (substring (vc-git-root default-directory) 0 -1)) ".git")
                "/home/jfeng/"))
  (jf-proc 
   "ssh" 'pr host
   (interp "mkdir #,[repo-name] && cd #,[repo-name] && git init --bare"))
  (jf-proc "git" 'pr "remote" "add" "ozbert" (interp "ssh://#,[host]#,[path]#,[repo-name]")))

(defun git-current-branch (cont)
  (interactive (list 'pr))
  (jf-proc "git" 
           (comp cont
                 (cur 'find-if (=~ "^\\*"))
                 (cut split-string <> "\n"))
           "branch"))

(global-set-key (kbd "C-x g w") 'git-current-branch)

(global-set-key (kbd "C-x g M-l") 'git-graph-log)
(provide 'git-wads)
