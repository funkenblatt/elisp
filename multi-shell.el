;; -*- lexical-binding: t; -*-
(require 'term)

(defvar multi-shell-history nil)

(defun multi-shell-init (n)
  (setq multi-shells 
        (lc (ansi-term "/bin/bash" (interp "multi-term-#,[x]")) x 
            (range 1 (+ 1 n))))
  (term-set-escape-char (aref (kbd "C-c") 0))
  (define-key term-raw-map (kbd "C-x") 'term-send-raw)
  (setup-multi-term-map)
  (w/buf (get-buffer-create "term-control")
         (use-local-map term-control-map))
  (dbind (t1 t2 t3 t4) (mapcar 'buffer-name multi-shells)
         (wconfig-recurse 
          `(h (v (v "term-control" ,t1) ,t2)
              (v  ,t3 ,t4)))))

(defun multi-shell-command (cmd)
  (interactive (list (str (strip (thing-at-point 'line)) "")))
  (dolist (shell multi-shells)
    (process-send-string (get-buffer-process shell) cmd)))

(defun multi-shell-login (servers &optional username)
  (mapcar*
   (lambda (shell server)
     (comint-send-string
      (get-buffer-process shell)
      (interp "ssh -A #,(or username \"ubuntu\")@#,[server]\n")))
   multi-shells servers))

(defun multi-shell-do-all (command)
  (interactive)
  (lambda ()
    (interactive)
    (let: window-bufs (index-by (window-list) 'window-buffer) in
      (dolist (i multi-shells)
        (with-selected-window (gethash i window-bufs)
          (term-check-size (get-buffer-process i))
          (funcall command))))))

(memoize 'multi-shell-do-all)

(jason-defkeys term-raw-map 
  (kbd "C-x") 'term-send-raw
  (kbd "C-SPC") (keycmd (term-send-raw-string "\000")))

(defun map-keymap-all (func keymap &optional prefix)
  (map-keymap
   (lambda (k v)
     (cond
      ((functionp v) (funcall func k v prefix))
      ((keymapp v) (map-keymap-all func v (cons k prefix)))
      (t nil)))
   keymap))

(defun setup-multi-term-map ()
  (setf term-control-map
        (copy-keymap term-raw-map))
  (define-key term-control-map
    (kbd "C-SPC") 
    (multi-shell-do-all (keycmd (term-send-raw-string "\000"))))
  (map-keymap-all
   (lambda (k v prefix) 
     (when (and (symbolp v)
                (startswith (symbol-name v) "term-send"))
       (define-key term-control-map
         (apply 'vector (reverse (cons k prefix)))
         (multi-shell-do-all v))))
   term-raw-map))

(provide 'multi-shell)
