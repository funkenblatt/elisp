(define-ibuffer-filter not-visible
  "Shows only buffers that aren't being displayed in a window."
  (:reader nil :description "Not visible")
  (let: visible-bufs (lc (window-buffer x) x (all-windows)) in
    (not (memq buf visible-bufs))))
