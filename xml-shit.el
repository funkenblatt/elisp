;; -*- lexical-binding: t; -*-
(require 'xml)

(defun nodes-where (pred tree)
  "Return every node in TREE matching PRED.  Does not recurse into
nodes which have matched PRED."
  (cond
   ((funcall pred tree) (list tree))
   ((consp tree) (mappend (cur 'nodes-where pred) (cddr tree)))))

(defun tagp (tag)
  (lambda (x) (and (consp x) (eq (car x) tag))))

(>>> (nodes-where
      (tagp 'Elevation)
      '(USGS_Elevation_Point_Query_Service nil
                                           (Elevation_Query
                                            ((x . "0")
                                             (y . "0"))
                                            (Data_Source nil "NED 1/3 arc-second")
                                            (Elevation nil "0")
                                            (Units nil "Meters"))))
     car xml-node-children car read)

(provide 'xml-shit)
