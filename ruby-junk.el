;; -*- lexical-binding: t; -*-
(require 'ruby-mode)
(require 'http-client)

(defvar rails-listener)
(defvar rails-input-proc)
(defvar rails-input-serv)

(defun dump-rb-scratch ()
  (interactive)
  (do-wad (get-buffer "rbwad")
          (when _ (w/buf _ (write-region nil nil "~/.emacs.d/rbwad")))))

(defun setup-rails ()
  (interactive)
  (w/buf (get-buffer-create "rbwad")
         (insert-file-contents-literally 
          "~/.emacs.d/rbwad"
          nil nil nil t)
         (ruby-mode)
         (derived-bind (kbd "C-x C-s") 'dump-rb-scratch))

  (add-hook 'kill-emacs-hook 'dump-rb-scratch)

  (setq rails-listener (make-server "rails-console" 9999 (lambda (p s) )))
  (setq rails-input-serv (make-server "rails-input" 9998 (lambda (p s) )))
  (set-process-sentinel rails-listener
    (lambda (p s)
      (when (str~ "^open" s)
        (setq rails-input-proc p)
        (wait 0.5 (ruby-req "Process.pid" (cut setf ruby-pid <>)))
        )))
  (set-process-sentinel rails-input-serv
    (lambda (p s)
      (if (str~ "^open" s)
          (letrec ((pipe (procpipe p))
                   (loopy (lambda ()
                            (cps> (input) (read-chunks pipe :cont!)
                              (ignore-errors (funcall ruby-recv-fn input))
                              (funcall loopy)))))
            (funcall loopy))))))

(defun rails-send (&optional cont)
  (interactive)
  (let: input (if (region-active-p)
                  (get-region)
                (thing-at-point 'line)) in
    (ruby-req (str "begin (" input ").inspect
  rescue Exception => e 
    e.message
  end")
              (or cont (lambda (x) (pr "  => " x))))))

(defun rails-send-raw ()
  (interactive)
  (process-send-string rails-input-proc (ruby-input)))

(defun ruby-input ()
  (do-wad
   (if (region-active-p) (get-region) (thing-at-point 'line))
   (if (endswith _ "\n") _ (str _ "\n"))))

(define-key ruby-mode-map (kbd "M-RET") 'rails-send)
(define-key ruby-mode-map 
  (kbd "<C-return>")
  (keycmd 
   (if current-prefix-arg
       (ruby-req (ruby-input) (x: (rdump x)))
     (rails-send (x: (dump x))))))

(define-key ruby-mode-map
  (kbd "<s-return>")
  (keycmd
   (ruby-req (str "pp_to_s("(ruby-input)")")
             (x: (dump x)))))

(define-key ruby-mode-map (kbd "M-p")
  (keycmd
   (insert
    (save-excursion
      (while (member (thing-at-point 'line) 
                     '("\n" ""))
        (forward-line -1))
      (strip (thing-at-point 'line))))))

(define-key ruby-mode-map (kbd "C-c C-s") 'rails-send-raw)

(defun last-ruby-stuff ()
  (buffer-substring (save-excursion 
                      (skip-chars-backward
                       "a-zA-Z0-9._:")
                      (point))
                    (point)))

(defvar ruby-req-id 0)

(defvar ruby-requests (make-hash-table))

(defun ruby-req (rb callback)
  (let: req-id (incf ruby-req-id) in
    (puthash req-id callback ruby-requests)
    (process-send-string
     rails-input-proc
     (str "SendToEmacs.tell_emacs([" req-id ", " rb "])\n"))))

(defun ruby-open-definition ()
  (interactive)
  (if (region-active-p)
      (ruby-open-source-location (get-region))
    (let: chainage (split-string (strip (thing-at-point 'line)) "\\." t)
          meth (lastcar chainage)
          prefix (strjoin "." (butlast chainage))
          lookup-expr (interp "#,[prefix].method(:#,[meth]).find_def") in
      (ruby-open-source-location lookup-expr))))

(defun ruby-open-source-location (method-junk)
  (ruby-req method-junk
            (x: (find-file (aref x 0)) (goto-line (aref x 1)))))

(defun register-request (callback)
  (let: req-id (incf ruby-req-id) in
    (puthash req-id callback ruby-requests)
    req-id))

(setq ruby-recv-fn
      (lambda (s)
        (setf ruby-last-received-junk s)
        (let: junk (read (string-as-multibyte s))
              fun (gethash (aref junk 0) ruby-requests) in
          (ignore-errors 
            (funcall fun (aref junk 1)))
          (when (not (keywordp (aref junk 0)))
            (remhash (aref junk 0) ruby-requests)))))

(defun ruby-completion-expr (s)
  (let: segments (re-findall (rx (or (+ (any alnum "_")) "::" ".")) s t)
        base-expr nil in
    (list
     (catch 'break
       (while t
         (pcase segments
           ((or `("::" ,prefix) `("::"))
            (>>> base-expr reverse (apply 'concat _)
                 (interp "#,[_].constants.map(&:to_s)")
                 (throw 'break _)))
           ((or `("." ,prefix) `("."))
            (>>> base-expr reverse (apply 'concat _)
                 (interp "#,[_].methods.map(&:to_s)")
                 (throw 'break _)))
           (`(,x) (throw 'break "Object.constants.map(&:to_s)"))
           (x (push (pop segments) base-expr)))))
     (if (member (lastcar segments) '("::" ".")) "" (lastcar segments)))))

(defun ruby-complete ()
  (interactive)
  (let: stuff (last-ruby-stuff)
        prefix-seed (ruby-completion-expr stuff)
        prefix (car prefix-seed)
        seed (cadr prefix-seed) in
    (if (or (startswith stuff ".") (= (length stuff) 0))
        (pr "Fuck you.")
      (ruby-req prefix
                (lambda (c)
                  (let: completions (all-completions seed (append c nil))
                        completion-at-point-functions
                        (list (lambda ()
                                (list (- (point) (length seed))
                                      (point)
                                      completions))) in
                    (completion-at-point)))))))

(define-key ruby-mode-map (kbd "M-TAB") 'ruby-complete)

(puthash :printer 'pr ruby-requests)

(provide 'ruby-junk)

(defvar ruby-backtoken
  (rx (or (regexp "[a-zA-Z0-9_!?:]+")
          (any "(),\"'.[]{}"))))

(defun ruby-next-backtoken ()
  (re-search-forward ruby-backtoken)
  (match-string 0))

;; (defun ruby-backparse (&optional bound)
;;   (let: chunk (areverse
;;                (buffer-substring-no-properties 
;;                 (max (point-min) (- (point) (or bound 50))) (point))) in
;;     (with-temp-buffer
;;       (insert chunk)
;;       (goto-char (point-min))
;;       (let: tok (ruby-next-backtoken) in
;;         (cond
;;          ((equal tok ")") 
;;           (ruby-take-til "(")
;;           (ruby-

(defun ruby-load ()
  (interactive)
  (process-send-string
   rails-input-proc
   (interp "load '#,[buffer-file-name]'\n")))

(defun ruby-interrupt ()
  (interactive)
  (signal-process ruby-pid 'SIGINT))

(jason-defkeys ruby-mode-map (kbd "{") nil (kbd "}") nil)

(defun ruby-method-doc-columns (row)
  (>>> row (split-string _ "\n" t) 
       (mapcar (cur 'replace-regexp-in-string "^ *# *" "") _)
       (let:: (first-row . extra-last-shit) _
              columns (split-string first-row "  +") in
         (>>> columns lastcar (cons _ extra-last-shit) (strjoin "\n" _)
              `(,(str "@" (car columns)) ,@(butlast (cdr columns)) ,_)))))

(defun ruby-commentify (s)
  (>>> s (split-string _ "\n")
       (mapcar (comp 'rstrip (cur 'str "# ")) _)
       (strjoin "\n" _)))

(defun ruby-format-method-docs ()
  (interactive)
  (>>> (get-region) substring-no-properties
       (split-string _ ".*@" t)
       (mapcar 'ruby-method-doc-columns _)
       (print-tabular _ "  ") ruby-commentify
       (progn (delete-region (region-beginning) (region-end))
              (pr (point))
              (insert _)
              (pr " " (point))
              (indent-region (- (point) (length _)) (point)))))

(define-key ruby-mode-map (kbd "C-c C-c") 'ruby-format-method-docs)

(defun repo-list ()
  (with-default-dir project-directory
    (ls "app/repositories/delegate_repository")))

(defun open-repository (repo)
  (interactive
   (list (ido-completing-read "Repository: " (repo-list))))
  (with-default-dir project-directory
    (let: bufs (mapcar (comp 'find-file-noselect
                             (cut str "app/repositories/" <> "/" repo))
                       '("delegate_repository" "active_record_repository")) in
      (mapcar*
       (λ (win buf) (with-selected-window win (switch-to-buffer buf)))
       (n-windows (length bufs) 2) bufs))))
