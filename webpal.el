(require 'infix)

(defun wpclr (wp)
  (setq wp (- 215 wp))
  (lc ($ wp/6^x % 6) x '(2 1 0)))

(defun clrwp (clr)
  (if (vectorp clr) (setq clr (vec->lst clr)))
  (- 215 
     (sum (lc: ($ x*6^y) x y in clr '(2 1 0)))))

(defun wpblend (bg fg alpha)
  (let: bgc (wpclr bg) fgc (wpclr fg)
	outc (lc: (round ($ x+[y-x]*alpha))
		  x y in bgc fgc)
	in (clrwp outc)))

(defun wpinterp (black white)
  (dump
   "(unsigned char []) "
   (bracify
    (mappity
     (lc (wpblend white black x) x (range 0 1.1 0.2))))))

(defun mappity (m)
  (let: 
   outv (make-vector 8 0) mv (apply 'vector m) in
   (dolist (i (range 6))
     ($ outv->[[i*43]%8] := mv->i))
   (mapcar 'identity outv)))

(provide 'webpal)
