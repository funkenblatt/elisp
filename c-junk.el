(defun list-cfuns ()
  (interactive)
  (foolet (buf (get-buffer-create "*C Function Listing*"))
    (save-excursion (set-buffer buf) (erase-buffer))
    (call-process-shell-command 
     "proto.py -A" nil buf nil
     (buffer-file-name (current-buffer)))
    (setq list-cfun-buf (current-buffer))
    (pop-to-buffer (get-buffer "*C Function Listing*"))
    (list-cfuns-propertize)
    (goto-char (point-min))))

(defun list-cfuns-propertize ()
  (foolet (kmap (make-sparse-keymap))
    (define-key kmap (kbd "RET") 'list-cfuns-go)
    (define-key kmap (kbd "<mouse-2>") 'list-cfuns-go)
    (define-key kmap (kbd "r") 'list-cfuns-refresh)
    (define-key kmap (kbd "q") 'bury-buffer)
    (fset 'list-cfun-keymap kmap)
    (kill-lines (lambda (x) (or (< (length x) 2) (startswith x "//"))))
    (dolines line
      (add-text-properties (line-beginning-position)
			   (line-end-position)
			   `(mouse-face highlight
			     follow-link foobite
			     keymap list-cfun-keymap)))
    (highlight-regexp "[^ ]+(")))

(defun list-cfuns-go ()
  (interactive)
  (foolet (line (list-cfun-get-lineno))
    (pop-to-buffer list-cfun-buf)
    (goto-line line)))

(defun list-cfun-get-lineno ()
  (save-excursion
    (set-buffer "*C Function Listing*")
    (beginning-of-line)
    (re-search-forward "\\([0-9]+\\)-\\([0-9]+\\)")
    (string-to-number (match-string 1))))

(defun list-cfuns-refresh ()
  (interactive)
  (set-buffer list-cfun-buf)
  (list-cfuns))

