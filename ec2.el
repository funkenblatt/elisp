;; -*- lexical-binding: t; -*-

(defvar instance-addr-fun 
  (orfn (:= 'dns-name) (:= 'ip-address) (:= 'private-ip-address)))

(defun refresh-ec2-instances ()
  (setf all-instances nil)
  (dolist (i ec2-creds)
    (setenv "EC2_ACCESS_KEY" (car i))
    (setenv "EC2_SECRET_KEY" (cdr i))
    (jf-proc "~/lisp/ec2-instances"
             (x: (do-wad 
                  (read x) downcase-tree 
                  (setf all-instances (append _ all-instances)
                        instances-by-dns
                        (index-by
                         (filter instance-addr-fun
                                 all-instances)
                         instance-addr-fun)
                        instances-by-name 
                        (index-by 
                         (lc x x all-instances (chain-ref x 'tags "Name"))
                         (cut chain-ref <> 'tags "Name")))) 
                 (pr "Done!")))))

(defun instances-where (&rest conditions)
  (let: checkers (lc (apply 'alist-checker x) x (sublist2 conditions)) in
    (lc x x all-instances (all (apply-fns x checkers)))))

(defun instances-with-purpose (purpose)
  (mapcar (cut chain-ref <> 'dns-name) 
          (instances-where '(tags "Purpose") purpose)))

(defun webs () (instances-with-purpose "webserver"))
(defun djs () (instances-with-purpose "dj-worker"))
(defun websters () 
  (mapcar
   (cut chain-ref <> 'dns-name)
   (instances-where '(tags "Name") (cut str~ "webster" <>))))

(defun instance-address (name)
  (interactive (list (completing-read "Node: " instances-by-name)))
  (kill-new (do-wad (gethash name instances-by-name)
                    (funcall instance-addr-fun _))))

(global-set-key (kbd "C-x j A") 'instance-address)

(defun instance-dns-where (&rest conditions)
  (mapcar (cut chain-ref <> 'dns-name) (apply 'instances-where conditions)))

(defun log-grep (grep-str &optional lines)
  (interactive (list (read-string "Grep: ")
                     (if current-prefix-arg (read-string "Number of lines: "))))
  (multi-exec 
   "web" (webs)
   (interp "tail -n #,(or lines 20000) www/identified/current/log/production.log |"
           " grep '#,[grep-str]' -n")))

(defun dump-server-info (s &optional refresh)
  (let: instance-info (gethash s instances-by-dns)
        refresh (keycmd (server-saved-point (or refresh 'show-server-set)))
        remover (keycmd (setf current-server-set 
                              (reverse (set-difference 
                                        current-server-set (list s)
                                        :test 'equal)))
                        (funcall refresh))
        adder (keycmd (setf current-server-set
                            (union current-server-set (list s) :test 'equal))
                      (funcall refresh))
        name (chain-ref instance-info 'tags "Name")
        txt (interp "#,s (#,name)") in
    (proper-dump
     (mklink txt 
             (if (member s current-server-set)
                 remover adder)
             (and (not (member s current-server-set))
                  '(:foreground "#660000")))
     "\n")))

(defun show-server-set ()
  (interactive)
  (with-output-to-temp-buffer "servers"
    (with-current-buffer "servers"
      (mapc 'dump-server-info current-server-set))))

(defun server-saved-point (fun)
  (let: pt (w/buf "servers" (point)) in
    (ignore-errors (funcall fun))
    (w/buf "servers" (goto-char pt))))

(defun show-all-servers ()
  (interactive)
  (with-output-to-temp-buffer "servers"
    (with-current-buffer "servers"
      (mapc (cut dump-server-info <> 'show-all-servers)
            (union
             current-server-set
             (mapcar instance-addr-fun
                     all-instances)
             :test 'equal)))))

(defvar instance-criteria
  '((purpose . (tags "Purpose"))
    (name . (tags "Name"))))

(defun get-instance-predicate ()
  (let: fields (append instance-criteria 
                       (mapcar (juxt 'car 'car)
                               (car all-instances)))
        field (assoc-default 
               (if current-prefix-arg
                   (intern (completing-read "Field: " fields))
                 'purpose)
               fields)
        values (mapcar (cut apply 'chain-ref <> field) all-instances)
        value (completing-read "Value: " values) in
    (list field value)))

(defun select-instances (field value)
  (interactive (get-instance-predicate))
  (setf current-server-set (instance-dns-where field value))
  (show-all-servers))

(defun clear-server-set ()
  (interactive)
  (setf current-server-set nil)
  (show-all-servers))

(defun foo-exec (user servers cmd)
  (setf foo-exec-bufs 
        (lc (let: instance (gethash x instances-by-dns)
                  name (chain-ref instance 'tags "Name")
                  cmd (if (functionp cmd) (funcall cmd x) cmd)
                  buf (get-buffer-create (interp "output-#,[x] (#,name)")) in
              (w/buf buf (erase-buffer))
              (start-process "fooproc" buf "ssh" (interp "#,[(or user \"ubuntu\")]@#,x") "-A" cmd)
              (buffer-name buf))
            x (or servers current-server-set))))

(defun foo-exec-binding (&optional k)
  "Set binding for interactively running foo-execs."
  (interactive)
  (local-set-key (or k (kbd "<C-return>"))
    (keycmd 
     (if current-prefix-arg
         (multi-exec current-multi-user
                     current-server-set
                     (eval (preceding-sexp)))
       (foo-exec current-multi-user nil (eval (preceding-sexp)))))))

(defun scroll-foo-exec (&optional fun)
  (interactive)
  (do-wad
   (mappend 'window-list (frame-list))
   (index-by _ (comp 'buffer-name 'window-buffer))
   (mapcar (cut gethash <> _) foo-exec-bufs)
   (ec (with-selected-window x (goto-char (if fun (funcall fun) (point-min)))) x _)))

(defun scroll-foo-exec-bottom () (interactive) (scroll-foo-exec 'point-max))

(defun show-foo-exec-bufs (&optional hthresh)
  (interactive)
  (with-selected-frame (make-frame '((width . 231) (height . 64)))
    (set-frame-height (selected-frame) 66)
    (delete-other-windows)
    (foo-exec-windows hthresh)))

(defun foo-exec-windows (&optional hthresh)
  (interactive)
  (n-windows (length foo-exec-bufs) hthresh)
  (show-bufs foo-exec-bufs))

(global-set-key 
 (kbd "C-x M-s s")
 'show-all-servers)

(global-set-key 
 (kbd "C-x M-s a")
 'show-all-servers)

(global-set-key
 (kbd "C-x M-s c")
 'clear-server-set)

(global-set-key 
 (kbd "C-x M-s i")
 'select-instances)

(global-set-key
 (kbd "C-x M-s C-s")
 (keycmd (multi-save
          current-multi-user current-server-set
          buffer-file-name (read-string "remote file: "))))

(global-set-key
 (kbd "C-x M-s r")
 (keycmd (refresh-ec2-instances)))

(global-set-key
 (kbd "C-x M-s M-<")
 'scroll-foo-exec)

(global-set-key
 (kbd "C-x M-s M->")
 'scroll-foo-exec-bottom)


;; Sensei functions

(defun find-emacs-ooms ()
  (interactive)
  (foo-exec
   current-multi-user nil
   (% (with-current-buffer "remote-commands"
        (goto-char (point-max))
        (search-backward "sensei node started successfully" nil t)
        (search-forward "outofmemory" nil t)))))

(defun grep-ooms ()
  (interactive)
  (foo-exec
   current-multi-user nil
   "date; grep -A 1 -B 1 -nH OutOfMemory sensei-checkout/logs/*.log   | tail -n 5"))

(defun query-senseis ()
  (interactive)
  (let: json-curl (% (do-wad
                      (process-lines "curl" "-s" "http://localhost:8080/sensei/")
                      car json-read-from-string)) in
    (shell-command 
     (interp 
      "for i in #,(strjoin \" \" current-server-set); do echo $i; "
      "ssh ubuntu@$i '#,[json-curl]'; echo; done &"))))

(defun remote-elisp (stuff)
  (do-wad
   stuff repr shell-quote-argument
   (interp "emacsclient -e #,_")))

(defmacro % (&rest stuff)
  `(remote-elisp '(progn ,@stuff)))

(provide 'ec2)
