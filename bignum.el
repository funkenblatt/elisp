(defun lstrip-zeros (l)
  (setq l (reverse l))
  (while (and l (= (car l) 0)) (pop l))
  (if (not l) (list 0)
    (reverse l)))

(defmacro zerostrip (&rest vars)
  (cons 'progn
	(lc `(setq ,var (lstrip-zeros ,var)) var vars)))

(defmacro zeropad (a b)
  `(cond ((< (length ,a) (length ,b))
	  (setq ,a (append ,a (zeros (- (length ,b) (length ,a))))))
	 ((< (length ,b) (length ,a))
	  (setq ,b (append ,b (zeros (- (length ,a) (length ,b))))))))

(defun zeros (n)
  (let ((out '()))
    (while (> n 0)
      (push 0 out)
      (dec n))
    out))

(defun bigcmp (a b)
  (zeropad a b)
  (catch 'return
    (dolist (i (map* '- (reverse a) (reverse b)))
      (if (/= i 0)
	  (throw 'return (if (> i 0) 1 -1))))
    0))

(defsubst big> (a b)
  (= (bigcmp a b) 1))
(defsubst big< (a b)
  (= (bigcmp b a) 1))
(defsubst big= (a b)
  (= (bigcmp a b) 0))

(defun bigadd (a b)
  (zeropad a b)
  (foolet (out '() carry 0 temp 0)
	  (dolist (i (map* '+ a b))
	    (setq temp (+ i carry))
	    (setq carry (/ temp 10))
	    (push (mod temp 10) out))
	  (if (/= 0 carry) (push carry out))
	  (nreverse out)))

(defun bigsub (a b)
  (zeropad a b)
  (foolet (out '() borrow 0 temp 0)
	  (dolist (i (map* '- a b))
	    (setq temp (- i borrow))
	    (setq borrow (if (< temp 0) 1 0))
	    (push (if (< temp 0) (+ temp 10) temp) out))
	  (nreverse out)))

(defun bigmul-1 (a dig)
  (foolet (out '() carry 0 temp 0)
	  (dolist (i (lc (* dig x) x a))
	    (setq temp (+ i carry))
	    (setq carry (/ temp 10))
	    (push (mod temp 10) out))
	  (if (/= 0 carry) (push carry out))
	  (nreverse out)))

(defun bigmul (a b)
  (foolet (out (list 0))	   
    (dolist (i (lc* 
		(append (zeros z) (bigmul-1 a y))
		(y z) (b (range (length b)))))
      (setq out (bigadd out i)))
    out))

(defun bigzero? (n)
  (catch 'break
    (dolist (i n) (if (/= i 0) (throw' break nil)))
    t))

(defrpn almost-last-car (l)
  (elt l (- (length l) 2)))

(defrpn lastcar (l)
  (car (last l)))

;; (defun pr (&rest args)
;;   (mapc 'princ args) nil)

(defun shifty (n places)
  (let (out)
    (cond ((< n 0) (setq out (tail places (- n))))
	  ((> n 0) (setq out (append (zeros n) places)))
	  (t (setq out places)))
    (if (not out) (list 0) out)))

(defmacro pop* (l n)
  `(setq ,l (tail ,l ,n)))

(defun str->bn (s)
  (reverse (lc (- x ?0) x s)))

(defun bn->str (n)
  (concat (lc (+ x ?0) x (reverse n))))

(defmacro append! (l item) `(setq ,l (append ,l (list ,item))))

(defun bigdiv-1 (a b alen blen)
  (foolet (arev (reverse a) temp (popn arev blen) out '() run 1)
	  (pop* arev blen)
	  (if (big< temp b) 
	      (push (pop arev) temp) 
	    (append! temp 0))
	  (while run
	    (let ((guess (rpn temp length blen > temp lastcar 10 * temp almost-last-car +
			      temp lastcar if
			      b lastcar /))
		  (guesses 0))
	      (while (big> (bigmul b (list guess)) temp) (dec guess) (incf guesses))
	      ;(if (>= guesses 2) (pr (bn->str temp) " " (bn->str b) " " guesses "\n"))
	      (push guess out)
	      (setq temp (bigsub temp (bigmul b (list guess)))))
	    (zerostrip temp)
	    (if arev (push (pop arev) temp) (setq run nil)))
	  (list out temp)))

(defun bigdiv (a b)
  (if (numberp b) (setq b (list b)))
  (zerostrip a b)
  (foolet (alen (length a) blen (length b) mulfac (/ 10 (1+ (lastcar b))))
	  (cond ((or (< alen blen) (big< a b))
		 (list (list 0) a))
		((bigzero? b) (error "Divide by zero!"))
		((> mulfac 1)
		 (setq a (bigmul a (list mulfac)) b (bigmul b (list mulfac)))
		 (setq alen (length a) blen (length b))
		 (let ((res (bigdiv-1 a b alen blen)))
		   (if (eq (car (cadr res)) 0)
		       res
		     (list (car res) (car (bigdiv-1 (cadr res) (list mulfac) 
						    (length (cadr res)) 1))))))
		(t (bigdiv-1 a b alen blen)))))

(defun bn->cbn (num)
  "Outputs a string suitable as an initializer to a C bignum with 16-bit limbs."
  (foolet (out '() result '())
    (while (not (equal num '(0)))
      (setq result (bigdiv num '(6 3 5 5 6)))
      (push (cadr result) out)
      (setq num (car result)))
    (setq out (nreverse out))
    (format "{(unsigned short [%d]) %s, %d, 0}"
	    (+ (length out) 4)
	    (bracify (mapcar 'bn->str out))
	    (length out))))

(defun make-bignum (str)
  (interactive "s\n")
  (bn->cbn (str->bn str)))

(provide 'bignum)
