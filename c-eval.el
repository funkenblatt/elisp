(defun c-eval (type c-code cont)
  (let: outfile (make-temp-file "/tmp/dicks")
        format-string (substring-no-properties
                       (case type
                         (long "%ld\n")
                         (int "%d\n")
                         (string "%s\n")
                         (otherwise "")))
        cast (case type
               (string "(char *)")
               (otherwise (interp "(#,[type])")))
        body (if type
                 (interp "printf(#,(pp-to-string format-string), #,[cast] #,[c-code]);")
               c-code)
        c-code (interp "#include <stdio.h>
#include <stdlib.h>
int main()
{
  #,[body]
  return 0;
}\n") 
        proc (->> output (jf-proc "gcc" :cont! "-x" "c" "-" "-o" outfile)
               (if (equal (strip output) "")
                   (->> output (jf-proc outfile :cont!)
                     (funcall cont output)
                     (delete-file outfile))
                 (pr "WTF!\n" output)
                 (delete-file outfile)))
    in
    (process-send-string proc c-code)
    (process-send-eof proc)))

;; (c-eval 'long 
;;         "({ struct timeval t; gettimeofday(&t, NULL); srandom(t.tv_usec); random(); })"
;;         'pr)

(provide 'c-eval)
