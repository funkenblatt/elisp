;; -*- lexical-binding: t; -*-

(defun cycle-perm (c)
  (do-wad 
   (mapcar* 'cons c (cdr c))
   (cons (cons (lastcar c) (car c)) _)))

(defun cycles->perm (cycles)
  "Convert a permutation in disjoint cycle notation into a permutation map"
  (reduce
   'compose-perms
   (mapcar 'cycle-perm cycles)))

(defun permute (perm)
  (orfn (cut chain-ref perm <>) 'identity))

(defun compose-perms (a b)
  "Apply permutation B after permutation A"
  (do-wad
   (append
    (mapcar (cut assoc <> b)
            (set-difference (mapcar 'car b) (mapcar 'cdr a)))
    (mapcar (juxtcons 'car (comp (permute b) 'cdr)) a))
   (lc x x _ (not (equal (car x) (cdr x))))))

(defun perm-sort (perm)
  (do-wad 
   perm copy-seq 
   (sort* _ 'string< :key (comp 'str 'car))))

(defun perm-equal (a b)
  "Return true if the two permutation maps are equivalent."
  (equal (perm-sort a) (perm-sort b)))

(defun compose-cycles (a b)
  (get-cycles (compose-perms (cycle-perm a) (cycle-perm b))))

(defun find-one-cycle (perm node start)
  (do-wad
   (funcall perm node)
   (if (equal _ start) 
       (list node)
     (cons node (find-one-cycle perm _ start)))))

(defun get-cycles (perm)
  (if perm
      (do-wad
       (find-one-cycle (permute perm) (caar perm) (caar perm))
       (cons _ (get-cycles (lc x x perm (not (member (car x) _))))))))

(defun compose-perms* (&rest perms)
  (reduce 'compose-perms perms))

(defun invert-perm (perm)
  (mapcar (juxtcons 'cdr 'car) perm))

(defun commutate (&rest perms)
  (apply 'compose-perms*
         (append perms (mapcar 'invert-perm perms))))

(setf f-perm (cycles->perm '((fu fr fd fl) (ful fur fdr fdl)))
      u-perm (cycles->perm '((fu ul bu ur) (ful bul bur fur)))
      r-perm (cycles->perm '((fr ur br dr) (fur bur bdr fdr)))
      d-perm (cycles->perm '((fd dr bd dl) (fdl fdr bdr bdl)))
      l-perm (cycles->perm '((ul fl dl bl) (ful fdl bdl bul)))
      b-perm (cycles->perm '((bu bl bd br) (bul bdl bdr bur)))
      slice-towards (cycles->perm '((fu fd bd bu) (f d b u)))
      slice-away (invert-perm slice-towards)
      slice-right (cycles->perm '((u r d l) (ur dr dl ul)))
      slice-left (invert-perm slice-right)
      slice-up (cycles->perm '((f r b l) (fr br bl fl)))
      slice-down (invert-perm slice-up)
      )

(defmacro rubik-move (&rest body)
  `(let: ,@(apply 'append
                  (lc (list x (symbol-cat x '-perm) 
                            (symbol-cat x '-) (list 'invert-perm 
                                                    (symbol-cat x '-perm)))
                      x '(l u r d f b))) in
     (do-wad ,@body)))

(defmacro $ (&rest body)
  `(rubik-move (compose-perms* ,@body) get-cycles))

(defun compose-cycles (&rest cycles)
  (get-cycles (apply 'compose-perms* (mapcar 'cycles->perm cycles))))
