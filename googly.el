;; -*- lexical-binding: t; -*-
(require 'json)
(require 'http-client)
(require 'http-junk)

(defun urlencode-query (query)
  (strjoin "+" (mapcar 'url-hexify-string
                       (split-string query))))

(defun jfgi (query)
  (interactive "sSearch for: ")
  (cps> (junk) (get-url
	       (str "http://www.google.com/uds/GwebSearch?q="
		    (urlencode-query query)
		    "&v=1.0") :cont!)
    (ec (dump (chain-ref x 'unescapedUrl) "\n") x 
	(chain-ref (json-read-from-string junk) 
		   'responseData 'results))))

(defun geocode (addr cont)
  (setq addr (strjoin "+" (split-string addr)))
  (get-url
   (format "http://maps.google.com/maps/api/geocode/json?address=%s&sensor=false"
	   addr)
   cont))

(defun get-relevant-crap (json)
  (do-wad 
   (json-read-from-string json)
   (aref (chain-ref _ 'results) 0)
   (dump "'latitude': " (chain-ref _ 'geometry 'location 'lat) ", "
	 "'longitude': " (chain-ref _ 'geometry 'location 'lng))))


;; (geocode "Ashby BART station"
;; 	 (λ (x)
;; 	    (do-wad
;; 	     (chain-ref (json-read-from-string x)
;; 			'results)
;; 	     (aref _ 0)
;; 	     (chain-ref _ 'geometry 'location)
;; 	     (dump _))))


(defun* mappity (lat long &key (zoom 14) (width 800) (height 600))
  (cps> (mapdata) (get-url (str "http://staticmap.openstreetmap.de/staticmap.php?center="
			       lat "," long "&zoom=" (or zoom 14) 
			       "&size=" width "x" height "&maptype=mapnik")
			  :cont!)
    (filedump mapdata "/tmp/map.png")
    (find-file "/tmp/map.png")))

(defun hnsearch (query &optional params)
  (interactive "sQuery: ")
  (cps> (junk) (get-url (str "http://api.thriftdb.com/api.hnsearch.com/items/_search?"
                            (urlencode-alist `((q . ,query) ,@params)))
                       :cont!)
    (dump-hn-results (json-read-from-string junk))))

(str* "=" 60)

(defun abbreviate-string (s)
  (if (> (length s) 60)
      (str (substring s 0 57) "...")
    s))

(defun make-item-url (item-id)
  (str "http://news.ycombinator.com/item?id="
       (if (numberp item-id) 
           item-id
         (str~ "^[^-]*" item-id))))

(defun dump-hn-results (json)
  (let: results (get-assoc json 'results) in
    (ec (cond ((equal (chain-ref result 'item 'type) "submission")
               (dump (chain-ref result 'item 'title) "\n"
                     (make-item-url (chain-ref result 'item '_id)) "\n"
                     (chain-ref result 'item 'url) "\n"
                     (chain-ref result 'item 'num_comments) " comments"
                     "\n\n"))
              ((equal (chain-ref result 'item 'type) "comment")
               (dump "Comment on "
                     (chain-ref result 'item 'discussion 'title) "\n"
                     (chain-ref result 'item 'text) "\n"
                     (make-item-url
                      (chain-ref result 'item 'discussion 'id))
                     "\n\n")))
        result results)))

(provide 'googly)
