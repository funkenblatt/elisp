;; Reaching into git's packfiles.
(require 'flate)
(require 'unpacker)

(defun git-strcmp (a b)
  (cond ((string< a b) -1)
	((string= a b) 0)
	(t 1)))

(defun git-pack-bsearch (v key cmp start end)
  (catch 'break
    (while (> (- end start) 1)
      (let: mid (/ (+ start end) 2)
	    c (funcall cmp key (aref v mid)) in
	(cond ((= c 0) (throw 'break mid))
	      ((> c 0) (setq start mid))
	      ((< c 0) (setq end mid)))))
    (if (= (funcall cmp key (aref v start)) 0)
	start)))

(defun read-git-obj-header (s ix)
  (let: c (aref s ix)
	type (logand (lsh c -4) #b111)
	size (logand c #xf)
	shift 4 in
    (while (/= (logand c #x80) 0)
      (incf ix)
      (setq c (aref s ix))
      (incf size (ash (logand c #x7f) shift))
      (incf shift 7))
    (list type size (+ ix 1))))

(defun git-ofs-delta-size (s ix)
  (let: c (aref s ix)
	out (logand c #x7f) in
    (while (/= (logand c #x80) 0)
      (incf out)
      (setq out (lsh out 7)
	    c (aref s (incf ix))
	    out (+ out (logand c #x7f))))
    (list out (1+ ix))))

(defun strhex (s)
  (apply 'concat (lc (format "%02x" x) x s)))

(defun show-hash-at-point ()
  (interactive)
  (if current-prefix-arg
      (shell-command (str "git show --raw " (strhex (read/die)) " | wc"))
    (shell-command (str "git show --raw " (strhex (read/die))))))

(defun git-find-ofs-delta-root (s ix)
  "Takes the index of a git delta object header, traverses the chain of
backwards deltas."
  (let: smeg-info (read-git-obj-header s ix)
	delta-offset nil in
    (while (= (car smeg-info) 6)
      (setq delta-offset 
	    (git-ofs-delta-size s (caddr smeg-info))
	    smeg-info (read-git-obj-header 
		       s (decf ix (car delta-offset)))))
    smeg-info))

(defun git-pack-sha1-lookup (pack-idx hash)
  (let: h1 (aref hash 0)
	fantab (get-assoc pack-idx 'fanout)
	start (if (= h1 0) 0 (aref fantab (- h1 1)))
	end (if (= h1 #xff) (get-assoc pack-idx 'size) (aref fantab h1)) in
    (git-pack-bsearch
     (get-assoc pack-idx 'sha1) hash 'git-strcmp start end)))

(defun git-pack-get (packfile pack-idx hash)
  (let: ix (git-pack-sha1-lookup pack-idx hash)
	offset (and ix (aref (get-assoc pack-idx 'offset) ix)) in
    (when ix
      (read-git-obj-header packfile offset))))

(defun git-loose-get (hash)
  "Assume we're starting in the directory containing all the git shits.
Find the loose object referred to by hash, return it as a deflated string."
  (let: coding-system-for-read 'binary
	dir (substring hash 0 2)
	filename (str "objects/" dir "/"
		      (substring hash 2)) in
    (if (file-exists-p filename)
	(with-temp-buffer
	  (call-process
	   "dflate" filename t)
	  (buffer-string))
      nil)))

(local-set-key (kbd "M-RET") 'show-hash-at-point)

(setq packix (make-unpacker
	      '((magic vec 2 u32)
		(fanout vec 255 u32)
		(size u32)
		(sha1 vec size str 20)
		(crc vec size u32)
		(offset vec size u32))))

(setq packwad (funcall packix (filestr "~/elisp/.git/objects/pack/pack-d366ac73b90d2e4a8a0275854288f69fcd40c0d4.idx")))

(progn (setq packfile (filestr "~/elisp/.git/objects/pack/pack-d366ac73b90d2e4a8a0275854288f69fcd40c0d4.pack")) nil)

;; ((commit . 1) (tree . 2) (blob . 3) (tag . 4) (ofs-delta . 6) (ref-delta . 7))
