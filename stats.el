(defun daysecs (tm)
  (/ (* 24.0 
	(sum (lc* (* x y)
		  (x y)
		  ((popn (decode-time tm) 3)
		   '(3600 60 1)))))
     86400))

(defun dayofweek (tm)
  (+ (* 24.0 (elt (decode-time tm) 6)) (daysecs tm)))

(defun get-tetris-scoretimes (&optional proc)
  (if (not proc) (setq proc 'daysecs))
  (lc (apply 'scratchdump 
	     (append (join x " ") '("\n"))) 
      x 
      (sort 
       (zip 
	(lc (funcall proc (time-add x '(0 25200))) x (grab-tetris-dates))
	(grab-tetris-scores))
       (lambda (a b) (< (car a) (car b))))))

(defun grab-date ()
  (foolet (digit "[0-9]"
	   time-rx (apply 'concat (join (lc (str* digit 2) x (range 3)) ":"))
	   date-rx (concat "[A-Z][a-z][a-z] +[0-9]+ " time-rx " " (str* digit 4)))
	  (if (re-search-forward date-rx nil t)
	      (match-string 0)
	    nil)))

(defun grab-tetris-dates ()
  (foolet (out '() date "")
	  (save-excursion
	    (goto-char (point-min))
	    (while (setq date (grab-date))
	      (push (date-to-time date) out)))
	  out))

(defun binwad (data binsize bstart)
  (foolet (dmax (apply 'max data)
	   bins (make-vector (+ (/ (- dmax bstart) binsize) 1) 0))
	  (dolist (i data) (vinc bins (/ (- i bstart) binsize)))
	  bins))

(defun showbins (data binsize binstart &optional cum)
  (foolet (bins (binwad data binsize binstart)
	   nslen (funcomp length number-to-string)
	   maxbin (max (funcall nslen binstart)
		       (funcall nslen (+ binstart (* (length bins) binsize))))
	   fmt (format "%%%dd ~ %%%dd |" maxbin maxbin))
	  (if cum (setq bins (cumulate bins)))
	  (dotimes (i (length bins))
	    (fdump fmt
		   (+ binstart (* binsize i))
		   (+ binstart (* binsize (+ i 1))))
	    (dotimes (j (aref bins i))
	      (dump "*"))
	    (dump "\n"))))

(defun cumulate (bins)
  (foolet (out (make-vector (length bins) 0) sum 0)
	  (dotimes (i (length bins))
	    (incf sum (aref bins i))
	    (aset out i sum))
	  out))

(defun percentiles (data)
  (setq data (apply 'list data))
  (sort data '<)
  (setq data (apply 'vector data))
  (foolet (tot (length data)
	   out '())
	  (for (1 i 11)
	    (push (cons (* 10 i) 
			(+ 1 (aref data (rpn tot i * 10 / 1 -))))
		  out))
	  out))

(defun grab-tetris-scores ()
  (save-excursion
    (foolet (stats '())
	    (goto-char (point-min))
	    (while (re-search-forward "^[0-9]+" nil t)
	      (push (string-to-number (match-string 0)) stats))
	    stats)))

(defun grab-tetris-stats ()
  (interactive)
  (save-excursion
    (foolet (stats (grab-tetris-scores))
      (switch-to-buffer (get-buffer-create "*Tetris Stats*"))
      (erase-buffer)
      (showbins stats 500 0)
      (dump "\n")
      (lc (fdump "%3d%%: %d\n" (car x) (cdr x)) x (percentiles stats)))))

(provide 'stats)
