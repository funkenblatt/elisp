;; -*- lexical-binding: t; -*-
(require 'json)

(defun eljs-maybe-add-gensym (expr thunk)
  (let ((v (if (symbolp expr) expr (gensym))))
    (>>> (funcall thunk v)
         (if (symbolp expr) _ (cons (list v expr) _)))))

(defun eljs-destructure (bindings expr)
  (cond
   ((vectorp bindings)
    (eljs-maybe-add-gensym expr
      (λ (v)
         (cl-mapcan (λ (b ix) (eljs-destructure b `(aref ,v ,ix)))
                    bindings (range (length bindings))))))
   ((consp bindings)
    (eljs-maybe-add-gensym expr
      (λ (v)
         (>>> bindings cdr sublist2
              (cl-mapcan
               (λ ((key b))
                  (>>>
                   (if (symbolp key) `(-> ,v ,key) `(aref ,v ,key))
                   (eljs-destructure b _)))
               _)))))
   ((symbolp bindings) (list (list bindings expr)))))

(defun eljs-arglist-and-header (form)
  (dbind (junk args &rest body) form
    (let ((arg-mapping (mapcar 
                        (x: (cons x (if (symbolp x) x (gensym))))
                        args)))
      `(lambda ,(mapcar (cut assoc-default <> arg-mapping) args)
         ,(>>> (filter (comp 'not 'symbolp) args)
               (mapcar (juxt 'identity (cut assoc-default <> arg-mapping)) _)
               `(let ,_ ,@body))))))

(defun to-js-obj (form context)
  (str "{"
       (strjoin
        ","
        (mapcar
         (lambda (x)
           (str (to-js (car x) '(expression))
                ":" (to-js (cadr x) '(expression))))
         (sublist-n (cdr form) 2)))
       "}"))

(defun to-js-quote (form context)
  (if (symbolp (cadr form)) 
      (repr (str (cadr form)))
    (json-encode (cadr form))))

(defun to-js-infixer (operator)
  (lambda (form context)
    (to-js-math form context operator)))

(defun to-js-maybe-wrap-return (s context)
  (if (memq 'tail context)
      (str "return " s)
    s))

(defun to-js-math (form context &optional operator)
  (to-js-maybe-wrap-return
   (pcase form
     (`(- ,arg) (str "-" (to-js arg '(expression))))
     (_ (let* ((operator (or operator (car form)))
               (needs-paren (or (and (memq operator '(+ -))
                                     (intersection '(* / %) context))
                                (member operator '(and or " instanceof "))
                                (intersection '(aref dot) context))))
          (str (if needs-paren "(" "")
               (strjoin (str operator) 
                        (mapcar (lambda (x)
                                  (to-js x (cons operator '(expression))))
                                (cdr form)))
               (if needs-paren ")" "")))))
   context))

(defun to-js-not (form context)
  (pcase form
    (`(not ,x) (str "(!(" (to-js x '(expression)) "))"))))

(defun to-js-setf (form context)
  (strjoin 
   ";\n"
   (mapcar
    (lambda (x)
      (str (to-js (car x) '(expression lvalue)) "="
           (to-js (cadr x) '(expression))))
    (sublist-n (cdr form) 2))))

(defun to-js-operatorf (operator)
  (lambda (form context)
    (str (to-js (cadr form) '(expression lvalue))
         operator
         (to-js (Caddr form) '(expression)))))

(defun to-js-incf (form context)
  (if (cddr form)
      (str (to-js (cadr form) '(expression lvalue)) 
           (if (eq (car form) 'incf) "+=" "-=")
           (to-js (caddr form) '(expression)))
    (str (if (eq (car form) 'incf) "++" "--") 
         "(" (to-js (cadr form) '(expression lvalue)) ")")))

(defun to-js-lambdify (form context)
  (to-js `((lambda () ,form)) context))

(defun to-js-for (form context)
  (if (memq 'expression context)
      (to-js-lambdify form context)
    (str "for ("
         (to-js (car (cadr form)) '(expression for)) "; "
         (to-js (cadr (cadr form)) '(expression for)) "; "
         (to-js (caddr (cadr form)) '(expression for))
         ") {\n"
         (to-js (cons 'progn (cddr form)) '(statement for))
         "\n}\n")))

(defun to-js-constant-p (form)
  (or (numberp form)
      (stringp form)
      (and (consp form) (eq (car form) 'quote))))

(defun to-js-dot (form context)
  (if (null (cdddr form))
      (cond
       ((to-js-constant-p (caddr form))
        (to-js-aref form context))
       ((consp (caddr form))
        (to-js `((-> ,(cadr form) ,(car (caddr form)))
                 ,@(cdr (caddr form)))
               '(expression)))
       ((vectorp (caddr form))
        (to-js `(aref ,(cadr form) ,(aref (caddr form) 0)) '(expression)))
       ((symbolp (caddr form))
        (str (to-js (cadr form) '(expression dot)) "." 
             (to-js (caddr form) '(expression)))))
    (to-js
     `(-> (-> ,(cadr form) ,(caddr form)) ,@(cdddr form))
     '(expression))))

(defun to-js-aref (form context)
  (str (to-js (cadr form) '(expression aref))
       "[" (to-js (caddr form) '(expression)) "]"))

(defun to-js-cons (form context)
  (iflet fun (gethash (car form) to-js-special-forms)
         (funcall fun form context)
         (funcall (to-js-wrap-return 'to-js-funcall) form context)))

(defun to-js-let (form context)
  (if (or (memq 'expression context) (memq 'toplevel context))
      (to-js-lambdify form context)
    (>>> form cadr (mapcan (cur 'apply 'eljs-destructure) _)
         (mapcar (x: (interp "var #,(to-js (car x)) = #,(to-js (cadr x) '(expression));\n")) _)
         (str (strjoin "" _) (to-js (cons 'progn (cddr form)) context)))))

(defun to-js-funcall (form context)
  (let ((fun (to-js (car form) '(expression function-call)))
        (args (mapcar
               (lambda (x) 
                 (to-js x '(expression function-arg)))
               (cdr form))))
    (str fun "(" (strjoin ", " args) ")")))

(defun to-js-return (form context)
  (if (memq 'expression context)
      (error "Can't return in expression context!")
    (str "return " (to-js (cadr form) '(expression)))))

(defun flatten-progns (expressions)
  (mapcan (lambda (x)
            (if (and (consp x) (eq (car x) 'progn))
                (flatten-progns (cdr x))
              (list x)))
          expressions))

(defun to-js-maybe-semicolon (s)
  (if (string-match (rx bol (any ";}") (* (any " \t\r\n")) eos) s)
      ""
    ";"))

(defun to-js-progn (form context)
  (let* ((exprs (flatten-progns (cdr form)))
         (side-effect-exprs (mapcar (lambda (x) (to-js x (remove 'tail context)))
                                    (butlast exprs)))
         (last-expr (to-js (car (last exprs)) context)))
    (str (strjoin (if (memq 'statement context) 
                      ";\n"
                    ", ")
                  side-effect-exprs) 
         (if side-effect-exprs
             (if (memq 'statement context) ";\n" ", ") 
           "")
         last-expr
         (if (and exprs (memq 'statement context)) 
             (to-js-maybe-semicolon last-expr) ""))))

(defun to-js-function (name form context)
  (let* ((form (eljs-arglist-and-header form))
         (arg-list (strjoin ", " (mapcar 'to-js-sym (cadr form))))
         (body (to-js (cons 'progn (cddr form)) '(statement tail)))
         (needs-parens (or (memq 'function-call context)
                           (memq 'dot context)
                           (and (null name)
                                (memq 'statement context)))))
    (str (if needs-parens "(" "")
         "function " (or (and name (to-js-sym name)) "")
         " (" arg-list ") {\n"
         body "\n"
         "}"
         (if needs-parens ")" ""))))

(defun to-js-lambda (form context)
  (to-js-function nil form context))

(defun to-js-defun (form context)
  (to-js-function (cadr form) 
                  (cons 'lambda (cddr form))
                  context))

(defun to-js-if (form context)
  (if (and (memq 'statement context)
           (not (memq 'toplevel context)))
      (str (interp "if (#,(to-js (cadr form) '(expression))) {\n"
                   "  #,(to-js (list 'progn (caddr form)) context)\n"
                   "}\n")
           (if (cadddr form)
               (interp "else {\n"
                       "  #,(to-js (list 'progn (cadddr form)) context)\n"
                       "}\n")
             ""))
    (str (if (memq 'tail context) "return " "")
         (if (intersection '(aref dot) context) "(" "")
         (interp "(#,(to-js (cadr form) '(expression)) ? "
                 "#,(to-js (caddr form) '(expression)) : "
                 "#,(to-js (cadddr form) '(expression)))")
         (if (intersection '(aref dot) context) ")" ""))))

(defun to-js-wrap-return (fun)
  (lambda (form context)
    (if (memq 'tail context)
        (str "return " (funcall fun form context))
      (funcall fun form context))))

(defun to-js-wrap-returnify (sym)
  (fset sym (to-js-wrap-return (symbol-function sym))))

(defun to-js-str (form context) (json-encode form))

(defun cap-first (s) (str (upcase (substring s 0 1)) (substring s 1)))

(defun to-js-camelize (s)
  (let ((parts (split-string s "-")))
    (apply 'concat (car parts)
           (mapcar 'cap-first (cdr parts)))))

(defun to-js-sym (form &optional context)
  (if form
      (>>> form symbol-name to-js-camelize
           (replace-regexp-in-string "!" "__BANG__" _))
    "null"))

(defun to-js-num (form context)
  (str form))

(defun to-js-vec (form context)
  (>>> (mapcar (cut to-js <> '(expression)) form)
       (strjoin ", " _)
       (interp "[#,_]")))

(defun to-js-try (form context)
  (if (memq 'expression context)
      (to-js-lambdify form context)
    (let: pred (orfn (comp 'not 'consp) (comp 'not (in '(catch finally)) 'car))
          body (seq-take-while pred (cdr form))
          catches (>>> (seq-drop-while pred (cdr form))
                       (mapcar (λ (stuff)
                                  (pcase stuff
                                    (`(catch ,var . ,body)
                                     (interp "catch (#,var) { #,(to-js (cons 'progn body) context) }"))
                                    (`(finally . ,body)
                                     (interp "finally { #,(to-js (cons 'progn body) '(expression)) }"))))
                               _)
                       (strjoin "\n" _)) in
      (interp
       "try {
#,(to-js (cons 'progn body) context)
} #,catches"))))

(defun to-js (form &optional context)
  (if (not context) (setf context '(statement toplevel)))
  (iflet fun (gethash (type-of form) to-js-compile-funs)
         (funcall fun form context)
         (error "Don't know how to compile" form)))

(defmacro defmacro-eljs (name params &rest body)
  (let: form (gensym) context (gensym) in
   `(setf (gethash ',name to-js-special-forms)
          (lambda (,form ,context)
            (to-js 
             (apply (lambda ,params ,@body) (cdr ,form))
             ,context)))))

(defun to-js-new (form &optional context)
  (interp "new #,(to-js (cadr form) '(expression))"))

(defun to-js-for-in (form &optional context)
  (dbind (junk (var expr) &rest body) form
    (interp "for (var #,(to-js var nil) in #,(to-js expr '(expression for))) "
            "{\n#,(to-js (cons 'progn body) '(statement for))\n}")))

(defun to-js-in (form &optional context)
  (pcase form
    (`(in ,key ,obj)
     (interp "#,(to-js key '(expression)) in #,(to-js obj '(expression))"))
    (_ (error "Invalid in expression!"))))

(defun to-js-defvar (form &optional context)
  (interp "var #,(to-js (cons 'setf (cdr form)) context)"))

(defmacro indented-js (expr)
  "Make some nicely indented JS."
  `(with-temp-buffer
     (javascript-mode)
     (insert (to-js ',expr '(statement toplevel)))
     (indent-region (point-min) (point-max))
     (buffer-string)))

(setf to-js-compile-funs
      (dict 'cons 'to-js-cons
            'vector (to-js-wrap-return 'to-js-vec)
            'string (to-js-wrap-return 'to-js-str)
            'symbol (to-js-wrap-return 'to-js-sym)
            'integer (to-js-wrap-return 'to-js-num)
            'float (to-js-wrap-return 'to-js-num)))

(setf to-js-special-forms
      (dict 'if 'to-js-if
            'let 'to-js-let
            'let* 'to-js-let
            'lambda (to-js-wrap-return 'to-js-lambda)
            'λ (to-js-wrap-return 'to-js-lambda)
            'defun (to-js-wrap-return 'to-js-defun)
            'for 'to-js-for
            'for-in 'to-js-for-in
            'in 'to-js-in
            'progn 'to-js-progn
            'return 'to-js-return
            'quote (to-js-wrap-return 'to-js-quote)
            'aref (to-js-wrap-return 'to-js-aref)
            '-> (to-js-wrap-return 'to-js-dot)
            '{} (to-js-wrap-return 'to-js-obj)
            'incf 'to-js-incf
            'decf 'to-js-incf
            'setf 'to-js-setf
            'not 'to-js-not
            'new (to-js-wrap-return 'to-js-new)
            '< 'to-js-math
            '> 'to-js-math
            '>= 'to-js-math
            '<= 'to-js-math
            '== 'to-js-math
            '=== 'to-js-math
            'eq (to-js-infixer "===")
            '= (to-js-infixer "===")
            '!= (to-js-infixer "!=")
            'and (to-js-infixer "&&")
            'or (to-js-infixer "||")
            'instanceof (to-js-infixer " instanceof ")
            'try 'to-js-try
            '+ 'to-js-math
            '- 'to-js-math
            '* 'to-js-math
            '/ 'to-js-math
            '% 'to-js-math
            'defvar 'to-js-defvar))

(defmacro-eljs >>> (&rest shits)
  `((lambda ()
      (let ((_ ,(car shits)))
        ,@(mapcar (x: (if (symbolp x)
                          `(setf _ (,x _))
                        `(setf _ ,x))) (cdr shits))
        _))))

(defmacro-eljs λ (&rest shit)
  `(lambda ,@shit))

(defun dotted-p (x)
  (if (or (stringp x) (numberp x) (vectorp x))
      t
    (pcase x
      (`((,_ . ,_) . ,_) nil)
      (`(,x . ,junk) (dotted-p x))
      (x (and (symbolp x) (not (eq x '..)) (startswith (symbol-name x) "."))))))

(defun undottify (x)
  (pcase x
    (`(,x . ,junk) `(,(undottify x) ,@junk))
    ((pred symbolp) (>>> x symbol-name (substring _ 1) intern))
    (x x)))

(defun not-dotted-clause (item other)
  (pcase other
    (`(,x . ,more) `(,x ,item ,@more))
    (x `(,x ,item))))

(defmacro-eljs .. (&rest stuff)
  (pcase stuff
    (`(,x) x)
    (`(,x ,y) (if (dotted-p y)
                  `(-> ,x ,(undottify y))
                (not-dotted-clause x y)))
    (`(,x ,y . ,more)
     `(.. (.. ,x ,y) ,@more))))

(defmacro-eljs cond (&rest stuff)
  (pcase stuff
    (`((,pred . ,then)) `(if ,pred (progn ,@then)))
    (`((,pred . ,then) . ,more) 
     `(if ,pred (progn ,@then) 
        (cond ,@more)))))

(defmacro-eljs dokeys (&rest body)
  `(keymap 
    ({} ,@(mapcan (λ ((key action)) (list key `(λ () ,action)))
                  (sublist2 body)))))

(defmacro-eljs @ (attr)
  (>>> (gensym) `(λ (,_) (-> ,_ ,attr))))

(defmacro-eljs @> (attr)
  (>>> (gensym) `(λ (,_) (-> ,_ (,attr)))))

(defmacro-eljs x: (&rest body)
  `(λ (x) ,@body))

(defmacro-eljs :: (&rest body) `({} ,@body))

(defmacro-eljs doto (item &rest actions)
  (let ((varname (gensym)))
    `((lambda ()
        (let ((,varname ,item))
          ,@(mapcar (cur 'list '.. varname) actions)
          ,varname)))))

(defmacro-eljs interp (&rest clauses)
  (>>> (macroexpand `(interp ,@clauses))
       cdr (cons '+ _)))

(defmacro-eljs cut (&rest stuff)
  (cadr (macroexpand `(cut ,@stuff))))

(defmacro-eljs >>> (&rest stuff)
  (macroexpand `(>>> ,@stuff)))

(defmacro eljs-use-macro (macro-sym)
  `(defmacro-eljs ,macro-sym (&rest args)
     (macroexpand (cons ',macro-sym args))))

(defmacro-eljs nlet (name bindings &rest body)
  `(let ((,name nil))
     (setf ,name (lambda ,(mapcar 'car bindings)
                   ,@body))
     (,name ,@(mapcar 'cadr bindings))))

(eljs-use-macro cps-let)

(defun eljs-bind ()
  (interactive)
  (local-set-key (kbd "M-RET")
    (keycmd 
     (>>> (preceding-sexp) to-js (interp "#,_;\n")
          (process-send-string mozrepl _))))

  (local-set-key (kbd "<C-return>")
    (keycmd 
     (>>> (preceding-sexp) to-js (interp "#,_;\n") (progn (kill-new _) (pr _)))))

  (local-set-key (kbd "<S-return>")
    (keycmd
     (let: insert-p current-prefix-arg
           buf (current-buffer) in
       (>>> (preceding-sexp) 
            (eljs->mozrepl 
             _ (x: (setf last-mozrepl-data x)
                   (if insert-p 
                       (pp x buf)
                     (pp x)))))))))

(provide 'eljs)
