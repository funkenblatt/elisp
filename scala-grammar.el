qual-id := id (* "." id)
ids := id (* "," id)
path := stable-id | (maybe id ".") "this"
stable-id := id | path "." id | (maybe id ".") "super" (maybe class-qualifier) "." id
class-qualifier := "[" id "]"
type := function-arg-types "=>" type | infix-type (maybe existential-clause)
function-arg-types := infix-type | "(" (maybe param-type (* "," param-type)) ")"
existential-clause := "forSome" "{" existential-dcl (* semi existential-dcl) "}"
existential-dcl := "type" type-dcl | "val" val-dcl
infix-type := compound-type (* id (maybe nl) compound-type)
compound-type := annot-type (* "with" annot-type) (maybe refinement) | refinement
annot-type := simple-type (* annotation)
simple-type := simple-type type-args | simple-type "#" id | stable-id | path "." "type" | "(" types ")"
type-args := "[" types "]"
types := type (* "," type)
refinement := (maybe nl) "{" refine-stat (* semi refine-stat) "}"
refine-stat := dcl | "type" type-def | ""
type-pat := type
ascription := ":" infix-type | ":" annotation (* annotation) | ":" "_" "*"
expr := (or bindings (: (maybe "implicit") id) "_") "=>" expr | expr1
(expr1 "if" "(" expr ")" (* nl) expr (maybe (maybe semi) "else" expr) |
       "while" "(" expr ")" (* nl) expr |
       "try" "{" block "}" (maybe "catch" "{" case-clauses "}") (maybe "finally" expr) |
       "do" expr [semi] "while" "(" expr ")" |
       "for" (or (: "(" enumerators ")") (: "{" enumerators "}")) (* nl) ["yield"] expr |
       "throw" expr |
       "return" [expr] |
       [simple-expr "."] id "=" expr |
       simple-expr1 argument-exprs "=" expr |
       postfix-expr |
       postfix-expr ascription |
       postfix-expr "match" "{" case-clauses "}")
postfix-expr := infix-expr [id [nl]]
infix-expr := prefix-expr | infix-expr id [nl] infix-expr
prefix-expr := [(or "-" "+" "~" "!")] simple-expr
simple-expr := "new" (or class-template template-body) | block-expr | simple-expr1 ["_"]
simple-expr1 := literal | path | "_" | "(" [exprs] ")" | simple-expr "." id | simple-expr type-args | simple-expr1 argument-exprs | xml-expr
exprs := expr (* "," expr)
argument-exprs := "(" [exprs] ")" | "(" [exprs ","] postfix-expr ":" "_" "*" ")" | [nl] block-expr
block-expr := "{" case-clauses "}" | "{" block "}"
block := (* block-stat semi) [result-expr]
block-stat := import | (* annotation) [(or "implicit" "lazy")] def | (* annotation) (* local-modifier) tmpl-def | expr1 | ""
result-expr := expr1 | (or bindings (: (or (: ["implicit"] id) "_") ":" compound-type)) "=>" block
enumerators := generator (* semi enumerator)
enumerator := generator | guard | "val" pattern1 "=" expr
generator := pattern1 "<-" expr [guard]
case-clauses := case-clause (* case-clause)
case-clause := "case" pattern [guard] "=>" block
guard := "if" postfix-expr
pattern := pattern1 (* "|" pattern1)
pattern1 := varid ":" type-pat | "_" ":" type-pat | pattern2
pattern2 := varid ["@" pattern3] | pattern3
pattern3 := simple-pattern | simple-pattern (* id [nl] simple-pattern)
(simple-pattern  "_" | varid | literal | stable-id | stable-id "(" [patterns] ")" | 
                 stable-id "(" [patterns ","] [varid "@"] "_" "*" ")" |
                 "(" [patterns] ")" | xml-pattern)
patterns := pattern ["," patterns] | "_" "*"
type-param-clause := "[" variant-type-param (* "," variant-type-param) "]"
fun-type-param-clause := "[" type-param (* "," type-param) "]"
variant-type-param := (* annotation) [(or "+" "-")] type-param
type-param := (or id "_") [type-param-clause] [">:" type] ["<:" type] (* "<%" type) (* ":" type)
param-clauses := (* param-clause) [[nl] "(" "implicit" params ")"]
param-clause := [nl] "(" [params] ")"
params := param (* "," param)
param := (* annotation) id [":" param-type] ["=" expr]
param-type := type | "=>" type | type "*"
class-param-clauses := (* class-param-clause) [[nl] "(" "implicit" class-params ")"]
class-param-clause := [nl] "(" [class-params] ")"
class-params := class-param (* "" class-param)
class-param := (* annotation) [(* modifier) (or "val" "var")] id ":" param-type ["=" expr]
bindings := "(" binding (* "," binding ")")
binding := (or id "_") [":" type]
modifier := local-modifier | access-modifier | "override"
local-modifier := "abstract" | "final" | "sealed" | "implicit" | "lazy"
access-modifier := "[" (or id "this") "]"
annotation := "@" simple-type (* argument-exprs)
constr-annotation := "@" simple-type argument-exprs
name-value-pair := "val" id "=" prefix-expr
template-body := [nl] "{" [self-type] template-stat (* semi template-stat) "}"
template-stat := import | (* annotation [nl]) (* modifier) def | (* annotation nl) (* modifier) dcl | expr | ""
self-type := id [":" type] "=>" | "this" ":" type "=>"
import := "import" import-expr (* "," import-expr)
import-expr := stable-id "." (or id "_" import-selectors)
import-selectors := "{" (* import-selector "<") (or import-selector "_") "}"
import-selector := id [(or (: "=>" id) (: "=>" "_"))]
dcl := "val" val-dcl | "var" var-dcl | "def" fun-dcl | "type" (* nl) type-dcl
val-dcl := ids ":" type
var-dcl := ids ":" type
fun-dcl := fun-sig [":" type]
fun-sig := id [fun-type-param-clause] param-clauses
type-dcl := id [type-param-clause] [">:" type] ["<:" type]
pat-var-def := "val" pat-def | "var" var-def
def := pat-var-def | "def" fun-def | "type" (* nl) type-def | tmpl-def
pat-def := pattern2 (* "," pattern2) [":" type] "=" expr
var-def := pat-def | ids ":" type "=" "_"
fun-def := fun-sig [":" type] "=" expr | fun-sig [nl] "{" block "}" | "this" param-clause param-clauses (or (: "=" constr-expr) (: [nl] constr-block))
type-def := id [type-param-clause] "=" type
tmpl-def := ["case"] "class" class-def | ["case"] "object" object-def | "trait" trait-def
class-def := id [type-param-clause] (* constr-annotation) [access-modifier] class-param-clauses class-template-opt
trait-def := id [type-param-clause] trait-template-opt
object-def := id class-template-opt
class-template-opt := "extends" class-template | [["extends"] template-body]
trait-template-opt := "extends" trait-template | [["extends"] template-body]
class-template := [early-defs] class-parents [template-body]
trait-template := [early-defs] trait-parents [template-body]
class-parents := constr (* "with" annot-type)
trait-parents := annot-type (* "with" annot-type)
constr := annot-type (* argument-exprs)
early-defs := "{" [early-def (* semi early-def)] "}" "with"
early-def := (* annotation [nl]) (* modifier) pat-var-def
constr-expr := self-invocation | constr-block
constr-block := "{" self-invocation (* semi block-stat) "}"
self-invocation := "this" argument-exprs (* argument-exprs)
top-stat-seq := top-stat (* semi top-stat)
top-stat := (* annotation [nl]) (* modifier) tmpl-def | import | packaging | package-object | ""
packaging := "package" qual-id [nl] "{" top-stat-seq "}"
package-object := "package" "object" object-def
compilation-unit := (* "package" qual-id semi) top-stat-seq
