(defrpn polar (r ph th)
  (vector (rpn r th sin * ph sin *)
	  (rpn r ph cos *)
	  (rpn r th cos * ph sin * neg)))


(let* ((r 4.25) (th (rpn pi 8 / 0.9 *)) (ph (rpn 11 pi * 32 /))
       (p2 [0.0 -1.0 12.5])
       (p1 (rpn p2 r ph th polar vec+))
       (p3 (rpn p2 3.5 5 pi * 12 / -0.1 polar vec+))
       (ray-scene-light [0.0 10.0 -5.0]);(vec+ (vec-scale (vec- p1 p2) 3) p2))
       (ray-scene-spheres (list (vector p1 1.2)
				(vector p2 3.0)
				(vector p3 0.75)))
       (width 320) (height 240) (start-time (current-time)))
  (do-wad
   (make-xpm width height (ray-grid width height))
   (create-image _ 'xpm t)
   (insert-image _))
  (pr (time-subtract (current-time) start-time)))
 
;Runtime: (0 4 719422)
