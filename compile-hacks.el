(defun find-undefined-junk ()
  (interactive)
  (do-wad
   (re-findall "undefined reference to `\\([^']*\\)'" 
	       (buffer-string) t 1)
   (mapcar 'read _)
   (delete-duplicates _)
   (if (called-interactively-p)
       (ec (scratchdump x "\n") x _)
     _)))

(defun kill-include (inc)
  (interactive "sInclude: ")
  (shell-command
   (format
    "for i in `projfind.py %s | cut -f 1 -d \":\"`; 
do echo $i; sed '/[\"<]%s/d' $i > $i.temp; mv $i.temp $i; done;"
    inc inc)))

(defun kill-functions ()
  (while (re-search-forward "^{" nil t)
    (unless (save-excursion (re-search-forward "^}")
			    (eq (char-after (point)) ?\;))
      (recenter 2)
      (if (member (read-char "Do it? (y or n)")
		  '(?y ?Y))
	  (delete-region (point)
			 (save-excursion
			   (re-search-forward "^}")
			   (forward-char -1)
			   (point)))))))

(defun for-errors (proc)
  (let: next-error-hook proc
	errjunk nil in
    (while (setq errjunk
		 (condition-case nil
		     (compilation-next-error 1)
		   (error nil)))
      (compile-goto-error))
    (message "Done")))

(defun fooify ()
  (interactive)
  (for-errors
   (lambda ()
     (if (eq (read-event) ?y)
	 (let: line (thing-at-point 'line) in
	   (kill-line)
	   (dump "#ifndef USING_GCC\n"
		 line
		 "#endif\n"))))))

(defun extract-objfile-und (file)
  (do-wad 
   (process-lines 
    "bash" "-c" 
    (str "objdump -t " file " | grep -e UND"))
   (lc (progn (string-match "[^ \t]*$" x) (match-string 0 x)) x _) 
   (filter (lambda (x) (and (not (equal "" x)) (not (startswith x ".")))) _)
   (cddr _)))

(defun extract-objfile-syms (file)
  (do-wad 
   (process-lines 
    "bash" "-c" 
    (str "objdump -t " file " | grep -v -e UND -e ABS"))
   (lc (progn (string-match "[^ \t]*$" x) (match-string 0 x)) x _) 
   (filter (lambda (x) (and (not (equal "" x)) (not (startswith x ".")))) _)
   (cddr _)))

(defun obj-symbols ()
  (with-default-dir project-directory
    (do-wad
     (lc (str (substring x 0 -1) "o") x (get-project-files) (endswith x ".c"))
     (lc (cons x (extract-objfile-syms x))
	 x _))))

(defun get-objtab ()
  (let: objsym-tab (make-hash-table :test 'equal) in
    (ec (ec (puthash sym (car objfile) objsym-tab) sym (cdr objfile))
	objfile (obj-symbols))
    objsym-tab))

(defun dump-undefined-locations ()
  (interactive)
  (ec (scratchdump x " -> " (gethash (symbol-name x) objsym-tab) "\n")
      x (cdr (find-undefined-junk))))

(defun fuckwad-split-args (args)
  (re-findall
   (rx (+ (or (: "\"" (* (or "\\\"" (not (any "\"")))) "\"")
	      (not (any ",")))))
   args
   t))

(defun defuckwad (fname arg-remap)
  "Hack function for taking a function call and mucking with its arguments."
  (while (re-search-forward (str fname "[ \t]*(") nil t)
    (forward-char -1)
    (let: args (substring (thing-at-point 'sexp) 1 -1)
	  args (fuckwad-split-args args)
	  bounds (bounds-of-thing-at-point 'sexp) in
      (delete-region (car bounds) (cdr bounds))
      (insert "(")
      (do-wad
       (lc (if (and (symbolp x) (startswith (symbol-name x) "%"))
	       (nth (read (substring (symbol-name x) 1)) args)
	     (str x)) x arg-remap)
       (strjoin "," _) (mapc 'insert _))
      (insert ")"))))

(defun grab-func (funcname)
  (when (re-search-forward
	 (rx bol (* nonl) (eval funcname)
	     (* (any " \t")) "("
	     (* (not (any ";"))) "\n{")
	 nil t)
    (let: start (match-beginning 0)
	  end (save-excursion (re-search-forward "^}") (point)) in
      (buffer-substring start end))))

(defun grab-var (varname)
  (when (re-search-forward
	 (rx bol (* nonl) (eval varname)
	     (* (any " \t")) (any "[=;"))
	 nil t)
    (goto-char (match-beginning 0))
    (buffer-substring (point)
		      (save-excursion 
			(search-forward ";")
			(point)))))
