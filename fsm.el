
(defun make-state-table (state-pairs)
  (foolet (out (make-hash-table) n 0)
    (dolist (i (sublist2 state-pairs))
      (puthash (car i) n out)
      (setq n (+ n 1)))
    out))

(defun get-action (action)
  (if (eq 'acc action) "return 1;"
    (format "*state=%d; break;" (gethash action stab))))

(defun dump-state (state)
  (fdump "\tcase %d:\n" (gethash (car state) stab))
  (dump "\t\tswitch(input) {\n")
  (dolist (i (sublist2 (cadr state)))
    (cond 
     ((integerp (car i)) (fdump "\t\tcase %d: %s\n" (car i) (get-action (cadr i))))
     ((eq '* (car i)) (fdump "\t\tdefault: %s\n" (get-action (cadr i))))))
  (dump "\t\t}\n"))


(defun cmp-fsm (fsm)
  "Compile a state machine description into a C function (composed of an
assload of switch statements).

The FSM returns 1 when accepted, 0 otherwise."
  (catch 'return
    (if (not (eq (car fsm) 'fsm)) (throw 'return nil)) ; Not an FSM!
    (pop fsm)
    (fdump "\n/* State machine compiled by cmp-fsm emacs lisp code. */\n")
    (fdump "int %s(int *state, int input)\n{\n\tswitch(*state) {\n" (car fsm))
    (pop fsm)
    (foolet (stab (make-state-table fsm)) ; Make a state table of all states
	    (mapcar 'dump-state (sublist2 fsm)))
    (dump "\t}\treturn 0;\n}\n")))

(defun get-fsm (buf)
  (interactive "bOutput buffer: ")
  (foolet (fsm (read (current-buffer)))
	  (set-buffer buf)
	  (cmp-fsm fsm)))

(fsm hobag 
     r1 (?\r n1 * r1 ?\n acc)
     n1 (?\n acc * r1))
