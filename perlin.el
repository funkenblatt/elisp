(defstruct img data w h)

(defun new-img (w h &optional fill)
  (if (not fill) (setq fill 0))
  (make-img :data (string-make-unibyte (make-string (* w h) fill))
	    :w w :h h))

(defun draw-square (img coord size color &optional alpha)
  (if (not alpha) (setq alpha 1.0))
  (foolet (rowstart (rpn (img-w img) (cdr coord) * size * 
			 (car coord) size *
			 +)
	   datas (img-data img)
	   ix 0)
    (for (0 y size)
      (for (0 x size)
	(setq ix (+ rowstart x))
	(aset datas ix
	      (rpn (aref datas ix)
		   color (aref datas ix) - alpha * + round)))
      (setq rowstart (+ rowstart (img-h img))))
    img))

(defsubst boundcheck (ow x)
  (if (>= x ow) (- ow 1) x))

(defun interpolate (img w h)
  (foolet (out (new-img w h)
           outdat (img-data out)
	   indat (img-data img)
	   ow (img-w img) oh (img-h img)
	   ix 0 tempx 0 tempy 0 z1 0 z2 0 z3 0 z4 0 fx 0 fy 0)
    (for ((0 y h) (0 x w))
      (setq fx (rpn x ow * float w /)
	    fy (rpn y oh * float h /)
	    tempx (truncate fx) tempy (truncate fy)
	    z1 (aref indat (+ (* ow tempy) tempx))
	    z2 (aref indat (+ (* ow tempy) (boundcheck ow (+ tempx 1))))
	    z3 (aref indat (+ (* ow (boundcheck oh (+ tempy 1))) tempx))
	    z4 (aref indat (+ (* ow (boundcheck oh (+ tempy 1)))
			      (boundcheck ow (+ tempx 1))))
	    tempx (- fx tempx) tempy (- fy tempy))
      (aset outdat ix
	    (round (+ (* z1 (- 1 tempx) (- 1 tempy))
		      (* z2 tempx (- 1 tempy))
		      (* z3 (- 1 tempx) tempy)
		      (* z4 tempx tempy))))
      (setq ix (+ ix 1)))
    out))

(defun add-img (img1 img2 &optional alpha)
  (if (not alpha) (setq alpha 1.0))
  (foolet (imgdata (img-data img1))
    (for (0 ix (* (img-w img1) (img-h img1)))
      (aset imgdata ix (rpn (aref imgdata ix)
			    (aref (img-data img2) ix) (aref imgdata ix) - 
			    alpha * + round)))
    img1))


(defun rand-img (w h)
  (foolet (out (new-img w h))
    (for (0 ix (* w h))
      (aset (img-data out) ix (random 256)))
    out))

(defun noise-octaves ()
  (foolet (out (new-img 512 512 0)
           sqsize 4
	   alpha 1.0
	   temp "")
    (for (0 level 5)
      (setq temp (rand-img sqsize sqsize))
      (add-img out (interpolate temp 512 512) alpha)
      (setq alpha (/ alpha 2)
	    sqsize (* sqsize 2)))
    (filedump (img-data out) "foo.bin")
    (shell-command "webpal.py -g foo.bin 512 512")))

;(progn (mapc (funcomp byte-compile car) (toplevel-items '(defun defsubst))) (noise-octaves))

