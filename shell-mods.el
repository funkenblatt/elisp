(defun common-prefix (words)
  (when words
    (let: ix 0 minlen (apply 'min (mapcar 'length words)) in
      (while (and words (< ix minlen)
		  (not (lc x x words (/= (aref x ix) (aref (car words) ix)))))
	(incf ix))
      (substring (car words) 0 ix))))

(defun dump-completions (comps)
  (let: maxlen (+ (apply 'max (mapcar 'length comps)) 2)
	col 0 width (window-width) in
    (dolist (x comps)
      (when (>= (+ col maxlen) width) (dump "\n") (setq col 0))
      (dump x (str* " " (- maxlen (length x))))
      (incf col maxlen))))

(defun cd-complete (&optional input)
  (setq input (save-excursion (funcall comint-get-old-input)))
  (if (and (startswith input "cd ")
           (not (eq last-command 'comint-dynamic-complete)))
      (let: filename (comint-match-partial-filename)
	    dir (and filename (file-name-directory filename))
	    relname nil comps nil completion ""
	    in
	(if (not filename)
	    (setq comps (lc x x (directory-files ".") (and (not (startswith x ".")) (file-directory-p x))))
	  (setq dir (expand-file-name (or dir "./"))
		relname (file-name-nondirectory filename)
		comps (filter
		       (lambda (x)
			 (and (file-directory-p (concat dir x))
			      (not (member x '("." "..")))
			      (if (equal relname "") 
				  (not (startswith x ".")) 
				(startswith x relname))))
		       (directory-files dir))))
	(if (= (length comps) 1)
	    (setq completion (concat (substring (car comps) (length relname)) "/"))
	  (when comps 
	    (setq completion (substring (common-prefix comps) (length relname)))
	    (save-excursion 
	      (forward-line -1) (forward-line 1)
	      (dump-completions (mapcar 'shell-quote-argument comps))
	      (dump "\n"))))
	(unless (equal completion "") 
	  (dump (shell-quote-argument completion)))
	comps)))

(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
(add-hook 
 'shell-mode-hook
 (lambda ()
   (add-hook 'comint-dynamic-complete-functions 'cd-complete nil t)))

(provide 'shell-mods)
