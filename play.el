;; -*- lexical-binding: t; -*-

(defun start-player-proc (filename)
  (pr filename "\n")
  (start-process "fooplay" nil "mplayer" filename))

(defun kill-player-proc (p)
  (set-process-sentinel p (lambda (p s) ))
  (delete-process p))

(defun current-line ()
  (let: out (thing-at-point 'line) in
    (if (endswith out "\n") (substring out 0 -1) out)))

(defun play-loop (buf setter)
  (let: win (lc x x (all-windows) (eq (window-buffer x) buf)) in
    (setq win (or (car win) (selected-window)))
    (with-selected-window win
      (with-current-buffer buf
	(unless (= (point) (point-max))
	  (let: proc (start-player-proc 
		      (expand-file-name (current-line))) in
	    (forward-line)
	    (set-process-sentinel proc
	      (lambda (p s)
		(pr s)
		(unless (string-match "killed\\|stopped\\|suspended\\|run" s)
                  (funcall setter nil)
		  (play-loop buf setter))))
	    (funcall setter proc)))))))

(defmacro var-setter (var)
  "This uses lexical scope to do a form of passing variables by reference."
  (let: arg (gensym) in
    `(lambda (,arg) (setq ,var ,arg))))

(defun play ()
  "Plays the filenames in a particular buffer.  Sets some bindings."
  (interactive)
  (switch-to-buffer (get-buffer-create "*play*"))
  (let: buf (current-buffer) 
        player-proc nil 
        paused t in
    (local-set-key (kbd "C-c q") 'bury-buffer)
    (onkey (kbd "M-RET")
      (when player-proc (kill-player-proc player-proc))
      (play-loop buf (var-setter player-proc))
      (setq paused nil))
    (onkey (kbd "S-SPC")
      (signal-process player-proc (if paused 'SIGCONT 'SIGSTOP))
      (setq paused (not paused)))
    (onkey (kbd "<left>")
      (when player-proc
        (process-send-string player-proc "\033\133D")))
    (onkey (kbd "<right>")
      (when player-proc
        (process-send-string player-proc "\033\133C")))
    (onkey (kbd "M-k")
      (delete-process player-proc)
      (setf player-proc nil))
    buf))

(global-set-key 
 (kbd "s-m") (keycmd (pop-to-buffer "*play*")))

(provide 'play)
